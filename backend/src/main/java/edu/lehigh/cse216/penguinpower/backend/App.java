package edu.lehigh.cse216.penguinpower.backend;

import java.util.ArrayList;
import java.util.Calendar;

// Import Google's JSON library
import com.google.gson.Gson;

import org.codehaus.plexus.archiver.zip.ZipArchiver;

import java.util.Base64;

import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.LoginResponse;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.MessageDocResponse;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.MessageDocResponseByMid;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.MessageResponse;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.MessageResponseLite;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.MessageResponseWithDoc;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.UserInfoResponse;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.UserResponse;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.CommentResponse.CommentDocResponse;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.CommentResponse.MessageId;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.MessageResponseByUserLite;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.CommentResponseByUser;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.AddMessageDocResponse;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.CommentDocResponseByCID;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.CommentResponseByCID;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.CommentResponse.CommentFinalResponse;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.AddCommentDocResponse;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.MessageVoiceResponse;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.CommentResponse.CommentFinalResponse;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.MapResponse;
import edu.lehigh.cse216.penguinpower.backend.GoogleDrive;

import edu.lehigh.cse216.penguinpower.backend.SimpleRequests.*;

// Import the Spark package, so that we can make use of the "get" function to 
// create an HTTP GET route
import spark.Spark;
import java.io.IOException;

public class App {
	public static void main(String[] args) {

		final Encryption encryption = Encryption.getEncryption();
		


		// gson provides us with a way to turn JSON into objects, and objects
		// into JSON.
		//
		// NB: it must be final, so that it can be accessed from our lambdas
		//
		// NB: Gson is thread-safe. See
		// https://stackoverflow.com/questions/10380835/is-it-ok-to-use-gson-instance-as-a-static-field-in-a-model-bean-reuse
		final Gson gson = new Gson();

		// dataStore holds all of the data that has been provided via HTTP
		// requests

		final Database database = Database.getDatabase();

		//database.dropUserTable();
		//database.createUserTable();
		
		// Get the port on which to listen for requests
		Spark.port(getIntFromEnv("PORT", 4567));

		// Set up the location for serving static files
		Spark.staticFileLocation("/web");

		// Set up a route for serving the main page
		Spark.get("/", (req, res) -> {
			res.redirect("/index.html");
			return "";
		});

		// Routes for messages.

		// Return all message titles and Ids. All we do is get
		// the data, embed it in a StructuredResponse, turn it into JSON, and
		// return it. In addition, the session key is checked in the process. If there's
		// no data, we return "[]", so there's no need
		// for error handling.
		Spark.get("/messages", (request, response) -> {
			// ensure status 200 OK, with a MIME type of JSON
			response.status(200);
			response.type("application/json");
			String username = request.headers("username");
			String sessionKey = request.headers("sessionKey");
			if (!encryption.checkSessionKey(username, sessionKey)) {
				return gson.toJson(new StructuredResponse("error",
						"username or session key invalid; failed authentication.", null));
			}
			ArrayList<MessageResponseLite> messagesList = database.selectAllMessage();
			if (messagesList == null) {
				return gson.toJson(new StructuredResponse("error", "Cannot get the listing of messages.", null));
			}
			
			return gson.toJson(new StructuredResponse("ok", null, messagesList));
		});


		/* Returns details of an single message. Login required. 
		   get the document in the message if there is one or get the link in the message
		*/
		Spark.get("/messages/:messageId", (request, response) -> {
			int idx = Integer.parseInt(request.params("messageId"));
			// ensure status 200 OK, with a MIME type of JSON
			response.status(200);
			response.type("application/json");
			String username = request.headers("username");
			String sessionKey = request.headers("sessionKey");
			if (!encryption.checkSessionKey(username, sessionKey)) {
				return gson.toJson(new StructuredResponse("error",
						"username or session key invalid; failed authentication.", null));
			}
			//contains {url, type, link}
			MessageDocResponseByMid dataList = database.selectMessageDocResponseByMid(idx);
			MessageResponse data = database.selectMessage(idx);
			MapResponse map = database.selectMap(idx);
			if(dataList ==null)
			{
				//if no document attached to this message, only return the message response				
				if (data == null) {
					return gson.toJson(new StructuredResponse("error", "cannot get message: #" + idx, null));
				} else {
					MessageResponseWithDoc result= new MessageResponseWithDoc(data, null, null, null);
					if(map != null) result = new MessageResponseWithDoc(result, map);
					return gson.toJson(new StructuredResponse("ok", null, result));
				}
			}	
			else { 
				//datalist has {url, type, link}
				if(dataList.external_url!= null)
				{	// have external url
					MessageResponseWithDoc result= new MessageResponseWithDoc(data, null, dataList.type, dataList.external_url);
					return gson.toJson(new StructuredResponse("ok", null, result));
				}
				else if(dataList.external_url == null && dataList.google_drive_url!="google drive error")
				{	// have google drive url
					String fileId = dataList.google_drive_url;
					System.err.println("what is the fileId in dataList?"+fileId);
					String encoded = GoogleDrive.downloadFile(fileId);
					MessageResponseWithDoc result= new MessageResponseWithDoc(data, encoded, dataList.type, null);
					if(map != null) result = new MessageResponseWithDoc(result, map);
					return gson.toJson(new StructuredResponse("ok", null, result));
				
				}
				else if(dataList.external_url == null & dataList.google_drive_url == "google drive error")
				{	// no external url & google drive url is error
					MessageResponseWithDoc result = new MessageResponseWithDoc(data, dataList.google_drive_url, dataList.type, null);
					if(map != null) result = new MessageResponseWithDoc(result, map);
					return gson.toJson(new StructuredResponse("ok", null, result));
				}
				
			}
			if(map != null) data = new MessageResponse(data, map);
			return gson.toJson(new StructuredResponse("ok", null, data));
			
		});

		Spark.get("/messages/voice/:messageId", (request, response) ->{
			int idx = Integer.parseInt(request.params("messageId"));
			// ensure status 200 OK, with a MIME type of JSON
			response.status(200);
			response.type("application/json");
			String username = request.headers("username");
			String sessionKey = request.headers("sessionKey");
			if (!encryption.checkSessionKey(username, sessionKey)) {
				return gson.toJson(new StructuredResponse("error",
						"username or session key invalid; failed authentication.", null));
			}
			MessageResponse data = database.selectMessage(idx);
			String audioContent = GoogleTextToSpeech.synthesizeText(data.content);
			if(audioContent == null){
				return gson.toJson(new StructuredResponse("error", "Cannot get the audio content for message" + idx + "." , null));
			}
			MessageVoiceResponse result = new MessageVoiceResponse(data.messageId, audioContent);
			return gson.toJson(new StructuredResponse("ok", null, result));
		});

		// Returns details of an single comment. Login required.
		Spark.get("/commentSingle/:commentId", (request, response) -> {
			int idx = Integer.parseInt(request.params("commentId"));
			// ensure status 200 OK, with a MIME type of JSON
			response.status(200);
			response.type("application/json");
			String username = request.headers("username");
			String sessionKey = request.headers("sessionKey");
			if (!encryption.checkSessionKey(username, sessionKey)) {
				return gson.toJson(new StructuredResponse("error",
						"username or session key invalid; failed authentication.", null));
			}
			CommentResponseByCID data = database.selectCommentByCID(idx);
			if (data == null) {
				return gson.toJson(new StructuredResponse("error", "cannot get comment: #" + idx, null));
			} else {
				return gson.toJson(new StructuredResponse("ok", null, data));
			}
		});
		
		Spark.get("/comments/voice/:commentId", (request, response) ->{
			int idx = Integer.parseInt(request.params("commentId"));
			// ensure status 200 OK, with a MIME type of JSON
			response.status(200);
			response.type("application/json");
			String username = request.headers("username");
			String sessionKey = request.headers("sessionKey");
			if (!encryption.checkSessionKey(username, sessionKey)) {
				return gson.toJson(new StructuredResponse("error",
						"username or session key invalid; failed authentication.", null));
			}
			CommentResponseByCID data = database.selectCommentByCID(idx);
			String audioContent = GoogleTextToSpeech.synthesizeText(data.commentText);
			if(audioContent == null){
				return gson.toJson(new StructuredResponse("error", "Cannot get the audio content for comment " + idx + "." , null));
			}
			MessageVoiceResponse result = new MessageVoiceResponse(idx, audioContent);
			return gson.toJson(new StructuredResponse("ok", null, result));
		});
		/*
		POST route for adding a new element to the Database. This will read
		JSON from the body of the request, turn it into a SimpleRequest
		object, extract the title and message, insert them, and return
		1 if successful -1 if error
		UPDATE: upload file to google drive and store fileId into table works
		*/
		Spark.post("/messages", (request, response) -> {
			// NB: if gson.Json fails, Spark will reply with status 500 Internal Server Error
			SimpleRequests.AddMessageRequest req = gson.fromJson(request.body(),
					SimpleRequests.AddMessageRequest.class);
			String external_url = req.external_url;
			System.err.println("external_url: "+external_url);
			String document = req.document;
			System.err.println("document: "+document);
			// ensure status 200 OK, with a MIME type of JSON
			// NB: even on error, we return 200, but with a JSON object that describes the error.
			response.status(200);
			response.type("application/json");
			if (!encryption.checkSessionKey(req.username, req.sessionKey)) {
				System.err.print("new user username: "+req.username+", sessionKey:"+req.sessionKey);
				return gson.toJson(new StructuredResponse("error",
						"username or session key invalid; failed authentication.", null));
			}
			// NB: createEntry checks for null title and message
			int userId = database.selectUser(req.username).userId;
			int M_id = database.insertMessage(userId, req.title, req.content);

			
			//after inserting, get the max mid which is the id that just got inserted 
			//int M_id = database.selectMessageMaxId();
			if (M_id <= 0) {
				return gson.toJson(new StructuredResponse("error", "Cannot register your message.", null));
			} 
			else {

				if(req.latitude != null && req.longitude != null && req.description != null){
					int result  = database.insertMap(M_id, req.latitude, req.longitude, req.description);
					if(result <= 0){
						return gson.toJson(new StructuredResponse("error", "cannot add map to database", null));
					}
				}

				if(external_url == null && document == null)
				{	
					//No document attached 	
					MessageId result = new MessageId(M_id);	
					return gson.toJson(new StructuredResponse("ok", "Added a message without a document and a link.", result));
				}
				else if(external_url != null && document == null) 
				{
					//update Mesdocument table 
					String type = "external_url";
					int result = database.insertMessageDocument(null, external_url,type, userId, M_id);
					if(result <=0) {
						return gson.toJson(new StructuredResponse("error", "Cannot add the link null.", null));
					}
					return gson.toJson(new StructuredResponse("ok", "Added the link", result));
				}
				else if(document != null && external_url == null)//document is not null
				{
					// save the link into MessageDocument table
					// document: "data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAHkAAAB5C..."
					String[] strings = document.split(",");	
					String type = "";
					switch(strings[0]) {
						case "data:image/jpeg;base64":
							type = "image/jpeg";
							break;
						case "data:application/pdf;base64":
							type = "application/pdf";
							break;
						case "data:image/jpg;base64":
							type = "image/jpg";
							break;
					}
					//upload the file and retreive the id
					String fileId = GoogleDrive.fileUpload(document, M_id);
					if (fileId == null) 
					{
						//if having problem with connecting to google drive，save "google drive have issue with storing" into table
						int result = database.insertMessageDocument("google drive error", null,type, userId, M_id);
						if(result <= 0) {
							return gson.toJson(new StructuredResponse("error", "Cannot add the document to message.", null));
						}
						AddMessageDocResponse data = new AddMessageDocResponse("google drive error", type, M_id);
						return gson.toJson(new StructuredResponse("ok", "Cannot upload the file to Google Drive.", data));
					}
					System.err.println("fileId is:"+fileId);

					//encoded successfully, insert the fileId into table				
					int result = database.insertMessageDocument(fileId, null,type, userId, M_id);
					if(result <= 0) {
						return gson.toJson(new StructuredResponse("error", "Cannot add the document to message.", null));
					}
					else {
						AddMessageDocResponse data = new AddMessageDocResponse(fileId, type, M_id);
						return gson.toJson(new StructuredResponse ("ok", null, data));
					}
				}
				
				return gson.toJson(new StructuredResponse ("ok", "", null));
			}
		});

		

		// PUT route for updating a row's title and content in the Database. This is
		// almost
		// exactly the same as POST
		// Update 4: Edit a link 
		Spark.put("/messages/:messageId", (request, response) -> {
			// If we can't get an ID or can't parse the JSON, Spark will send
			// a status 500
			SimpleRequests.UpdateMessageRequest req = gson.fromJson(request.body(),
					SimpleRequests.UpdateMessageRequest.class);
			// ensure status 200 OK, with a MIME type of JSON
			String link = req.external_url;
			String document = req.document;
			response.status(200);
			response.type("application/json");
			if (!encryption.checkSessionKey(req.username, req.sessionKey)) {
				return gson.toJson(new StructuredResponse("error",
						"username or session key invalid; failed authentication.", null));
			}
			// check if userid matches the author of the message.
			int messageUserId = database.selectMessage(req.messageId).userId;
			int providedUserId = database.selectUser(req.username).userId;
			if (messageUserId != providedUserId) {
				return gson.toJson(new StructuredResponse("error",
						"Provided user is not the author of this message, editing denied.", null));
			}
			//update message
			int result = database.updateMessage(req.messageId, req.title, req.content);
			if (result <= 0) {
					return gson.toJson(new StructuredResponse("error", "unable to update message: #" + req.messageId, null));
			}
			//checking if the entry is inside the MessageDoc. 
			MessageDocResponse checkExist = database.selectMessageDocByMID(req.messageId);
			//no link and document updated 
			if(link==null && document ==null)
			{
				//nothing to update in messageDoc		
				return gson.toJson(new StructuredResponse("ok", "Update message sucessfully", result));
		  	}
		   if (link != null && document == null){
				//check if the message exists in messageDoc
				if(checkExist == null)
				{
					int insertLink = database.insertMessageDocument(null, link, "external_url", messageUserId, req.messageId);
					if (insertLink <= 0)
					{
						return gson.toJson(new StructuredResponse("error", "unable to add the link: # in message" + req.messageId, null));
					}
					return gson.toJson(new StructuredResponse("ok", "Update message sucessfully", insertLink));
				}
				else {
					int updateLink = database.updateMessageDocLink(req.messageId , link);
					if(updateLink <= 0)
					{
						return gson.toJson(new StructuredResponse("error", "unable to update the link in message: #" + req.messageId, null));
					}
					return gson.toJson(new StructuredResponse("ok", "Update message sucessfully", updateLink));
				}	
		   } 
		  //update document
		  else if (document !=null && link ==null){
			String[] strings = document.split(",");	
			String type = "";
			switch(strings[0]) {
				case "data:image/jpeg;base64":
					type = "image/jpeg";
					break;
				case "data:application/pdf;base64":
					type = "application/pdf";
					break;
				case "data:image/jpg;base64":
					type = "image/jpg";
					break;
			}
			//upload the file and retreive the id
			String fileId = GoogleDrive.fileUpload(document, req.messageId);
			if (fileId == null) 
			{
				return gson.toJson(new StructuredResponse("error", "Cannot add the document to message.", null));
			}
			System.err.println("New fileId is:"+fileId);
			//encoded successfully
			// insert the fileId into table	if the message does not exist in MDoc, else update
			if (checkExist == null)
			{	System.err.println("This message did not have a document before");
				int insertMDoc = database.insertMessageDocument(fileId, null,type, messageUserId, req.messageId);
				if(insertMDoc <= 0) {
					return gson.toJson(new StructuredResponse("error", "Cannot add the document to message.", null));
				}
				AddMessageDocResponse data = new AddMessageDocResponse(fileId, type, req.messageId);
				return gson.toJson(new StructuredResponse ("ok", "Add the document successfully.", data));
			}			
			else {
				System.err.println("This message had a document before");
				int updateMDoc = database.UpdateMessageDocUrl(fileId, type, req.messageId);
				if(updateMDoc <= 0) {
					return gson.toJson(new StructuredResponse("error", "Cannot update the document to message.", null));
				}
				System.err.println("Update the MessageDocument table successfully.");
				AddMessageDocResponse data = new AddMessageDocResponse(fileId, type, req.messageId);
				return gson.toJson(new StructuredResponse ("ok", "Update the document successfully.", data));
			}
		  }
		  return gson.toJson(new StructuredResponse("error", "", null));
		});

		// Register a row's vote in the Database
		Spark.put("/vote/message", (request, response) -> {
			// ensure status 200 OK, with a MIME type of JSON
			response.status(200);
			response.type("application/json");
			// If we can't get an ID or can't parse the JSON, Spark will send
			// a status 500
			SimpleRequests.VoteRequest req = gson.fromJson(request.body(), SimpleRequests.VoteRequest.class);
			int result;
			if (!encryption.checkSessionKey(req.username, req.sessionKey)) {
				return gson.toJson(new StructuredResponse("error",
						"username or session key invalid; failed authentication.", null));
			}
			int userId = database.selectUser(req.username).userId;
			result = database.castVote(req.messageId, userId, req.voteStatus);
			if (result < 0) {
				return gson.toJson(
						new StructuredResponse("error", "SQL Error encountered. Unable to register your vote.", null));
			} else {
				return gson.toJson(new StructuredResponse("ok", null, result));
			}
		});

		// DELETE route for removing a row from the Database
		Spark.delete("/messages/:messageId", (request, response) -> {
			try {
				// If we can't get an ID, Spark will send a status 500
				// ensure status 200 OK, with a MIME type of JSON
				response.status(200);
				response.type("application/json");
				String username = request.headers("username");
				String sessionKey = request.headers("sessionKey");
				if (!encryption.checkSessionKey(username, sessionKey)) {
					return gson.toJson(new StructuredResponse("error",
							"username or session key invalid; failed authentication.", null));
				}
				// check if this is the author of the message.
				int messageId = Integer.parseInt(request.params("messageId"));
				int messageUserId = database.selectMessage(messageId).userId;
				int providedUserId = database.selectUser(username).userId;
				if (messageUserId != providedUserId) {
					return gson.toJson(new StructuredResponse("error",
							"Provided user is not the author of this message, deleting denied.", null));
				}
				/*check if there is a document table for message. If there is one, delete document table first 
				and then delete the message table
				*/
				MessageDocResponseByMid checkDocument = database.selectMessageDocResponseByMid(messageId);
				if(checkDocument == null)
				{
					//no document table
					int result = database.deleteMessage(messageId);
					if (result <= 0) {
						return gson.toJson(new StructuredResponse("error", "unable to delete row." + messageId, null));
					} else {
						return gson.toJson(new StructuredResponse("ok", null, null));
					}
				}
				//delete document table first, mid is enought to be key 
				int deleteDoc = database.deleteMessageDoc(messageId);
				if(deleteDoc <= 0)
				{
					return gson.toJson(new StructuredResponse("error", "unable to delete." + messageId, null));
				}
				//Delete message
				int result = database.deleteMessage(messageId);
				if (result <= 0) {
					return gson.toJson(new StructuredResponse("error", "unable to delete row." + messageId, null));
				} else {
					return gson.toJson(new StructuredResponse("ok", null, null));
				}
			} catch (NullPointerException e) {
				return gson.toJson(
						new StructuredResponse("error", "Cannot find the message, unable to delete row.", null));
			}
		});

		// users related routes:

		// Provide a login functionality. If the provided username and password passes
		// the authentication, backend issues a session key generated randomly. The
		// caller will get the session key and use that for any activities that need
		// authentication.
		Spark.post("/users/login", (request, response) -> {
			response.status(200);
			response.type("application/json");
			LoginRequest req = gson.fromJson(request.body(), SimpleRequests.LoginRequest.class);
			String accessCode = req.accessCode;

			// token verified
			if (GoogleAuthenticator.verifyUser(accessCode) == true) {
				//GoogleDriveAPI test = new GoogleDriveAPI();
				//String filename = GoogleDriveAPI.executGoogleDrive();
				String username = GoogleAuthenticator.storeUserId(accessCode);
				UserResponse data = database.selectUser(username);
				/*
				//getting the current file
				Path currentRelativePath = Paths.get("");
				String s = currentRelativePath.toAbsolutePath().toString();
				System.out.println("Current relative path is: " + s);
				*/
				if (data == null) 
				{
					// this user is not in database, but is lehigh.edu
					//insertUser(String userName, String email, String familyName, String givenName) 
					String familyName = GoogleAuthenticator.storeFamilyName(accessCode);
					String givenName = GoogleAuthenticator.storeGivenName(accessCode);
					String email = GoogleAuthenticator.storeEamil(accessCode);
					String picture = GoogleAuthenticator.storePicture(accessCode);
					
					database.insertUser(username, email, familyName, givenName, picture);
					String sessionKey = encryption.addSessionkey(username);
					LoginResponse loginResponse = new LoginResponse(username, sessionKey);
					System.err.println("new user username: "+username+", sessionKey:"+sessionKey);
					return gson.toJson(new StructuredResponse("The new user " + username + "is added into database", null, loginResponse));
				}
				else 
				{
					String sessionKey = encryption.addSessionkey(username);
					LoginResponse loginResponse = new LoginResponse(username, sessionKey);
					System.err.println("exising user username: "+username +", sessionKey:"+sessionKey);
					return gson.toJson(new StructuredResponse("ok", null, loginResponse));
				}
			}
			
			return gson.toJson(new StructuredResponse("error", "ID token invalid.", null));
		});

		// Provide a logout functionality. Gives the frontend some level of control
		// over safty.
		Spark.post("/users/logout", (request, response) -> {
			response.status(200);
			response.type("application/json");
			LogoutRequest req = gson.fromJson(request.body(), LogoutRequest.class);
			String username = req.username;
			if (!encryption.removeUser(username)) {
				return gson.toJson(new StructuredResponse("error", "Cannot logout user: " + username, null));
			}
			return gson.toJson(new StructuredResponse("ok", null, null));
		});

		// Provide detailed information about the user, login required.
		Spark.post("/users/info", (request, response) -> {
			response.status(200);
			response.type("application/json");
			SimpleAuthenticationRequest req = gson.fromJson(request.body(), SimpleAuthenticationRequest.class);
			String username = req.username;
			String sessionKey = req.sessionKey;
			String usernameAskedFor = req.usernameAskedFor;
			System.err.println("username for:"+username);
			System.err.println("sessionKey for:"+sessionKey);
			System.err.println("usernameAsked for:"+usernameAskedFor);
			
			if (!encryption.checkSessionKey(username, sessionKey)) {
				// not in hashmap table
				return gson.toJson(new StructuredResponse("error", "Illegal username or session key.", null));
			}

			//getting all user info for the user Asked For
			UserResponse temp = database.selectUser(usernameAskedFor);

			ArrayList<MessageResponseByUserLite> messageList = database.selectAllMessageByUser(temp.userId);
			if (messageList == null) {
				return gson.toJson(new StructuredResponse("error", "Cannot get the listing of messages.", null));
			}

			ArrayList<CommentResponseByUser> commentList = database.selectAllCommentByUser(temp.userId);
			if (commentList == null) {
				return gson.toJson(new StructuredResponse("error", "Cannot get the listing of comments.", null));
			}

			//UserInfoResponse(int userId, String userName, String email, String familyName, String givenName)
			UserInfoResponse userInfoResponse = new UserInfoResponse(temp.userId, temp.userName, temp.email,
					temp.familyName, temp.givenName, temp.picture, messageList, commentList);
			return gson.toJson(new StructuredResponse("ok", null, userInfoResponse));
		});

		// Route for Comments.

			// Return a list of comments for a specific message.
		Spark.get("/comments/:messageId", (request, response) -> {
			int messageId = Integer.parseInt(request.params("messageId"));
			response.status(200);
			response.type("application/json");
			String username = request.headers("username");
			String sessionKey = request.headers("sessionKey");
			if (!encryption.checkSessionKey(username, sessionKey)) {
				return gson.toJson(new StructuredResponse("error",
						"username or session key invalid; failed authentication.", null));
			}
			ArrayList<CommentResponse> commentList = database.selectAllCommentByMessageId(messageId);
			if (commentList == null) {
				return gson.toJson(new StructuredResponse("error", "Cannot get the listing of messages.", null));
			}
			
			else {
				ArrayList<CommentFinalResponse> commentFinalList = database.selectAllCommentByMessageIdFinal(messageId);
				if(commentFinalList.isEmpty())
				{
					return gson.toJson(new StructuredResponse("ok", null, commentList));
				}
				
				for(CommentResponse y: commentList)
				{
					int exist = 0;
					for(CommentFinalResponse x: commentFinalList)
					{
						if(x.messageId == y.messageId && x.commentId == y.commentId)
						{
							int id = y.userId;
							String userName = database.selectUserNameByUid(id);
							x.username = userName;
							x.commentText = y.commentText;	
							exist = 1;
						}
					}
					if(exist == 0)
					{
						int id = y.userId;
						String userName = database.selectUserNameByUid(id);
						commentFinalList.add(new CommentFinalResponse(userName, y.commentText, "", "", y.dateCreated, "", y.userId, y.commentId, y.messageId));
					}
					System.err.println("commentFinalList: "+commentFinalList);
				}

				//decoded every document url
				for(CommentFinalResponse x: commentFinalList)
				{
					if(x.external_url==null && x.document!= "google drive error")
					{
						//google drive FileId replace it with base64 string
						String encoded = GoogleDrive.downloadFile(x.document);
						x.document = encoded;
					}
				}
				System.err.println("commentFinalList after encode: "+commentFinalList);
				return gson.toJson(new StructuredResponse("ok", null, commentFinalList));
				
			}

		});
		
		// Add a comment to a specific message.
		Spark.post("/comments/:messageId", (request, response) -> {
			int messageId = Integer.parseInt(request.params("messageId"));
			response.status(200);
			response.type("application/json");
			AddCommentRequest req = gson.fromJson(request.body(), AddCommentRequest.class);
			if (!encryption.checkSessionKey(req.username, req.sessionKey)) {
				return gson.toJson(new StructuredResponse("error",
						"username or session key invalid; failed authentication.", null));
			}
			String document = req.document;
			String link = req.external_url;
			int userId = database.selectUser(req.username).userId;
			int c_id = database.insertComment(messageId, userId, req.content);
				if (c_id <= 0) {
					return gson.toJson(new StructuredResponse("error", "Comment is not added successfully into database.", null));
				}

			if(document == null && link == null)
			{	//nothing to be added 			
			   return gson.toJson(new StructuredResponse("ok", null, c_id));
			}
			else if(link!=null && document ==null)
			{
				//update Mesdocument table 
				String type = "external_url";
				int result = database.insertCommentDocument(null, link, type, userId, c_id, messageId);
				if(result <=0) {
					return gson.toJson(new StructuredResponse("error", "Cannot add the link.", null));
				}
				return gson.toJson(new StructuredResponse("ok", "Added the link"+result, result));
			}
			else if(link == null && document!=null)
			{
				String[] strings = document.split(",");	
					String type = "";
					switch(strings[0]) {
						case "data:image/jpeg;base64":
							type = "image/jpeg";
							break;
						case "data:application/pdf;base64":
							type = "application/pdf";
							break;
						case "data:image/jpg;base64":
							type = "image/jpg";
							break;
					}		
					//upload the file and retreive the id
					String fileId = GoogleDrive.fileUpload(document, c_id);
					if (fileId == null) 
					{
						//if having problem with connecting to google drivego
						// google drive have issue with storing into table
						int result = database.insertCommentDocument("google drive error", null,type, userId, c_id, messageId);
						if(result <= 0) {
							return gson.toJson(new StructuredResponse("error", "Cannot add the document to comemnt.", null));
						}
						AddCommentDocResponse data = new AddCommentDocResponse("google drive error", type, c_id);
						return gson.toJson(new StructuredResponse("ok", "Cannot upload the file to Google Drive.", data));
					}
					//insert the fileId into table				
					int result = database.insertCommentDocument(fileId, null,type, userId, c_id, messageId);
					if(result <= 0) 
						return gson.toJson(new StructuredResponse("error", "Cannot add the document to message.", null));
					else {
						AddCommentDocResponse data = new AddCommentDocResponse(fileId, type, c_id);
						return gson.toJson(new StructuredResponse ("ok", null, data));
					}
					
			}
			
			int result = database.insertComment(messageId, userId, req.content);
			if (result <= 0) {
				return gson.toJson(new StructuredResponse("error", "Comment is not added successfully into database.", null));
			}
			return gson.toJson(new StructuredResponse("ok", null, result));
		});

		// Edit a comment.
		Spark.put("/comments", (request, response) -> {
			response.status(200);
			response.type("application/json");
			EditCommentRequest req = gson.fromJson(request.body(), EditCommentRequest.class);
			if (!encryption.checkSessionKey(req.username, req.sessionKey)) {
				return gson.toJson(new StructuredResponse("error",
						"username or session key invalid; failed authentication.", null));
			}
			String document = req.document;
			String link = req.external_url;

			UserResponse ur = database.selectUser(req.username);
			int userId = ur.userId;

			CommentResponse cr = database.selectComment(req.commentId);
			int messageId = cr.messageId;
			if (ur == null || cr == null) {
				return gson.toJson(new StructuredResponse("error", "Username or comment Id provided is not valid.", null));
			}
			int providedUserId = ur.userId;
			int commentUserId = cr.userId;
			if (providedUserId != commentUserId) {
				return gson.toJson(new StructuredResponse("error",
						"Provided user is not the author of this comment, editing denied.", null));
			}
			int result = database.updateComment(req.commentId, req.content);
				if (result <= 0) {
					return gson.toJson(new StructuredResponse("error", "Database denied updates.", null));
				}
			if(document == null && link == null)
			{	
				//only update the comment table
				 //No change made
					return gson.toJson(new StructuredResponse("okay", "Update works", null));
			}
			else if(link!= null && document == null)
			{
				///only update the link
				int result1 = database.updateCommentDocLink(link, req.commentId);
				if (result1 <= 0) {
					return gson.toJson(new StructuredResponse("error", "Database denied updates.", null));
				}
			}
			else if(document!= null && link ==null)
			{  
				
				String[] strings = document.split(",");	
					String type = "";
					switch(strings[0]) {
						case "data:image/jpeg;base64":
							type = "image/jpeg";
							break;
						case "data:application/pdf;base64":
							type = "application/pdf";
							break;
						case "data:image/jpg;base64":
							type = "image/jpg";
							break;
					}
				//upload the file and retreive the id
				String fileId = GoogleDrive.fileUpload(document, req.commentId);
				if (fileId == null) 
				{
					//if having problem with connecting to google driveï¼Œsave "google drive have issue with storing" into table
					int result2 = database.mUpdateCommentDocUrl("google drive error", type, req.commentId);
					if(result2 <= 0) 
						return gson.toJson(new StructuredResponse("error", "Cannot add the document to message.", null));
					AddCommentDocResponse data = new AddCommentDocResponse("google drive error", type, req.commentId);
					return gson.toJson(new StructuredResponse("error", "Cannot upload the file to Google Drive.", data));
				}
				//encoded successfully, insert the fileId into table				
				int result3 = database.insertCommentDocument(fileId, null,type, userId, req.commentId, messageId);
				if(result3 <= 0) 
					return gson.toJson(new StructuredResponse("error", "Cannot add the document to message.", null));

				else {
					AddCommentDocResponse data = new AddCommentDocResponse(fileId, type, req.commentId);
					return gson.toJson(new StructuredResponse ("ok", null, data));
				}
				
			}
			return gson.toJson(new StructuredResponse("ok", null, null));
		});

		// Delete a comment.
		Spark.delete("/comments/:commentId", (request, response) -> {
			response.status(200);
			response.type("application/json");
			String username = request.headers("username");
			String sessionKey = request.headers("sessionKey");
			System.err.println(username);
			System.err.println(sessionKey);
			if (!encryption.checkSessionKey(username, sessionKey)) {
				return gson.toJson(new StructuredResponse("error",
						"username or session key invalid; failed authentication.", null));
			}
			int providedUserId = database.selectUser(username).userId;
			int commentId = Integer.parseInt(request.params("commentId"));
			int commentUserId = database.selectComment(commentId).userId;
			if (providedUserId != commentUserId) {
				return gson.toJson(new StructuredResponse("error",
						"Provided user is not the author of this comment, deleting denied.", null));
			}

			//check if there is a document table for comment. If there is one, delete document table first 
			//	and then delete the message table
				CommentDocResponseByCID checkDocument = database.selectCommentDocByCID(commentId);
				if(checkDocument == null)
				{
					//no document table
					int result = database.deleteComment(commentId);
					if (result <= 0) {
					return gson.toJson(new StructuredResponse("error", "Database denied deleting.", null));
					}			
				}
				else {
					//delete comment_document table first and then delete comment table
					int deleteDoc = database.deleteCommentDoc(commentId);
					if(deleteDoc <= 0)
					{
						return gson.toJson(new StructuredResponse("error", "unable to delete comment document table." + commentId, null));
					}
					//and then delete comment table
					int result = database.deleteComment(commentId);
					if (result <= 0) {
						return gson.toJson(new StructuredResponse("error", "unable to delete comment." + commentId, null));
					} 	
					else {
					return gson.toJson(new StructuredResponse("ok", null, null));
					}
				}

			return gson.toJson(new StructuredResponse("ok", null, null));
		});
	}


	/**
	 * Get an integer environment variable if it exists, and otherwise return the
	 * default value.
	 * 
	 * @envar The name of the environment variable to get.
	 * @defaultVal The integer value to use as the default if envar isn't found
	 * 
	 * @returns The best answer we could come up with for a value for envar
	 */
	static int getIntFromEnv(String envar, int defaultVal) {
		ProcessBuilder processBuilder = new ProcessBuilder();
		if (processBuilder.environment().get(envar) != null) {
			return Integer.parseInt(processBuilder.environment().get(envar));
		}
		return defaultVal;
	}
	static String getBase64Encoded(String URL) throws IOException {

		String encodedString = Base64.getUrlEncoder().encodeToString(URL.getBytes());;
		return encodedString;
	}



}
