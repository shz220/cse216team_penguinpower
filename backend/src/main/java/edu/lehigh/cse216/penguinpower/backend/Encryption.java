package edu.lehigh.cse216.penguinpower.backend;

import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Hashtable;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

// OAuth2 required imports
/**
 * idea get from:
 * https://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/#PBKDF2WithHmacSHA1
 * now no need to encrypt the password to get sessionKey. The sessionKey is the tokenId get from OAuth2
 * which is in type GoogleIdToken
 */
public class Encryption {
    // Interations of SHA1 hasing.
    public static final int ITERATIONS = 1000;
    // Results length in bits.
    public static final int HASH_LENGTH = 64 * 8;
    public static final int SALT_LENGTH = 64;
    public static final int SESSION_KEY_LENGTH = 128;

    static class EncryptResult {
        private byte[] mHashedPassword;
        private byte[] mSalt;

        // Methods in this class are the only place to generate an EncryptResult, so the
        // constructor is private.
        private EncryptResult(byte[] hashedPassword, byte[] salt) {
            mHashedPassword = hashedPassword;
            mSalt = salt;
        }

        public byte[] getHashedPassword() {
            return mHashedPassword;
        }

        public byte[] getSalt() {
            return mSalt;
        }
    }

    private Hashtable<String, String> sessionKeyTable;

    private static Encryption INSTANCE;

    private Encryption() {
        sessionKeyTable = new Hashtable<>();
    }

    public static Encryption getEncryption() {
        if (INSTANCE == null) {
            INSTANCE = new Encryption();
        }
        return INSTANCE;
    }

    synchronized boolean checkSessionKey(String username, String sessionKey) {
        if (username == null || sessionKey == null) {
            return false;
        }
        if (!sessionKeyTable.containsKey(username)) {
            return false;
        }
        if (!sessionKey.equals(sessionKeyTable.get(username))) {
            return false;
        }
        return true;
    }

    synchronized String addSessionkey(String username) throws NoSuchAlgorithmException {
        if (username == null) {
            return null;
        }
        String sessionKey = getRandomSessionKey();
        // hashmap will automatically replace the previous sessionKey, so no need for
        // test here.
        sessionKeyTable.put(username, sessionKey);
        return sessionKey;
    }

    synchronized boolean removeUser(String username) {
        if (username == null) {
            return false;
        }
        if (!sessionKeyTable.containsKey(username)) {
            return false;
        }
        sessionKeyTable.remove(username);
        return true;
    }

    static EncryptResult generateStorngPasswordHash(String password)
            throws NoSuchAlgorithmException, InvalidKeySpecException {
        char[] chars = password.toCharArray();
        byte[] salt = getSafeRandom(SALT_LENGTH);
        PBEKeySpec spec = new PBEKeySpec(chars, salt, ITERATIONS, HASH_LENGTH);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return new EncryptResult(hash, salt);
    }

    private static String getRandomSessionKey() throws NoSuchAlgorithmException {
        byte[] rand = getSafeRandom(SESSION_KEY_LENGTH);
        return toHex(rand);
    }

    private static byte[] getSafeRandom(int bytes) throws NoSuchAlgorithmException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[bytes];
        sr.nextBytes(salt);
        return salt;
    }

    private static String toHex(byte[] array) throws NoSuchAlgorithmException {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if (paddingLength > 0) {
            return String.format("%0" + paddingLength + "d", 0) + hex;
        } else {
            return hex;
        }
    }

}