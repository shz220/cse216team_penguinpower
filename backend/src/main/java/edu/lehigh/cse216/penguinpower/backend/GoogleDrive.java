package edu.lehigh.cse216.penguinpower.backend;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.media.MediaHttpDownloader;
import com.google.api.client.googleapis.media.MediaHttpDownloaderProgressListener;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Base64;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class GoogleDrive {

    private static final String APPLICATION_NAME = "Admin Drive API";

    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    // Directory to store user credentials for this application.
    private static final java.io.File CREDENTIALS_FOLDER = new java.io.File("target/classes/credentials");

    private static final String CLIENT_SECRET_FILE_NAME = "client_secret.json";

    //
    // Global instance of the scopes required by this quickstart. If modifying these
    // scopes, delete your previously saved credentials/ folder.
    //
    private static final List<String> SCOPES = Collections.singletonList(DriveScopes.DRIVE);

    private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT) throws IOException {

        java.io.File clientSecretFilePath = new java.io.File(CREDENTIALS_FOLDER, CLIENT_SECRET_FILE_NAME);

        if (!clientSecretFilePath.exists()) {
            throw new FileNotFoundException("Please copy " + CLIENT_SECRET_FILE_NAME //
                    + " to folder: " + CREDENTIALS_FOLDER.getAbsolutePath());
        }

        // Load client secrets.
        InputStream in = new FileInputStream(clientSecretFilePath);

        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

        // Build flow and trigger user authorization request.
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY,
                clientSecrets, SCOPES).setDataStoreFactory(new FileDataStoreFactory(CREDENTIALS_FOLDER))
                        .setAccessType("offline").build();

        return new AuthorizationCodeInstalledApp(flow, new LocalServerReceiver()).authorize("user");
    }

    // main has no value, never used
    public static void main(String... args) throws IOException, GeneralSecurityException {
        System.out.println("CREDENTIALS_FOLDER: " + CREDENTIALS_FOLDER.getAbsolutePath());

        // Create CREDENTIALS_FOLDER
        if (!CREDENTIALS_FOLDER.exists()) {
            CREDENTIALS_FOLDER.mkdirs();

            System.out.println("Created Folder: " + CREDENTIALS_FOLDER.getAbsolutePath());
            System.out.println("Copy file " + CLIENT_SECRET_FILE_NAME + " into folder above.. and rerun this class!!");
            return;
        }

        // Build a new authorized API client service.
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

        // Read client_secret.json file & create Credential object.
        Credential credential = getCredentials(HTTP_TRANSPORT);

        // Create Google Drive Service.
        Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential) //
                .setApplicationName(APPLICATION_NAME).build();

        // Print the names and IDs for up to 10 files.
        FileList result = service.files().list().setPageSize(10).setFields("nextPageToken, files(id, name)").execute();
        List<File> files = result.getFiles();
        if (files == null || files.isEmpty()) {
            System.out.println("No files found.");
        } else {
            System.out.println("Files:");
            for (File file : files) {
                System.out.printf("%s (%s)\n", file.getName(), file.getId());
            }
        }
    }

    // upload file to google drive
    public static String fileUpload(String document, int M_id) {
        System.out.println("CREDENTIALS_FOLDER: " + CREDENTIALS_FOLDER.getAbsolutePath());

        try {
            if (!CREDENTIALS_FOLDER.exists()) {
                CREDENTIALS_FOLDER.mkdirs();

                System.out.println("Created Folder: " + CREDENTIALS_FOLDER.getAbsolutePath());
                System.out.println(
                        "Copy file " + CLIENT_SECRET_FILE_NAME + " into folder above.. and rerun this class!!");
            }
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Credential credential = getCredentials(HTTP_TRANSPORT);
            Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential) //
                    .setApplicationName(APPLICATION_NAME).build();

            String result = null;

            String[] strings = document.split(",");
            String extension = "error";
            String mimeType = "error";
            switch (strings[0]) {
            case "data:image/jpeg;base64":
                mimeType = "image/jpeg";
                extension = ".jpeg";
                break;
            case "data:application/pdf;base64":
                mimeType = "application/pdf";
                extension = ".pdf";
                break;
            case "data:image/jpg;base64":
                mimeType = "image/jpg";
                extension = ".jpg";
                break;
            }

            // convert base64 string to binary data
            byte[] data = Base64.getDecoder().decode(strings[1]);
            String path = "Message" + M_id;
            java.io.File file = java.io.File.createTempFile(path, "extension", null);

            // writing to the FILE
            try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
                outputStream.write(data);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Convert to Googlefile metadata
            File fileMetadata = new File();
            fileMetadata.setName(file.getName());
            fileMetadata.setMimeType(mimeType);

            FileContent mediaContent = new FileContent(mimeType, file);
            try {
                File file1 = service.files().create(fileMetadata, mediaContent).setFields("id").execute();
                System.err.println("File ID: " + file1.getId());
                result = file1.getId();
                if (result == null) {
                    return extension + mimeType;
                }
                return result;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        } catch (Exception ex) {
            return "";
        }
    }

    /**
     * Downloads a file using either resumable or direct media download.
     * return the base64 of that file
     * @throws GeneralSecurityException
     */
    public static String downloadFile(String fileId) throws IOException, GeneralSecurityException {
        System.out.println("CREDENTIALS_FOLDER: " + CREDENTIALS_FOLDER.getAbsolutePath());
        if (!CREDENTIALS_FOLDER.exists()) {
            CREDENTIALS_FOLDER.mkdirs();
            System.out.println("Created Folder: " + CREDENTIALS_FOLDER.getAbsolutePath());
            System.out.println("Copy file " + CLIENT_SECRET_FILE_NAME + " into folder above.. and rerun this class!!");
        }
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Credential credential = getCredentials(HTTP_TRANSPORT);
        Drive drive = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential) //
                .setApplicationName(APPLICATION_NAME).build();
        FileList result = drive.files().list().setFields("nextPageToken, files(id, name)").execute();
        List<File> files = result.getFiles();
        File file = null;
        if (files == null || files.isEmpty()) {
            System.out.println("No files found.");
        } else {
            System.err.println("fileId: "+fileId);
            for (File temp : files) {
                String tempFileId = temp.getId();
                if (tempFileId.equals(fileId)) {
                    System.err.println("have same fileId: "+fileId);
                    System.out.printf("%s (%s) \n", temp.getName(), temp.getId());
                    file = temp;
                }
            }
        }
        if(file == null){
            System.err.println("no files with given fileID");
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        drive.files().get(fileId)
        .executeMediaAndDownloadTo(outputStream);
        String encoded = Base64.getEncoder().encodeToString(outputStream.toByteArray());
        return encoded;
/*
        byte[] dataInByteArray;
        ByteArrayOutputStream dataStream = new ByteArrayOutputStream();
        MediaHttpDownloader downloader = new MediaHttpDownloader(HTTP_TRANSPORT,
                drive.getRequestFactory().getInitializer());
        downloader.setDirectDownloadEnabled(true);
        downloader.download(new GenericUrl(file.getWebViewLink()), dataStream);
        dataInByteArray = dataStream.toByteArray();

        // convert to Base64
        return Base64.getEncoder().encodeToString(dataInByteArray);*/
    }

    // String driveConstantLocation = "https://drive.google.com/uc?export=view&id=";

    // returns arraylist of file ids == url of first ten documents in the drive
    public static ArrayList<String> getFileId() {
        try {
            System.out.println("CREDENTIALS_FOLDER: " + CREDENTIALS_FOLDER.getAbsolutePath());

            if (!CREDENTIALS_FOLDER.exists()) {
                CREDENTIALS_FOLDER.mkdirs();

                System.out.println("Created Folder: " + CREDENTIALS_FOLDER.getAbsolutePath());
                System.out.println(
                        "Copy file " + CLIENT_SECRET_FILE_NAME + " into folder above.. and rerun this class!!");
            }
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Credential credential = getCredentials(HTTP_TRANSPORT);
            Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential) //
                    .setApplicationName(APPLICATION_NAME).build();
            FileList result = service.files().list().setPageSize(10).setFields("nextPageToken, files(id, name)")
                    .execute();
            List<File> files = result.getFiles();
            ArrayList<String> filesId = new ArrayList<>();
            
            if (files == null || files.isEmpty()) {
                System.out.println("No files found.");
            } else {
                System.out.println("Files:");
                for (File file : files) {
                    filesId.add(file.getId());
                    System.out.printf("%s (%s) \n", file.getName(), file.getId());
                }
            }
            return filesId;
        } catch (Exception ex) {
            ArrayList<String> filesId = new ArrayList<>();
            System.out.println("No files, exception caught.");
            return filesId;
        }
    }

    // get filenames of first ten documents in
    public static ArrayList<String> getFileName() {
        try {
            if (!CREDENTIALS_FOLDER.exists()) {
                CREDENTIALS_FOLDER.mkdirs();
            }
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Credential credential = getCredentials(HTTP_TRANSPORT);
            Drive service = new Drive.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential) //
                    .setApplicationName(APPLICATION_NAME).build();
            FileList result = service.files().list().setPageSize(10).setFields("nextPageToken, files(id, name)")
                    .execute();
            List<File> files = result.getFiles();
            ArrayList<String> filesName = new ArrayList<>();
            if (files == null || files.isEmpty()) {
                System.out.println("No files found.");
            } else {
                for (File file : files) {
                    filesName.add(file.getName());
                }
            }
            return filesName;
        } catch (Exception ex) {
            ArrayList<String> filesName = new ArrayList<>();
            return filesName;
        }
    }
}