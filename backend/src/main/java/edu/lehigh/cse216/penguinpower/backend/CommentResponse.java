package edu.lehigh.cse216.penguinpower.backend;

import java.util.Date;

public class CommentResponse {
    int messageId;
    int userId;
    int commentId;
    String commentText;
    Date dateCreated;

    public CommentResponse(int messageId, int userId, int commentId, String commentText, Date dateCreated) {
        this.messageId = messageId;
        this.userId = userId;
        this.commentId = commentId;
        this.commentText = commentText;
        this.dateCreated = dateCreated;
    }

}