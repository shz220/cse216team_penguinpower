package edu.lehigh.cse216.penguinpower.backend;

/**
 * SimpleRequest provides a format for clients to present information to the
 * server.
 * 
 * NB: since this will be created from JSON, all fields must be public, and we
 * do not need a constructor.
 */
// OAuth2 required imports
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;

public class SimpleRequests {
    class AddMessageRequest {
        public String username;
        public String sessionKey;
        public String title;
        public String content;
        //base 64 
        public String document;
        //external_url
        public String external_url;
        public Float latitude;
        public Float longitude;
        public String description;
    }

    class UpdateMessageRequest{
        public String username;
        public String sessionKey;
        public int messageId;
        public String title;
        public String content;
        public String document;
        public String external_url;
        
    }
    class DeleteMessageRequest{
        public String username;
        public String sessionKey;
        public int messageId;
    }

    class UserRequest {
        public byte[] hasedPassword;
        public byte[] salt;
    }

    class LoginRequest {
        //public String username; 
        public String accessCode;
    }

    class LogoutRequest {
        public String username;
        public String sessionKey;
    }

    class VoteRequest {
        public String username;
        public String sessionKey;
        /** True for upVote. */
        public boolean voteStatus;
        public int messageId;
    }

    class SimpleAuthenticationRequest {
        public String username;
        public String sessionKey;
        public String usernameAskedFor;
    }

    class AddCommentRequest {
        public String username;
        public String sessionKey;
        public String content;
        public String document;
        public String external_url;
    }

    class EditCommentRequest {
        public String username;
        public String sessionKey;
        public int commentId;
        public String content;
        //Add two elements from CommentDocument Table
        public String document;
        public String external_url;
    }

    class DeleteCommentRequest {
        public String username;
        public String sessionKey;
        public int commentId;
    }
}
