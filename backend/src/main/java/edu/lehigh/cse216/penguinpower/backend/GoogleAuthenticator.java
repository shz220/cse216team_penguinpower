package edu.lehigh.cse216.penguinpower.backend;
//https://developers.google.com/identity/sign-in/web/backend-auth
//https://stackoverflow.com/questions/43455629/googleidtokenverifier-does-not-return-name-picture-etc
//oauth2 login

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.http.javanet.NetHttpTransport;

import java.util.logging.Logger;

import java.security.GeneralSecurityException;
import java.io.IOException;
import java.net.Authenticator;
import java.util.Arrays;

public class GoogleAuthenticator extends Authenticator {

    private static final Logger log = Logger.getLogger(GoogleAuthenticator.class.getName());
    private static final JacksonFactory jacksonFactory = new JacksonFactory();

    // From: https://developers.google.com/identity/sign-in/android/backend-auth#using-a-google-api-client-library
    // If you retrieved the token on Android using the Play Services 8.3 API or newer, set
    // the issuer to "https://accounts.google.com". Otherwise, set the issuer to
    // "accounts.google.com". If you need to verify tokens from multiple sources, build
    // a GoogleIdTokenVerifier for each issuer and try them both.

    static String CLIENT_ID_1 = "202310197711-h3q8s3oinuqedd9jis502taei8u76t4j.apps.googleusercontent.com";//web
    static String CLIENT_ID_2 = "202310197711-ihifqmaihqjqcqa4sjg8grojounc0rog.apps.googleusercontent.com";//android
    static String CLIENT_ID_3 = "202310197711-gmsp6ekelmhrt628ecgnivr82pmdd5lc.apps.googleusercontent.com";//backend

    static GoogleIdTokenVerifier verifierForNewAndroidClients = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), jacksonFactory)
            .setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
            .setIssuer("https://accounts.google.com")
            .build();

    static GoogleIdTokenVerifier verifierForOtherClients = new GoogleIdTokenVerifier.Builder(new NetHttpTransport(), jacksonFactory)
            .setAudience(Arrays.asList(CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3))
            .setIssuer("accounts.google.com")
            .build();

    // Custom Authenticator class for authenticating google accounts
    public static Boolean verifyUser(String token) {
        if (token != null) {
            GoogleIdToken idToken = null;
            try {
                idToken = verifierForNewAndroidClients.verify(token);
                // send idToken to frontend
                if(idToken == null) idToken = verifierForOtherClients.verify(token);
                if (idToken != null) {
                    GoogleIdToken.Payload payload = idToken.getPayload();
                    String hostedDomain = (String) payload.get("hd");
                    if(hostedDomain.equals("lehigh.edu"))
                    {
                        // if verified
                        return true;
                    }
                    else
                    {
                        log.warning("You are not from the lehigh.edu domain.");
                        return false;
                    }

                } else {
                    log.warning("Invalid Google ID token.");
                    return false;
                }
            } catch (GeneralSecurityException e) {
                log.warning(e.getLocalizedMessage());
            } catch (IOException e) {
                log.warning(e.getLocalizedMessage());
            }

        }
        return false;
    }

    // this method is only called when verified that this user is from lehigh.edu domain
    // return the userId (which is the email, because it is unique)
    public static String storeUserId(String token) {
        String userId = "";
        if (token != null) {
            GoogleIdToken idToken = null;
            try {
                idToken = verifierForNewAndroidClients.verify(token);
                // send idToken to frontend
                if(idToken == null) idToken = verifierForOtherClients.verify(token);
                if (idToken != null) {
                    GoogleIdToken.Payload payload = idToken.getPayload();
                    String hostedDomain = (String) payload.get("hd");
                    if(hostedDomain.equals("lehigh.edu"))
                    {
                        // Get profile information from payload
                        userId = payload.getEmail();
                        System.err.println("userId: "+userId);
                        return userId;
                    }
                    else {
                        log.warning("You are not from the lehigh.edu domain.");
                    }
                } else {
                    log.warning("Invalid Google ID token.");
                }
            } catch (GeneralSecurityException e) {
                log.warning(e.getLocalizedMessage());
            } catch (IOException e) {
                log.warning(e.getLocalizedMessage());
            }
        }
        return userId;
    }

    // this method is only called when verified that this user is from lehigh.edu domain
    // return the email
    public static String storeEamil(String token) {
        String email = "";
        GoogleIdToken idToken = null;
        try {
            idToken = verifierForNewAndroidClients.verify(token);
            if(idToken == null) idToken = verifierForOtherClients.verify(token);
            if (idToken != null) {

                GoogleIdToken.Payload payload = idToken.getPayload();
                String hostedDomain = (String) payload.get("hd");
                if(hostedDomain.equals("lehigh.edu"))
                {
                    // Get profile information from payload
                    email = payload.getEmail();
                    System.err.println("email: "+email);
                    return email;
                }
            } else {
                log.warning("Invalid Google ID token.");
            }
        } catch (GeneralSecurityException e) {
            log.warning(e.getLocalizedMessage());
        } catch (IOException e) {
            log.warning(e.getLocalizedMessage());
        }
        return email;
    }

    // this method is only called when verified that this user is from lehigh.edu domain
    // return the familyName
    public static String storeFamilyName(String token) {
        String familyName = "";
        GoogleIdToken idToken = null;
        try {
            idToken = verifierForNewAndroidClients.verify(token);
            if(idToken == null) idToken = verifierForOtherClients.verify(token);
            if (idToken != null) {

                GoogleIdToken.Payload payload = idToken.getPayload();
                String hostedDomain = (String) payload.get("hd");
                if(hostedDomain.equals("lehigh.edu"))
                {
                    // Get profile information from payload
                    familyName = (String) payload.get("family_name");
                    System.err.println("familyName: "+familyName);
                    return familyName;
                }
            } else {
                log.warning("Invalid Google ID token.");
            }
        } catch (GeneralSecurityException e) {
            log.warning(e.getLocalizedMessage());
        } catch (IOException e) {
            log.warning(e.getLocalizedMessage());
        }
        return familyName;
    }

    // this method is only called when verified that this user is from lehigh.edu domain
    // return the givenName
    public static String storeGivenName(String token) {
        String givenName = "";
        GoogleIdToken idToken = null;
        try {
            idToken = verifierForNewAndroidClients.verify(token);
            if(idToken == null) idToken = verifierForOtherClients.verify(token);
            if (idToken != null) {

                GoogleIdToken.Payload payload = idToken.getPayload();
                String hostedDomain = (String) payload.get("hd");
                if(hostedDomain.equals("lehigh.edu"))
                {
                    // Get profile information from payload
                    givenName = (String) payload.get("given_name");
                    return givenName;
                }
            } else {
                log.warning("Invalid Google ID token.");
            }
        } catch (GeneralSecurityException e) {
            log.warning(e.getLocalizedMessage());
        } catch (IOException e) {
            log.warning(e.getLocalizedMessage());
        }
        return givenName;
    }

    // this method is only called when verified that this user is from lehigh.edu domain
    // return the givenName
    public static String storePicture(String token) {
        String picture = "";
        GoogleIdToken idToken = null;
        try {
            idToken = verifierForNewAndroidClients.verify(token);
            if(idToken == null) idToken = verifierForOtherClients.verify(token);
            if (idToken != null) {

                GoogleIdToken.Payload payload = idToken.getPayload();
                String hostedDomain = (String) payload.get("hd");
                if(hostedDomain.equals("lehigh.edu"))
                {
                    // Get profile information from payload
                    picture = (String) payload.get("picture");
                    return picture;
                }
            } else {
                log.warning("Invalid Google ID token.");
            }
        } catch (GeneralSecurityException e) {
            log.warning(e.getLocalizedMessage());
        } catch (IOException e) {
            log.warning(e.getLocalizedMessage());
        }
        return picture;
    }

}