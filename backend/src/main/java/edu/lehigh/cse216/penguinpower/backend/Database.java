package edu.lehigh.cse216.penguinpower.backend;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.net.URI;
import java.net.*;

import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.*;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.CommentResponse.CommentDocResponse;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.CommentResponse.CommentFinalResponse;

public class Database {
    /**
     * The connection to the database. When there is no connection, it should be
     * null. Otherwise, there is a valid open connection
     */
    private Connection mConnection;

    /**
     * A prepared statement for getting all data in the database
     */
    private PreparedStatement mSelectAllMessage;
    private PreparedStatement mSelectAllMessageByUser;
    private PreparedStatement mSelectAllUser;
    private PreparedStatement mSelectAllComment;
    private PreparedStatement mSelectAllVote;
    private PreparedStatement mSelectAllCommentDocByMessageId;
    /**
     * A prepared statement for getting one row from the database
     */
    private PreparedStatement mSelectOneMessage;
    private PreparedStatement mSelectOneMessageByTitle;
    private PreparedStatement mSelectUser;
    private PreparedStatement mSelectUserByName;
    private PreparedStatement mSelectComment;
    private PreparedStatement mSelectCommentByContent;
    private PreparedStatement mSelectMessageDocument;
    private PreparedStatement mSelectMessageDocByMid;
    private PreparedStatement mSelectMaxMid;
    private PreparedStatement mSelectCommentDocument;
    private PreparedStatement mSelectCommentDocByCId;
    private PreparedStatement mSelectAllCommentsByMessageId;
    private PreparedStatement mSelectAllCommentsByUser;
    private PreparedStatement mSelectVote;
    private PreparedStatement mSelectMap;
    /**
     * A prepared statement for deleting a row from the database
     */
    private PreparedStatement mDeleteOneMessage;
    private PreparedStatement mDeleteMessagesByUid;
    private PreparedStatement mDeleteComment;
    private PreparedStatement mDeleteUser;
    private PreparedStatement mDeleteOneMessageDoc;
    
    
    /**
     * A prepared statement for inserting into the database
     */
    private PreparedStatement mInsertOneMessage;
    private PreparedStatement mInsertUser;
    private PreparedStatement mInsertComment;
    private PreparedStatement mInsertVote;
    private PreparedStatement mInsertMessageDocument;
    private PreparedStatement mInsertCommentDocument;
    private PreparedStatement mInsertMap;
    /**
     * A prepared statement for updating a single row in the database
     */
    private PreparedStatement mUpdateOneMessage; // update message
    private PreparedStatement mUpdateComment;
    private PreparedStatement mUpdateVote;
    private PreparedStatement mUpdateUser;
    private PreparedStatement mUpdateMap;
    private PreparedStatement mUpdateMessageDocLink;
    private PreparedStatement mUpdateMessageDocUrl;

    private PreparedStatement mIncreaseUpvote;
    private PreparedStatement mIncreaseDownvote;
    private PreparedStatement mDecreaseUpvote;
    private PreparedStatement mDecreaseDownvote;
    /**
     * A prepared statement for creating the table in our database
     */
    private PreparedStatement mCreateMessageTable;
    private PreparedStatement mCreateUserTable;
    private PreparedStatement mCreateCommentTable;
    private PreparedStatement mCreateVoteTable;
    private PreparedStatement mCreateCommentDocumentTable;
    private PreparedStatement mCreatePermissionTable;
    //private PreparedStatement mCreateMessageDocumentTable;
    /**
     * A prepared statement for dropping the table in our database
     */
    private PreparedStatement mDropMessageTable;
    private PreparedStatement mDropUserTable;
    private PreparedStatement mDropCommentTable;
    private PreparedStatement mDropVoteTable;

    private PreparedStatement mVoteExists;

    private PreparedStatement mCreateMessageDocumentTable;
    private PreparedStatement mDeleteOneCommentDoc;

    private PreparedStatement mSelectUserById;

    private PreparedStatement mUpdateCommentDocLink;

    private PreparedStatement mUpdateCommentDocUrl;



    /**
     * The Database constructor is private: we only create Database objects through
     * the getDatabase() method.
     */
    private Database() {
    }

    /**
     * Get a fully-configured connection to the database
     * 
     * @param ip   The IP address of the database server
     * @param port The port on the database server to which connection requests
     *             should be sent
     * @param user The user ID to use when connecting
     * @param pass The password to use when connecting
     * 
     * @return A Database object, or null if we cannot connect properly
     */
    static Database getDatabase() {
        // Create an un-configured Database object
        Database db = new Database();


        Map<String, String> env = System.getenv();
        String db_url = "postgres://bgrbiorcgqgciu:943ee61bbd785c65306d808a0e2965d313190920d149e8b048963558685141a2@ec2-50-17-194-186.compute-1.amazonaws.com:5432/d7pqs6ll42inpq";

        // Give the Database object a connection, fail if we cannot get one
        try {
            Class.forName("org.postgresql.Driver");
            URI dbUri = new URI(db_url);
            String username = dbUri.getUserInfo().split(":")[0];
            String password = dbUri.getUserInfo().split(":")[1];
            String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath()
                    + "?sslmode=require";
            Connection conn = DriverManager.getConnection(dbUrl, username, password);
            if (conn == null) {
                System.err.println("Error: DriverManager.getConnection() returned a null object");
                return null;
            }
            db.mConnection = conn;
        } catch (SQLException e) {
            System.err.println("Error: DriverManager.getConnection() threw a SQLException");
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException cnfe) {
            System.out.println("Unable to find postgresql driver");
            return null;
        } catch (URISyntaxException s) {
            System.out.println("URI Syntax Error");
            return null;
        }

        // Attempt to create all of our prepared statements. If any of these
        // fail, the whole getDatabase() call should fail
        try {
            // NB: we can easily get ourselves in trouble here by typing the
            // SQL incorrectly. We really should have things like "tblData"
            // as constants, and then build the strings for the statements
            // from those constants.

            // Note: no "IF NOT EXISTS" or "IF EXISTS" checks on table
            // creation/deletion, so multiple executions will cause an exception
            // creating message table
            db.mCreateMessageTable = db.mConnection
                    .prepareStatement("create table if not exists Message (M_id SERIAL PRIMARY KEY not null,"
                            + "U_id int not null," + "Title varchar(100) not null," + "Content varchar(500) not null,"
                            + "Date_created date," + "Upvote_count int not null," + "Downvote_count int not null,"
                            + "foreign key(U_id) references User1 on delete set null)");
            // "foreign key(U_id) references User on delete set null)");
            db.mDropMessageTable = db.mConnection.prepareStatement("DROP TABLE IF EXISTS Message");
            // Standard CRUD operations
            db.mDeleteOneMessage = db.mConnection.prepareStatement("DELETE FROM message WHERE m_id = ?");
            db.mDeleteMessagesByUid = db.mConnection.prepareStatement("DELETE FROM message WHERE u_id = ?");
            db.mInsertOneMessage = db.mConnection
                    .prepareStatement("INSERT INTO Message VALUES (default, ?, ?, ?, ?, 0, 0) returning M_id");
            db.mSelectAllMessage = db.mConnection.prepareStatement(
                    "SELECT M_id, U_id, Title, Content, Date_Created, Upvote_count, Downvote_count FROM Message");
            db.mSelectAllMessageByUser = db.mConnection.prepareStatement("SELECT M_id FROM Message WHERE u_id = ?");
            db.mSelectOneMessage = db.mConnection.prepareStatement("SELECT * from Message WHERE M_id= ?");
            db.mSelectOneMessageByTitle = db.mConnection.prepareStatement("SELECT * from message WHERE title=?");
            db.mUpdateOneMessage = db.mConnection
                    .prepareStatement("UPDATE Message SET Title= ?, Content= ? WHERE M_id = ?");

            db.mCreateUserTable = db.mConnection
                    .prepareStatement("Create table if not exists User1 (U_id Serial primary key not null,"
                            + "UserName varchar(50) not null unique, " + "Email varchar(100) not null unique,"
                            + "FamilyName varchar(100) not null," + "GivenName varchar(100) not null,"
                            + "picture varchar(1000) not null)");
            db.mDropUserTable = db.mConnection.prepareStatement("Drop Table if exists User1 CASCADE");
            // db.mDeleteUser = db.mConnection.prepareStatement("Delete From User1 wherer
            // U_id = ?");
            db.mInsertUser = db.mConnection.prepareStatement("Insert into User1 Values(default, ?, ?, ?, ?, ?)");
            db.mSelectAllUser = db.mConnection.prepareStatement("Select * From User1");
            db.mSelectUser = db.mConnection.prepareStatement("Select * From User1 where U_id = ?");
            db.mSelectUserByName = db.mConnection.prepareStatement("select * from user1 where username = ?");
            db.mSelectUserById = db.mConnection.prepareStatement("select * from user1 where u_id = ?");
            db.mUpdateUser = db.mConnection.prepareStatement(
                    "UPDATE User1 SET UserName = ?, Email = ?, FamilyName = ?, GivenName=?, picture=? WHERE U_id = ?");
            db.mDeleteUser = db.mConnection.prepareStatement("DELETE FROM user1 where username = ?");

            db.mCreateCommentTable = db.mConnection
                    .prepareStatement("CREATE TABLE IF NOT EXISTS Comment (Comment_id SERIAL PRIMARY KEY NOT NULL, "
                            + "M_id INTEGER NOT NULL," + "U_id INTEGER NOT NULL,"
                            + "Comment_Text VARCHAR(500) NOT NULL," + "Date_created date,"
                            + "FOREIGN KEY(U_id) REFERENCES User1(U_id) ON DELETE CASCADE,"
                            + "FOREIGN KEY(M_id) REFERENCES Message(M_id) on delete CASCADE)");
            db.mDropCommentTable = db.mConnection.prepareStatement("DROP TABLE IF EXISTS Comment");
            db.mSelectComment = db.mConnection.prepareStatement("SELECT * FROM Comment WHERE Comment_id = ?");
            db.mSelectCommentByContent = db.mConnection
                    .prepareStatement("SELECT * FROM comment WHERE comment_text = ?");
            db.mSelectAllCommentsByMessageId = db.mConnection.prepareStatement("SELECT * FROM comment WHERE m_id = ?");
            db.mSelectAllCommentsByUser = db.mConnection
                    .prepareStatement("SELECT Comment_id FROM comment WHERE u_id = ?");

            db.mSelectAllComment = db.mConnection
                    .prepareStatement("SELECT Comment_id, M_id, U_id, Comment_Text, Date_created FROM Comment");
            db.mInsertComment = db.mConnection.prepareStatement("INSERT INTO Comment VALUES(default, ?, ?, ?, ?) returning Comment_id");
            db.mDeleteComment = db.mConnection.prepareStatement("DELETE FROM Comment where Comment_id = ?");
            db.mUpdateComment = db.mConnection
                    .prepareStatement("UPDATE Comment SET Comment_Text = ? WHERE Comment_id = ?");

            db.mCreateVoteTable = db.mConnection.prepareStatement("CREATE TABLE IF NOT EXISTS Vote ("
                    + "M_id INTEGER NOT NULL," + "U_id INTEGER NOT NULL," + "Vote_status BOOLEAN NOT NULL,"
                    + "FOREIGN KEY(M_id) REFERENCES Message(M_id) ON DELETE CASCADE,"
                    + "FOREIGN KEY(U_id) REFERENCES User1(U_id) ON DELETE CASCADE," + "PRIMARY KEY(M_id, U_id))");
            db.mDropVoteTable = db.mConnection.prepareStatement("DROP TABLE IF EXISTS Vote");
            db.mSelectVote = db.mConnection.prepareStatement("SELECT * FROM Vote WHERE M_id = ? AND U_id = ?");
            db.mSelectAllVote = db.mConnection
                    .prepareStatement("SELECT M_id, U_id, Vote_status FROM Vote ORDER BY U_id DESC");
            db.mInsertVote = db.mConnection.prepareStatement("INSERT INTO Vote VALUES(?,?,?)");
            db.mUpdateVote = db.mConnection
                    .prepareStatement("UPDATE Vote SET Vote_status = ? WHERE M_id = ? AND U_id = ?");
            db.mVoteExists = db.mConnection
                    .prepareStatement("SELECT exists (SELECT * FROM Vote WHERE M_id = ? AND U_id = ?)");

            db.mIncreaseUpvote = db.mConnection
                    .prepareStatement("UPDATE Message SET Upvote_count = upvote_count + 1 WHERE m_id = ?");
            db.mIncreaseDownvote = db.mConnection
                    .prepareStatement("UPDATE Message SET Downvote_count = downvote_count + 1 WHERE m_id = ?");
            db.mDecreaseUpvote = db.mConnection
                    .prepareStatement("UPDATE message SET upvote_count = upvote_count - 1 WHERE m_id = ?");
            db.mDecreaseDownvote = db.mConnection
                    .prepareStatement("UPDATE message SET downvote_count = downvote_count - 1 WHERE m_id = ?");
            
             //phase 4 Added 3 tables 
             db.mCreateCommentDocumentTable = db.mConnection.prepareStatement(
                "CREATE TABLE IF NOT EXISTS comment_document( "
                + "document_id SERIAL PRIMARY KEY NOT NULL," 
                + "google_drive_url VARCHAR(10000),"
                + "external_url VARCHAR(500),"
                + "date_created DATE NOT NULL," 
                + "type VARCHAR(20) NOT NULL," 
                + "U_id INTEGER NOT NULL,"
                + "comment_id INTEGER NOT NULL," 
                + "m_id INTEGER NOT NULL,"
                + "FOREIGN KEY(u_id) REFERENCES user1(u_id) ON DELETE CASCADE,"
                + "FOREIGN KEY(comment_id) REFERENCES comment(comment_id) ON DELETE CASCADE)");
            
                db.mCreatePermissionTable = db.mConnection
                    .prepareStatement("CREATE TABLE IF NOT EXISTS user_permision( "
                    + "permision VARCHAR(20) NOT NULL,"
                    + "U_id INTEGER NOT NULL,"
                    + "FOREIGN KEY(u_id) REFERENCES user1(u_id) ON DELETE CASCADE)"); 
                       
                db.mCreateMessageDocumentTable = db.mConnection
                    .prepareStatement("CREATE TABLE IF NOT EXISTS message_document( "
                    + "document_id SERIAL PRIMARY KEY NOT NULL," + "google_drive_url VARCHAR(500),"
                    + "externa_url varchar(500),"
                    + "date_created DATE NOT NULL," + "type VARCHAR(20)," + "U_id INTEGER NOT NULL,"
                    + "M_id INTEGER NOT NULL," + "FOREIGN KEY(U_id) REFERENCES User1(U_id) ON DELETE CASCADE,"
                    + "FOREIGN KEY(M_id) REFERENCES Message(M_id) ON DELETE CASCADE)");

            //Select * From User1 where U_id = ?"
            db.mSelectMessageDocument = db.mConnection
            .prepareStatement("select * from message_document where Document_id = ? ");
            db.mUpdateMessageDocLink = db.mConnection
            .prepareStatement("UPDATE Message_Document SET external_url = ? WHERE M_id = ?");
            db.mUpdateCommentDocLink = db.mConnection
            .prepareStatement("UPDATE Comment_Document SET external_url = ? WHERE comment_id = ?");
            db.mUpdateMessageDocUrl = db.mConnection
            .prepareStatement("UPDATE Message_Document SET google_drive_url = ?, type = ? WHERE M_id = ?");
            db.mUpdateCommentDocUrl = db.mConnection
            .prepareStatement("UPDATE Comment_Document SET google_drive_url = ?, type = ? WHERE comment_id = ?");
            db.mSelectMessageDocByMid = db.mConnection
            .prepareStatement("select * from Message_Document where M_id = ? ");
            db.mSelectMaxMid = db.mConnection
            .prepareStatement("select Max(m_id) from Message Table");
            db.mInsertMessageDocument = db.mConnection
            .prepareStatement("Insert into message_document Values(default, ?, ?, ?, ?, ?, ?)");
            
            db.mDeleteOneMessageDoc = db.mConnection
            .prepareStatement("DELETE FROM message_document WHERE m_id = ?");
            db.mInsertCommentDocument = db.mConnection
            .prepareStatement("Insert into comment_document Values(default, ?, ?, ?, ?, ? ,?, ?)");
           
            db.mSelectAllCommentDocByMessageId = db.mConnection
            .prepareStatement("select * from comment_document where m_id = ? ");
            db.mDeleteOneCommentDoc = db.mConnection
            .prepareStatement("DELETE FROM comment_document WHERE comment_id = ?");
            db.mSelectCommentDocByCId = db.mConnection
            .prepareStatement("Select * from comment_document WHERE comment_id = ?");

            db.mSelectMap = db.mConnection.prepareStatement("Select from map WHERE m_id = ?");
            db.mInsertMap = db.mConnection.prepareStatement("Insert into map Values(default, ?, ?, ?, ?)");

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
            db.disconnect();
            return null;
        }
        return db;
    }

    /**
     * Close the current connection to the database, if one exists.
     * 
     * NB: The connection will always be null after this call, even if an error
     * occurred during the closing operation.
     * 
     * @return True if the connection was cleanly closed, false otherwise
     */
    boolean disconnect() {
        if (mConnection == null) {
            System.err.println("Unable to close connection: Connection was null");
            return false;
        }
        try {
            mConnection.close();
        } catch (SQLException e) {
            System.err.println("Error: Connection.close() threw a SQLException");
            e.printStackTrace();
            mConnection = null;
            return false;
        }
        mConnection = null;
        return true;
    }

    /**
     * Insert a row into the Message database
     * 
     * @param U_id
     * @param title
     * @param content
     * 
     * @return The number of rows that were inserted
     */

    int insertMessage(int U_id, String title, String content) {
        int count = 0;
        try {
            java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            mInsertOneMessage.setInt(1, U_id);
            mInsertOneMessage.setString(2, title);
            mInsertOneMessage.setString(3, content);
            mInsertOneMessage.setDate(4, date);

            //returning M_id *count is not number of messages any more
            ResultSet res = mInsertOneMessage.executeQuery();
            if(res.next()) count += res.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }
    int insertMessageDocument(String url, String link, String type, int U_id, int M_id)
    {
        int count = 0;
        try {
            java.sql.Date date_created = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            mInsertMessageDocument.setString(1, url);
            mInsertMessageDocument.setString(2, link);
            mInsertMessageDocument.setDate(3, date_created);
            mInsertMessageDocument.setString(4, type);
            mInsertMessageDocument.setInt(5, U_id);
            mInsertMessageDocument.setInt(6, M_id);
            count += mInsertMessageDocument.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }
    int insertCommentDocument(String url, String link, String type, int u_id, int comment_id, int m_id)
    {
        int count = 0;
        try {
            java.sql.Date date_created = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            mInsertCommentDocument.setString(1, url);
            mInsertCommentDocument.setString(2, link);
            mInsertCommentDocument.setInt(3, m_id);
            mInsertCommentDocument.setDate(4, date_created);
            mInsertCommentDocument.setString(5, type);
            mInsertCommentDocument.setInt(6, u_id);
            mInsertCommentDocument.setInt(7, comment_id);
            
            count += mInsertCommentDocument.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }
    int insertUser(String userName, String email, String familyName, String givenName, String picture) {
        int count = 0;
        try {
            mInsertUser.setString(1, userName);
            mInsertUser.setString(2, email);
            mInsertUser.setString(3, familyName);
            mInsertUser.setString(4, givenName);
            mInsertUser.setString(5, picture);
            count += mInsertUser.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    int insertComment(int M_id, int U_id, String comment_text) {
        int count = 0;
        try {
            java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            mInsertComment.setInt(1, M_id);
            mInsertComment.setInt(2, U_id);
            mInsertComment.setString(3, comment_text);
            mInsertComment.setDate(4, date);
            ResultSet res = mInsertComment.executeQuery();
            if(res.next()) count+= res.getInt(1);
            
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    int insertVote(int M_id, int U_id, boolean voteStatus) {
        int count = 0;
        try {
            mInsertVote.setInt(1, M_id);
            mInsertVote.setInt(2, U_id);
            mInsertVote.setBoolean(3, voteStatus);
            count += mInsertComment.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    int insertMap(int M_id, Float latitude, Float longitude, String description){
        int count = 0;
        try{
            mInsertMap.setInt(1, M_id);
            mInsertMap.setFloat(2, latitude);
            mInsertMap.setFloat(3, longitude);
            mInsertMap.setString(4, description);
            count += mInsertMap.executeUpdate();
        } catch(SQLException e){
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Query the database for a list of information
     * 
     * @return All rows, as an ArrayList
     */

    // int M_id, int U_id, String title, String content, Date date_created
    ArrayList<MessageResponseLite> selectAllMessage() {
        ArrayList<MessageResponseLite> res = new ArrayList<>();
        try {
            ResultSet rs = mSelectAllMessage.executeQuery();
            while (rs.next()) {
                res.add(new MessageResponseLite(rs.getInt("M_id"), rs.getInt("U_id"), rs.getString("Title"),
                        rs.getInt("upvote_count") - rs.getInt("downvote_count"), rs.getDate("Date_created")));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    // int M_id
    ArrayList<MessageResponseByUserLite> selectAllMessageByUser(int userId) {
        ArrayList<MessageResponseByUserLite> res = new ArrayList<>();
        try {
            mSelectAllMessageByUser.setInt(1, userId);
            ResultSet rs = mSelectAllMessageByUser.executeQuery();
            while (rs.next()) {
                res.add(new MessageResponseByUserLite(rs.getInt("M_id")));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    ArrayList<CommentResponse> selectAllComment() {
        ArrayList<CommentResponse> res = new ArrayList<>();
        try {
            ResultSet rs = mSelectAllComment.executeQuery();
            while (rs.next()) {
                res.add(new CommentResponse(rs.getInt("M_id"), rs.getInt("U_id"), rs.getInt("Comment_id"),
                        rs.getString("Comment_Text"), rs.getDate("Date_created")));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    ArrayList<CommentResponseByUser> selectAllCommentByUser(int userId) {
        ArrayList<CommentResponseByUser> res = new ArrayList<>();
        try {
            mSelectAllCommentsByUser.setInt(1, userId);
            ResultSet rs = mSelectAllCommentsByUser.executeQuery();
            while (rs.next()) {
                res.add(new CommentResponseByUser(rs.getInt("Comment_id")));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    ArrayList<VoteResponse> selectAllVote() {
        ArrayList<VoteResponse> res = new ArrayList<>();
        try {
            ResultSet rs = mSelectAllVote.executeQuery();
            while (rs.next()) {
                res.add(new VoteResponse(rs.getInt("M_id"), rs.getInt("U_id"), rs.getBoolean("Vote_status")));
            }
            rs.close();
            return res;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get all data for a specific row, by ID
     * 
     * @param M_id The id of the row being requested
     * 
     * @return The data for the requested row, or null if the ID was invalid
     */

    // int M_id, int U_id, String title, String content, Date date_created, int
    // upvote
    MessageResponse selectMessage(int M_id) {
        MessageResponse res = null;
        try {
            mSelectOneMessage.setInt(1, M_id);
            ResultSet rs = mSelectOneMessage.executeQuery();
            if (rs.next()) {
                int uid = rs.getInt("U_id");
                UserResponse ur = selectUser(uid);
                String username;
                if (ur == null) {   //due to frontend sometimes send a null ur, it causes comments 
                                    //cannot showed at all. So, we set username as "" for now. Need 
                                    //to solve this problem by web in future.
                    username = " ";
                } else {
                    username = ur.userName;
                }
                //getting the username of the user who wrtie that comment
                UserResponse CommentUser = selectUser(rs.getInt("U_id"));
                res = new MessageResponse(rs.getInt("M_id"), rs.getInt("U_id"), CommentUser.userName, rs.getString("Title"),
                        rs.getString("Content"), rs.getInt("upvote_count"), rs.getInt("downvote_count"),
                        rs.getDate("Date_created"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    MessageResponse selectMessage(String title) {

        MessageResponse res = null;
        try {
            mSelectOneMessageByTitle.setString(1, title);
            ResultSet rs = mSelectOneMessageByTitle.executeQuery();
            if (rs.next()) {
                res = new MessageResponse(rs.getInt("M_id"), rs.getInt("U_id"), rs.getString("username"),
                        rs.getString("Title"), rs.getString("Content"), rs.getInt("upvote_count"),
                        rs.getInt("downvote_count"), rs.getDate("Date_created"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
    int selectMessageMaxId()
    {
        int res = -1;
        try {
            
            ResultSet rs = mSelectMaxMid.executeQuery();
            if (rs.next()) {
                res = rs.getInt("M_id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    //only responding the url
    MessageDocResponse selectMessageDoc(int document_id)
    {
        MessageDocResponse res = null; 
        try {
            mSelectMessageDocument.setInt(1, document_id);
            ResultSet rs = mSelectMessageDocument.executeQuery();
            if (rs.next()) {
                res = new MessageDocResponse(rs.getInt("Document_id"), rs.getString("google_drive_url"), rs.getDate("date_created"),
                        rs.getString("Type"), rs.getInt("U_id"), rs.getInt("M_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;

    }
    MessageDocResponse selectMessageDocByMID(int message_id)
    {
        MessageDocResponse res = null;
        try {
            mSelectMessageDocByMid.setInt(1, message_id);
            ResultSet rs = mSelectMessageDocByMid.executeQuery();
            if (rs.next()) {
                res = new MessageDocResponse(rs.getInt("Document_id"), rs.getString("google_drive_url"), rs.getDate("date_created"),
                        rs.getString("Type"), rs.getInt("U_id"), rs.getInt("M_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
    MessageDocResponseByMid selectMessageDocResponseByMid(int M_id)
    {
        MessageDocResponseByMid res = null;
        try {
            mSelectMessageDocByMid.setInt(1, M_id);
            ResultSet rs = mSelectMessageDocByMid.executeQuery();
            if (rs.next()){
                res = new MessageDocResponseByMid(rs.getString("google_drive_url"), rs.getString("type"), rs.getString("external_url"));
                //rs = rs.next();? will this pointer automatically go down? 
            }
            rs.close();
        }
            catch(SQLException e){
                e.printStackTrace();
            }
        return res;
    }


    MapResponse selectMap(int M_id){
        MapResponse res = null;
        try{
            mSelectMap.setInt(1, M_id);
            ResultSet rs = mSelectMap.executeQuery();
            if(rs.next()){
                res = new MapResponse(rs.getInt("Map_id"), rs.getFloat("latitude"), rs.getFloat("longitude"), rs.getString("description"));
            }
            rs.close();
        }catch(SQLException e){
            e.printStackTrace();
        }
        return res;
    }
    
    
    
    //UPDATE MessageDocument SET link = ? WHERE M_id = ?
    int updateMessageDocLink (int M_id, String link)
    {
        int res = 0;
        try {
            mUpdateMessageDocLink.setString(1, link);
            mUpdateMessageDocLink.setInt(2, M_id);
            
            res = mUpdateMessageDocLink.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
    int updateCommentDocLink(String link, int comment_id)
    {
        int res = 0;
        try {
            mUpdateCommentDocLink.setString(1, link);
            mUpdateCommentDocLink.setInt(2, comment_id);
            res = mUpdateCommentDocLink.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
    
    //Message_Document SET google_drive_url = ?, type = ? WHERE M_id = ?
    int UpdateMessageDocUrl(String google_drive_url, String type, int M_id)
    {
        int res = 0;
        try {
            mUpdateMessageDocUrl.setString(1, google_drive_url);
            mUpdateMessageDocUrl.setString(2, type);
            mUpdateMessageDocUrl.setInt(3, M_id);
            res = mUpdateMessageDocUrl.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
    int mUpdateCommentDocUrl(String google_drive_url, String type, int c_id)
    {
        int res = 0;
        try {
            mUpdateCommentDocUrl.setString(1, google_drive_url);
            mUpdateCommentDocUrl.setString(2, type);
            mUpdateCommentDocUrl.setInt(3, c_id);
            res = mUpdateCommentDocUrl.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
    
    String selectUserNameByUid(int U_id)
    {
        String res = null;
        try {
            mSelectUser.setInt(1, U_id);
            ResultSet rs = mSelectUser.executeQuery();
            if (rs.next()) {
                res = rs.getString("username");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
    UserResponse selectUser(int U_id) {
        UserResponse res = null;
        try {
            mSelectUser.setInt(1, U_id);
            ResultSet rs = mSelectUser.executeQuery();
            if (rs.next()) {
                res = new UserResponse(rs.getInt("U_id"), rs.getString("Username"), rs.getString("Email"),
                        rs.getString("FamilyName"), rs.getString("GivenName"), rs.getString("picture"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    UserResponse selectUser(String username) {
        UserResponse res = null;
        try {
            mSelectUserByName.setString(1, username);
            ResultSet rs = mSelectUserByName.executeQuery();
            if (rs.next()) {
                res = new UserResponse(rs.getInt("U_id"), rs.getString("Username"), rs.getString("Email"),
                        rs.getString("FamilyName"), rs.getString("GivenName"), rs.getString("picture"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    CommentResponse selectComment(int comment_id) {
        CommentResponse res = null;
        try {
            mSelectComment.setInt(1, comment_id);
            ResultSet rs = mSelectComment.executeQuery();
            if (rs.next()) {
                res = new CommentResponse(rs.getInt("M_id"), rs.getInt("U_id"), rs.getInt("Comment_id"),
                        rs.getString("Comment_Text"), rs.getDate("Date_created"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    CommentResponseByCID selectCommentByCID(int comment_id) {
        CommentResponseByCID res = null;
        try {
            mSelectComment.setInt(1, comment_id);
            ResultSet rs = mSelectComment.executeQuery();
            if (rs.next()) {
                res = new CommentResponseByCID(rs.getString("Comment_Text"), rs.getDate("Date_created"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
    CommentDocResponseByCID selectCommentDocByCID (int comment_id) {
        CommentDocResponseByCID res = null;
        try {
            mSelectCommentDocByCId.setInt(1, comment_id);
            ResultSet rs = mSelectCommentDocByCId.executeQuery();
            if (rs.next()) {
                res = new CommentDocResponseByCID(rs.getInt("comment_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    CommentResponse selectComment(String comment) {

        CommentResponse res = null;
        try {
            mSelectCommentByContent.setString(1, comment);
            ResultSet rs = mSelectCommentByContent.executeQuery();
            if (rs.next()) {
                res = new CommentResponse(rs.getInt("M_id"), rs.getInt("U_id"), rs.getInt("Comment_id"),
                        rs.getString("Comment_Text"), rs.getDate("Date_created"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    ArrayList<CommentResponse> selectAllCommentByMessageId(int messageId) {
        ArrayList<CommentResponse> retval = new ArrayList<>();
        try {
            mSelectAllCommentsByMessageId.setInt(1, messageId);
            ResultSet rs = mSelectAllCommentsByMessageId.executeQuery();
            while (rs.next()) {
                retval.add(new CommentResponse(rs.getInt("M_id"), rs.getInt("U_id"), rs.getInt("Comment_id"),
                        rs.getString("Comment_Text"), rs.getDate("Date_created")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return retval;
    }
    
    ArrayList<CommentFinalResponse> selectAllCommentByMessageIdFinal(int messageId) {
        ArrayList<CommentFinalResponse> retval = new ArrayList<>();
        String def = "";
        try {
            mSelectAllCommentDocByMessageId.setInt(1, messageId);
            ResultSet rs = mSelectAllCommentDocByMessageId.executeQuery();
            while (rs.next()) {
                retval.add(new CommentFinalResponse(def, def, rs.getString("google_drive_url"), 
                rs.getString("external_url"),rs.getDate("date_created"), 
                rs.getString("type"),rs.getInt("u_id"),rs.getInt("comment_id"), rs.getInt("m_id")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return retval;
    }
    
    //String document, String external_url, Date dateCreated, String type, int userId, int commentId, int messageId
    ArrayList<CommentDocResponse> selectAllCommentDocResponsesByMessageId(int messageId) {
        ArrayList<CommentDocResponse> retval = new ArrayList<>();
        try{
            mSelectAllCommentDocByMessageId.setInt(1, messageId);
            ResultSet rs = mSelectAllCommentsByMessageId.executeQuery();
            while (rs.next()) {
                retval.add(new CommentDocResponse(rs.getString("document"), rs.getString("external_url"), rs.getDate("Date_created"),
                    rs.getString("type"),rs.getInt("u_id"), rs.getInt("comment_id"),rs.getInt("m_id")));
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        return retval;
    }

    VoteResponse selectVote(int M_id, int U_id) {
        VoteResponse res = null;
        try {
            mSelectVote.setInt(1, M_id);
            mSelectVote.setInt(2, U_id);
            ResultSet rs = mSelectVote.executeQuery();
            if (rs.next()) {
                res = new VoteResponse(rs.getInt("M_id"), rs.getInt("U_id"), rs.getBoolean("Vote_status"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Delete a row by ID
     * 
     * @param M_id The id of the row to delete
     * 
     * @return The number of rows that were deleted. -1 indicates an error.
     */

    int deleteMessage(int M_id) {
        int res = -1;
        try {
            mDeleteOneMessage.setInt(1, M_id);
            res = mDeleteOneMessage.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
    int deleteMessageDoc(int M_id){
        int res = -1;
        try{
            mDeleteOneMessageDoc.setInt(1, M_id);
            res = mDeleteOneMessageDoc.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }
    int deleteCommentDoc(int C_id)
    {
        int res = -1;
        try{
            mDeleteOneCommentDoc.setInt(1, C_id);
            res = mDeleteOneCommentDoc.executeUpdate();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    int deleteMessagesByUid(int uid) {
        try {
            mDeleteMessagesByUid.setInt(1, uid);

            mDeleteMessagesByUid.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
        return 1;
    }

    int deleteUser(String username) {
        try {
            mDeleteUser.setString(1, username);
            mDeleteUser.executeUpdate();
            return 1;
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    int deleteComment(int comment_id) {
        int res = -1;
        try {
            mDeleteComment.setInt(1, comment_id);
            res = mDeleteComment.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Update the content for a row in the database
     * 
     * @param M_id    The id of the row to update
     * @param Title   The new title of the message
     * @param Content The new content of the message
     * 
     * @return The number of rows that were updated. -1 indicates an error.
     */

    // update the title and content of message
    int updateMessage(int M_id, String Title, String Content) {
        int res = -1;
        try {
            mUpdateOneMessage.setString(1, Title);
            mUpdateOneMessage.setString(2, Content);
            mUpdateOneMessage.setInt(3, M_id);
            res = mUpdateOneMessage.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    int updateComment(int Comment_id, String Content) {
        int res = -1;
        try {
            mUpdateComment.setString(1, Content);
            mUpdateComment.setInt(2, Comment_id);
            res = mUpdateComment.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    int updateUser(int U_id, String username, String email, String familyName, String givenName) {
        int res = -1;
        try {
            mUpdateUser.setString(1, username);
            mUpdateUser.setString(2, email);
            mUpdateUser.setString(3, familyName);
            mUpdateUser.setString(4, givenName);
            mUpdateUser.setInt(5, U_id);
            res = mUpdateUser.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * 
     * @return -1: unknown error. 0: same vote with the same status exists, abandon.
     *         1: add a new vote. 2: same vote with different status exists, change
     *         the status.
     */
    int castVote(int messageId, int userId, boolean voteStatus) {
        int res = -1;
        try {
            mVoteExists.setInt(1, messageId);
            mVoteExists.setInt(2, userId);
            ResultSet res1 = mVoteExists.executeQuery();
            if (res1.next() && res1.getBoolean(1) == true) {
                try {
                    VoteResponse res2 = selectVote(messageId, userId);
                    if (res2.voteStatus == voteStatus) {
                        return 0;
                    }
                    // if the voteStatus is different, replace it with the new voteStatus.
                    mUpdateVote.setBoolean(1, voteStatus);
                    mUpdateVote.setInt(2, messageId);
                    mUpdateVote.setInt(3, userId);
                    res = mUpdateVote.executeUpdate();
                    if (res <= 0) {
                        return -1;
                    }
                    if (voteStatus == true) {
                        mDecreaseDownvote.setInt(1, messageId);
                        mIncreaseUpvote.setInt(1, messageId);
                        mDecreaseDownvote.executeUpdate();
                        mIncreaseUpvote.executeUpdate();
                        return 2;
                    } else {
                        mDecreaseUpvote.setInt(1, messageId);
                        mIncreaseDownvote.setInt(1, messageId);
                        mDecreaseUpvote.executeUpdate();
                        mIncreaseDownvote.executeUpdate();
                        return 2;
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    return res;
                }
            } else { // else insert it
                mInsertVote.setInt(1, messageId);
                mInsertVote.setInt(2, userId);
                mInsertVote.setBoolean(3, true);
                res = mInsertVote.executeUpdate();
                try {
                    mIncreaseUpvote.setInt(1, messageId);
                    mIncreaseUpvote.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return res;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return res;
        }

        return res;
    }

    /**
     * Create Message. If it already exists, this will print an error
     */
    void createMessageTable() {
        try {
            mCreateMessageTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void createUserTable() {
        try {
            mCreateUserTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void createCommentTable() {
        try {
            mCreateCommentTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void createVoteTable() {
        try {
            mCreateVoteTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove Message from the database. If it does not exist, this will print an
     * error.
     */
    void dropMessageTable() {
        try {
            mDropMessageTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Remove User from the database. If it does not exist, this will print an
     * error.
     */
    void dropUserTable() {
        try {
            mDropUserTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void dropCommentTable() {
        try {
            mDropCommentTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    void dropVoteTable() {
        try {
            mDropVoteTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}