package edu.lehigh.cse216.penguinpower.backend;

import java.util.Date;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import java.util.ArrayList;

import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.MessageResponseByUserLite;
import edu.lehigh.cse216.penguinpower.backend.SimpleResponses.CommentResponseByUser;

/**
 * SimpleResponses is like a struct in C: we use it to hold data, and we allow
 * direct access to its fields. In the context of this Database, SimpleResponses
 * represents the data we'd see in a row.
 * 
 * We make responses static classes because we don't really want to encourage
 * users to think of SimpleResponses as being anything other than an abstract
 * representation of a row of the database.
 * 
 * Xuewei: I am not using the m naming convension here because objects of this
 * class are intended to be sent to a user, and the user should not deal with
 * the extra naming complexity.
 */
public class SimpleResponses {
    static class MessageResponse {
        /**
         * The variables in Message table
         */
        int messageId;
        int userId;
        String username;
        String title;
        String content;
        int upVoteCount;
        int downVoteCount;
        Date dateCreated;
        Float latitude;
        Float longitude;
        String description;

        

        public MessageResponse(int messageId, int userId, String username, String title, String content, int upVoteCount,
                int downVoteCount, Date dateCreated) {
            this.messageId = messageId;
            this.userId = userId;
            this.username = username;
            this.title = title;
            this.content = content;
            this.upVoteCount = upVoteCount;
            this.downVoteCount = downVoteCount;
            this.dateCreated = dateCreated;
            this.longitude = null;
            this.latitude = null;
            this.description = null;
        }

        public MessageResponse(MessageResponse data, MapResponse mapInfo){
            this.messageId = data.messageId;
            this.userId = data.userId;
            this.username = data.username;
            this.title = data.title;
            this.upVoteCount = data.upVoteCount;
            this.downVoteCount = data.downVoteCount;
            this.dateCreated = data.dateCreated;
            this.latitude = mapInfo.latitude;
            this.longitude = mapInfo.longitude;
            this.description = mapInfo.description;
        }



    }

    static class MessageVoiceResponse {
        String audioContent;
        int messageId;
        public MessageVoiceResponse(int messageId, String audioContent){
            this.messageId = messageId;
            this.audioContent = audioContent;
        }
    }

    static class MapResponse{
        int mapId;
        Float latitude;
        Float longitude;
        String description;

        public MapResponse(int mapId, Float latitude, Float longitude, String description){
            this.mapId = mapId;
            this.latitude = latitude;
            this.longitude = latitude;
            this.description = description;
        }

    }


    static class MessageResponseLite {
        int messageId;
        int userId;
        String title;
        int popularity;
        Date dateCreated;

        public MessageResponseLite(int messageId, int userId, String title, int popularity, Date dateCreated) {
            this.messageId = messageId;
            this.userId = userId;
            this.title = title;
            this.popularity = popularity;
            this.dateCreated = dateCreated;
        }
    }

    static class MessageResponseByUserLite {
        int messageId;

        public MessageResponseByUserLite(int messageId) {
            this.messageId = messageId;
        }
    }
    static class MessageDocResponse {
        int document_id;
        String google_drive_url;
        Date dateCreated;
        String type;
        int userId;
        int messageId;
        public MessageDocResponse(int document_id, String google_drive_url, Date dateCreated, String type, int userId, int messageId)
        {
            this.document_id = document_id;
            this.google_drive_url = google_drive_url;
            this.dateCreated = dateCreated;
            this.type = type;
            this.userId = userId;
            this.messageId = messageId; 
        }

    }
    static class MessageDocResponseByMid {
        String google_drive_url;
        String type;
        String external_url;
        public MessageDocResponseByMid(String google_drive_url, String type, String external_url)
        {
            this.google_drive_url = google_drive_url;
            this.type = type;
            this.external_url = external_url;
        }
    }
    static class MessageResponseWithDoc {
        //from Message Table
        int messageId;
        int userId;
        String username;
        String title;
        String content;
        int upVoteCount;
        int downVoteCount;
        Date dateCreated;
        
        Float latitude;
        Float longitude;
        String description;

        //from MessageDocument Table
        String encoded;      
        String type;
        String external_url;
        public MessageResponseWithDoc (MessageResponse data, String encoded, String type, String external_url){
            this.messageId = data.messageId;
            this.userId = data.userId;
            this.username = data.username;
            this.title = data.title;
            this.content = data.content;
            this.upVoteCount = data.upVoteCount;
            this.downVoteCount = data.downVoteCount;
            this.dateCreated = data.dateCreated;
            this.encoded = encoded;
            this.type = type;
            this.external_url = external_url;
            this.longitude = null;
            this.latitude = null;
            this.description = null; 
        }

        public MessageResponseWithDoc(MessageResponseWithDoc data, MapResponse mapInfo){
            this.messageId = data.messageId;
            this.userId = data.userId;
            this.username = data.username;
            this.title = data.title;
            this.upVoteCount = data.upVoteCount;
            this.downVoteCount = data.downVoteCount;
            this.dateCreated = data.dateCreated;
            this.encoded = data.encoded;
            this.type = data.type;
            this.external_url = data.external_url;
            this.latitude = mapInfo.latitude;
            this.longitude = mapInfo.longitude;
            this.description = mapInfo.description;
        }

    }


    static class AddMessageDocResponse{
        String encoded;
        String type;
        int M_id;
        public AddMessageDocResponse(String encoded, String type, int M_id)
        {
            this.encoded = encoded;
            this.type = type;
            this.M_id = M_id; 
        }
    }
    static class AddCommentDocResponse{
        String encoded;
        String type;
        int c_id;
        public AddCommentDocResponse(String encoded, String type, int c_id)
        {
            this.encoded = encoded;
            this.type = type;
            this.c_id = c_id; 
        }
    }

    static class CommentDocResponseByCID{
        int comment_id;
		public CommentDocResponseByCID(int comment_id) {
            this.comment_id = comment_id;
		}
        
    }

    
    static class CommentResponseByUser {
        int commentId;

        public CommentResponseByUser(int commentId) {
            this.commentId = commentId;
        }
    }

    static class CommentResponseByCID {
        String commentText;
        Date dateCreated;

        public CommentResponseByCID(String commentText, Date dateCreated) {
            this.commentText = commentText;
            this.dateCreated = dateCreated;
        }
    }
    

    static class LoginResponse {
        String username;
        String sessionKey;

        public LoginResponse(String username, String sessionKey) {
            this.username = username;
            this.sessionKey = sessionKey;
        }
    }

    static class UserResponse{
        int userId;
        String userName;
        String email;
        String familyName;
        String givenName;
        String picture;
    
        public UserResponse(int userId, String userName, String email, String familyName, String givenName, String picture) {
            this.userId = userId;
            this.userName = userName;
            this.email = email;
            this.familyName = familyName;
            this.givenName = givenName;
            this.picture = picture;
        }
    
    }

    static class VoteResponse {
        int messageId;
        int userId;
        boolean voteStatus;

        public VoteResponse(int messageId, int userId, boolean voteStatus) {
            this.messageId = messageId;
            this.userId = userId;
            this.voteStatus = voteStatus;
        }
    }

    static class UserInfoResponse {
        int userId;
        String userName;
        String email;
        String familyName;
        String givenName;
        String picture;
        ArrayList<MessageResponseByUserLite> messageList;
        ArrayList<CommentResponseByUser> commentList;
    
        public UserInfoResponse(int userId, String userName, String email, String familyName, String givenName, String picture, ArrayList<MessageResponseByUserLite> messageList, ArrayList<CommentResponseByUser> commentList) {
            this.userId = userId;
            this.userName = userName;
            this.email = email;
            this.familyName = familyName;
            this.givenName = givenName;
            this.picture = picture;
            this.messageList = messageList;
            this.commentList = commentList;
        }
    }

    static class CommentResponse {
        int messageId;
        int userId;
        int commentId;
        String commentText;
        Date dateCreated;

        public CommentResponse(int messageId, int userId, int commentId, String commentText, Date dateCreated) {
            this.messageId = messageId;
            this.userId = userId;
            this.commentId = commentId;
            this.commentText = commentText;
            this.dateCreated = dateCreated;
        }
        //{ int messageId, int userId, int commentId, String commentText, 
        //Date dateCreated, 
        //String document, String type, String external_url
        static class CommentDocResponse {
            int messageId;
            int userId;
            int commentId;
            Date dateCreated;

            String document;
            String type;
            String external_url;
        
            public CommentDocResponse(String document, String external_url, Date dateCreated, String type, int userId, int commentId, int messageId)
            {
                this.messageId = messageId;
                this.userId = userId;
                this.commentId = commentId;
                this.dateCreated = dateCreated;
                this.document = document;
                this.type = type;
                this.external_url = external_url;
            
            }
        }

        static class CommentFinalResponse {
            int messageId;
            int userId;
            int commentId;
            Date dateCreated;
            String document;
            String type;
            String external_url;
            String commentText;
            String username;
        
            public CommentFinalResponse(String username, String commentText, String document, String external_url, Date dateCreated, String type, int userId, int commentId, int messageId)
            {
                this.username = username;
                this.commentText = commentText;

                this.document = document;
                this.external_url = external_url;
                this.dateCreated = dateCreated;
                this.type = type;
                this.userId = userId;
                this.commentId = commentId;
                this.messageId = messageId;  
            
            }
        }

        static class CommentVoiceResponse {
            int commentId;
            String audioContent;

            public CommentVoiceResponse(int commentId, String audioContent){
                this.commentId = commentId;
                this.audioContent = audioContent;
            }
        }
        

    static class MessageId{
        int M_id;
        public MessageId(int M_id) {
            this.M_id = M_id;
        }
    }
    }
}