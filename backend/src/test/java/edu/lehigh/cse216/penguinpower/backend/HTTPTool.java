package edu.lehigh.cse216.penguinpower.backend;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

import org.json.JSONObject;

/**
 * A static class that contains methods to help connect to the server, send
 * data, get result, and parse result.
 */
class HTTPTool {

    private HTTPTool() {
    }

    /** Send data to the server. */
    static void sendData(HttpURLConnection con, String data) throws IOException {
        DataOutputStream wr = null;
        try {
            wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(data);
            wr.flush();
            wr.close();
        } catch (IOException exception) {
            throw exception;
        } finally {
            if (wr != null) {
                wr.close();
            }
        }
    }

    /**
     * get the response from the server and parse it into a json file.
     * 
     * @return the JSON file send be the server. null if the server returns an error
     *         status.
     * @throws IOException
     */
    
    static JSONObject getResponse(HttpURLConnection con) throws IOException {
        int responseCode = con.getResponseCode();
        if(responseCode != 200){
            return null;
        }
        InputStream inputStream = con.getInputStream();
        String response = inputStreamToString(inputStream);
        JSONObject json = new JSONObject(response.toString());
        return json;
    }

    private static String inputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

        StringBuilder response = new StringBuilder();
        String currentLine;

        while ((currentLine = in.readLine()) != null)
            response.append(currentLine);
        in.close();
        return response.toString();
    }

}