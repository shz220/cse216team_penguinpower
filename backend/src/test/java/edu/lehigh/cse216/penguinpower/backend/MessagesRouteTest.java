package edu.lehigh.cse216.penguinpower.backend;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.IOException;
import java.net.*;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.json.JSONArray;
import org.json.JSONObject;
/**
 * The enviromental variable DATABASE_URL should be set first before run any test.
 */

public class MessagesRouteTest extends TestCase {
	final Database database = Database.getDatabase();
	final static String BASE_URL = "https://cse216-penguin-power.herokuapp.com";
	final static String TEST_USERNAME = "shz220@lehigh.edu";
    final static String TEST_FAMILY_NAME = "Zhu";
    final static String TEST_GIVEN_NAME = "Shengli";
    final static String TEST_EMAIL = "shz220@lehigh.edu";
    final static String TEST_ACCESSCODE = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImFhNDM2YzNmNjNiMjgxY2UwZDk3NmRhMGI1MWEzNDg2MGZmOTYwZWIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiMjAyMzEwMTk3NzExLWgzcThzM29pbnVxZWRkOWppczUwMnRhZWk4dTc2dDRqLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiMjAyMzEwMTk3NzExLWgzcThzM29pbnVxZWRkOWppczUwMnRhZWk4dTc2dDRqLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTAwMDkxMDc0NDY4NTMxNDk4MDI2IiwiaGQiOiJsZWhpZ2guZWR1IiwiZW1haWwiOiJzaHoyMjBAbGVoaWdoLmVkdSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiZ19kVW45SGsyZzRjbktMZjFlNEJtdyIsIm5hbWUiOiJTaGVuZ2xpIFpodSIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vLVhYN3hOcXJWSm9FL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL0FCdE5sYkMxSzg5SlM3YlFfVEF0ZC03c0x3SG1KVGpXZGcvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6IlNoZW5nbGkiLCJmYW1pbHlfbmFtZSI6IlpodSIsImxvY2FsZSI6ImVuIiwiaWF0IjoxNTQxNDc2MzMzLCJleHAiOjE1NDE0Nzk5MzMsImp0aSI6ImYxYjA0MmIwMGEwZDU0ODgxMGY4NTcwNWVmYzNmMWIxNTRlYTBkZGYifQ.d5m-EWdc_9gcQUci7WM583QWeZu4VndQDWBHyW1XOCglokSBYvB1H2lMlN7ikIFVbS8mccCwugmgyP2KqtjbmYSSs0KmbGYp-cCnIYqeOL5NRtTxtCGUlzjcicpE-F8X_CShDv1WMy052kl-stWmRhVU1E8AoAwSrTkLiVuSSkEkdWSHddi0XiD8o_admFCZg6p64YnFvPmxzYQKc6geYu4qRlOX4h_2uAVIKUmiE00mV_PcPbEpX0c4eTQsXD15NDcPrl2_RKeKYWK-79EM7-QgBZfmGC76lOgGtr92jkPGCYePngaB1F3i1Yj1obVSKgiRDU9HuMb4YGMF25QpHw";
	final static String TEST_PICTURE = "http://lh3.googleusercontent.com/-YwqoQCjwnCk/AAAAAAAAAAI/AAAAAAAAAAA/ABtNlbDxAp4mnprclziwKKRlDBkeMSfwFA/s96-c/photo.jpg";

	String sessionKey;
	final static String TEST_MESSAGE_TITLE = "Test from the backend unit test.";
	final static String TEST_MESSAGE_CONTENT = "Testing messages route. In progress.";

	/**
	 * Create the test case
	 *
	 * @param testName name of the test case
	 */
	public MessagesRouteTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		TestSuite suite = new TestSuite(MessagesRouteTest.class);
		return suite;
	}

	/**
	 * set up a made-up user. Then get sessionKey. Finally enter a message by this
	 * made-up user.
	 * 
	 * @throws InvalidKeySpecException
	 * @throws NoSuchAlgorithmException
	 * @throws IOException
	 */
	@Override
	protected void setUp() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
		// try to delete this user if exist to avoid duplicate.
		database.deleteUser(TEST_USERNAME);
		database.insertUser(TEST_USERNAME, TEST_EMAIL, TEST_FAMILY_NAME, TEST_GIVEN_NAME, TEST_PICTURE);
		// get the sessionKey by trying to login.
		URL url = new URL(BASE_URL + "/users/login");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		HTTPTool.sendData(con, String.format("{'accessCode': '%s'}", TEST_ACCESSCODE));
		JSONObject json = HTTPTool.getResponse(con);
		assertTrue(json.getString("mStatus").equals("ok"));
		JSONObject data = json.getJSONObject("mData");
		sessionKey = (String) data.get("sessionKey");
		// insert a testing message.
		database.insertMessage(database.selectUser(TEST_USERNAME).userId, TEST_MESSAGE_TITLE, TEST_MESSAGE_CONTENT);
	}

	/** delete user. delete messages entered. */
	@Override
	protected void tearDown() {
		database.deleteMessagesByUid(database.selectUser(TEST_USERNAME).userId);
		database.deleteUser(TEST_USERNAME);
	}

	/**
	 * Tests if all messages can be viewed, and the specific testing message is
	 * there.
	 * 
	 * @throws IOException
	 */
	public void testGetMessages() throws IOException {
		// a successful attempt with correct username and sessionKey provided.
		URL url = new URL(BASE_URL + "/messages");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("username", TEST_USERNAME);
		con.setRequestProperty("sessionKey", sessionKey);
		JSONObject json = HTTPTool.getResponse(con);
		assertTrue(json.getString("mStatus").equals("ok"));
		JSONArray data = json.getJSONArray("mData");
		boolean containsTestMessage = false;
		for (int i = 0; i < data.length(); i++) {
			if (data.getJSONObject(i).getString("title").equals(TEST_MESSAGE_TITLE)) {
				containsTestMessage = true;
				assertTrue(data.getJSONObject(i).getInt("userId") == database.selectUser(TEST_USERNAME).userId);
			}
		}
		assertTrue(containsTestMessage);
		// a false attempt with incorrect sessionKey
		url = new URL(BASE_URL + "/messages");
		con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("username", TEST_USERNAME);
		con.setRequestProperty("sessionKey", "False SessionKey.");
		json = HTTPTool.getResponse(con);
		assertTrue(json.getString("mStatus").equals("error"));
	}

	/**
	 * Tests if the get one message route could return the testing message.
	 * 
	 * @throws IOException
	 */
	public void testGetMessage() throws IOException {
		// get the messageId.
		int messageId = database.selectMessage(TEST_MESSAGE_TITLE).messageId;
		// a succesful attempt.
		URL url = new URL(BASE_URL + "/messages/" + messageId);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("username", TEST_USERNAME);
		con.setRequestProperty("sessionKey", sessionKey);
		JSONObject json = HTTPTool.getResponse(con);
		assertTrue(json.getString("mStatus").equals("ok"));
		JSONObject data = json.getJSONObject("mData");
		assertTrue(data.getString("title").equals(TEST_MESSAGE_TITLE));
		assertTrue(data.getString("content").equals(TEST_MESSAGE_CONTENT));
		assertTrue(data.getInt("userId") == database.selectUser(TEST_USERNAME).userId);
		// wrong sessionKey.
		url = new URL(BASE_URL + "/messages/" + messageId);
		con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("username", TEST_USERNAME);
		con.setRequestProperty("sessionKey", "wrong sessionKey");
		json = HTTPTool.getResponse(con);
		assertTrue(json.getString("mStatus").equals("error"));
	}

	/**
	 * Test the add a new message route. If this went wrong, database could end up
	 * with one testing message.
	 */
	public void testAddMessage() throws IOException {
		URL url = new URL(BASE_URL + "/messages");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("POST");
		con.setDoOutput(true);
		String title = "add message unit test from backend";
		String content = "If this message remains after test is done, that means the test went wrong. Please manually delete this message.";
		HTTPTool.sendData(con, String.format("{'username': '%s', 'sessionKey': '%s', 'title': '%s', 'content': '%s'}",
				TEST_USERNAME, sessionKey, title, content));
		JSONObject json = HTTPTool.getResponse(con);
		assertTrue(json.getString("mStatus").equals("ok"));
		// check if the message ends up in the database with right title and content.
		SimpleResponses.MessageResponse response = database.selectMessage(title);
		assertFalse(response == null);
		assertTrue(response.title.equals(title));
		assertTrue(response.content.equals(content));
		// delete the message.
		database.deleteMessage(response.messageId);
	}

	/**
	 * Updates the test message and check if the update takes effect in the
	 * database. Then reset it back.
	 */
	public void testUpdateMessage() throws IOException {
		// find message id
		int messageId = database.selectMessage(TEST_MESSAGE_TITLE).messageId;
		// update the message
		URL url = new URL(BASE_URL + "/messages/" + messageId);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("PUT");
		con.setDoOutput(true);
		String title = "edit message unit test from backend";
		String content = "(Edited) If this message remains after test is done, that means the test went wrong. Please manually delete this message.";
		HTTPTool.sendData(con,
				String.format("{'username': '%s', 'sessionKey': '%s', 'messageId': %d, 'title': '%s', 'content': '%s'}",
						TEST_USERNAME, sessionKey, messageId, title, content));
		JSONObject json = HTTPTool.getResponse(con);
		assertTrue(json.getString("mStatus").equals("ok"));
		// check if the change takes effect in database
		SimpleResponses.MessageResponse response = database.selectMessage(title);
		assertFalse(response == null);
		assertTrue(response.title.equals(title));
		assertTrue(response.content.equals(content));
		// reset this message.
		database.updateMessage(messageId, TEST_MESSAGE_TITLE, TEST_MESSAGE_CONTENT);
	}

	/**
	 * Upvote the message, check if changes applied in backend, then downvote the
	 * same massage and check again. Then recreate this message.
	 */
	public void testVoteMessage() throws IOException {
		// find message id
		int messageId = database.selectMessage(TEST_MESSAGE_TITLE).messageId;
		// upvote
		URL url = new URL(BASE_URL + "/vote/message");
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("PUT");
		con.setDoOutput(true);
		HTTPTool.sendData(con,
				String.format("{'sessionKey': '%s', 'username': '%s', 'voteStatus': %s, 'messageId': %d}", sessionKey,
						TEST_USERNAME, true, messageId));
		JSONObject json = HTTPTool.getResponse(con);
		assertTrue(json.getString("mStatus").equals("ok"));
		// check backend
		SimpleResponses.VoteResponse voteResponse = database.selectVote(messageId,
				database.selectUser(TEST_USERNAME).userId);
		assertTrue(voteResponse.voteStatus == true);
		SimpleResponses.MessageResponse messageResponse = database.selectMessage(messageId);
		assertTrue(messageResponse.upVoteCount == 1);
		assertTrue(messageResponse.downVoteCount == 0);
		// downvote
		url = new URL(BASE_URL + "/vote/message");
		con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("PUT");
		con.setDoOutput(true);
		HTTPTool.sendData(con,
				String.format("{'sessionKey': '%s', 'username': '%s', 'voteStatus': %s, 'messageId': %d}", sessionKey,
						TEST_USERNAME, false, messageId));
		json = HTTPTool.getResponse(con);
		assertTrue(json.getString("mStatus").equals("ok"));
		// check backend
		voteResponse = database.selectVote(messageId, database.selectUser(TEST_USERNAME).userId);
		assertTrue(voteResponse.voteStatus == false);
		messageResponse = database.selectMessage(messageId);
		assertTrue(messageResponse.upVoteCount == 0);
		assertTrue(messageResponse.downVoteCount == 1);
		// reset the message
		database.deleteMessage(messageId);
		database.insertMessage(database.selectUser(TEST_USERNAME).userId, TEST_MESSAGE_TITLE, TEST_MESSAGE_CONTENT);
	}

	/**
	 * Try to delete the message. First with a false sessionKey, then with the true
	 * sessionKey, then try to delete again with the true sessionKey. Effect on
	 * database is checked at every stage.
	 */
	public void testDeleteMessage() throws IOException {
		// find message id
		int messageId = database.selectMessage(TEST_MESSAGE_TITLE).messageId;
		// try to delete with wrong credential.
		URL url = new URL(BASE_URL + "/messages/" + messageId);
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("DELETE");
		con.setRequestProperty("username", TEST_USERNAME);
		con.setRequestProperty("sessionKey", "false sessionKey.");
		JSONObject json = HTTPTool.getResponse(con);
		assertTrue(json.getString("mStatus").equals("error"));
		//check database
		SimpleResponses.MessageResponse response = database.selectMessage(messageId);
		assertFalse(response == null);
		// delete with the right credential
		url = new URL(BASE_URL + "/messages/" + messageId);
		con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("DELETE");
		con.setRequestProperty("username", TEST_USERNAME);
		con.setRequestProperty("sessionKey", sessionKey);
		json = HTTPTool.getResponse(con);
		assertTrue(json.getString("mStatus").equals("ok"));
		//check database
		response = database.selectMessage(messageId);
		assertTrue(response == null);
		//try to delete again
		url = new URL(BASE_URL + "/messages/" + messageId);
		con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("DELETE");
		con.setRequestProperty("username", TEST_USERNAME);
		con.setRequestProperty("sessionKey", sessionKey);
		json = HTTPTool.getResponse(con);
		assertTrue(json.getString("mStatus").equals("error"));
		//check database
		response = database.selectMessage(messageId);
		assertTrue(response == null);
	}

}
