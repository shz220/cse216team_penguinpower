package edu.lehigh.cse216.penguinpower.backend;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
/**
 * The enviromental variable DATABASE_URL should be set first before run any test.
 */
public class UsersRouteTest extends TestCase {
    final Database database = Database.getDatabase();
    final static String BASE_URL = "https://cse216-penguin-power.herokuapp.com";
    final static String TEST_USERNAME = "shz220@lehigh.edu";
    final static String TEST_FAMILY_NAME = "Zhu";
    final static String TEST_GIVEN_NAME = "Shengli";
    final static String TEST_EMAIL = "shz220@lehigh.edu";
    final static String TEST_ACCESSCODE = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImFhNDM2YzNmNjNiMjgxY2UwZDk3NmRhMGI1MWEzNDg2MGZmOTYwZWIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiMjAyMzEwMTk3NzExLWgzcThzM29pbnVxZWRkOWppczUwMnRhZWk4dTc2dDRqLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiMjAyMzEwMTk3NzExLWgzcThzM29pbnVxZWRkOWppczUwMnRhZWk4dTc2dDRqLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTAwMDkxMDc0NDY4NTMxNDk4MDI2IiwiaGQiOiJsZWhpZ2guZWR1IiwiZW1haWwiOiJzaHoyMjBAbGVoaWdoLmVkdSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiZ19kVW45SGsyZzRjbktMZjFlNEJtdyIsIm5hbWUiOiJTaGVuZ2xpIFpodSIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vLVhYN3hOcXJWSm9FL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL0FCdE5sYkMxSzg5SlM3YlFfVEF0ZC03c0x3SG1KVGpXZGcvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6IlNoZW5nbGkiLCJmYW1pbHlfbmFtZSI6IlpodSIsImxvY2FsZSI6ImVuIiwiaWF0IjoxNTQxNDc2MzMzLCJleHAiOjE1NDE0Nzk5MzMsImp0aSI6ImYxYjA0MmIwMGEwZDU0ODgxMGY4NTcwNWVmYzNmMWIxNTRlYTBkZGYifQ.d5m-EWdc_9gcQUci7WM583QWeZu4VndQDWBHyW1XOCglokSBYvB1H2lMlN7ikIFVbS8mccCwugmgyP2KqtjbmYSSs0KmbGYp-cCnIYqeOL5NRtTxtCGUlzjcicpE-F8X_CShDv1WMy052kl-stWmRhVU1E8AoAwSrTkLiVuSSkEkdWSHddi0XiD8o_admFCZg6p64YnFvPmxzYQKc6geYu4qRlOX4h_2uAVIKUmiE00mV_PcPbEpX0c4eTQsXD15NDcPrl2_RKeKYWK-79EM7-QgBZfmGC76lOgGtr92jkPGCYePngaB1F3i1Yj1obVSKgiRDU9HuMb4YGMF25QpHw";
    final static String TEST_PICTURE = "http://lh3.googleusercontent.com/-YwqoQCjwnCk/AAAAAAAAAAI/AAAAAAAAAAA/ABtNlbDxAp4mnprclziwKKRlDBkeMSfwFA/s96-c/photo.jpg";
    String sessionKey;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public UsersRouteTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        TestSuite suite = new TestSuite(UsersRouteTest.class);
        return suite;
    }

    /**
     * Insert a user, then try to login and get the sessionKey.
     * 
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    @Override
    protected void setUp() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        // try to delete this user if exist to avoid duplicate.
        database.deleteUser(TEST_USERNAME);
        database.insertUser(TEST_USERNAME, TEST_EMAIL, TEST_FAMILY_NAME, TEST_GIVEN_NAME, TEST_PICTURE);
        // get the sessionKey by trying to login.
        URL url = new URL(BASE_URL + "/users/login");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        HTTPTool.sendData(con, String.format("{'accessCode': '%s'}", TEST_ACCESSCODE));
        JSONObject json = HTTPTool.getResponse(con);
        assertTrue(json.getString("mStatus").equals("ok"));
        JSONObject data = json.getJSONObject("mData");
        sessionKey = (String) data.get("sessionKey");
    }

    /** delete user without using any route. */
    @Override
    protected void tearDown() {
        database.deleteUser(TEST_USERNAME);
    }

    /**
     * Get the infomation of the user. Check if that matches with the value we
     * previously set.
     * 
     * @throws IOException
     */
    public void testGetUserInfomation() throws IOException {
        // a succesful attempt.
        URL url = new URL(BASE_URL + "/users/info");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        HTTPTool.sendData(con, String.format("{'username': '%s', 'sessionKey': '%s', 'usernameAskedFor': '%s'}", TEST_USERNAME, sessionKey, TEST_USERNAME));
        JSONObject json = HTTPTool.getResponse(con);
        assertTrue(json.getString("mStatus").equals("ok"));
        JSONObject data = json.getJSONObject("mData");
        assertTrue(data.getString("email").equals(TEST_EMAIL));
        assertTrue(data.getString("picture").equals(TEST_PICTURE));
    }

    /**
     * Log out the user. Check if the username is deleted from the hashtable. Then
     * log the user back in.
     */
    public void testLogoutUser() throws IOException {
        // a legit attempt to logout
        URL url = new URL(BASE_URL + "/users/logout");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        HTTPTool.sendData(con, String.format("{'username': '%s', 'sessionKey': '%s'}", TEST_USERNAME, sessionKey));
        JSONObject json = HTTPTool.getResponse(con);
        assertTrue(json.getString("mStatus").equals("ok"));
        // check if the user is loged out by a simple get information request.
        url = new URL(BASE_URL + "/users/info");
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        HTTPTool.sendData(con, String.format("{'username': '%s', 'sessionKey': '%s'}", TEST_USERNAME, sessionKey));
        json = HTTPTool.getResponse(con);
        assertTrue(json.getString("mStatus").equals("error"));
        // log back in and reset the sessionKey for future use.
        url = new URL(BASE_URL + "/users/login");
        con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        HTTPTool.sendData(con, String.format("{'accessCode': '%s'}", TEST_ACCESSCODE));
        json = HTTPTool.getResponse(con);
        assertTrue(json.getString("mStatus").equals("ok"));
        JSONObject data = json.getJSONObject("mData");
        sessionKey = (String) data.get("sessionKey");
    }

    
}
