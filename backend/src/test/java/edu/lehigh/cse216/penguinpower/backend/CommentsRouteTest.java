package edu.lehigh.cse216.penguinpower.backend;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.IOException;
import java.net.*;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * The enviromental variable DATABASE_URL should be set first before run any test.
 */

public class CommentsRouteTest extends TestCase {
    final Database database = Database.getDatabase();
    final static String BASE_URL = "https://cse216-penguin-power.herokuapp.com";
    final static String TEST_USERNAME = "shz220@lehigh.edu";
    final static String TEST_FAMILY_NAME = "Zhu";
    final static String TEST_GIVEN_NAME = "Shengli";
    final static String TEST_EMAIL = "shz220@lehigh.edu";
    final static String TEST_ACCESSCODE = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImFhNDM2YzNmNjNiMjgxY2UwZDk3NmRhMGI1MWEzNDg2MGZmOTYwZWIiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiMjAyMzEwMTk3NzExLWgzcThzM29pbnVxZWRkOWppczUwMnRhZWk4dTc2dDRqLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiMjAyMzEwMTk3NzExLWgzcThzM29pbnVxZWRkOWppczUwMnRhZWk4dTc2dDRqLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTAwMDkxMDc0NDY4NTMxNDk4MDI2IiwiaGQiOiJsZWhpZ2guZWR1IiwiZW1haWwiOiJzaHoyMjBAbGVoaWdoLmVkdSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoiZ19kVW45SGsyZzRjbktMZjFlNEJtdyIsIm5hbWUiOiJTaGVuZ2xpIFpodSIsInBpY3R1cmUiOiJodHRwczovL2xoMy5nb29nbGV1c2VyY29udGVudC5jb20vLVhYN3hOcXJWSm9FL0FBQUFBQUFBQUFJL0FBQUFBQUFBQUFBL0FCdE5sYkMxSzg5SlM3YlFfVEF0ZC03c0x3SG1KVGpXZGcvczk2LWMvcGhvdG8uanBnIiwiZ2l2ZW5fbmFtZSI6IlNoZW5nbGkiLCJmYW1pbHlfbmFtZSI6IlpodSIsImxvY2FsZSI6ImVuIiwiaWF0IjoxNTQxNDc2MzMzLCJleHAiOjE1NDE0Nzk5MzMsImp0aSI6ImYxYjA0MmIwMGEwZDU0ODgxMGY4NTcwNWVmYzNmMWIxNTRlYTBkZGYifQ.d5m-EWdc_9gcQUci7WM583QWeZu4VndQDWBHyW1XOCglokSBYvB1H2lMlN7ikIFVbS8mccCwugmgyP2KqtjbmYSSs0KmbGYp-cCnIYqeOL5NRtTxtCGUlzjcicpE-F8X_CShDv1WMy052kl-stWmRhVU1E8AoAwSrTkLiVuSSkEkdWSHddi0XiD8o_admFCZg6p64YnFvPmxzYQKc6geYu4qRlOX4h_2uAVIKUmiE00mV_PcPbEpX0c4eTQsXD15NDcPrl2_RKeKYWK-79EM7-QgBZfmGC76lOgGtr92jkPGCYePngaB1F3i1Yj1obVSKgiRDU9HuMb4YGMF25QpHw";
    final static String TEST_PICTURE = "http://lh3.googleusercontent.com/-YwqoQCjwnCk/AAAAAAAAAAI/AAAAAAAAAAA/ABtNlbDxAp4mnprclziwKKRlDBkeMSfwFA/s96-c/photo.jpg";
    String sessionKey;
    final static String TEST_MESSAGE_TITLE = "Test from the backend unit test.";
    final static String TEST_MESSAGE_CONTENT = "Testing messages route. In progress.";
    final static String TEST_COMMENT = "Testing for comment routes.";
    
    int messageId;
    int commentId;

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public CommentsRouteTest(String testName) {
        super(testName);
    } 

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        TestSuite suite = new TestSuite(CommentsRouteTest.class);
        return suite;
    }

    /**
     * set up a made-up user. Then get sessionKey. Finally enter a message by this
     * made-up user.
     * 
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws IOException
     */
    @Override
    protected void setUp() throws NoSuchAlgorithmException, InvalidKeySpecException, IOException {
        // try to delete this user if exist to avoid duplicate.
        database.deleteUser(TEST_USERNAME);
        database.insertUser(TEST_USERNAME, TEST_EMAIL, TEST_FAMILY_NAME, TEST_GIVEN_NAME, TEST_PICTURE);
        // get the sessionKey by trying to login.
        URL url = new URL(BASE_URL + "/users/login");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        HTTPTool.sendData(con, String.format("{'accessCode': '%s'}", TEST_ACCESSCODE));
        JSONObject json = HTTPTool.getResponse(con);
        assertTrue(json.getString("mStatus").equals("ok"));
        JSONObject data = json.getJSONObject("mData");
        sessionKey = (String) data.get("sessionKey");
        // insert a testing message.
        database.insertMessage(database.selectUser(TEST_USERNAME).userId, TEST_MESSAGE_TITLE, TEST_MESSAGE_CONTENT);
        // get the messageId.
        messageId = database.selectMessage(TEST_MESSAGE_TITLE).messageId;
        // insert the comment.
        database.insertComment(messageId, database.selectUser(TEST_USERNAME).userId, TEST_COMMENT);
        // get the comment id.
        commentId = database.selectComment(TEST_COMMENT).commentId;
    }

    /** delete user. delete messages entered. Comment will also be deleted on cascade. */
    @Override
    protected void tearDown() {
        database.deleteMessagesByUid(database.selectUser(TEST_USERNAME).userId);
        database.deleteUser(TEST_USERNAME);
    }

    /**
     * Tests if all comment can be viewed, and the specific testing comment is
     * there.
     * 
     * @throws IOException
     */
    
    public void testGetMessages() throws IOException {
        // a successful attempt with correct username and sessionKey provided.
        URL url = new URL(BASE_URL + "/comments/" + messageId);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("username", TEST_USERNAME);
        con.setRequestProperty("sessionKey", sessionKey);
        JSONObject json = HTTPTool.getResponse(con);
        assertTrue(json.getString("mStatus").equals("ok"));
        JSONArray data = json.getJSONArray("mData");
        boolean containsComments = false;
        for (int i = 0; i < data.length(); i++) {
            if (data.getJSONObject(i).getString("commentText").equals(TEST_COMMENT)) {
                containsComments = true;
                assertTrue(data.getJSONObject(i).getInt("userId") == database.selectUser(TEST_USERNAME).userId);
            }
        }
        assertTrue(containsComments);
    }

   
    /**
     * Test the add a new comment route. If this went wrong, database could end up
     * with one testing comment.
     */
    
    public void testAddMessage() throws IOException {
        URL url = new URL(BASE_URL + "/comments/" + messageId);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setDoOutput(true);
        String comment = "If this message remains after test is done, that means the test went wrong. Please manually delete this message.";
        HTTPTool.sendData(con, String.format("{'username': '%s', 'sessionKey': '%s',  'content': '%s'}",
                TEST_USERNAME, sessionKey, comment));
        JSONObject json = HTTPTool.getResponse(con);
        assertTrue(json.getString("mStatus").equals("ok"));
        // check if the comment ends up in the database.
        CommentResponse response = database.selectComment(comment);
        assertFalse(response == null);
        // delete the comment.
        database.deleteComment(response.commentId);
    }
}
