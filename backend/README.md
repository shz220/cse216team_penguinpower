# Backend-End Server
## Functionalities So Far:
### REST API
#### Current Problems in Backend (written by Konka Shi)
1. Files in google drive are not deleted when the message or comment is deleted from web or android
2. In a comment with only a link, the link could not be updated again. An error will occur. 
3. In a comment with only a pdf/image, this document could not updated again. An error will occur. 

**get all messages**   
GET /messages  
request header: { String username, String sessionKey }
response: { int messageId, int userId, String title, int popularity, Date dateCreated }

**get one message**   
GET /messages/:messageId  
request header: { String username, String sessionKey }
response: { int messageId, int userId, String title, String content, int upVoteCount, int downVoteCount, Date dateCreated, String encoded, String type:null, String external_url, Float latitude, Float longitude, String Description}

**add a new message**   
###CHANGED
POST /messages  
request: { String username, String sessionKey, String title, String content, String document, String external_url, Float latitude, Float longitude, String description}
response: {M_id} if no file attached. Otherwise, respond {String encoded, String type, int M_Id}

**edit a message**   
PUT /messages/:messageId  
request: {String username, String sessionKey, int messageId, String title, String content }

**vote a message**   
PUT /vote/message   
request: { String sessionKey, String username, boolean voteStatus, int messageId }
voteStatus - true for up vote.  
response: { int result }
- 0 for the same vote exits in database, abandoned.
- 1 for add a new vote.
- 2 for the opposite vote exits in database, the vote status is changed.

**delete a message**   
DELETE /messages/:messageId   
request header:  { String username, String sessionKey }  

**login**   
Provides a login functionality which requests accessCode (accessCode is the one frontend get from google - OAuth2)   
POST /users/login  
request: { String accessCode }  
response: { String username, String sessionKey }

**logout**   
Provides a logout functionality. Gives the frontend some level of control over safty.  
POST /users/logout   
request: { String username, String sessionKey }  

**get one user's info**   
Provides detailed information about the user (for profile page)   
POST /users/info    
request: { String username, String sessionKey, String usernameAskedFor }   
response: { int userId, String username, String email, String familyName, String givenName, String picture, messageList:{int messageId} , commentList:{int commentId} }    

**get all comments**   
Returns a list of comments for a specific message.  
GET /comments/:messageId  
request header: { String username, String sessionKey }    
response: { {int messageId, int userId, int commentId, String commentText, Date dateCreated, String document, String type, String external_url}}

**add a comment**   
POST /comments/:messageId  
request: { String username, String sessionKey, String content, String document, String external_url}
response: {c_id} if no file attached. otherwise, {String encoded, String type, int c_id}

**edit a comment**   
PUT /comments  
request: { String username, String sessionKey, int commentId, String content }

**delete a comment**   
DELETE /comments/commentId  
request header:  { String username, String sessionKey }  

**get a comment**    
GET /commentSingle/:commentId   
request header: String username, String sessionKey   
response: {String commentText, Date dateCreated }

### Unit Tests
**LoginRouteTest**    
tested route:  log in    
**MessagesRouteTest**    
tested route:  get all messages, get one message, add a message, edit a message, delete a message    
**UserRouteTest**    
tested route:  get user info, update user info   
**CommentRouteTest**    
tested route:  get comment list, add comment, edit comment


## Contributors Notes:
### Phase 5:
#### Updates (Shengli Zhu)
Solved last phase google drive cannot connect problem. Created new GoogleDrive.java class.     
Update "POST message" and "POST comments/:messageId" to upload file to google drive    
Update "GET message/:messageId" and "GET comments/:messageId" to download file from google drive and send base64 string to frontend    
Update "PUT messages/:messageId" and "PUT comments" to upload new file to googledrive successfully.    

#### Updates (Konka(Kangjia) Shi)
Fixed put route for message to replace the old document with the new document
Fixed put route for message to replace the old link with a new link 

### Phase 4: Konka Shi
Users now can add a document or a link to a document, and it is not allowed to upload both a link and a document. 
#### Updates 
**edit GET /messages/:messageId**
- request : { String username, String sessionKey }
- response: { int messageId, int userId, String title, String content, int upVoteCount, int downVoteCount, Date dateCreated, String encoded, String type, String external_url }
- Note: the bicode of image or pdf sended back is without the prefix (the mimetype of the document)
- String type should be “data:image/jpeg” or “data:application/pdf”

**edit POST /messages**
- request: { String username, String sessionKey, String title, String content, String document, String external_url}
- response: M_id if without anything attached. Otherwise, respond String encoded, String type, int M_Id
- String link: is an external link, store directly into table
- either document or external_url should be empty
- type would be "external_link" if a external_url is attached 

**edit PUT/messages/:messageId**
- request: {String username, String sessionKey, int messageId, String title, String content, String document, String external_url}
- When the user is updating a docoment, we assume he only updates the same type of document. A user can't add a link if he already had a document in the message.
**edit DELETE//messages/:messageId**
- Delete the message. If the message has a MessageComment table, it will also be deleted. 
**edit GET/comments/:messageId**
- Get a list of comments of the message. If the message has an url, encoded it to base64 and sent it to frontend
**edit POST /comments**
- request: { String username, String sessionKey, String title, String content, String document, String external_url}
- response: c_id if without anything attached. Otherwise, respond String encoded, String type, int c_Id
- type would be "external_link" if a external_url is attached 
**edit PUT/comments/:messageId**
- request: {String username, String sessionKey, int commentId, String content, String document, String external_url}
**edit DELETE/comments/:messageId**
- Delete the comment. If the comment has a MessageComment table, it will also be deleted. 
**Memcachier**
- account infor: account login: kas320@lehigh.edu  pw:shikangjia20. Memcachier is not implemented yet. It is supposed to replace the hash table. 

#### Problems Left
- No Xmecache implemented 
- Google drive connection is not working 
- When uploading a message with a document, the message will be added twice 
- When added with a comment with a link first time, the comments without documents will be deleted. After, only a comment with a link/document is allowed to be added. 
- Put Route with message does not work with updating link.
- Put Route with comment might have small issues. 
### Phase 3: Shengli Zhu
#### Update to REST API:
**edit POST /users/login route:**     
OAuth2: to get accessCode from frontend and send back the username and a random sessionkey    
- Register: If user is not in the database but from lehigh.edu, will gather the user info and add this new user in User table   
- Login: If user is verified and in the database, will response the username and a random sessionKey directly    
- AccessCode: only ask for accessCode from frontEnd, because backend can get the username from the accessCode    
- Username: sends back is the email, because email is unique for all users. If we have two users having the same name, it will cause problem if we store that as username.    
- SessionKey: send back is a random number generated everytime based on the username    

POST /users/login   
request: { String accessCode }    
response: { String username, String sessionKey }    

**edit POST /users/info route:**   
- From google we can get email, familyName, givenName, picture and userId which need to be sended to frontend for profile page.    
- Add two array lists (messageList and commentList) in response which contains all messages and comments written by this user.    
- The responsed user info are all the info for the usernameAskedFor not username. In this way, after user login, the user(username) can see profile page of one user (usernameAskedFor).    

POST /users/info    
request: { String username, String sessionKey, String usernameAskedFor }   
response: { int userId, String username, String email, String familyName, String givenName, String picture, messageList:{int messageId} , commentList:{int commentId} }    

**new GET /comments/:commentId**
- For frontend to display one comment based on the commentId in the profile page   

GET /commentSingle/:commentId   
request header: String username, String sessionKey   
response: {String commentText, Date dateCreated }

**edit GET /messages/:messageId**
- For frontend to display the username instead of userId in "Created By: xxx", add String username in response   

GET /messages/:messageId   
request header:  String username, String sessionKey    
response: { int messageId, int userId, String username, String title, String content, int upVoteCount, int downVoteCount, Date dateCreated }

**removed unnecessary codes:**   
remove change password route and password encryption. (These are not needed anymore with the OAuth2.)    

#### Unit Test:
- update all testing classes based on the usage of OAuth2:    
AppTest.java; CommentsRouteTest.java; HTTPTool.java; LoginRouteTest.java; MessagesRouteTest.java; UsersRouteTest.java   
- All tests are passed successfully    
- NOTE: all tests can be passed only when the accessCode is working. If this accessCode is expired, we need to change all String TEST_ACCESSCODE in all classes.

### Phase 2: Xuewei Wang

#### Update to Old REST API:
* Combined the upvote and downvote routes into one single vote route.
* Uniformed the naming convension as the camel case style. **IMPORTANT: all routes are effected.**
* Refactored the Database.java code and created different Request/ Response classes to make requests easier and responses cleaner.  
* Users must be logged in to view posts, up-vote, down-vote, and have the ability to leave comments as required. **Therefore, all related routes are chaged such that they could take in a pair of username and session key.**
* For an extra level security, when changing user information, users are required to provide password again.  

#### New REST Routes:
* Added user acount relateted routes: login, logout, get user information, update user information.

#### Unit Tests:
Removed incompatible upvote/downvote tests.  
Added tests: LoginRouteTest, MessageRouteTest, CommentRouteTest, UserRouteTest


### Phase 1: Yan Lin Chen
#### This is the REST api that we are implementing
- GET /messages  -> all message (with total vote) 
- GET /messages/:M_id  -> one message
- POST /messages -> add a new message
- PUT /messages/:M_id -> update message (check whether U_id matches the U_id of the message)
- PUT /messages/:M_id/upvote -> add upvote by one
- PUT /messages/:M_id/downvote -> add downvote by one
- DELETE /messages/:M_id -> delete one message(without checking the U_id)
- GET /users/U_id -> one user 

It's hard to for the front end to add the functionality to check user id in the DELETE route, so I left that part out.

#### Issue
For the unit test, I tested the update functions in DataStore.java. In order for the test to execute, I needed to conect
to the database and thus the test makes updates everytime it is called (meaning mvn package with environoment Database_URL).
This changes the database and I couldn't get the data back to its original state.