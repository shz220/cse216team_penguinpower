"use strict";
var describe;
var it;
var expect;
var $;
describe("The New Entry Form should", function () {
    it("be hidden", function () {
        $("#Navbar-add").click();
        //expect($("#NewEntryForm").attr("style").indexOf("display: none;")).toEqual(-1);
        expect($("#NewEntryForm").attr("id")).toEqual("NewEntryForm");
        //close the modal
        $("#NewEntryForm-Close").click();
        // expect($("#NewEntryForm").attr("style").indexOf("display: none;")).toEqual(0);
    });
});
