(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['ElementList.hb'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "        <tbody class=\"message-show\">\r\n \r\n            <tr class=\"messageMain\">\r\n                \r\n                <td class=\"ElementList-messageTitle\" id=\"ElementList-messageTitle"
    + alias4(((helper = (helper = helpers.messageId || (depth0 != null ? depth0.messageId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageId","hash":{},"data":data}) : helper)))
    + "\" data-value=\""
    + alias4(((helper = (helper = helpers.messageId || (depth0 != null ? depth0.messageId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageId","hash":{},"data":data}) : helper)))
    + "\" data-toggle=\"collapse\" data-target=\"#messageMoreInfo-"
    + alias4(((helper = (helper = helpers.messageId || (depth0 != null ? depth0.messageId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageId","hash":{},"data":data}) : helper)))
    + "\" aria-expanded=\"false\" aria-controls=\"messageMoreInfo-"
    + alias4(((helper = (helper = helpers.messageId || (depth0 != null ? depth0.messageId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageId","hash":{},"data":data}) : helper)))
    + "\">Title: "
    + alias4(alias5((depth0 != null ? depth0.title : depth0), depth0))
    + "</td>\r\n\r\n                <td class=\"ElementList-popularity\">\r\n                    <div id=\"ElementList-popularity"
    + alias4(((helper = (helper = helpers.messageId || (depth0 != null ? depth0.messageId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageId","hash":{},"data":data}) : helper)))
    + "\" data-value=\""
    + alias4(((helper = (helper = helpers.popularity || (depth0 != null ? depth0.popularity : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"popularity","hash":{},"data":data}) : helper)))
    + "\">Popularity: "
    + alias4(((helper = (helper = helpers.popularity || (depth0 != null ? depth0.popularity : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"popularity","hash":{},"data":data}) : helper)))
    + "</div>        \r\n                </td>\r\n\r\n                <td id=\"ElementList-votebtns\">\r\n                    <button class=\"ElementList-upVotebtn\" data-value=\""
    + alias4(alias5((depth0 != null ? depth0.messageId : depth0), depth0))
    + "\">\r\n                        <span class=\"glyphicon glyphicon-arrow-up\"></span>\r\n                    </button>\r\n                    <button class=\"ElementList-downVotebtn\" data-value=\""
    + alias4(alias5((depth0 != null ? depth0.messageId : depth0), depth0))
    + "\">\r\n                        <span class=\"glyphicon glyphicon-arrow-down\"></span>\r\n                    </button>\r\n                </td>\r\n            </tr>\r\n        </tbody>\r\n        <tbody id=\"messageMoreInfo-"
    + alias4(((helper = (helper = helpers.messageId || (depth0 != null ? depth0.messageId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageId","hash":{},"data":data}) : helper)))
    + "\" class=\"collapse\">\r\n          \r\n            <tr id=\"firstRow"
    + alias4(((helper = (helper = helpers.messageId || (depth0 != null ? depth0.messageId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageId","hash":{},"data":data}) : helper)))
    + "\"> </tr>\r\n            <tr id=\"secondRow"
    + alias4(((helper = (helper = helpers.messageId || (depth0 != null ? depth0.messageId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageId","hash":{},"data":data}) : helper)))
    + "\"> </tr>\r\n            <tr id=\"readDocument"
    + alias4(((helper = (helper = helpers.messageId || (depth0 != null ? depth0.messageId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageId","hash":{},"data":data}) : helper)))
    + "\">  </tr>\r\n            <tr id=\"displayDocument"
    + alias4(((helper = (helper = helpers.messageId || (depth0 != null ? depth0.messageId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageId","hash":{},"data":data}) : helper)))
    + "\"> </tr>\r\n            <tr id =\"commentsRow"
    + alias4(((helper = (helper = helpers.messageId || (depth0 != null ? depth0.messageId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"messageId","hash":{},"data":data}) : helper)))
    + "\"></tr>      \r\n        </tbody>\r\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<div class=\"panel panel-default\" id=\"ElementList\">\r\n    <div class=\"panel-heading\">\r\n        <h3 class=\"panel-title\">All Messages & Comments</h3>\r\n    </div>\r\n    <table class=\"table\">\r\n        <col width=\"1200px\" />\r\n        <col width=\"250px\" />\r\n        <col width=\"250px\" />\r\n"
    + ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.mData : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </table>\r\n</div>\r\n";
},"useData":true});
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['EditEntryForm.hb'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div id=\"EditEntryForm\" class=\"modal fade\" role=\"dialog\">\r\n    <div class=\"modal-dialog\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h4 class=\"modal-title\">Edit an Entry</h4>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <label for=\"EditEntryForm-title\">Title</label>\r\n                <input class=\"form-control\" type=\"text\" id=\"EditEntryForm-title\" value= \"\" />\r\n                <label for=\"EditEntryForm-message\">Message</label>\r\n                <textarea class=\"form-control\" id=\"EditEntryForm-message\"></textarea>\r\n                <label for=\"EditEntryForm-link\">Link to a file:</label>\r\n                <input class=\"form-control\" type=\"text\" id=\"EditEntryForm-link\" />\r\n                <label for=\"EditEntryForm-upload\">Or upload file: </label>\r\n                <input ng-model=\"csv\" type=\"file\" id=\"EditEntryForm-upload\" multiple size=\"50\" accept=\".pdf,.jpeg\">\r\n\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-default\" data-value=\"\" id=\"EditEntryForm-OK\">OK</button>\r\n                <button type=\"button\" class=\"btn btn-default\" id=\"EditEntryForm-Cancel\">Cancel</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>";
},"useData":true});
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['NewEntryForm.hb'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div id=\"NewEntryForm\" class=\"modal fade\" role=\"dialog\">\r\n    <div class=\"modal-dialog\">\r\n        <div class=\"modal-content\">\r\n            <div class=\"modal-header\">\r\n                <h4 class=\"modal-title\">Add a New Entry</h4>\r\n            </div>\r\n            <div class=\"modal-body\">\r\n                <label for=\"NewEntryForm-title\">Title</label>\r\n                <input class=\"form-control\" type=\"text\" id=\"NewEntryForm-title\" />\r\n                <label for=\"NewEntryForm-message\">Message</label>\r\n                <textarea class=\"form-control\" id=\"NewEntryForm-message\"></textarea>\r\n                <label for=\"NewEntryForm-link\">Link to a file:</label>\r\n                <input class=\"form-control\" type=\"text\" id=\"NewEntryForm-link\" />\r\n                <label for=\"NewEntryForm-upload\">Or upload file: </label>\r\n                <input ng-model=\"csv\" type=\"file\" id=\"NewEntryForm-upload\" multiple size=\"50\" accept=\".pdf,.jpeg\">\r\n                \r\n\r\n            </div>\r\n            <div class=\"modal-footer\">\r\n                <button type=\"button\" class=\"btn btn-default\" id=\"NewEntryForm-OK\">OK</button>\r\n                <button type=\"button\" class=\"btn btn-default\" id=\"NewEntryForm-Close\">Close</button>\r\n            </div>\r\n\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n";
},"useData":true});
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Navbar.hb'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<nav class=\"navbar navbar-default\">\r\n    <div class=\"container-fluid\">\r\n        <!-- Brand and toggle get grouped for better mobile display -->\r\n        <div class=\"navbar-header\">\r\n            <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" \r\n                data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"false\">\r\n              <span class=\"sr-only\">Toggle navigation</span>\r\n              <span class=\"icon-bar\"></span>\r\n              <span class=\"icon-bar\"></span>\r\n              <span class=\"icon-bar\"></span>\r\n            </button>\r\n            <!-- Clicking the brand refreshes the page -->\r\n            <a class=\"navbar-brand\" href=\"/\">Penguin Power</a>\r\n        </div>\r\n\r\n        <!-- Collect the nav links, forms, and other content for toggling -->\r\n        <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-1\">\r\n            <ul class=\"nav navbar-nav\">\r\n                \r\n                <li>\r\n                    <a class=\"btn btn-link\" id=\"Navbar-messages\">\r\n                        All Messages\r\n                    </a>\r\n                </li>\r\n                <li>\r\n                    <a class=\"btn btn-link\" id=\"Navbar-add\">\r\n                        Add Entry\r\n                        <span class=\"glyphicon glyphicon-plus\"></span><span class=\"sr-only\">Show Trending Posts</span>\r\n                    </a>\r\n                </li>\r\n                <li>\r\n                    <a class=\"btn btn-link\" id=\"Navbar-profile\">\r\n                        Your Profile\r\n                    </a>\r\n                </li>\r\n                <li>\r\n                    <a class=\"btn btn-link\" id=\"Navbar-your-messages\">\r\n                        Your Messages & Comments\r\n                    </a>\r\n                </li>\r\n                <li>\r\n                    <a class=\"btn btn-link\" id=\"Navbar-logout\">\r\n                        Logout\r\n                    </a>\r\n                </li>\r\n            </ul>\r\n        </div>\r\n    </div>\r\n</nav>";
},"useData":true});
})();
(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['Login.hb'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"g-gsignin2\" id=\"googleSignIn\" data-theme=\"dark\"></div>";
},"useData":true});
})();
