"use strict";
// a global for the EditEntryForm of the program.  See newEntryForm for 
// explanation
var editEntryForm;
/**
 * EditEntryForm encapsulates all of the code for the form for editing an entry
 */
var EditEntryForm = /** @class */ (function () {
    function EditEntryForm() {
    }
    EditEntryForm.init = function () {
        if (!EditEntryForm.isInit) {
            $("body").append(Handlebars.templates[EditEntryForm.NAME + ".hb"]());
            $("#" + EditEntryForm.NAME + "-OK").click(EditEntryForm.submitForm);
            $("#" + EditEntryForm.NAME + "-Cancel").click(EditEntryForm.hide);
            EditEntryForm.isInit = true;
        }
    };
    EditEntryForm.refresh = function () {
        EditEntryForm.init();
    };
    /**
     * get() is called from the AJAX GET in clickEdit in ElementList.ts, and should populate the form if and
     * only if the GET did not have an error
     */
    EditEntryForm.get = function (data) {
        if (data.mStatus === "ok") {
            $("#" + EditEntryForm.NAME + "-title").val(data.mData.title);
            $("#" + EditEntryForm.NAME + "-message").val(data.mData.content);
            // TODO: add back after backend works
            //$("#" + EditEntryForm.NAME + "-link").val(data.mData.link);
            //$("#" + EditEntryForm.NAME + "-upload").val(data.mData.document);
            $("#" + EditEntryForm.NAME + "-OK").data("value", data.mData.messageId);
            // show the edit form
            $("#" + EditEntryForm.NAME).modal("show");
        }
        else if (data.mStatus === "error") {
            console.log("get in EditEntryForm");
            window.alert("Error: " + data.mMessage);
        }
        else {
            window.alert("An unspecified error occurred");
        }
    };
    /**
     * Hide the EditEntryForm, clearing its fields first
     */
    EditEntryForm.hide = function () {
        $("#" + EditEntryForm.NAME + "-title").val("");
        $("#" + EditEntryForm.NAME + "-message").val("");
        $("#" + EditEntryForm.NAME).modal("hide");
    };
    /**
     * Check if the input fields are both valid, and if so, do an AJAX call.
     */
    EditEntryForm.submitForm = function () {
        // get the values of the two fields, force them to be strings, and check 
        // that neither is empty
        var title = "" + $("#" + EditEntryForm.NAME + "-title").val();
        var msg = "" + $("#" + EditEntryForm.NAME + "-message").val();
        // TODO: add back after backend works, send base64 to backend as String document
        // send link to backend as String link
        /*let link = "" + $("#" + EditEntryForm.NAME + "-link").val();

        // get base64 String of uploaded file
        var base64;
        var input : any = document.getElementById("EditEntryForm-upload");
        var selectedFile = input.files;
        //Check File is not Empty
        if (selectedFile.length > 0) {
            // Select the very first file from list
            var fileToLoad = selectedFile[0];
            // FileReader function for read the file.
            var fileReader = new FileReader();
            // Onload of file read the file content
            fileReader.onload = function(fileLoadedEvent: any) {
                base64 = fileLoadedEvent.target.result;
                // Print data in console
                console.log("The base 64 String is: "+base64);
                
            };
            // Convert data to base64
            fileReader.readAsDataURL(fileToLoad);
        }*/
        var username = Login.username;
        // NB: we assume that the user didn't modify the value of #editId
        var id = "" + $("#" + EditEntryForm.NAME + "-OK").data("value");
        if (title === "" || msg === "") {
            window.alert("Error: title or message is not valid");
            return;
        }
        EditEntryForm.hide();
        // set up an AJAX post.  When the server replies, the result will go to
        // onSubmitResponse
        console.log("in submit form in editentryForm");
        $.ajax({
            type: "PUT",
            url: "/messages/" + id,
            dataType: "json",
            data: JSON.stringify({
                sessionKey: Login.sessionKey,
                username: Login.username,
                title: title,
                content: msg,
                messageId: id
            }),
            success: EditEntryForm.onSubmitResponse
        });
    };
    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a
     * result.
     *
     * @param data The object returned by the server
     */
    EditEntryForm.onSubmitResponse = function (data) {
        // If we get an "ok" message, clear the form and refresh the main 
        // listing of messages
        if (data.mStatus === "ok") {
            console.log("mstatus is ok onSumbitReponse in editentryForm");
            ElementList.refresh();
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            window.alert("Edit response:");
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
    };
    /**
     * The name of the DOM entry associated with EditEntryForm
     */
    EditEntryForm.NAME = "EditEntryForm";
    /**
     * Track if the Singleton has been initialized
     */
    EditEntryForm.isInit = false;
    return EditEntryForm;
}()); // end class EditEntryForm
/**
 * NewEntryForm encapsulates all of the code for the form for adding an entry
 */
var NewEntryForm = /** @class */ (function () {
    function NewEntryForm() {
    }
    /**
     * Initialize the NewEntryForm by creating its element in the DOM and
     * configuring its buttons.  This needs to be called from any public static
     * method, to ensure that the Singleton is initialized before use
     */
    NewEntryForm.init = function () {
        if (!NewEntryForm.isInit) {
            $("body").append(Handlebars.templates[NewEntryForm.NAME + ".hb"]());
            $("#" + NewEntryForm.NAME + "-OK").click(NewEntryForm.submitForm);
            $("#" + NewEntryForm.NAME + "-Close").click(NewEntryForm.hide);
            NewEntryForm.isInit = true;
        }
    };
    /**
     * Refresh() doesn't really have much meaning, but just like in sNavbar, we
     * have a refresh() method so that we don't have front-end code calling
     * init().
     */
    NewEntryForm.refresh = function () {
        NewEntryForm.init();
    };
    /**
     * Hide the NewEntryForm.  Be sure to clear its fields first
     */
    NewEntryForm.hide = function () {
        $("#" + NewEntryForm.NAME + "-title").val("");
        $("#" + NewEntryForm.NAME + "-message").val("");
        $("#" + NewEntryForm.NAME).modal("hide");
    };
    /**
     * Show the NewEntryForm.  Be sure to clear its fields, because there are
     * ways of making a Bootstrap modal disapper without clicking Close, and
     * we haven't set up the hooks to clear the fields on the events associated
     * with those ways of making the modal disappear.
     */
    NewEntryForm.show = function () {
        if (!Login.isLogIn) {
            window.alert("You are not signed in.");
            return;
        }
        $("#" + NewEntryForm.NAME + "-title").val("");
        $("#" + NewEntryForm.NAME + "-message").val("");
        $("#" + NewEntryForm.NAME).modal("show");
    };
    /**
     * Send data to submit the form only if the fields are both valid.
     * Immediately hide the form when we send data, so that the user knows that
     * their click was received.
     */
    NewEntryForm.submitForm = function () {
        // get the values of the two fields, force them to be strings, and check 
        // that neither is empty
        var title = "" + $("#" + NewEntryForm.NAME + "-title").val();
        var msg = "" + $("#" + NewEntryForm.NAME + "-message").val();
        // TODO: add back after backend works, send base64 to backend as String document
        // send link to backend as String link
        var link = "" + $("#" + NewEntryForm.NAME + "-link").val();
        console.log("link is: " + link);
        // get base64 String of uploaded file
        var base64;
        var input = document.getElementById("NewEntryForm-upload");
        var selectedFile = input.files;
        //Check File is not Empty
        if (selectedFile.length > 0) {
            // Select the very first file from list
            var fileToLoad = selectedFile[0];
            // FileReader function for read the file.
            var fileReader = new FileReader();
            // Onload of file read the file content
            fileReader.onload = function (fileLoadedEvent) {
                base64 = fileLoadedEvent.target.result;
                // Print data in console
                console.log("The base 64 String is: " + base64);
            };
            // Convert data to base64
            fileReader.readAsDataURL(fileToLoad);
        }
        if (title === "" || msg === "") {
            window.alert("Error: title or message is not valid");
            return;
        }
        NewEntryForm.hide();
        // set up an AJAX post.  When the server replies, the result will go to
        // onSubmitResponse
        $.ajax({
            type: "POST",
            url: "/messages",
            dataType: "json",
            data: JSON.stringify({ username: Login.username, sessionKey: Login.sessionKey, title: title, content: msg, document: base64, link: link }),
            success: NewEntryForm.onSubmitResponse
        });
    };
    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a
     * result.
     *
     * @param data The object returned by the server
     */
    NewEntryForm.onSubmitResponse = function (data) {
        // If we get an "ok" message, clear the form and refresh the main 
        // listing of messages
        if (data.mStatus === "ok") {
            ElementList.refresh();
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            console.log("NewEntryForm");
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
    };
    /**
     * The name of the DOM entry associated with NewEntryForm
     */
    NewEntryForm.NAME = "NewEntryForm";
    /**
     * Track if the Singleton has been initialized
     */
    NewEntryForm.isInit = false;
    return NewEntryForm;
}());
/**
 * The ElementList Singleton provides a way of displaying all of the data
 * stored on the server as an HTML table.
 */
var ElementList = /** @class */ (function () {
    function ElementList() {
    }
    /**
     * Initialize the ElementList singleton by creating its element in the DOM.
     * This needs to be called from any public static method, to ensure that the
     * Singleton is initialized before use.
     */
    ElementList.init = function () {
        if (!ElementList.isInit) {
            ElementList.isInit = true;
        }
    };
    /**
     * refresh() is the public method for updating the ElementList
     */
    ElementList.refresh = function () {
        $('#' + ElementList.NAME).remove();
        // Make sure the singleton is initialized
        if (!Login.isLogIn) {
            return;
        }
        ElementList.init();
        console.log("elementList-refresh");
        // Issue a GET, and then pass the result to update()
        $.ajax({
            type: "GET",
            url: "/messages",
            dataType: "json",
            headers: {
                'sessionKey': Login.sessionKey,
                'username': Login.username
            },
            success: ElementList.update
        });
    };
    /**
     * update() is the private method used by refresh() to update the
     * ElementList
     */
    ElementList.update = function (data) {
        // Remove the table of data, if it exists
        $('#' + ElementList.NAME).remove();
        $("#UserMessagesList").remove();
        // Use a template to re-generate a table from the provided data, and put 
        // the table into our messageList element.
        $("body").append(Handlebars.templates[ElementList.NAME + ".hb"](data));
        $("." + ElementList.NAME + "-upVotebtn").click(ElementList.clickUpVote);
        $("." + ElementList.NAME + "-downVotebtn").click(ElementList.clickDownVote);
        $("." + ElementList.NAME + "-messageTitle").click(ElementList.clickMessageTitle);
    };
    ElementList.clickMessageTitle = function () {
        // we need the messageId of the message
        var id = $(this).data("value");
        //if we dont have the message yet
        if (!($("#content" + id).length)) {
            $.ajax({
                type: "GET",
                url: "/messages/" + id,
                dataType: "json",
                headers: {
                    'sessionKey': Login.sessionKey,
                    'username': Login.username
                },
                success: ElementList.showMessage
            });
        }
    };
    // due to backend not finished, I used the base64 I get to be used in ElementList.ts
    //display document after click "display document" button
    ElementList.readDocument = function (data) {
        console.log("readdocument: " + JSON.stringify(data));
        $("#displayDocument" + data.mData.messageId).prepend('<td></td>');
        $("#displayDocument" + data.mData.messageId).prepend('<td></td>');
        // for testing (no backend send back binary file)
        // image display works
        //$("#displayDocument" + data.mData.messageId).prepend('<td> <img id = "Display-image" src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAGcAAABZCAYAAAAjMTokAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABCuSURBVHhe7d0FkFzFFgbg4O7uwd3d3Z3CHu7u7hQOBVRB4VK4FB6ch0sgQIgCgbxAIIGgAUIgQIDQ736906nNvtnNzuzM7J28OVWnNnNn5t7u8/fRPj3pMmrUqNDgfHIDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBzzBAfO77//HkaPHh3+/PPPyH///Xf4559/2mSfSZ/H7lHs3rXmugfnt99+C3/88UcU8JgxY6Kwf/jhh/Dhhx+GF154Idxwww3htNNOC/vvv3/Yb7/94t/m/z755JPD1VdfHZ588snQv3//8O2330Zw3cs9/dszij272ly34Fjdf/31VwSD8F599dVw/vnnh3XXXTfMM888YaKJJgqTTDJJmHHGGcNcc80VFlxwwbDQQgv9D88777xhlllmCZNPPnno0qVLmHXWWcMqq6wSjj766PDMM89EoJN2WQTFxlItrjtwkpZY0T169AhHHnlkFPDEE08cpptuurDmmmuGww47LNx4441RuH369AlDhw4NI0eOLHq/r7/+OmrZiy++GO6+++6oZZtuumkEFMCA22GHHcLDDz8cfvrppwhUrbSpbsBJmgKY5557LmywwQZxpdOMXXbZJdx2223ho48+iu9XggDarVu3cMQRR0QN86zFFlssXHrppeHHH3+MIFVbk+oCHEIgjH79+o0FhcAuvPDCCEi1iWl74IEHwmabbRa1iVZdddVV4ddff42LoVoBRO7BET2ha665Jkw99dRhzjnnDJdffnn46quv4vVakgXy/PPPh6222iouEL7p7bffjterAVBuwWHTRUzs/K677hqFYeV+/PHHBVF1Lt1///1h0UUXDVNMMUW49dZb4zULqdhcyuVcgpOA+fLLL8Pyyy8foy4mjCPOExnf5ptvHhfOqaeeGq9VEqDcgZOAGTRoUOjatWuMlp566qk48TwSc3b44YdHgORNqFIA5Q4cDpYD5vD5Fza9Huicc86pOEC5AseErESOduaZZw7vvPNOnGi90FlnnRUBOvPMM+PrjgYJuQHHRNAhhxwSE8o8m7K2SFkIQI8++miM4orNtb2cG3D4GfUtE7vkkksKU60/GjFiRFh11VWj5g8bNiwmzsXm2x7OBTipHDL//POHjTbaKJq3eqa33norTDXVVLFygco1b50ODlAQOy1n6N27d3xd76RGxwq8+eabZWtPp4Nj4N99912Ydtppw1FHHVWYWv2Tgup8880XNtlkk/i6HO3pdHA4zXPPPTdWlOU2ExKdd955UXvee++9mCIUm39b3Kng8DWKhwqJBxxwQGFKEw5ZbDPNNFM46KCD4iJkwovJoTXuVHBEaPfcc0+s9Nosaw+ptbHnu+++e6wUl0uet9dee4XjjjsufPPNN4WrlSdBwdxzzx2BKXWLodPASXWyFVZYIW6Qtbdudvzxx0cwp5xyylhBeP/99wvvtJ+++OKLsPTSS8fdT/fad999C+9Unm6//fZo2l5//fWSA4NOA4fWWPkGbqOsvaTQCBimcJpppol9AqWSiNAm3RxzzBEDkdVXX73wTuXJfhN/etFFF8U5F5NFa9wp4Mhj8BJLLBGrzvxOpGzwYeTIkIVvwp1x+fvvhTzh8W7dwvxdu8a9HdvHkr5SiXmxvQ3c2TPtsz2dLWsZZAjDh4/LmRnNBm0zp/Dt0sizVl555bDxxhvH16VEbTUHJ5Vp2Hpa89BDD8XXEZQBA0Lo0SOE7t1DZgfGZdfU2oYODR9l0c9z//53+OWXX5q+WwZZxfoGenuejbtevWSPIUtMxmXXPHfgwCaQyiD+0RY3f1lKUFBzcISU/ARgOORIVm2fPiG89BJPHcJrrxVn7/nM4MFN36sEZWCHV15p4tae7frLL4fQv79yc+GL7aezzz47lnPs/5RSra4pOFaNkHK33XYLs88+e/j000+bRg8cvQCEkIRUjL1HWP/5T5MJ7ChpBrGz2hycYuw94Ag+jLVEuvPOO2PgoS+ulKCgpuBYNd9nvkPdSdQ1DonWhg0L4YMPQujZs8mcNOfMlEWzxwQ1W70SvHfffTfee3zEt2lx0jaVzGvmFJq0p1+/EOwdJVOWmNnr2zeEzz4L2epq+k6J9Oyzz8bdXP0HudUcdj6toqeffrow9BZEIwiBD2rOrrVwymy4BsJJJ500rLPOOrF7k3N/JVvpr2Uahl/KzKBrJ5xwQgw+mFNhtM7OcYhG8GE//zzuc10rQ1ua08uZ1qkb3nffffkFh0kTJc0222zR/naUmAgBhT0UHZ1AInyhNu3E/m1/SOisrUp7bjWTzmJkgcip5Dy5BYcwhb/yivaYoVJIodGW9i233BKbQeQV/l555ZXh8ccfz9xU5qc6iWgOcFiN3IJjYHq+xPxM3P8L6RxlyvmeXGvOtttuG9Zff/0YUteMxmRJ7p8dN6Plku7Q3EdrtEV3yiKLLJL52szZ1ooG/yuL8s4tvKg97bPPPtHP2rZWQywmm2Jc84DAMQ1lkw+EzLWgb6/KwuEuIQw5snChQKOHZBqVhdFVJhGl6sDaa68drUVuKwRU2v66iIpzrAn9nuVGo7I8ZXSWy4QsFP/p0Szx3DqEQTvUBBxdOOarv7tTC5/pNIBBFFshEj/XF1544bD99tsXhl9jGpGB03ueEL5vfyW8XCKHbbbZJloK2xSlBAO4YuAQPM1wfkVBsbVVAjytT3KPN954ozCN6pAc6IILLohHR5xK+EyWj8aMyJKkH7Ik85Us6Xw++/ti0/UKk6ST1hx66KFx3qWYNFxRcACit3nPPfeMgyn2OQ6R7V1ggQXCaqutFg8iVYMsEHs1ElM2f/HFF4/VhNNPPz2MUWj4JzNpg7YLoXvmj765vOlLFaQhQ4ZEC6GGqGRVqtbgioLDrMnU9QO0Bo7VA0Sxv1Ulu68GHXzwwbHfWqSkrqXF1+tll1228ImMRn+RRXEXFF5UjoYPHx6Pq5ifbfhytAZXFBxH9awUe/xtDchnkSN9JnDiiSdGk1gpAr5amwZFdTZlGwmgLfGllloq3HTTTXE1V4PU7LbYYos4r3QsxHyLyWF8PF5wmCGTJezEBElNmwvfNeGxWtbFF18cB+Wz3vPZlmrtNaJlJrLzzjuHzz//PF4rl1SdlWuAMtlkk4Utt9wyXmdG+R7PsXj8VZmuNKmOr7TSSvH+xx57bLxWjjlL3CY4SYDO6av40giCV3a3QoCWAAJOr169oo3XG+A9jt9gaQZquYJ8x+fcU4DAJBJaOQmq+2hBIhjVZ2CP3WXNSNGRwGgr4O64447COx0nWih/0walhmaBoI4Ag9sEx8q/7LLL4oS19zijr/TttQaLTz75JK5KnyVo7UYmrlyRBKWJoi27m8Jvx9bTqvO3VNMjIhMAqHqrOhc7yKs4aj+f7xm70dcBco7o5ptvjn7MuDWwa781H/NqOddSuVVw3BzyohwT4k9w37594+pWgrc6EaED54knnoidJrZkaZDfA+AcDbatsoXvJ5Cvv/76WN4xWUL0Yw2OthNsW0RbbRHouZZDzTDDDPG8THPae++9430lhOWSeTg35DkaVNzPCbzrrrsuzgGX62NacqvgAGbgwIHRhyQHDzAPR4ARojJBCRwaQq3XWGONCFQCpZjGFOOkRe5lg2zDDTeMAk8C2GOPPSJ4NHTw4MHjhOEDBgyIB2iZFhrOtzBvzU2kfRXhrZAacEr5aT5tkUXpvJAwXBkmWY8VV1wxjkcHELPa1gIsh1sFx6D1hAlD7VoSWHqPANlu4FDtBM61114bzZqqM9Pic+WsIpNMQrMPwz/oO3BEhG8iHP6JhonGEgnPNRoyuVqnCC9FhomYO5VxwvU5xci2iK/1XPMiC9GeLXZNgggolTBhxbhNcCRyzJOBJOeWJrvjjjtGW2tgySxx7MwJ4VmhnLDPe68ckHzHcxNQTiPQELuZtAQ4Y7P+jDQL8jvrrbde1DwBilX9s63nFtS9e/fo5wi3LXLyASh8mfsZV9LuaoGSuFVwrF5byepC+q6QiRgYm8vcSfAQIbrOx/A3wlamzeqUY6SznR2ZjAXgOYSCtt566/gjRM1J/UrVgd/baaedorYtueSSMcAQ5pZDtJHWmEPSkvaa6Y5yq+AYAIGnY9x6zM4444xozpw7mX766eMqTuYOyfYJJhX5TjnllDgx3zdJk2v5nHLYuLbbbruw1lprjdUqpXlgcdRMEUCwHxkSMfJZfE6pJNDgR2lNpX3K+LhVcDABI5rDXDEZQCGUZHPTKiKwK664IoaTOjHTCmcaZcwcaiXB4TfSHgmiGRaBnAwZO01D/BKAmFtOvZRzQEr+wNHJkytwCD4BJOdQzFPdJfiWfoS6I8AkEHzfvxNXykYncJjOBI78QmQn7yhG8iCnzAAIKNUDxzMUaW0ft0ZAEVzcdddd8VnFxlMtbhOcxEAg2MTNQSn2uWrbZOAwswICrxFtoNlt5TDGJ/Kzx8LMCalFfTYAWyPRIqvBXHpuy7FUk9sFTt6YkE466aSYy8h3kIhMaD+2/3o8JNQXRvNVbZHnLbPMMrFryHOrvfCac12Cw7zce++9MedRPUh04IEHxjLT+HKXUknya5/KAqil36lLcPhByaT8Q/UikV5k19JPbFWK+DG+SsG3ln6nLsHhO7CcRomGn0OCEfUutTUmqFJkK0MQIW9CtTJtdQsO4at+W9HNtwYUNzl5BddKkrxKAKKe5/nFxlVprjtwEjByF2Ua4Kh3SXLVAmmS182PIzKD8iylGLuT7lMq2a+R49Ei92s5rmpw3YEjx/JzxMJmWwo6W0RpaoCSRVVp1fFEMnuFWCAKIOySKtaWSgIQ1Q6V+gY4RZitR7YSFD5TGO2v/MUGnf2mRK6rrQHNGVRVBJpXDgky1BlLPTrYEa4rcJLj11ZFY8ZHquQ0xlZGW6T3QJnG51S8E7tmL8dzJa58Dq1jVgFU7cCgLsAhBMIgFMISpemkGV+vgaq5/Z3xBQf8EBCbH7rCzJhfe6ep/I3CrnJO+mEK4wFcsTFXgusCHMAgZRYbf6lSLjJrK+F0UMtqT+awNXKulE+xx6NGh/1bx6b+N1vyggyv/WCf6FAAkn7buloaVBfgSPyA4LecRV2PPfZYNDMcvGBAd49KgY23sT84kZFfo6INkkiF13LI71jroVAqsnno/0dwUo7Jw6ha2lM34Pj74IMPRoEQkBWsvmYrWtXYdgATpLNT9dk2haq1Vc+R+1kWRxJtmjGH7gmw9JeALQD92z6n0Klr07kahU+LwaLwXOAYS/ppl/9rcFKEpVhJcwiGoJRrmBd7OMccc0ysDPiRIxGaaM6paedPhdL6BZR2gKWKoCPUdbmSv8stt1zsFeBb+BqACDyA6nfTNKxgJRzM96BqlnPqAhxsdXLASDjLLwAmrWZmjUYRoOvYCvean/LjRzpuFDGBwVQltmNqF1UVQL+dw77+7wT5knv7vvu5v1DdeJCcq5oRW92Ag1PUlkwRYqJEcD179oxmRk80QID2yCOPjGXXCDeZxKQBiROIQLZ7m37LQIGVxqpGIwsEKNWM0hLXFTjNGVAERlCASloFOAASHuB07PAlzVmUpUkxMeGrmbmv+yUgkHvjdM+W46gm1y04LRlYfBMmSEIul4GQ7lXsWbXiCQacCZEb4OSYG+DkmBvg5Jgb4OSYG+DkmBvg5Jgb4OSWR4X/Asu1zr9odBI4AAAAAElFTkSuQmCC" height = "150"> </td>');
        // pdf display works
        //$("#displayDocument" + data.mData.messageId).prepend('<td> <embed id = "Display-document" src="https://coursesite.lehigh.edu/pluginfile.php/2162525/mod_resource/content/1/syllabus340-2018.pdf" width = "500" height = "375" type ="application/pdf"> </td>');
        // after backend works, add these codes, everything should work well
        if (data.mData.type == "data:image/jpeg") {
            $("#displayDocument" + data.mData.messageId).prepend('<td> <img id = "Display-image" src="data:image/jpeg;base64,"' + data.mData.document + 'height = "150"> </td>');
        }
        else if (data.mData.type == "data:application/pdf") {
            $("#displayDocument" + data.mData.messageId).prepend('<td> <embed id = "Display-document" src="data:application/pdf;base64,"' + data.mData.document + 'width = "500" height = "375" type ="application/pdf"> </td>');
        }
    };
    ElementList.showMessage = function (data) {
        var count = 0;
        console.log(JSON.stringify(data));
        //remove old message
        $(".additionalClass").remove();
        //if the author of the message matches the user, render delete and edit button.
        if (data.mData.userId == Login.userId) {
            $("#firstRow" + data.mData.messageId).prepend('<td><button class="additionalClass" id = "ElementList-editbtn' + data.mData.messageId + '" data-value="' + data.mData.messageId + '">Edit</button></td>');
            $("#secondRow" + data.mData.messageId).prepend('<td><button class="additionalClass" id="ElementList-delbtn' + data.mData.messageId + '" data-value="' + data.mData.messageId + '">Delete</button></td> ');
            $('#ElementList-delbtn' + data.mData.messageId).click(ElementList.clickDelete);
            $('#ElementList-editbtn' + data.mData.messageId).click(ElementList.clickEdit);
        }
        //prepend messages
        $("#firstRow" + data.mData.messageId).prepend('<td class="additionalClass" id="vote' + data.mData.messageId + '">' + "Total upvote:" + data.mData.upVoteCount + "</td>");
        $("#secondRow" + data.mData.messageId).prepend('<td class="additionalClass" id="vote' + data.mData.messageId + '">' + "Total downvote:" + data.mData.downVoteCount + "</td>");
        $("#secondRow" + data.mData.messageId).prepend('<td class="additionalClass" id="content' + data.mData.messageId + '">' + "  Created by:" + data.mData.username + " on " + data.mData.dateCreated + "</td>");
        $("#firstRow" + data.mData.messageId).prepend('<td class="additionalClass" id="content' + data.mData.messageId + '">' + "  Content:" + data.mData.content + "</td>");
        $("#readDocument" + data.mData.messageId).prepend('<td class="additionalClass"></td>');
        $("#readDocument" + data.mData.messageId).prepend('<td class="additionalClass"></td>');
        $("#readDocument" + data.mData.messageId).prepend('<td class="additionalClass"><button id=display' + data.mData.messageId + '> Display Document </button></td>');
        // after click the button, display document
        var button = document.getElementById("display" + data.mData.messageId);
        button.onclick = function () {
            if (count == 0) {
                ElementList.readDocument(data); //only click the button first time
                count = count + 1;
            }
            if (count == 1) {
                button.style.display = 'none'; //hide the display button after document displayed
            }
        };
        //show comments
        Comments.refresh(data.mData.messageId);
    };
    /**
     * clickDelete is the code we run in response to a click of a delete button
     */
    ElementList.clickDelete = function () {
        var id = $(this).data("value");
        $.ajax({
            type: "DELETE",
            url: "/messages/" + id,
            dataType: "json",
            headers: {
                'sessionKey': Login.sessionKey,
                'username': Login.username
            },
            /*data: JSON.stringify({
                sessionKey: Login.sessionKey,
                username:Login.username
            }),*/
            // TODO: we should really have a function that looks at the return
            //       value and possibly prints an error message.
            success: ElementList.checkError
        });
    };
    /**
     * clickEdit is the code we run in response to a click of a edit button
     */
    ElementList.clickEdit = function () {
        // as in clickedit, we need the ID of the row
        //wer are getting the message info, then update it in EditEntryForm.get
        var id = $(this).data("value");
        $.ajax({
            type: "Get",
            url: "/messages/" + id,
            dataType: "json",
            headers: {
                'sessionKey': Login.sessionKey,
                'username': Login.username
            },
            success: EditEntryForm.get
        });
    };
    /**
     * clickUpVote is the code we run in response to the click of an upvote (like) button
     */
    ElementList.clickUpVote = function () {
        var id = $(this).data("value");
        $.ajax({
            type: "PUT",
            url: "/vote/message",
            dataType: "json",
            data: JSON.stringify({ sessionKey: Login.sessionKey, username: Login.username, voteStatus: true, messageId: id }),
            success: ElementList.checkError
        });
    };
    /**
    * clickDownVote is the code we run in response to the click of a downvote (dislike) button
    */
    ElementList.clickDownVote = function () {
        var id = $(this).data("value");
        $.ajax({
            type: "PUT",
            url: "/vote/message",
            dataType: "json",
            data: JSON.stringify({ sessionKey: Login.sessionKey, username: Login.username, voteStatus: false, messageId: id }),
            success: ElementList.checkError
        });
    };
    ElementList.checkError = function (data) {
        if (data.mStatus === "ok") {
            ElementList.refresh();
        }
        else if (data.mStatus === "error") {
            console.log("vote mStatus error");
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Get ElementList: Unspecified error");
        }
    };
    /**
     * The name of the DOM entry associated with ElementList
     */
    ElementList.NAME = "ElementList";
    /**
     * Track if the Singleton has been initialized
     */
    ElementList.isInit = false;
    return ElementList;
}());
/**
 * The Navbar Singleton is the navigation bar at the top of the page.  Through
 * its HTML, it is designed so that clicking the "brand" part will refresh the
 * page.  Apart from that, it has an "add" button, which forwards to
 * NewEntryForm
 */
var Navbar = /** @class */ (function () {
    function Navbar() {
    }
    /**
     * Initialize the Navbar Singleton by creating its element in the DOM and
     * configuring its button.  This needs to be called from any public static
     * method, to ensure that the Singleton is initialized before use.
     */
    Navbar.init = function () {
        if (!Navbar.isInit) {
            $("body").prepend(Handlebars.templates[Navbar.NAME + ".hb"]());
            console.log("navbar initialized");
            $('#Navbar-messages').click(ElementList.refresh);
            $("#" + Navbar.NAME + "-add").click(NewEntryForm.show);
            $("#" + Navbar.NAME + "-logout").click(Logout.show);
            $("#" + Navbar.NAME + "-profile").click(Profile.show);
            $('#Navbar-your-messages').click(UserMessagesList.refresh);
            Navbar.isInit = true;
        }
    };
    /**
     * Refresh() doesn't really have much meaning for the navbar, but we'd
     * rather not have anyone call init(), so we'll have this as a stub that
     * can be called during front-end initialization to ensure the navbar
     * is configured.
     */
    Navbar.refresh = function () {
        Navbar.init();
    };
    /**
     * Track if the Singleton has been initialized
     */
    Navbar.isInit = false;
    /**
     * The name of the DOM entry associated with Navbar
     */
    Navbar.NAME = "Navbar";
    return Navbar;
}());
/**
 * This class tests the UI, instead of Jasmine, as Jasmine asynchronous testing wasn't really asynchronous
 * Jasmine tests would run before proper html elements were loaded onto the page
 * So as to not go without any testing, this class was written
 * Errors will be stated in the form of alerts and statements written to the console
 *
 * Ran into the same problem here, where JQuery runs before the dom is effectively loaded,
 * thus these tests are not in a working state
 *
 * Been working on this for over 18 hours, still couln't get this to work.
 */
var UiTests = /** @class */ (function () {
    function UiTests() {
    }
    UiTests.runTests = function () {
        //UiTests.navButtonShowsAddEntry();
        //UiTests.addEntryShowsNewEntryForm();
        //UiTests.newEntryFormCloseBtnWorks();
        //UiTests.newEntryFormOKBtnWorks();
    };
    /**
     * Tests if the navBar Button shows the add element button when clicked
     */
    UiTests.navButtonShowsAddEntry = function () {
        $(".navbar-toggle").click();
        doThis(1);
        function doThis(num) {
            if (num < 2 && !$("#Navbar-add").is(":visible")) {
                //wait for the frame to load
                window.requestAnimationFrame(doThis);
                num++;
            }
            else if ($("#Navbar-add").is(":visible")) {
                console.log("Nav Button shows addEntry button");
                $(".navbar-toggle").click();
                return;
            }
            else {
                //sends and alert to the screen
                alert("Navbar button does not work");
                console.log("!! Navbar button does not show addEntryButton");
            }
        }
    };
    UiTests.addEntryShowsNewEntryForm = function () {
        $("#Navbar-add").click();
        var num = 1;
        $(document).ready(function () {
            if (($("#NewEntryForm").attr("class") == "modal fade in")) {
                console.log("Navbar-add button shows NewEntryForm");
                $("NewEntryForm-Close").click();
            }
            else {
                alert("Navbar-add button does not show NewEntryForm");
                console.log("!! Navbar-add button does not show NewEntryForm");
            }
        });
    };
    UiTests.newEntryFormCloseBtnWorks = function () {
        $("#Navbar-add").click();
        $("#NewEntryForm-Close").click();
        if ($("#NewEntryForm").is(":hidden")) {
            console.log("NewEntryForm Close button works");
        }
        else {
            alert("NewEntryForm Close Button does not close the NewEntryForm modal");
            console.log("!! NewEntryForm-Close button does not close the NewEntryForm modal");
        }
    };
    UiTests.newEntryFormOKBtnWorks = function () {
        $("#Navbar-add").click();
        $("#NewEntryForm-userId").val("1");
        $("#NewEntryForm-title").val("test");
        $("#NewEntryForm-message").val("testing");
        $("#NewEntryForm-OK").click();
        if ($("#NewEntryForm").is(":hidden")) {
            console.log("NewEntryForm-OK button works");
            //UiTests.testDeleteButton();
        }
        else {
            alert("NewEntryForm OK button does not close the NewEntryForm modal");
            console.log("!! NewEntryForm-OK button does not close the NewEntryForm modal,  edit or delete buttons functionality has not been tested");
        }
    };
    return UiTests;
}());
var $;
/**
 * Login encapsulates all of the code for the form for adding an entry
 */
var Login = /** @class */ (function () {
    function Login() {
    }
    /**
     * Initialize the Login by creating its element in the DOM and
     * configuring its buttons.  This needs to be called from any public static
     * method, to ensure that the Singleton is initialized before use
     */
    Login.init = function () {
        if (Login.isInit == true) {
            return;
        }
        Login.isInit = true;
        $("body").append(Handlebars.templates[Login.NAME + ".hb"]());
        gapi.signin2.render('googleSignIn', {
            width: 480,
            height: 100,
            theme: 'dark',
            onsuccess: signedIn,
            onfailure: function () {
                window.alert("Google sign in failed.");
                Login.isLogIn = false;
                ElementList.refresh();
            }
        });
        function signedIn(googleUser) {
            Login.GoogleUser = googleUser;
            Login.GoogleAuth = gapi.auth2.getAuthInstance();
            Login.isLogIn = true;
            var id_token = googleUser.getAuthResponse().id_token;
            console.log("id_token = " + id_token);
            Login.requestLogin(id_token);
        }
    };
    /**
     * Refresh() doesn't really have much meaning, but just like in sNavbar, we
     * have a refresh() method so that we don't have front-end code calling
     * init().
     */
    Login.refresh = function () {
        Login.init();
    };
    /**
     * Hide the Login.  Be sure to clear its fields first
     */
    Login.hide = function () {
        $("#googleSignIn").css("display", "none");
    };
    /**
     * Show the Login.  Be sure to clear its fields, because there are
     * ways of making a Bootstrap modal disapper without clicking Close, and
     * we haven't set up the hooks to clear the fields on the events associated
     * with those ways of making the modal disappear.
     */
    Login.show = function () {
        $("#googleSignIn").css("display", "block");
    };
    Login.getInit = function () {
        return Login.isInit;
    };
    /**
     * Send data to submit the form only if the fields are both valid.
     * Immediately hide the form when we send data, so that the user knows that
     * their click was received.
     */
    Login.requestLogin = function (token) {
        //onSuccess runs when the AJAX call in submitForm() returns a result.
        function onSuccess(data) {
            // If we get an "ok" message, find the userId.
            if (data.mStatus === "ok") {
                Login.sessionKey = data.mData.sessionKey;
                Login.username = data.mData.username;
                Login.requestUserInfo(Login.sessionKey, Login.username);
            }
            // Handle explicit errors with a detailed popup message
            else if (data.mStatus === "error") {
                window.alert("The server replied with an error:\n" + data.mMessage);
                Login.GoogleAuth.signOut();
                Login.show();
            }
            // Handle other errors with a less-detailed popup message
            else {
                window.alert("Login: Unspecified error");
                Login.GoogleAuth.signOut();
                Login.show();
            }
        }
        // set up an AJAX post.  When the server replies, the result will go to onSuccess
        $.ajax({
            type: "POST",
            url: "/users/login",
            dataType: "json",
            data: JSON.stringify({ accessCode: token }),
            success: onSuccess,
            error: function () {
                alert("Internet Error: Cannot connect to the database.");
                Login.GoogleAuth.signOut();
                Login.show();
            }
        });
    };
    /**
     *  Get the user information. Specifically, get userId and store it in the local variable Login.userId.
     */
    Login.requestUserInfo = function (sessionKey, username) {
        $.ajax({
            type: "POST",
            url: "/users/info",
            dataType: "json",
            data: JSON.stringify({ username: username, sessionKey: sessionKey, usernameAskedFor: username }),
            success: onSuccess,
            error: function () {
                alert("Internet Error: Cannot connect to the database.");
                Login.GoogleAuth.signOut();
                Login.isLogIn = false;
                ElementList.refresh();
                Login.show();
            }
        });
        function onSuccess(data) {
            // If we get an "ok" message, load all forms.
            if (data.mStatus === "ok") {
                Login.userId = data.mData.userId;
                console.log("user id = " + Login.userId);
                //get all the forms refreshed.
                ElementList.refresh();
                Logout.refresh();
                NewEntryForm.refresh();
                EditEntryForm.refresh();
                Profile.refresh();
                Login.hide();
            }
            // Handle explicit errors with a detailed popup message
            else if (data.mStatus === "error") {
                window.alert("The server replied with an error:\n" + data.mMessage);
                Login.GoogleAuth.signOut();
                Login.isLogIn = false;
                ElementList.refresh();
                Login.show();
            }
            // Handle other errors with a less-detailed popup message
            else {
                window.alert("Login: Unspecified error");
                Login.GoogleAuth.signOut();
                Login.isLogIn = false;
                ElementList.refresh();
                Login.show();
            }
        }
    };
    /**
     * The name of the DOM entry associated with Login
     */
    Login.NAME = "Login";
    /**
     * Track if the Singleton has been initialized
     */
    Login.isInit = false;
    Login.sessionKey = "";
    Login.username = "";
    Login.isLogIn = false;
    return Login;
}());
var $;
/**
 * The Commentss Singleton provides a way of displaying all of the data
 * stored on the server as an HTML table.
 */
var Comments = /** @class */ (function () {
    function Comments() {
    }
    /**
     * Initialize the Comments singleton by creating its element in the DOM.
     * This needs to be called from any public static method, to ensure that the
     * Singleton is initialized before use.
     */
    Comments.init = function () {
        if (!Comments.isInit) {
            Comments.isInit = true;
        }
    };
    /**
     * refresh() is the public method for updating the Comments
     */
    Comments.refresh = function (id) {
        // Make sure the singleton is initialized
        Comments.init();
        Comments.messageId = id;
        console.log("in refresh() in comments id=" + id);
        $.ajax({
            type: "GET",
            url: "/comments/" + id,
            dataType: "json",
            headers: {
                'username': Login.username,
                'sessionKey': Login.sessionKey
            },
            success: Comments.update
        });
    };
    //display document after click "display document" button
    Comments.readDocument = function (commentId) {
        $("#commentdisplayDocument" + commentId).prepend('<td></td>');
        $("#commentdisplayDocument" + commentId).prepend('<td></td>');
        // for testing (no backend send back binary file)
        // image display works
        $("#commentdisplayDocument" + commentId).prepend('<td> <img id = "Display-image" src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAGcAAABZCAYAAAAjMTokAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABCuSURBVHhe7d0FkFzFFgbg4O7uwd3d3Z3CHu7u7hQOBVRB4VK4FB6ch0sgQIgCgbxAIIGgAUIgQIDQ736906nNvtnNzuzM7J28OVWnNnNn5t7u8/fRPj3pMmrUqNDgfHIDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBzzBAfO77//HkaPHh3+/PPPyH///Xf4559/2mSfSZ/H7lHs3rXmugfnt99+C3/88UcU8JgxY6Kwf/jhh/Dhhx+GF154Idxwww3htNNOC/vvv3/Yb7/94t/m/z755JPD1VdfHZ588snQv3//8O2330Zw3cs9/dszij272ly34Fjdf/31VwSD8F599dVw/vnnh3XXXTfMM888YaKJJgqTTDJJmHHGGcNcc80VFlxwwbDQQgv9D88777xhlllmCZNPPnno0qVLmHXWWcMqq6wSjj766PDMM89EoJN2WQTFxlItrjtwkpZY0T169AhHHnlkFPDEE08cpptuurDmmmuGww47LNx4441RuH369AlDhw4NI0eOLHq/r7/+OmrZiy++GO6+++6oZZtuumkEFMCA22GHHcLDDz8cfvrppwhUrbSpbsBJmgKY5557LmywwQZxpdOMXXbZJdx2223ho48+iu9XggDarVu3cMQRR0QN86zFFlssXHrppeHHH3+MIFVbk+oCHEIgjH79+o0FhcAuvPDCCEi1iWl74IEHwmabbRa1iVZdddVV4ddff42LoVoBRO7BET2ha665Jkw99dRhzjnnDJdffnn46quv4vVakgXy/PPPh6222iouEL7p7bffjterAVBuwWHTRUzs/K677hqFYeV+/PHHBVF1Lt1///1h0UUXDVNMMUW49dZb4zULqdhcyuVcgpOA+fLLL8Pyyy8foy4mjCPOExnf5ptvHhfOqaeeGq9VEqDcgZOAGTRoUOjatWuMlp566qk48TwSc3b44YdHgORNqFIA5Q4cDpYD5vD5Fza9Huicc86pOEC5AseErESOduaZZw7vvPNOnGi90FlnnRUBOvPMM+PrjgYJuQHHRNAhhxwSE8o8m7K2SFkIQI8++miM4orNtb2cG3D4GfUtE7vkkksKU60/GjFiRFh11VWj5g8bNiwmzsXm2x7OBTipHDL//POHjTbaKJq3eqa33norTDXVVLFygco1b50ODlAQOy1n6N27d3xd76RGxwq8+eabZWtPp4Nj4N99912Ydtppw1FHHVWYWv2Tgup8880XNtlkk/i6HO3pdHA4zXPPPTdWlOU2ExKdd955UXvee++9mCIUm39b3Kng8DWKhwqJBxxwQGFKEw5ZbDPNNFM46KCD4iJkwovJoTXuVHBEaPfcc0+s9Nosaw+ptbHnu+++e6wUl0uet9dee4XjjjsufPPNN4WrlSdBwdxzzx2BKXWLodPASXWyFVZYIW6Qtbdudvzxx0cwp5xyylhBeP/99wvvtJ+++OKLsPTSS8fdT/fad999C+9Unm6//fZo2l5//fWSA4NOA4fWWPkGbqOsvaTQCBimcJpppol9AqWSiNAm3RxzzBEDkdVXX73wTuXJfhN/etFFF8U5F5NFa9wp4Mhj8BJLLBGrzvxOpGzwYeTIkIVvwp1x+fvvhTzh8W7dwvxdu8a9HdvHkr5SiXmxvQ3c2TPtsz2dLWsZZAjDh4/LmRnNBm0zp/Dt0sizVl555bDxxhvH16VEbTUHJ5Vp2Hpa89BDD8XXEZQBA0Lo0SOE7t1DZgfGZdfU2oYODR9l0c9z//53+OWXX5q+WwZZxfoGenuejbtevWSPIUtMxmXXPHfgwCaQyiD+0RY3f1lKUFBzcISU/ARgOORIVm2fPiG89BJPHcJrrxVn7/nM4MFN36sEZWCHV15p4tae7frLL4fQv79yc+GL7aezzz47lnPs/5RSra4pOFaNkHK33XYLs88+e/j000+bRg8cvQCEkIRUjL1HWP/5T5MJ7ChpBrGz2hycYuw94Ag+jLVEuvPOO2PgoS+ulKCgpuBYNd9nvkPdSdQ1DonWhg0L4YMPQujZs8mcNOfMlEWzxwQ1W70SvHfffTfee3zEt2lx0jaVzGvmFJq0p1+/EOwdJVOWmNnr2zeEzz4L2epq+k6J9Oyzz8bdXP0HudUcdj6toqeffrow9BZEIwiBD2rOrrVwymy4BsJJJ500rLPOOrF7k3N/JVvpr2Uahl/KzKBrJ5xwQgw+mFNhtM7OcYhG8GE//zzuc10rQ1ua08uZ1qkb3nffffkFh0kTJc0222zR/naUmAgBhT0UHZ1AInyhNu3E/m1/SOisrUp7bjWTzmJkgcip5Dy5BYcwhb/yivaYoVJIodGW9i233BKbQeQV/l555ZXh8ccfz9xU5qc6iWgOcFiN3IJjYHq+xPxM3P8L6RxlyvmeXGvOtttuG9Zff/0YUteMxmRJ7p8dN6Plku7Q3EdrtEV3yiKLLJL52szZ1ooG/yuL8s4tvKg97bPPPtHP2rZWQywmm2Jc84DAMQ1lkw+EzLWgb6/KwuEuIQw5snChQKOHZBqVhdFVJhGl6sDaa68drUVuKwRU2v66iIpzrAn9nuVGo7I8ZXSWy4QsFP/p0Szx3DqEQTvUBBxdOOarv7tTC5/pNIBBFFshEj/XF1544bD99tsXhl9jGpGB03ueEL5vfyW8XCKHbbbZJloK2xSlBAO4YuAQPM1wfkVBsbVVAjytT3KPN954ozCN6pAc6IILLohHR5xK+EyWj8aMyJKkH7Ik85Us6Xw++/ti0/UKk6ST1hx66KFx3qWYNFxRcACit3nPPfeMgyn2OQ6R7V1ggQXCaqutFg8iVYMsEHs1ElM2f/HFF4/VhNNPPz2MUWj4JzNpg7YLoXvmj765vOlLFaQhQ4ZEC6GGqGRVqtbgioLDrMnU9QO0Bo7VA0Sxv1Ulu68GHXzwwbHfWqSkrqXF1+tll1228ImMRn+RRXEXFF5UjoYPHx6Pq5ifbfhytAZXFBxH9awUe/xtDchnkSN9JnDiiSdGk1gpAr5amwZFdTZlGwmgLfGllloq3HTTTXE1V4PU7LbYYos4r3QsxHyLyWF8PF5wmCGTJezEBElNmwvfNeGxWtbFF18cB+Wz3vPZlmrtNaJlJrLzzjuHzz//PF4rl1SdlWuAMtlkk4Utt9wyXmdG+R7PsXj8VZmuNKmOr7TSSvH+xx57bLxWjjlL3CY4SYDO6av40giCV3a3QoCWAAJOr169oo3XG+A9jt9gaQZquYJ8x+fcU4DAJBJaOQmq+2hBIhjVZ2CP3WXNSNGRwGgr4O64447COx0nWih/0walhmaBoI4Ag9sEx8q/7LLL4oS19zijr/TttQaLTz75JK5KnyVo7UYmrlyRBKWJoi27m8Jvx9bTqvO3VNMjIhMAqHqrOhc7yKs4aj+f7xm70dcBco7o5ptvjn7MuDWwa781H/NqOddSuVVw3BzyohwT4k9w37594+pWgrc6EaED54knnoidJrZkaZDfA+AcDbatsoXvJ5Cvv/76WN4xWUL0Yw2OthNsW0RbbRHouZZDzTDDDPG8THPae++9430lhOWSeTg35DkaVNzPCbzrrrsuzgGX62NacqvgAGbgwIHRhyQHDzAPR4ARojJBCRwaQq3XWGONCFQCpZjGFOOkRe5lg2zDDTeMAk8C2GOPPSJ4NHTw4MHjhOEDBgyIB2iZFhrOtzBvzU2kfRXhrZAacEr5aT5tkUXpvJAwXBkmWY8VV1wxjkcHELPa1gIsh1sFx6D1hAlD7VoSWHqPANlu4FDtBM61114bzZqqM9Pic+WsIpNMQrMPwz/oO3BEhG8iHP6JhonGEgnPNRoyuVqnCC9FhomYO5VxwvU5xci2iK/1XPMiC9GeLXZNgggolTBhxbhNcCRyzJOBJOeWJrvjjjtGW2tgySxx7MwJ4VmhnLDPe68ckHzHcxNQTiPQELuZtAQ4Y7P+jDQL8jvrrbde1DwBilX9s63nFtS9e/fo5wi3LXLyASh8mfsZV9LuaoGSuFVwrF5byepC+q6QiRgYm8vcSfAQIbrOx/A3wlamzeqUY6SznR2ZjAXgOYSCtt566/gjRM1J/UrVgd/baaedorYtueSSMcAQ5pZDtJHWmEPSkvaa6Y5yq+AYAIGnY9x6zM4444xozpw7mX766eMqTuYOyfYJJhX5TjnllDgx3zdJk2v5nHLYuLbbbruw1lprjdUqpXlgcdRMEUCwHxkSMfJZfE6pJNDgR2lNpX3K+LhVcDABI5rDXDEZQCGUZHPTKiKwK664IoaTOjHTCmcaZcwcaiXB4TfSHgmiGRaBnAwZO01D/BKAmFtOvZRzQEr+wNHJkytwCD4BJOdQzFPdJfiWfoS6I8AkEHzfvxNXykYncJjOBI78QmQn7yhG8iCnzAAIKNUDxzMUaW0ft0ZAEVzcdddd8VnFxlMtbhOcxEAg2MTNQSn2uWrbZOAwswICrxFtoNlt5TDGJ/Kzx8LMCalFfTYAWyPRIqvBXHpuy7FUk9sFTt6YkE466aSYy8h3kIhMaD+2/3o8JNQXRvNVbZHnLbPMMrFryHOrvfCac12Cw7zce++9MedRPUh04IEHxjLT+HKXUknya5/KAqil36lLcPhByaT8Q/UikV5k19JPbFWK+DG+SsG3ln6nLsHhO7CcRomGn0OCEfUutTUmqFJkK0MQIW9CtTJtdQsO4at+W9HNtwYUNzl5BddKkrxKAKKe5/nFxlVprjtwEjByF2Ua4Kh3SXLVAmmS182PIzKD8iylGLuT7lMq2a+R49Ei92s5rmpw3YEjx/JzxMJmWwo6W0RpaoCSRVVp1fFEMnuFWCAKIOySKtaWSgIQ1Q6V+gY4RZitR7YSFD5TGO2v/MUGnf2mRK6rrQHNGVRVBJpXDgky1BlLPTrYEa4rcJLj11ZFY8ZHquQ0xlZGW6T3QJnG51S8E7tmL8dzJa58Dq1jVgFU7cCgLsAhBMIgFMISpemkGV+vgaq5/Z3xBQf8EBCbH7rCzJhfe6ep/I3CrnJO+mEK4wFcsTFXgusCHMAgZRYbf6lSLjJrK+F0UMtqT+awNXKulE+xx6NGh/1bx6b+N1vyggyv/WCf6FAAkn7buloaVBfgSPyA4LecRV2PPfZYNDMcvGBAd49KgY23sT84kZFfo6INkkiF13LI71jroVAqsnno/0dwUo7Jw6ha2lM34Pj74IMPRoEQkBWsvmYrWtXYdgATpLNT9dk2haq1Vc+R+1kWRxJtmjGH7gmw9JeALQD92z6n0Klr07kahU+LwaLwXOAYS/ppl/9rcFKEpVhJcwiGoJRrmBd7OMccc0ysDPiRIxGaaM6paedPhdL6BZR2gKWKoCPUdbmSv8stt1zsFeBb+BqACDyA6nfTNKxgJRzM96BqlnPqAhxsdXLASDjLLwAmrWZmjUYRoOvYCvean/LjRzpuFDGBwVQltmNqF1UVQL+dw77+7wT5knv7vvu5v1DdeJCcq5oRW92Ag1PUlkwRYqJEcD179oxmRk80QID2yCOPjGXXCDeZxKQBiROIQLZ7m37LQIGVxqpGIwsEKNWM0hLXFTjNGVAERlCASloFOAASHuB07PAlzVmUpUkxMeGrmbmv+yUgkHvjdM+W46gm1y04LRlYfBMmSEIul4GQ7lXsWbXiCQacCZEb4OSYG+DkmBvg5Jgb4OSYG+DkmBvg5Jgb4OSWR4X/Asu1zr9odBI4AAAAAElFTkSuQmCC" height = "150"> </td>');
        // pdf display works
        //$("#commentdisplayDocument" + commentId).prepend('<td> <embed id = "Display-document" src="https://coursesite.lehigh.edu/pluginfile.php/2192904/mod_resource/content/3/quiz-1%20%284%29.pdf" width = "500" height = "375" type ="application/pdf"> </td>');
        /* after backend works, add these codes, everything should work well
        if (data.mData.type == "image")
        {
            $("#commentdisplayDocument" + commentId).prepend('<td> <img id = "Display-image" src="data:image/jpeg;base64,"'+data.mData.url +'height = "150"> </td>');
        }
        else
        {
            $("#commentdisplayDocument" + commentId).prepend('<td> <embed id = "Display-document" src="data:application/pdf;base64,"'+ data.mData.url +'width = "500" height = "375" type ="application/pdf"> </td>');
        }
        */
    };
    /**
     * update() is the private method used by refresh() to update the
     * Comments
     */
    Comments.update = function (data) {
        // Remove the table of data, if it exists
        console.log("begin in update in comments");
        $("#" + Comments.NAME).remove();
        // Use a template to re-generate a table from the provided data, and put 
        // the table into our messageList element.
        console.log("before append data in comments");
        console.log("data from backend = " + JSON.stringify(data));
        $("#commentsRow" + Comments.messageId).append(Handlebars.templates[Comments.NAME + ".hb"](data));
        console.log("after append data in update in comments");
        // Find all of the delete buttons, and set their behavior
        $("." + Comments.NAME + "-delbtn").click(Comments.clickDelete);
        // Find all of the Edit buttons, and set their behavior
        $("." + Comments.NAME + "-editbtn").click(Comments.clickEdit);
        //  $("." + Comments.NAME + "-messageTitle").click(Comments.clickComments);
        console.log("in update in comments");
        //hide buttons if comment author does not match up with user signed in.
        $('#Comments-newCommentbtn').click(Comments.clickAddComments);
        var commentsList = data.mData;
        // for each comment display documents
        commentsList.forEach(function (comment) {
            $("#commentreadDocument" + comment.commentId).prepend('<td></td>');
            $("#commentreadDocument" + comment.commentId).prepend('<td"><button id=commentdisplay' + comment.commentId + '> Display Doc </button></td>');
        });
        $("." + "commentdisplay").click(Comments.clickDocument(data));
    };
    Comments.clickDocument = function (data) {
        var commentsList = data.mData;
        var count = 0;
        commentsList.forEach(function (comment) {
            // after click the button, display document
            var button = document.getElementById("commentdisplay" + comment.commentId);
            button.onclick = function () {
                if (count == 0) {
                    Comments.readDocument(comment.commentId); //only click the button first time
                    count = count + 1;
                }
                if (count == 1) {
                    button.style.display = 'none'; //hide the display button after document displayed
                }
            };
            $.ajax({
                type: "GET",
                url: "/commentSingle/" + comment.commentId,
                dataType: "json",
                headers: {
                    username: Login.username,
                    sessionKey: Login.sessionKey
                },
                success: onSuccess,
                error: function () {
                    window.alert("Internet Connection Error.");
                }
            });
            function onSuccess(data) {
                if (data.mStatus == "error") {
                    window.alert("Error from the database: " + data.mMessage);
                    return;
                }
            }
        });
    };
    Comments.clickAddComments = function () {
        console.log("meesageId get by AddComments method: " + Comments.messageId);
        var comment = "" + $("#Comments-newCommentText").val();
        // TODO: add back after backend works
        // send base64 to backend as String document
        // send link to backend as String link
        /*let link = "" + $("#Comments-link").val();
        console.log("link for comment is: " + link);

        // get base64 String of uploaded file
        var base64;
        var input : any = document.getElementById("Comments-upload");
        var selectedFile = input.files;
        //Check File is not Empty
        if (selectedFile.length > 0) {
            // Select the very first file from list
            var fileToLoad = selectedFile[0];
            // FileReader function for read the file.
            var fileReader = new FileReader();
            // Onload of file read the file content
            fileReader.onload = function(fileLoadedEvent: any) {
                base64 = fileLoadedEvent.target.result;
                // Print data in console
                console.log("The file upload for comment is: "+base64);
            };
            // Convert data to base64
            fileReader.readAsDataURL(fileToLoad);
        }*/
        $.ajax({
            type: "POST",
            url: "/comments/" + Comments.messageId,
            dataType: "json",
            data: JSON.stringify({
                sessionKey: Login.sessionKey,
                username: Login.username,
                content: comment
            }),
            success: function (data) {
                if (data.mStatus === "ok") {
                    window.alert("Your comment has been successfully added");
                    Comments.refresh(Comments.messageId);
                }
                else if (data.mStatus === "error") {
                    window.alert("The server replied with an error:\n" + data.mMessage);
                }
                // Handle other errors with a less-detailed popup message
                else {
                    window.alert("Unspecified error");
                }
            },
            error: function (data) {
                window.alert("Internet Connection Error encountered.");
            }
        });
    };
    /**
     * clickDelete is the code we run in response to a click of a delete button
     */
    Comments.clickDelete = function () {
        // for now, just print the ID that goes along with the data in the row
        // whose "delete" button was clicked
        var id = $(this).data("value");
        console.log("here in clickDelete");
        $.ajax({
            type: "DELETE",
            url: "/comments/" + id,
            dataType: "json",
            headers: {
                'username': Login.username, 'sessionKey': Login.sessionKey
            },
            // TODO: we should really have a function that looks at the return
            //       value and possibly prints an error message.
            success: Comments.checkError
        });
    };
    Comments.removeAllElements = function () {
        $("#" + Comments.NAME).remove();
    };
    Comments.checkError = function (data) {
        if (data.mStatus === "ok") {
            console.log("mstatus ok in checkerr");
            window.alert("Your comment has been successfully deleted");
            //ElementList.refresh();
            Comments.refresh(Comments.messageId);
        }
        else if (data.mStatus === "error") {
            console.log("comment delete mStatus error");
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
    };
    /**
     * clickEdit is the code we run in response to a click of a delete button
     */
    Comments.clickEdit = function () {
        // as in clickDelete, we need the ID of the row
        var id = $(this).data("value");
        var text = $(this).data("text");
        var userId = $(this).data("userId");
        var username = $(this).data("username");
        // TODO: add back after backend finish
        //let link = $(this).data("link");
        //let upload = $(this).data("upload");
        console.log("messages's username:" + username);
        console.log("messages's userId:" + userId + " my userId=" + Login.userId);
        console.log("id=" + id + " text=" + text);
        EditComment.get(id, text);
        console.log("click edit in comments" + id);
    };
    /**
     * The name of the DOM entry associated with Comments
     */
    Comments.NAME = "Comments";
    /**
     * Track if the Singleton has been initialized
     */
    Comments.isInit = false;
    return Comments;
}());
var $;
/**
 * Logout encapsulates all of the code for the form for adding an entry
 */
var Logout = /** @class */ (function () {
    function Logout() {
    }
    /**
     * Initialize the Logout by creating its element in the DOM and
     * configuring its buttons.  This needs to be called from any public static
     * method, to ensure that the Singleton is initialized before use
     */
    Logout.init = function () {
        if (Logout.isInit) {
            return;
        }
        $("body").append(Handlebars.templates[Logout.NAME + ".hb"]());
        $("#" + Logout.NAME + "-yes").click(Logout.submitForm);
        $("#" + Logout.NAME + "-cancel").click(Logout.hide);
        Logout.isInit = true;
    };
    /**
     * Refresh() doesn't really have much meaning, but just like in sNavbar, we
     * have a refresh() method so that we don't have front-end code calling
     * init().
     */
    Logout.refresh = function () {
        Logout.init();
    };
    Logout.show = function () {
        if (!Login.isLogIn) {
            window.alert("You are not signed in.");
            return;
        }
        $("#" + Logout.NAME).modal("show");
    };
    Logout.hide = function () {
        $("#" + Logout.NAME).modal("hide");
    };
    /**
     * Send data to submit the form only if the fields are both valid.
     * Immediately hide the form when we send data, so that the user knows that
     * their click was received.
     */
    Logout.submitForm = function () {
        // get the values of the two fields, force them to be strings, and check 
        // that neither is empty
        console.log("logout submit is called");
        Login.isLogIn = false;
        Login.GoogleAuth.signOut();
        // set up an AJAX post.  When the server replies, the result will go to
        // onSubmitResponse
        $.ajax({
            type: "POST",
            url: "/users/logout",
            dataType: "json",
            data: JSON.stringify({ username: Login.username }),
            success: Logout.onSubmitResponse,
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                window.alert("Internet Error! Status: " + textStatus + "Error: " + errorThrown);
                ElementList.refresh();
                Login.show();
            }
        });
    };
    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a
     * result.
     *
     * @param data The object returned by the server
     */
    Logout.onSubmitResponse = function (data) {
        // If we get an "ok" message, clear the form and refresh the main 
        // listing of messages
        ElementList.refresh();
        if (data.mStatus === "ok") {
            console.log("logout status ok");
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
        Logout.hide();
        Login.show();
    };
    /**
     * The name of the DOM entry associated with Logout
     */
    Logout.NAME = "Logout";
    /**
     * Track if the Singleton has been initialized
     */
    Logout.isInit = false;
    return Logout;
}());
/**
 * The Profile Singleton provides a way of displaying all of the data
 * stored on the server as an HTML table.
 */
var Profile = /** @class */ (function () {
    function Profile() {
    }
    /**
     * Initialize the Profile singleton by creating its element in the DOM.
     * This needs to be called from any public static method, to ensure that the
     * Singleton is initialized before use.
     */
    Profile.init = function () {
        if (!Profile.isInit) {
            Profile.isInit = true;
        }
    };
    /**
     * refresh() is the public method for updating the Profile
     */
    Profile.refresh = function () {
        // Make sure the singleton is initialized
        Profile.init();
    };
    Profile.show = function () {
        if (!Login.isLogIn) {
            window.alert("You are not signed in.");
            return;
        }
        Profile.update();
    };
    Profile.hide = function () {
        $("#" + Profile.NAME).modal("hide");
    };
    /**
     * update() is the private method used by refresh() to update the
     * Profile
     */
    Profile.update = function () {
        $.ajax({
            type: 'POST',
            url: "/users/info",
            dataType: "json",
            data: JSON.stringify({
                'sessionKey': Login.sessionKey,
                'username': Login.username,
                'usernameAskedFor': Login.username
            }),
            success: onSuccess,
            error: function () {
                window.alert("Internet Connection Error");
            }
        });
        function onSuccess(data) {
            console.log(JSON.stringify(data));
            if (data.mStatus === "error") {
                window.alert("error from database: " + data.mMessage);
                return;
            }
            // Remove the table of data, if it exists
            $("#" + Profile.NAME).remove();
            // Use a template to re-generate a table from the provided data, and put 
            // the table into our messageList element.
            $("body").append(Handlebars.templates[Profile.NAME + ".hb"]());
            $("#user-Profile").prepend('<img src ="' + data.mData.picture + '" style="width:104px;height:104px;"> </img>');
            $("#user-Profile").prepend('<p> first name: ' + data.mData.givenName + '</p>');
            $("#user-Profile").prepend('<p> last name: ' + data.mData.familyName + '</p>');
            $("#user-Profile").prepend('<p> email: ' + data.mData.email + '</p>');
            $("#Profile-confirm").click(Profile.hide);
            $("#" + Profile.NAME).modal("show");
        }
    };
    /**
     * The name of the DOM entry associated with Profile
     */
    Profile.NAME = "Profile";
    /**
     * Track if the Singleton has been initialized
     */
    Profile.isInit = false;
    return Profile;
}());
// "any", so that we can use it anywhere, and assume it has any fields or
// methods, without the compiler producing an error.
var $;
// a global for the EditComment of the program.  See newEntryForm for 
// explanation
var editComment;
/**
 * EditComment encapsulates all of the code for the form for editing an entry
 */
var EditComment = /** @class */ (function () {
    function EditComment() {
    }
    EditComment.init = function () {
        if (!EditComment.isInit) {
            $("body").append(Handlebars.templates[EditComment.NAME + ".hb"]());
            $("#" + EditComment.NAME + "-OK").click(EditComment.submitForm);
            $("#" + EditComment.NAME + "-Cancel").click(EditComment.hide);
            EditComment.isInit = true;
        }
    };
    EditComment.refresh = function () {
        EditComment.init();
    };
    /**
     * get() is called from the AJAX GET in clickEdit in ElementList.ts, and should populate the form if and
     * only if the GET did not have an error
     */
    EditComment.get = function (id, text) {
        $("#" + EditComment.NAME + "-commentText").val(text);
        $("#" + EditComment.NAME + "-OK").data("value", id);
        // show the edit form
        $("#" + EditComment.NAME).modal("show");
        EditComment.messageId = id;
    };
    /**
     * Hide the EditComment, clearing its fields first
     */
    EditComment.hide = function () {
        $("#" + EditComment.NAME + "-message").val("");
        $("#" + EditComment.NAME + "-commenText").val("");
        $("#" + EditComment.NAME).modal("hide");
    };
    /**
     * Check if the input fields are both valid, and if so, do an AJAX call.
     */
    EditComment.submitForm = function () {
        // get the values of the two fields, force them to be strings, and check 
        // that neither is empty
        var msg = "" + $("#" + EditComment.NAME + "-commentText").val();
        var username = Login.username;
        // NB: we assume that the user didn't modify the value of #editId
        var id = "" + $("#" + EditComment.NAME + "-OK").data("value");
        // TODO: add back after backend works
        // send base64 to backend as String document
        // send link to backend as String link
        /* let link = "" + $("#" + EditComment.NAME + "-link").data("");
        console.log("link for comment is: " + link);

        // get base64 String of uploaded file
        var base64;
        var input : any = document.getElementById("EditComment-upload");
        var selectedFile = input.files;
        //Check File is not Empty
        if (selectedFile.length > 0) {
            // Select the very first file from list
            var fileToLoad = selectedFile[0];
            // FileReader function for read the file.
            var fileReader = new FileReader();
            // Onload of file read the file content
            fileReader.onload = function(fileLoadedEvent: any) {
                base64 = fileLoadedEvent.target.result;
                // Print data in console
                console.log("The file upload for comment is: "+base64);
            };
            // Convert data to base64
            fileReader.readAsDataURL(fileToLoad);
        } */
        if (msg === "") {
            window.alert("Error:comment is not valid");
            return;
        }
        EditComment.hide();
        // set up an AJAX post.  When the server replies, the result will go to
        // onSubmitResponse
        $.ajax({
            type: "PUT",
            url: "/comments",
            dataType: "json",
            //to add user id also put mU_id: U_id in the Json string 
            data: JSON.stringify({ username: username, sessionKey: Login.sessionKey, commentId: id, content: msg }),
            success: EditComment.onSubmitResponse
        });
    };
    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a
     * result.
     *
     * @param data The object returned by the server
     */
    EditComment.onSubmitResponse = function (data) {
        // If we get an "ok" message, clear the form and refresh the main 
        // listing of messages
        if (data.mStatus === "ok") {
            window.alert("Your comment has been successfully changed");
            Comments.refresh(EditComment.messageId);
            console.log("in onsumbitResponse in EditComments " + EditComment.messageId);
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
    };
    /**
     * The name of the DOM entry associated with EditComment
     */
    EditComment.NAME = "EditComment";
    /**
     * Track if the Singleton has been initialized
     */
    EditComment.isInit = false;
    return EditComment;
}()); // end class EditComment
/**
 * The ElementList Singleton provides a way of displaying all of the data
 * stored on the server as an HTML table.
 */
var UserMessagesList = /** @class */ (function () {
    function UserMessagesList() {
    }
    /**
     * Initialize the ElementList singleton by creating its element in the DOM.
     * This needs to be called from any public static method, to ensure that the
     * Singleton is initialized before use.
     */
    UserMessagesList.init = function () {
        if (!UserMessagesList.isInit) {
            UserMessagesList.isInit = true;
        }
    };
    /**
     * refresh() is the public method for updating the ElementList
     */
    UserMessagesList.refresh = function () {
        $('#' + UserMessagesList.NAME).remove();
        // Make sure the singleton is initialized
        if (!Login.isLogIn) {
            window.alert("You are not signed in.");
            return;
        }
        UserMessagesList.init();
        // Issue a GET, and then pass the result to update()
        $.ajax({
            type: "POST",
            url: "/users/info",
            dataType: "json",
            data: JSON.stringify({
                'sessionKey': Login.sessionKey,
                'username': Login.username,
                'usernameAskedFor': Login.username
            }),
            success: UserMessagesList.update,
            error: function () {
                window.alert("Internet Error Encountered.");
            }
        });
    };
    /**
     * update() is the private method used by refresh() to update the
     * ElementList
     */
    UserMessagesList.update = function (data) {
        if (data.mStatus === "error") {
            window.alert("Error from database: " + data.mMessage);
            return;
        }
        $('#ElementList').remove();
        $("#UserMessagesList").remove();
        $("body").append(Handlebars.templates["UserMessagesList.hb"]());
        console.log(JSON.stringify(data));
        var messageArray = data.mData.messageList;
        UserMessagesList.populateMessageList(messageArray);
        var commentArray = data.mData.commentList;
        UserMessagesList.populateCommentList(commentArray);
    };
    UserMessagesList.populateCommentList = function (commentArray) {
        commentArray.forEach(function (comment) {
            $.ajax({
                type: "GET",
                url: "/commentSingle/" + comment.commentId,
                dataType: "json",
                headers: {
                    username: Login.username,
                    sessionKey: Login.sessionKey
                },
                success: onSuccess,
                error: function () {
                    window.alert("Internet Connection Error.");
                }
            });
            function onSuccess(data) {
                if (data.mStatus == "error") {
                    window.alert("Error from the database: " + data.mMessage);
                    return;
                }
                UserMessagesList.showComment(data);
            }
        });
    };
    UserMessagesList.populateMessageList = function (messageArray) {
        messageArray.forEach(function (message) {
            $.ajax({
                type: "GET",
                url: "/messages/" + message.messageId,
                dataType: "json",
                headers: {
                    username: Login.username,
                    sessionKey: Login.sessionKey
                },
                success: onSuccess,
                error: function () {
                    window.alert("Internet Connection Error.");
                }
            });
            function onSuccess(data) {
                if (data.mStatus == "error") {
                    window.alert("Error from the database: " + data.mMessage);
                    return;
                }
                UserMessagesList.showMessage(data);
            }
        });
    };
    UserMessagesList.showComment = function (data) {
        console.log(JSON.stringify(data));
        //prepend messages
        $("#UserCommentsTable").prepend('<tr> <td> ' + data.mData.commentText + " </td> <td> " + data.mData.dateCreated + "</td> </tr>");
    };
    UserMessagesList.showMessage = function (data) {
        console.log(JSON.stringify(data));
        //prepend messages
        $("#UserMessagesTable").prepend('<p>' + data.mData.title + "<br \>"
            + data.mData.content + "<br \>" + data.mData.dateCreated + "</p>");
    };
    /**
     * The name of the DOM entry associated with ElementList
     */
    UserMessagesList.NAME = "ElementList";
    /**
     * Track if the Singleton has been initialized
     */
    UserMessagesList.isInit = false;
    return UserMessagesList;
}());
/// <reference path="ts/EditEntryForm.ts"/>
/// <reference path="ts/NewEntryForm.ts"/>
/// <reference path="ts/ElementList.ts"/>
/// <reference path="ts/Navbar.ts"/>
/// <reference path="ts/UiTests.ts"/>
/// <reference path="ts/Login.ts"/>
/// <reference path="ts/Comments.ts"/>
/// <reference path="ts/Logout.ts"/>
/// <reference path="ts/Profile.ts"/>
/// <reference path="ts/EditComment.ts"/>
/// <reference path="ts/UserMessagesList.ts"/>
/// This constant indicates the path to our backend server
//const backendUrl = "https://cse216-penguin-power.herokuapp.com/";
// Prevent compiler errors
var Handlebars;
var $;
// Run some configuration code when the web page loads
$(document).ready(function () {
    Navbar.refresh();
    Login.refresh();
    //Login.show();
    Logout.refresh();
    NewEntryForm.refresh();
    ElementList.refresh();
    EditEntryForm.refresh();
    EditComment.refresh();
    Profile.refresh();
    window.onload = function () { UiTests.runTests(); };
});
