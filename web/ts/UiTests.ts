
/**
 * This class tests the UI, instead of Jasmine, as Jasmine asynchronous testing wasn't really asynchronous
 * Jasmine tests would run before proper html elements were loaded onto the page
 * So as to not go without any testing, this class was written
 * Errors will be stated in the form of alerts and statements written to the console
 * 
 * Ran into the same problem here, where JQuery runs before the dom is effectively loaded,
 * thus these tests are not in a working state
 * 
 * Been working on this for over 18 hours, still couln't get this to work.
 */
class UiTests {

    public static runTests(){
        //UiTests.navButtonShowsAddEntry();
        //UiTests.addEntryShowsNewEntryForm();
        //UiTests.newEntryFormCloseBtnWorks();
        //UiTests.newEntryFormOKBtnWorks();
    }

    /**
     * Tests if the navBar Button shows the add element button when clicked
     */
    private static navButtonShowsAddEntry(){
        $(".navbar-toggle").click() 
        
        doThis(1);
       
        
        function doThis ( num: number){
            if(num < 2 && !$("#Navbar-add").is(":visible")){
                    //wait for the frame to load
                    window.requestAnimationFrame(doThis);
                    num++;
                }
            else if($("#Navbar-add").is(":visible")){
                console.log("Nav Button shows addEntry button");
                $(".navbar-toggle").click();
                return;
            }
            else {
                //sends and alert to the screen
                    alert("Navbar button does not work");
                    console.log("!! Navbar button does not show addEntryButton");
                }
        }
            
    }

    private static addEntryShowsNewEntryForm(){
       
        $("#Navbar-add").click();
        var num = 1;
        
       
            $(document).ready(function(){
                if(($("#NewEntryForm").attr("class") == "modal fade in")){
                    console.log("Navbar-add button shows NewEntryForm");
                    $("NewEntryForm-Close").click();
                }
                    
                else{
                    alert("Navbar-add button does not show NewEntryForm");
                    console.log("!! Navbar-add button does not show NewEntryForm");
                }
            });
        
        
        
    }
    
    private static newEntryFormCloseBtnWorks(){
        $("#Navbar-add").click();
        $("#NewEntryForm-Close").click();
        if($("#NewEntryForm").is(":hidden")){
            console.log("NewEntryForm Close button works");
        }
        else{
            alert("NewEntryForm Close Button does not close the NewEntryForm modal");
            console.log("!! NewEntryForm-Close button does not close the NewEntryForm modal");
        }
    }

    private static newEntryFormOKBtnWorks(){
       
        $("#Navbar-add").click();

        $("#NewEntryForm-userId").val("1");
        $("#NewEntryForm-title").val("test");
        $("#NewEntryForm-message").val("testing");
        $("#NewEntryForm-OK").click();
        
        if($("#NewEntryForm").is(":hidden")){
            console.log("NewEntryForm-OK button works");
            //UiTests.testDeleteButton();
        }
        else{
            alert("NewEntryForm OK button does not close the NewEntryForm modal");
            console.log("!! NewEntryForm-OK button does not close the NewEntryForm modal,  edit or delete buttons functionality has not been tested");
        }
    }

    
    
}