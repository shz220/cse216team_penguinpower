
/**
 * The ElementList Singleton provides a way of displaying all of the data 
 * stored on the server as an HTML table.
 */
class UserMessagesList {
    /**
     * The name of the DOM entry associated with ElementList
     */
    private static readonly NAME = "ElementList";

    /**
     * Track if the Singleton has been initialized
     */
    private static isInit = false;

    /**
     * Initialize the ElementList singleton by creating its element in the DOM.
     * This needs to be called from any public static method, to ensure that the 
     * Singleton is initialized before use.
     */
    private static init() {
        if (!UserMessagesList.isInit) {
            UserMessagesList.isInit = true;
        }
    }

    /**
     * refresh() is the public method for updating the ElementList
     */
    public static refresh() {
        $('#' + UserMessagesList.NAME).remove();
        // Make sure the singleton is initialized

        if (!Login.isLogIn) {
            window.alert("You are not signed in.");
            return;
        }
        UserMessagesList.init();

        // Issue a GET, and then pass the result to update()
        $.ajax({
            type: "POST",
            url: "/users/info",
            dataType: "json",
            data: JSON.stringify({
                'sessionKey': Login.sessionKey,
                'username': Login.username,
                'usernameAskedFor': Login.username
            }),
            success: UserMessagesList.update,
            error: function () {
                window.alert("Internet Error Encountered.");
            }
        });
    }

    /**
     * update() is the private method used by refresh() to update the 
     * ElementList
     */
    private static update(data: any) {
        if (data.mStatus === "error") {
            window.alert("Error from database: " + data.mMessage);
            return;
        }
        $('#ElementList').remove();
        $("#UserMessagesList").remove();
        $("body").append(Handlebars.templates["UserMessagesList.hb"]());
        console.log(JSON.stringify(data));
        var messageArray: Array<any> = data.mData.messageList;
        UserMessagesList.populateMessageList(messageArray);
        var commentArray: Array<any> = data.mData.commentList;
        UserMessagesList.populateCommentList(commentArray);
    }
    private static populateCommentList(commentArray: any[]) {
        commentArray.forEach(comment => {
            $.ajax({
                type: "GET",
                url: "/commentSingle/" + comment.commentId,
                dataType: "json",
                headers: {
                    username: Login.username,
                    sessionKey: Login.sessionKey
                },
                success: onSuccess,
                error: function () {
                    window.alert("Internet Connection Error.");
                }
            })
            function onSuccess(data: any) {
                if (data.mStatus == "error") {
                    window.alert("Error from the database: " + data.mMessage);
                    return;
                }
                UserMessagesList.showComment(data);
            }
        });
    }


    private static populateMessageList(messageArray: Array<any>) {
        messageArray.forEach(message => {
            $.ajax({
                type: "GET",
                url: "/messages/" + message.messageId,
                dataType: "json",
                headers: {
                    username: Login.username,
                    sessionKey: Login.sessionKey
                },
                success: onSuccess,
                error: function () {
                    window.alert("Internet Connection Error.");
                }
            })
            function onSuccess(data: any) {
                if (data.mStatus == "error") {
                    window.alert("Error from the database: " + data.mMessage);
                    return;
                }
                UserMessagesList.showMessage(data);
            }
        });
    }
    private static showComment(data: any) {
        console.log(JSON.stringify(data));
        //prepend messages
        $("#UserCommentsTable").prepend('<tr> <td> ' + data.mData.commentText + " </td> <td> " + data.mData.dateCreated + "</td> </tr>");
    }

    private static showMessage(data: any) {
        console.log(JSON.stringify(data));
        //prepend messages
        $("#UserMessagesTable").prepend('<p>' + data.mData.title + "<br \>"
            + data.mData.content + "<br \>" + data.mData.dateCreated + "</p>");
    }
}