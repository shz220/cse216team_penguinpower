
/**
 * NewEntryForm encapsulates all of the code for the form for adding an entry
 */
class NewEntryForm {

    /**
     * The name of the DOM entry associated with NewEntryForm
     */
    private static readonly NAME = "NewEntryForm";

    /**
     * Track if the Singleton has been initialized
     */
    private static isInit = false;

    /**
     * Initialize the NewEntryForm by creating its element in the DOM and 
     * configuring its buttons.  This needs to be called from any public static 
     * method, to ensure that the Singleton is initialized before use
     */
    private static init() {
        if (!NewEntryForm.isInit) {
            $("body").append(Handlebars.templates[NewEntryForm.NAME + ".hb"]());
            $("#" + NewEntryForm.NAME + "-OK").click(NewEntryForm.submitForm);
            $("#" + NewEntryForm.NAME + "-Close").click(NewEntryForm.hide);
            NewEntryForm.isInit = true;
        }
    }

    /**
     * Refresh() doesn't really have much meaning, but just like in sNavbar, we
     * have a refresh() method so that we don't have front-end code calling
     * init().
     */
    public static refresh() {
        NewEntryForm.init();
    }

    /**
     * Hide the NewEntryForm.  Be sure to clear its fields first
     */
    private static hide() {
        $("#" + NewEntryForm.NAME + "-title").val("");
        $("#" + NewEntryForm.NAME + "-message").val("");
        $("#" + NewEntryForm.NAME + "-link").val("");
        $("#" + NewEntryForm.NAME + "-upload").val("");
        $("#" + NewEntryForm.NAME).modal("hide");
    }

    /**
     * Show the NewEntryForm.  Be sure to clear its fields, because there are
     * ways of making a Bootstrap modal disapper without clicking Close, and
     * we haven't set up the hooks to clear the fields on the events associated
     * with those ways of making the modal disappear.
     */
    public static show() {
        if(!Login.isLogIn){
            window.alert("You are not signed in.");
            return;
        }
        $("#" + NewEntryForm.NAME + "-title").val("");
        $("#" + NewEntryForm.NAME + "-message").val("");
        $("#" + NewEntryForm.NAME).modal("show");
    }

    
    /**
     * Send data to submit the form only if the fields are both valid.  
     * Immediately hide the form when we send data, so that the user knows that 
     * their click was received.
     */
    private static submitForm() {
        // get the values of the two fields, force them to be strings, and check 
        // that neither is empty
        let title = "" + $("#" + NewEntryForm.NAME + "-title").val();
        let msg = "" + $("#" + NewEntryForm.NAME + "-message").val();
        let link = "" + $("#" + NewEntryForm.NAME + "-link").val();
        console.log("link is: " + link);

        var base64 = "";
        if (link != ""){
            if (title === "" || msg === "") {
                window.alert("Error: title or message is not valid");
                return;
            }
            NewEntryForm.hide();
            // set up an AJAX post.  When the server replies, the result will go to
            // onSubmitResponse
            $.ajax({
                type: "POST",
                url: "/messages",
                dataType: "json",
                data: JSON.stringify({
                    username: Login.username, 
                    sessionKey:Login.sessionKey,
                    title: title, 
                    content: msg, 
                    external_url: link}),
                success: NewEntryForm.onSubmitResponse
            });
        }
        else    //link is empty
        {
            var input : any = document.getElementById("NewEntryForm-upload");    // get base64 String of uploaded file
            var selectedFile = input.files;
            if (selectedFile.length > 0) {                                      //Check File is not Empty
                var fileToLoad = selectedFile[0];
                var fileReader = new FileReader();
                fileReader.onload = function(fileLoadedEvent: any) {
                    base64 = fileLoadedEvent.target.result;   
                    console.log("Base 64 String is: "+base64);

                    if (base64 != "")  //no link or base64
                    {
                        if (title === "" || msg === "") {
                            window.alert("Error: title or message is not valid");
                            return;
                        }
                        NewEntryForm.hide();
                        // set up an AJAX post.  When the server replies, the result will go to
                        // onSubmitResponse
                        $.ajax({
                            type: "POST",
                            url: "/messages",
                            dataType: "json",
                            data: JSON.stringify({
                                username: Login.username, 
                                sessionKey:Login.sessionKey,
                                title: title, 
                                content: msg, 
                                document: base64}),
                            success: NewEntryForm.onSubmitResponse
                        });
                    }                    
                };
                fileReader.readAsDataURL(fileToLoad);       // Convert data to base64
            }
            else{
                if (title === "" || msg === "") {
                    window.alert("Error: title or message is not valid");
                    return;
                }
                NewEntryForm.hide();
                // set up an AJAX post.  When the server replies, the result will go to
                // onSubmitResponse
                $.ajax({
                    type: "POST",
                    url: "/messages",
                    dataType: "json",
                    data: JSON.stringify({
                        username: Login.username, 
                        sessionKey:Login.sessionKey,
                        title: title, 
                        content: msg
                    }),
                    success: NewEntryForm.onSubmitResponse
                });
            }
        }
    }

    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a 
     * result.
     * 
     * @param data The object returned by the server
     */
    private static onSubmitResponse(data: any) {
        // If we get an "ok" message, clear the form and refresh the main 
        // listing of messages
        if (data.mStatus === "ok") {
            ElementList.refresh();
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            console.log("NewEntryForm");
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
        
    }
}