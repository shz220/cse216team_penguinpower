/**
 * Logout encapsulates all of the code for the form for adding an entry
 */
class Logout {

    /**
     * The name of the DOM entry associated with Logout
     */
    private static readonly NAME = "Logout";

    /**
     * Track if the Singleton has been initialized
     */
    private static isInit = false;


    /**
     * Initialize the Logout by creating its element in the DOM and 
     * configuring its buttons.  This needs to be called from any public static 
     * method, to ensure that the Singleton is initialized before use
     */
    private static init() {
        if (Logout.isInit) {
            return;
        }
        $("body").append(Handlebars.templates[Logout.NAME + ".hb"]());
        $("#" + Logout.NAME + "-yes").click(Logout.submitForm);
        $("#" + Logout.NAME + "-cancel").click(Logout.hide);
        Logout.isInit = true;
    }

    /**
     * Refresh() doesn't really have much meaning, but just like in sNavbar, we
     * have a refresh() method so that we don't have front-end code calling
     * init().
     */
    public static refresh() {
        Logout.init();
    }

    public static show() {
        if (!Login.isLogIn) {
            window.alert("You are not signed in.");
            return;
        }
        $("#" + Logout.NAME).modal("show");
    }

    private static hide() {
        $("#" + Logout.NAME).modal("hide");
    }

    /**
     * Send data to submit the form only if the fields are both valid.  
     * Immediately hide the form when we send data, so that the user knows that 
     * their click was received.
     */
    private static submitForm() {
        // get the values of the two fields, force them to be strings, and check 
        // that neither is empty
        console.log("logout submit is called");
        Login.isLogIn = false;
        Login.GoogleAuth.signOut();
        // set up an AJAX post.  When the server replies, the result will go to
        // onSubmitResponse
        $.ajax({
            type: "POST",
            url: "/users/logout",
            dataType: "json",
            data: JSON.stringify({ username: Login.username }),
            success: Logout.onSubmitResponse,
            error: function (XMLHttpRequest: any, textStatus: any, errorThrown: any) {
                window.alert("Internet Error! Status: " + textStatus + "Error: " + errorThrown);
                ElementList.refresh();

                Login.show();
            }
        });
    }

    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a 
     * result.
     * 
     * @param data The object returned by the server
     */
    private static onSubmitResponse(data: any) {
        // If we get an "ok" message, clear the form and refresh the main 
        // listing of messages

        ElementList.refresh();
        if (data.mStatus === "ok") {
            console.log("logout status ok");
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }

        Logout.hide();
        Login.show();
    }
}