
/**
 * The ElementList Singleton provides a way of displaying all of the data 
 * stored on the server as an HTML table.
 */
class ElementList {
    /**
     * The name of the DOM entry associated with ElementList
     */
    private static readonly NAME = "ElementList";

    /**
     * Track if the Singleton has been initialized
     */
    private static isInit = false;

    /**
     * Initialize the ElementList singleton by creating its element in the DOM.
     * This needs to be called from any public static method, to ensure that the 
     * Singleton is initialized before use.
     */
    private static init() {
        if (!ElementList.isInit) {
            ElementList.isInit = true;
        }
    }

    /**
     * refresh() is the public method for updating the ElementList
     */
    public static refresh() {
        $('#' + ElementList.NAME).remove();
        // Make sure the singleton is initialized

        if (!Login.isLogIn) {
            return;
        }
        ElementList.init();

        console.log("elementList-refresh")
        // Issue a GET, and then pass the result to update()
        $.ajax({
            type: "GET",
            url: "/messages",
            dataType: "json",
            headers: {
                'sessionKey': Login.sessionKey,
                'username': Login.username
            },
            success: ElementList.update
        });
    }

    /**
     * update() is the private method used by refresh() to update the 
     * ElementList
     */
    private static update(data: any) {
        // Remove the table of data, if it exists
        $('#' + ElementList.NAME).remove();
        $("#UserMessagesList").remove();
        // Use a template to re-generate a table from the provided data, and put 
        // the table into our messageList element.
        $("body").append(Handlebars.templates[ElementList.NAME + ".hb"](data));
        $("." + ElementList.NAME + "-upVotebtn").click(ElementList.clickUpVote);
        $("." + ElementList.NAME + "-downVotebtn").click(ElementList.clickDownVote);
        $("." + ElementList.NAME + "-messageTitle").click(ElementList.clickMessageTitle);
    }

    private static clickMessageTitle() {
        // we need the messageId of the message
        let id = $(this).data("value");
        //if we dont have the message yet
        if (!($("#content" + id).length)) {
            $.ajax({
                type: "GET",
                url: "/messages/" + id,
                dataType: "json",
                headers: {
                    'sessionKey': Login.sessionKey,
                    'username': Login.username
                },
                success: ElementList.showMessage
            });
        }
    }

    // due to backend not finished, I used the base64 I get to be used in ElementList.ts


    //display document after click "display document" button
    private static readDocument(data: any){
        console.log("readdocument: "+JSON.stringify(data));
        $("#displayDocument" + data.mData.messageId).prepend('<td></td>');
        $("#displayDocument" + data.mData.messageId).prepend('<td></td>');
        
        // for testing (no backend send back correct binary file)
        // image display works
        //$("#displayDocument" + data.mData.messageId).prepend('<td> <img id = "Display-image" src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAGcAAABZCAYAAAAjMTokAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABCuSURBVHhe7d0FkFzFFgbg4O7uwd3d3Z3CHu7u7hQOBVRB4VK4FB6ch0sgQIgCgbxAIIGgAUIgQIDQ736906nNvtnNzuzM7J28OVWnNnNn5t7u8/fRPj3pMmrUqNDgfHIDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBzzBAfO77//HkaPHh3+/PPPyH///Xf4559/2mSfSZ/H7lHs3rXmugfnt99+C3/88UcU8JgxY6Kwf/jhh/Dhhx+GF154Idxwww3htNNOC/vvv3/Yb7/94t/m/z755JPD1VdfHZ588snQv3//8O2330Zw3cs9/dszij272ly34Fjdf/31VwSD8F599dVw/vnnh3XXXTfMM888YaKJJgqTTDJJmHHGGcNcc80VFlxwwbDQQgv9D88777xhlllmCZNPPnno0qVLmHXWWcMqq6wSjj766PDMM89EoJN2WQTFxlItrjtwkpZY0T169AhHHnlkFPDEE08cpptuurDmmmuGww47LNx4441RuH369AlDhw4NI0eOLHq/r7/+OmrZiy++GO6+++6oZZtuumkEFMCA22GHHcLDDz8cfvrppwhUrbSpbsBJmgKY5557LmywwQZxpdOMXXbZJdx2223ho48+iu9XggDarVu3cMQRR0QN86zFFlssXHrppeHHH3+MIFVbk+oCHEIgjH79+o0FhcAuvPDCCEi1iWl74IEHwmabbRa1iVZdddVV4ddff42LoVoBRO7BET2ha665Jkw99dRhzjnnDJdffnn46quv4vVakgXy/PPPh6222iouEL7p7bffjterAVBuwWHTRUzs/K677hqFYeV+/PHHBVF1Lt1///1h0UUXDVNMMUW49dZb4zULqdhcyuVcgpOA+fLLL8Pyyy8foy4mjCPOExnf5ptvHhfOqaeeGq9VEqDcgZOAGTRoUOjatWuMlp566qk48TwSc3b44YdHgORNqFIA5Q4cDpYD5vD5Fza9Huicc86pOEC5AseErESOduaZZw7vvPNOnGi90FlnnRUBOvPMM+PrjgYJuQHHRNAhhxwSE8o8m7K2SFkIQI8++miM4orNtb2cG3D4GfUtE7vkkksKU60/GjFiRFh11VWj5g8bNiwmzsXm2x7OBTipHDL//POHjTbaKJq3eqa33norTDXVVLFygco1b50ODlAQOy1n6N27d3xd76RGxwq8+eabZWtPp4Nj4N99912Ydtppw1FHHVWYWv2Tgup8880XNtlkk/i6HO3pdHA4zXPPPTdWlOU2ExKdd955UXvee++9mCIUm39b3Kng8DWKhwqJBxxwQGFKEw5ZbDPNNFM46KCD4iJkwovJoTXuVHBEaPfcc0+s9Nosaw+ptbHnu+++e6wUl0uet9dee4XjjjsufPPNN4WrlSdBwdxzzx2BKXWLodPASXWyFVZYIW6Qtbdudvzxx0cwp5xyylhBeP/99wvvtJ+++OKLsPTSS8fdT/fad999C+9Unm6//fZo2l5//fWSA4NOA4fWWPkGbqOsvaTQCBimcJpppol9AqWSiNAm3RxzzBEDkdVXX73wTuXJfhN/etFFF8U5F5NFa9wp4Mhj8BJLLBGrzvxOpGzwYeTIkIVvwp1x+fvvhTzh8W7dwvxdu8a9HdvHkr5SiXmxvQ3c2TPtsz2dLWsZZAjDh4/LmRnNBm0zp/Dt0sizVl555bDxxhvH16VEbTUHJ5Vp2Hpa89BDD8XXEZQBA0Lo0SOE7t1DZgfGZdfU2oYODR9l0c9z//53+OWXX5q+WwZZxfoGenuejbtevWSPIUtMxmXXPHfgwCaQyiD+0RY3f1lKUFBzcISU/ARgOORIVm2fPiG89BJPHcJrrxVn7/nM4MFN36sEZWCHV15p4tae7frLL4fQv79yc+GL7aezzz47lnPs/5RSra4pOFaNkHK33XYLs88+e/j000+bRg8cvQCEkIRUjL1HWP/5T5MJ7ChpBrGz2hycYuw94Ag+jLVEuvPOO2PgoS+ulKCgpuBYNd9nvkPdSdQ1DonWhg0L4YMPQujZs8mcNOfMlEWzxwQ1W70SvHfffTfee3zEt2lx0jaVzGvmFJq0p1+/EOwdJVOWmNnr2zeEzz4L2epq+k6J9Oyzz8bdXP0HudUcdj6toqeffrow9BZEIwiBD2rOrrVwymy4BsJJJ500rLPOOrF7k3N/JVvpr2Uahl/KzKBrJ5xwQgw+mFNhtM7OcYhG8GE//zzuc10rQ1ua08uZ1qkb3nffffkFh0kTJc0222zR/naUmAgBhT0UHZ1AInyhNu3E/m1/SOisrUp7bjWTzmJkgcip5Dy5BYcwhb/yivaYoVJIodGW9i233BKbQeQV/l555ZXh8ccfz9xU5qc6iWgOcFiN3IJjYHq+xPxM3P8L6RxlyvmeXGvOtttuG9Zff/0YUteMxmRJ7p8dN6Plku7Q3EdrtEV3yiKLLJL52szZ1ooG/yuL8s4tvKg97bPPPtHP2rZWQywmm2Jc84DAMQ1lkw+EzLWgb6/KwuEuIQw5snChQKOHZBqVhdFVJhGl6sDaa68drUVuKwRU2v66iIpzrAn9nuVGo7I8ZXSWy4QsFP/p0Szx3DqEQTvUBBxdOOarv7tTC5/pNIBBFFshEj/XF1544bD99tsXhl9jGpGB03ueEL5vfyW8XCKHbbbZJloK2xSlBAO4YuAQPM1wfkVBsbVVAjytT3KPN954ozCN6pAc6IILLohHR5xK+EyWj8aMyJKkH7Ik85Us6Xw++/ti0/UKk6ST1hx66KFx3qWYNFxRcACit3nPPfeMgyn2OQ6R7V1ggQXCaqutFg8iVYMsEHs1ElM2f/HFF4/VhNNPPz2MUWj4JzNpg7YLoXvmj765vOlLFaQhQ4ZEC6GGqGRVqtbgioLDrMnU9QO0Bo7VA0Sxv1Ulu68GHXzwwbHfWqSkrqXF1+tll1228ImMRn+RRXEXFF5UjoYPHx6Pq5ifbfhytAZXFBxH9awUe/xtDchnkSN9JnDiiSdGk1gpAr5amwZFdTZlGwmgLfGllloq3HTTTXE1V4PU7LbYYos4r3QsxHyLyWF8PF5wmCGTJezEBElNmwvfNeGxWtbFF18cB+Wz3vPZlmrtNaJlJrLzzjuHzz//PF4rl1SdlWuAMtlkk4Utt9wyXmdG+R7PsXj8VZmuNKmOr7TSSvH+xx57bLxWjjlL3CY4SYDO6av40giCV3a3QoCWAAJOr169oo3XG+A9jt9gaQZquYJ8x+fcU4DAJBJaOQmq+2hBIhjVZ2CP3WXNSNGRwGgr4O64447COx0nWih/0walhmaBoI4Ag9sEx8q/7LLL4oS19zijr/TttQaLTz75JK5KnyVo7UYmrlyRBKWJoi27m8Jvx9bTqvO3VNMjIhMAqHqrOhc7yKs4aj+f7xm70dcBco7o5ptvjn7MuDWwa781H/NqOddSuVVw3BzyohwT4k9w37594+pWgrc6EaED54knnoidJrZkaZDfA+AcDbatsoXvJ5Cvv/76WN4xWUL0Yw2OthNsW0RbbRHouZZDzTDDDPG8THPae++9430lhOWSeTg35DkaVNzPCbzrrrsuzgGX62NacqvgAGbgwIHRhyQHDzAPR4ARojJBCRwaQq3XWGONCFQCpZjGFOOkRe5lg2zDDTeMAk8C2GOPPSJ4NHTw4MHjhOEDBgyIB2iZFhrOtzBvzU2kfRXhrZAacEr5aT5tkUXpvJAwXBkmWY8VV1wxjkcHELPa1gIsh1sFx6D1hAlD7VoSWHqPANlu4FDtBM61114bzZqqM9Pic+WsIpNMQrMPwz/oO3BEhG8iHP6JhonGEgnPNRoyuVqnCC9FhomYO5VxwvU5xci2iK/1XPMiC9GeLXZNgggolTBhxbhNcCRyzJOBJOeWJrvjjjtGW2tgySxx7MwJ4VmhnLDPe68ckHzHcxNQTiPQELuZtAQ4Y7P+jDQL8jvrrbde1DwBilX9s63nFtS9e/fo5wi3LXLyASh8mfsZV9LuaoGSuFVwrF5byepC+q6QiRgYm8vcSfAQIbrOx/A3wlamzeqUY6SznR2ZjAXgOYSCtt566/gjRM1J/UrVgd/baaedorYtueSSMcAQ5pZDtJHWmEPSkvaa6Y5yq+AYAIGnY9x6zM4444xozpw7mX766eMqTuYOyfYJJhX5TjnllDgx3zdJk2v5nHLYuLbbbruw1lprjdUqpXlgcdRMEUCwHxkSMfJZfE6pJNDgR2lNpX3K+LhVcDABI5rDXDEZQCGUZHPTKiKwK664IoaTOjHTCmcaZcwcaiXB4TfSHgmiGRaBnAwZO01D/BKAmFtOvZRzQEr+wNHJkytwCD4BJOdQzFPdJfiWfoS6I8AkEHzfvxNXykYncJjOBI78QmQn7yhG8iCnzAAIKNUDxzMUaW0ft0ZAEVzcdddd8VnFxlMtbhOcxEAg2MTNQSn2uWrbZOAwswICrxFtoNlt5TDGJ/Kzx8LMCalFfTYAWyPRIqvBXHpuy7FUk9sFTt6YkE466aSYy8h3kIhMaD+2/3o8JNQXRvNVbZHnLbPMMrFryHOrvfCac12Cw7zce++9MedRPUh04IEHxjLT+HKXUknya5/KAqil36lLcPhByaT8Q/UikV5k19JPbFWK+DG+SsG3ln6nLsHhO7CcRomGn0OCEfUutTUmqFJkK0MQIW9CtTJtdQsO4at+W9HNtwYUNzl5BddKkrxKAKKe5/nFxlVprjtwEjByF2Ua4Kh3SXLVAmmS182PIzKD8iylGLuT7lMq2a+R49Ei92s5rmpw3YEjx/JzxMJmWwo6W0RpaoCSRVVp1fFEMnuFWCAKIOySKtaWSgIQ1Q6V+gY4RZitR7YSFD5TGO2v/MUGnf2mRK6rrQHNGVRVBJpXDgky1BlLPTrYEa4rcJLj11ZFY8ZHquQ0xlZGW6T3QJnG51S8E7tmL8dzJa58Dq1jVgFU7cCgLsAhBMIgFMISpemkGV+vgaq5/Z3xBQf8EBCbH7rCzJhfe6ep/I3CrnJO+mEK4wFcsTFXgusCHMAgZRYbf6lSLjJrK+F0UMtqT+awNXKulE+xx6NGh/1bx6b+N1vyggyv/WCf6FAAkn7buloaVBfgSPyA4LecRV2PPfZYNDMcvGBAd49KgY23sT84kZFfo6INkkiF13LI71jroVAqsnno/0dwUo7Jw6ha2lM34Pj74IMPRoEQkBWsvmYrWtXYdgATpLNT9dk2haq1Vc+R+1kWRxJtmjGH7gmw9JeALQD92z6n0Klr07kahU+LwaLwXOAYS/ppl/9rcFKEpVhJcwiGoJRrmBd7OMccc0ysDPiRIxGaaM6paedPhdL6BZR2gKWKoCPUdbmSv8stt1zsFeBb+BqACDyA6nfTNKxgJRzM96BqlnPqAhxsdXLASDjLLwAmrWZmjUYRoOvYCvean/LjRzpuFDGBwVQltmNqF1UVQL+dw77+7wT5knv7vvu5v1DdeJCcq5oRW92Ag1PUlkwRYqJEcD179oxmRk80QID2yCOPjGXXCDeZxKQBiROIQLZ7m37LQIGVxqpGIwsEKNWM0hLXFTjNGVAERlCASloFOAASHuB07PAlzVmUpUkxMeGrmbmv+yUgkHvjdM+W46gm1y04LRlYfBMmSEIul4GQ7lXsWbXiCQacCZEb4OSYG+DkmBvg5Jgb4OSYG+DkmBvg5Jgb4OSWR4X/Asu1zr9odBI4AAAAAElFTkSuQmCC" height = "150"> </td>');
        // pdf display works
        //$("#displayDocument" + data.mData.messageId).prepend('<td> <embed id = "Display-document" src="https://coursesite.lehigh.edu/pluginfile.php/2162525/mod_resource/content/1/syllabus340-2018.pdf" width = "500" height = "375" type ="application/pdf"> </td>');

        if (data.mData.type == "image/jpeg")
        {
            if(data.mData.encoded == "google drive error")
            {
                console.log("should display an image, but backend have issue connecting with google drive, so web will display default image for now");
                $("#displayDocument" + data.mData.messageId).prepend('<td> <img id = "Display-image" src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAGcAAABZCAYAAAAjMTokAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABCuSURBVHhe7d0FkFzFFgbg4O7uwd3d3Z3CHu7u7hQOBVRB4VK4FB6ch0sgQIgCgbxAIIGgAUIgQIDQ736906nNvtnNzuzM7J28OVWnNnNn5t7u8/fRPj3pMmrUqNDgfHIDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBzzBAfO77//HkaPHh3+/PPPyH///Xf4559/2mSfSZ/H7lHs3rXmugfnt99+C3/88UcU8JgxY6Kwf/jhh/Dhhx+GF154Idxwww3htNNOC/vvv3/Yb7/94t/m/z755JPD1VdfHZ588snQv3//8O2330Zw3cs9/dszij272ly34Fjdf/31VwSD8F599dVw/vnnh3XXXTfMM888YaKJJgqTTDJJmHHGGcNcc80VFlxwwbDQQgv9D88777xhlllmCZNPPnno0qVLmHXWWcMqq6wSjj766PDMM89EoJN2WQTFxlItrjtwkpZY0T169AhHHnlkFPDEE08cpptuurDmmmuGww47LNx4441RuH369AlDhw4NI0eOLHq/r7/+OmrZiy++GO6+++6oZZtuumkEFMCA22GHHcLDDz8cfvrppwhUrbSpbsBJmgKY5557LmywwQZxpdOMXXbZJdx2223ho48+iu9XggDarVu3cMQRR0QN86zFFlssXHrppeHHH3+MIFVbk+oCHEIgjH79+o0FhcAuvPDCCEi1iWl74IEHwmabbRa1iVZdddVV4ddff42LoVoBRO7BET2ha665Jkw99dRhzjnnDJdffnn46quv4vVakgXy/PPPh6222iouEL7p7bffjterAVBuwWHTRUzs/K677hqFYeV+/PHHBVF1Lt1///1h0UUXDVNMMUW49dZb4zULqdhcyuVcgpOA+fLLL8Pyyy8foy4mjCPOExnf5ptvHhfOqaeeGq9VEqDcgZOAGTRoUOjatWuMlp566qk48TwSc3b44YdHgORNqFIA5Q4cDpYD5vD5Fza9Huicc86pOEC5AseErESOduaZZw7vvPNOnGi90FlnnRUBOvPMM+PrjgYJuQHHRNAhhxwSE8o8m7K2SFkIQI8++miM4orNtb2cG3D4GfUtE7vkkksKU60/GjFiRFh11VWj5g8bNiwmzsXm2x7OBTipHDL//POHjTbaKJq3eqa33norTDXVVLFygco1b50ODlAQOy1n6N27d3xd76RGxwq8+eabZWtPp4Nj4N99912Ydtppw1FHHVWYWv2Tgup8880XNtlkk/i6HO3pdHA4zXPPPTdWlOU2ExKdd955UXvee++9mCIUm39b3Kng8DWKhwqJBxxwQGFKEw5ZbDPNNFM46KCD4iJkwovJoTXuVHBEaPfcc0+s9Nosaw+ptbHnu+++e6wUl0uet9dee4XjjjsufPPNN4WrlSdBwdxzzx2BKXWLodPASXWyFVZYIW6Qtbdudvzxx0cwp5xyylhBeP/99wvvtJ+++OKLsPTSS8fdT/fad999C+9Unm6//fZo2l5//fWSA4NOA4fWWPkGbqOsvaTQCBimcJpppol9AqWSiNAm3RxzzBEDkdVXX73wTuXJfhN/etFFF8U5F5NFa9wp4Mhj8BJLLBGrzvxOpGzwYeTIkIVvwp1x+fvvhTzh8W7dwvxdu8a9HdvHkr5SiXmxvQ3c2TPtsz2dLWsZZAjDh4/LmRnNBm0zp/Dt0sizVl555bDxxhvH16VEbTUHJ5Vp2Hpa89BDD8XXEZQBA0Lo0SOE7t1DZgfGZdfU2oYODR9l0c9z//53+OWXX5q+WwZZxfoGenuejbtevWSPIUtMxmXXPHfgwCaQyiD+0RY3f1lKUFBzcISU/ARgOORIVm2fPiG89BJPHcJrrxVn7/nM4MFN36sEZWCHV15p4tae7frLL4fQv79yc+GL7aezzz47lnPs/5RSra4pOFaNkHK33XYLs88+e/j000+bRg8cvQCEkIRUjL1HWP/5T5MJ7ChpBrGz2hycYuw94Ag+jLVEuvPOO2PgoS+ulKCgpuBYNd9nvkPdSdQ1DonWhg0L4YMPQujZs8mcNOfMlEWzxwQ1W70SvHfffTfee3zEt2lx0jaVzGvmFJq0p1+/EOwdJVOWmNnr2zeEzz4L2epq+k6J9Oyzz8bdXP0HudUcdj6toqeffrow9BZEIwiBD2rOrrVwymy4BsJJJ500rLPOOrF7k3N/JVvpr2Uahl/KzKBrJ5xwQgw+mFNhtM7OcYhG8GE//zzuc10rQ1ua08uZ1qkb3nffffkFh0kTJc0222zR/naUmAgBhT0UHZ1AInyhNu3E/m1/SOisrUp7bjWTzmJkgcip5Dy5BYcwhb/yivaYoVJIodGW9i233BKbQeQV/l555ZXh8ccfz9xU5qc6iWgOcFiN3IJjYHq+xPxM3P8L6RxlyvmeXGvOtttuG9Zff/0YUteMxmRJ7p8dN6Plku7Q3EdrtEV3yiKLLJL52szZ1ooG/yuL8s4tvKg97bPPPtHP2rZWQywmm2Jc84DAMQ1lkw+EzLWgb6/KwuEuIQw5snChQKOHZBqVhdFVJhGl6sDaa68drUVuKwRU2v66iIpzrAn9nuVGo7I8ZXSWy4QsFP/p0Szx3DqEQTvUBBxdOOarv7tTC5/pNIBBFFshEj/XF1544bD99tsXhl9jGpGB03ueEL5vfyW8XCKHbbbZJloK2xSlBAO4YuAQPM1wfkVBsbVVAjytT3KPN954ozCN6pAc6IILLohHR5xK+EyWj8aMyJKkH7Ik85Us6Xw++/ti0/UKk6ST1hx66KFx3qWYNFxRcACit3nPPfeMgyn2OQ6R7V1ggQXCaqutFg8iVYMsEHs1ElM2f/HFF4/VhNNPPz2MUWj4JzNpg7YLoXvmj765vOlLFaQhQ4ZEC6GGqGRVqtbgioLDrMnU9QO0Bo7VA0Sxv1Ulu68GHXzwwbHfWqSkrqXF1+tll1228ImMRn+RRXEXFF5UjoYPHx6Pq5ifbfhytAZXFBxH9awUe/xtDchnkSN9JnDiiSdGk1gpAr5amwZFdTZlGwmgLfGllloq3HTTTXE1V4PU7LbYYos4r3QsxHyLyWF8PF5wmCGTJezEBElNmwvfNeGxWtbFF18cB+Wz3vPZlmrtNaJlJrLzzjuHzz//PF4rl1SdlWuAMtlkk4Utt9wyXmdG+R7PsXj8VZmuNKmOr7TSSvH+xx57bLxWjjlL3CY4SYDO6av40giCV3a3QoCWAAJOr169oo3XG+A9jt9gaQZquYJ8x+fcU4DAJBJaOQmq+2hBIhjVZ2CP3WXNSNGRwGgr4O64447COx0nWih/0walhmaBoI4Ag9sEx8q/7LLL4oS19zijr/TttQaLTz75JK5KnyVo7UYmrlyRBKWJoi27m8Jvx9bTqvO3VNMjIhMAqHqrOhc7yKs4aj+f7xm70dcBco7o5ptvjn7MuDWwa781H/NqOddSuVVw3BzyohwT4k9w37594+pWgrc6EaED54knnoidJrZkaZDfA+AcDbatsoXvJ5Cvv/76WN4xWUL0Yw2OthNsW0RbbRHouZZDzTDDDPG8THPae++9430lhOWSeTg35DkaVNzPCbzrrrsuzgGX62NacqvgAGbgwIHRhyQHDzAPR4ARojJBCRwaQq3XWGONCFQCpZjGFOOkRe5lg2zDDTeMAk8C2GOPPSJ4NHTw4MHjhOEDBgyIB2iZFhrOtzBvzU2kfRXhrZAacEr5aT5tkUXpvJAwXBkmWY8VV1wxjkcHELPa1gIsh1sFx6D1hAlD7VoSWHqPANlu4FDtBM61114bzZqqM9Pic+WsIpNMQrMPwz/oO3BEhG8iHP6JhonGEgnPNRoyuVqnCC9FhomYO5VxwvU5xci2iK/1XPMiC9GeLXZNgggolTBhxbhNcCRyzJOBJOeWJrvjjjtGW2tgySxx7MwJ4VmhnLDPe68ckHzHcxNQTiPQELuZtAQ4Y7P+jDQL8jvrrbde1DwBilX9s63nFtS9e/fo5wi3LXLyASh8mfsZV9LuaoGSuFVwrF5byepC+q6QiRgYm8vcSfAQIbrOx/A3wlamzeqUY6SznR2ZjAXgOYSCtt566/gjRM1J/UrVgd/baaedorYtueSSMcAQ5pZDtJHWmEPSkvaa6Y5yq+AYAIGnY9x6zM4444xozpw7mX766eMqTuYOyfYJJhX5TjnllDgx3zdJk2v5nHLYuLbbbruw1lprjdUqpXlgcdRMEUCwHxkSMfJZfE6pJNDgR2lNpX3K+LhVcDABI5rDXDEZQCGUZHPTKiKwK664IoaTOjHTCmcaZcwcaiXB4TfSHgmiGRaBnAwZO01D/BKAmFtOvZRzQEr+wNHJkytwCD4BJOdQzFPdJfiWfoS6I8AkEHzfvxNXykYncJjOBI78QmQn7yhG8iCnzAAIKNUDxzMUaW0ft0ZAEVzcdddd8VnFxlMtbhOcxEAg2MTNQSn2uWrbZOAwswICrxFtoNlt5TDGJ/Kzx8LMCalFfTYAWyPRIqvBXHpuy7FUk9sFTt6YkE466aSYy8h3kIhMaD+2/3o8JNQXRvNVbZHnLbPMMrFryHOrvfCac12Cw7zce++9MedRPUh04IEHxjLT+HKXUknya5/KAqil36lLcPhByaT8Q/UikV5k19JPbFWK+DG+SsG3ln6nLsHhO7CcRomGn0OCEfUutTUmqFJkK0MQIW9CtTJtdQsO4at+W9HNtwYUNzl5BddKkrxKAKKe5/nFxlVprjtwEjByF2Ua4Kh3SXLVAmmS182PIzKD8iylGLuT7lMq2a+R49Ei92s5rmpw3YEjx/JzxMJmWwo6W0RpaoCSRVVp1fFEMnuFWCAKIOySKtaWSgIQ1Q6V+gY4RZitR7YSFD5TGO2v/MUGnf2mRK6rrQHNGVRVBJpXDgky1BlLPTrYEa4rcJLj11ZFY8ZHquQ0xlZGW6T3QJnG51S8E7tmL8dzJa58Dq1jVgFU7cCgLsAhBMIgFMISpemkGV+vgaq5/Z3xBQf8EBCbH7rCzJhfe6ep/I3CrnJO+mEK4wFcsTFXgusCHMAgZRYbf6lSLjJrK+F0UMtqT+awNXKulE+xx6NGh/1bx6b+N1vyggyv/WCf6FAAkn7buloaVBfgSPyA4LecRV2PPfZYNDMcvGBAd49KgY23sT84kZFfo6INkkiF13LI71jroVAqsnno/0dwUo7Jw6ha2lM34Pj74IMPRoEQkBWsvmYrWtXYdgATpLNT9dk2haq1Vc+R+1kWRxJtmjGH7gmw9JeALQD92z6n0Klr07kahU+LwaLwXOAYS/ppl/9rcFKEpVhJcwiGoJRrmBd7OMccc0ysDPiRIxGaaM6paedPhdL6BZR2gKWKoCPUdbmSv8stt1zsFeBb+BqACDyA6nfTNKxgJRzM96BqlnPqAhxsdXLASDjLLwAmrWZmjUYRoOvYCvean/LjRzpuFDGBwVQltmNqF1UVQL+dw77+7wT5knv7vvu5v1DdeJCcq5oRW92Ag1PUlkwRYqJEcD179oxmRk80QID2yCOPjGXXCDeZxKQBiROIQLZ7m37LQIGVxqpGIwsEKNWM0hLXFTjNGVAERlCASloFOAASHuB07PAlzVmUpUkxMeGrmbmv+yUgkHvjdM+W46gm1y04LRlYfBMmSEIul4GQ7lXsWbXiCQacCZEb4OSYG+DkmBvg5Jgb4OSYG+DkmBvg5Jgb4OSWR4X/Asu1zr9odBI4AAAAAElFTkSuQmCC" height = "150"> </td>');
            }
            else
            {
                $("#displayDocument" + data.mData.messageId).prepend('<td> <img id = "Display-image" src="data:image/jpeg;base64,'+data.mData.encoded + '" height = "150"> </td>');
            }
        }
        else if (data.mData.type == "application/pdf") 
        {
            if(data.mData.encoded == "google drive error")
            {
                console.log("should display a pdf, but backend have issue connecting with google drive, so web will display default image for now");
                $("#displayDocument" + data.mData.messageId).prepend('<td> <img id = "Display-image" src="data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAGcAAABZCAYAAAAjMTokAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABCuSURBVHhe7d0FkFzFFgbg4O7uwd3d3Z3CHu7u7hQOBVRB4VK4FB6ch0sgQIgCgbxAIIGgAUIgQIDQ736906nNvtnNzuzM7J28OVWnNnNn5t7u8/fRPj3pMmrUqNDgfHIDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBzzBAfO77//HkaPHh3+/PPPyH///Xf4559/2mSfSZ/H7lHs3rXmugfnt99+C3/88UcU8JgxY6Kwf/jhh/Dhhx+GF154Idxwww3htNNOC/vvv3/Yb7/94t/m/z755JPD1VdfHZ588snQv3//8O2330Zw3cs9/dszij272ly34Fjdf/31VwSD8F599dVw/vnnh3XXXTfMM888YaKJJgqTTDJJmHHGGcNcc80VFlxwwbDQQgv9D88777xhlllmCZNPPnno0qVLmHXWWcMqq6wSjj766PDMM89EoJN2WQTFxlItrjtwkpZY0T169AhHHnlkFPDEE08cpptuurDmmmuGww47LNx4441RuH369AlDhw4NI0eOLHq/r7/+OmrZiy++GO6+++6oZZtuumkEFMCA22GHHcLDDz8cfvrppwhUrbSpbsBJmgKY5557LmywwQZxpdOMXXbZJdx2223ho48+iu9XggDarVu3cMQRR0QN86zFFlssXHrppeHHH3+MIFVbk+oCHEIgjH79+o0FhcAuvPDCCEi1iWl74IEHwmabbRa1iVZdddVV4ddff42LoVoBRO7BET2ha665Jkw99dRhzjnnDJdffnn46quv4vVakgXy/PPPh6222iouEL7p7bffjterAVBuwWHTRUzs/K677hqFYeV+/PHHBVF1Lt1///1h0UUXDVNMMUW49dZb4zULqdhcyuVcgpOA+fLLL8Pyyy8foy4mjCPOExnf5ptvHhfOqaeeGq9VEqDcgZOAGTRoUOjatWuMlp566qk48TwSc3b44YdHgORNqFIA5Q4cDpYD5vD5Fza9Huicc86pOEC5AseErESOduaZZw7vvPNOnGi90FlnnRUBOvPMM+PrjgYJuQHHRNAhhxwSE8o8m7K2SFkIQI8++miM4orNtb2cG3D4GfUtE7vkkksKU60/GjFiRFh11VWj5g8bNiwmzsXm2x7OBTipHDL//POHjTbaKJq3eqa33norTDXVVLFygco1b50ODlAQOy1n6N27d3xd76RGxwq8+eabZWtPp4Nj4N99912Ydtppw1FHHVWYWv2Tgup8880XNtlkk/i6HO3pdHA4zXPPPTdWlOU2ExKdd955UXvee++9mCIUm39b3Kng8DWKhwqJBxxwQGFKEw5ZbDPNNFM46KCD4iJkwovJoTXuVHBEaPfcc0+s9Nosaw+ptbHnu+++e6wUl0uet9dee4XjjjsufPPNN4WrlSdBwdxzzx2BKXWLodPASXWyFVZYIW6Qtbdudvzxx0cwp5xyylhBeP/99wvvtJ+++OKLsPTSS8fdT/fad999C+9Unm6//fZo2l5//fWSA4NOA4fWWPkGbqOsvaTQCBimcJpppol9AqWSiNAm3RxzzBEDkdVXX73wTuXJfhN/etFFF8U5F5NFa9wp4Mhj8BJLLBGrzvxOpGzwYeTIkIVvwp1x+fvvhTzh8W7dwvxdu8a9HdvHkr5SiXmxvQ3c2TPtsz2dLWsZZAjDh4/LmRnNBm0zp/Dt0sizVl555bDxxhvH16VEbTUHJ5Vp2Hpa89BDD8XXEZQBA0Lo0SOE7t1DZgfGZdfU2oYODR9l0c9z//53+OWXX5q+WwZZxfoGenuejbtevWSPIUtMxmXXPHfgwCaQyiD+0RY3f1lKUFBzcISU/ARgOORIVm2fPiG89BJPHcJrrxVn7/nM4MFN36sEZWCHV15p4tae7frLL4fQv79yc+GL7aezzz47lnPs/5RSra4pOFaNkHK33XYLs88+e/j000+bRg8cvQCEkIRUjL1HWP/5T5MJ7ChpBrGz2hycYuw94Ag+jLVEuvPOO2PgoS+ulKCgpuBYNd9nvkPdSdQ1DonWhg0L4YMPQujZs8mcNOfMlEWzxwQ1W70SvHfffTfee3zEt2lx0jaVzGvmFJq0p1+/EOwdJVOWmNnr2zeEzz4L2epq+k6J9Oyzz8bdXP0HudUcdj6toqeffrow9BZEIwiBD2rOrrVwymy4BsJJJ500rLPOOrF7k3N/JVvpr2Uahl/KzKBrJ5xwQgw+mFNhtM7OcYhG8GE//zzuc10rQ1ua08uZ1qkb3nffffkFh0kTJc0222zR/naUmAgBhT0UHZ1AInyhNu3E/m1/SOisrUp7bjWTzmJkgcip5Dy5BYcwhb/yivaYoVJIodGW9i233BKbQeQV/l555ZXh8ccfz9xU5qc6iWgOcFiN3IJjYHq+xPxM3P8L6RxlyvmeXGvOtttuG9Zff/0YUteMxmRJ7p8dN6Plku7Q3EdrtEV3yiKLLJL52szZ1ooG/yuL8s4tvKg97bPPPtHP2rZWQywmm2Jc84DAMQ1lkw+EzLWgb6/KwuEuIQw5snChQKOHZBqVhdFVJhGl6sDaa68drUVuKwRU2v66iIpzrAn9nuVGo7I8ZXSWy4QsFP/p0Szx3DqEQTvUBBxdOOarv7tTC5/pNIBBFFshEj/XF1544bD99tsXhl9jGpGB03ueEL5vfyW8XCKHbbbZJloK2xSlBAO4YuAQPM1wfkVBsbVVAjytT3KPN954ozCN6pAc6IILLohHR5xK+EyWj8aMyJKkH7Ik85Us6Xw++/ti0/UKk6ST1hx66KFx3qWYNFxRcACit3nPPfeMgyn2OQ6R7V1ggQXCaqutFg8iVYMsEHs1ElM2f/HFF4/VhNNPPz2MUWj4JzNpg7YLoXvmj765vOlLFaQhQ4ZEC6GGqGRVqtbgioLDrMnU9QO0Bo7VA0Sxv1Ulu68GHXzwwbHfWqSkrqXF1+tll1228ImMRn+RRXEXFF5UjoYPHx6Pq5ifbfhytAZXFBxH9awUe/xtDchnkSN9JnDiiSdGk1gpAr5amwZFdTZlGwmgLfGllloq3HTTTXE1V4PU7LbYYos4r3QsxHyLyWF8PF5wmCGTJezEBElNmwvfNeGxWtbFF18cB+Wz3vPZlmrtNaJlJrLzzjuHzz//PF4rl1SdlWuAMtlkk4Utt9wyXmdG+R7PsXj8VZmuNKmOr7TSSvH+xx57bLxWjjlL3CY4SYDO6av40giCV3a3QoCWAAJOr169oo3XG+A9jt9gaQZquYJ8x+fcU4DAJBJaOQmq+2hBIhjVZ2CP3WXNSNGRwGgr4O64447COx0nWih/0walhmaBoI4Ag9sEx8q/7LLL4oS19zijr/TttQaLTz75JK5KnyVo7UYmrlyRBKWJoi27m8Jvx9bTqvO3VNMjIhMAqHqrOhc7yKs4aj+f7xm70dcBco7o5ptvjn7MuDWwa781H/NqOddSuVVw3BzyohwT4k9w37594+pWgrc6EaED54knnoidJrZkaZDfA+AcDbatsoXvJ5Cvv/76WN4xWUL0Yw2OthNsW0RbbRHouZZDzTDDDPG8THPae++9430lhOWSeTg35DkaVNzPCbzrrrsuzgGX62NacqvgAGbgwIHRhyQHDzAPR4ARojJBCRwaQq3XWGONCFQCpZjGFOOkRe5lg2zDDTeMAk8C2GOPPSJ4NHTw4MHjhOEDBgyIB2iZFhrOtzBvzU2kfRXhrZAacEr5aT5tkUXpvJAwXBkmWY8VV1wxjkcHELPa1gIsh1sFx6D1hAlD7VoSWHqPANlu4FDtBM61114bzZqqM9Pic+WsIpNMQrMPwz/oO3BEhG8iHP6JhonGEgnPNRoyuVqnCC9FhomYO5VxwvU5xci2iK/1XPMiC9GeLXZNgggolTBhxbhNcCRyzJOBJOeWJrvjjjtGW2tgySxx7MwJ4VmhnLDPe68ckHzHcxNQTiPQELuZtAQ4Y7P+jDQL8jvrrbde1DwBilX9s63nFtS9e/fo5wi3LXLyASh8mfsZV9LuaoGSuFVwrF5byepC+q6QiRgYm8vcSfAQIbrOx/A3wlamzeqUY6SznR2ZjAXgOYSCtt566/gjRM1J/UrVgd/baaedorYtueSSMcAQ5pZDtJHWmEPSkvaa6Y5yq+AYAIGnY9x6zM4444xozpw7mX766eMqTuYOyfYJJhX5TjnllDgx3zdJk2v5nHLYuLbbbruw1lprjdUqpXlgcdRMEUCwHxkSMfJZfE6pJNDgR2lNpX3K+LhVcDABI5rDXDEZQCGUZHPTKiKwK664IoaTOjHTCmcaZcwcaiXB4TfSHgmiGRaBnAwZO01D/BKAmFtOvZRzQEr+wNHJkytwCD4BJOdQzFPdJfiWfoS6I8AkEHzfvxNXykYncJjOBI78QmQn7yhG8iCnzAAIKNUDxzMUaW0ft0ZAEVzcdddd8VnFxlMtbhOcxEAg2MTNQSn2uWrbZOAwswICrxFtoNlt5TDGJ/Kzx8LMCalFfTYAWyPRIqvBXHpuy7FUk9sFTt6YkE466aSYy8h3kIhMaD+2/3o8JNQXRvNVbZHnLbPMMrFryHOrvfCac12Cw7zce++9MedRPUh04IEHxjLT+HKXUknya5/KAqil36lLcPhByaT8Q/UikV5k19JPbFWK+DG+SsG3ln6nLsHhO7CcRomGn0OCEfUutTUmqFJkK0MQIW9CtTJtdQsO4at+W9HNtwYUNzl5BddKkrxKAKKe5/nFxlVprjtwEjByF2Ua4Kh3SXLVAmmS182PIzKD8iylGLuT7lMq2a+R49Ei92s5rmpw3YEjx/JzxMJmWwo6W0RpaoCSRVVp1fFEMnuFWCAKIOySKtaWSgIQ1Q6V+gY4RZitR7YSFD5TGO2v/MUGnf2mRK6rrQHNGVRVBJpXDgky1BlLPTrYEa4rcJLj11ZFY8ZHquQ0xlZGW6T3QJnG51S8E7tmL8dzJa58Dq1jVgFU7cCgLsAhBMIgFMISpemkGV+vgaq5/Z3xBQf8EBCbH7rCzJhfe6ep/I3CrnJO+mEK4wFcsTFXgusCHMAgZRYbf6lSLjJrK+F0UMtqT+awNXKulE+xx6NGh/1bx6b+N1vyggyv/WCf6FAAkn7buloaVBfgSPyA4LecRV2PPfZYNDMcvGBAd49KgY23sT84kZFfo6INkkiF13LI71jroVAqsnno/0dwUo7Jw6ha2lM34Pj74IMPRoEQkBWsvmYrWtXYdgATpLNT9dk2haq1Vc+R+1kWRxJtmjGH7gmw9JeALQD92z6n0Klr07kahU+LwaLwXOAYS/ppl/9rcFKEpVhJcwiGoJRrmBd7OMccc0ysDPiRIxGaaM6paedPhdL6BZR2gKWKoCPUdbmSv8stt1zsFeBb+BqACDyA6nfTNKxgJRzM96BqlnPqAhxsdXLASDjLLwAmrWZmjUYRoOvYCvean/LjRzpuFDGBwVQltmNqF1UVQL+dw77+7wT5knv7vvu5v1DdeJCcq5oRW92Ag1PUlkwRYqJEcD179oxmRk80QID2yCOPjGXXCDeZxKQBiROIQLZ7m37LQIGVxqpGIwsEKNWM0hLXFTjNGVAERlCASloFOAASHuB07PAlzVmUpUkxMeGrmbmv+yUgkHvjdM+W46gm1y04LRlYfBMmSEIul4GQ7lXsWbXiCQacCZEb4OSYG+DkmBvg5Jgb4OSYG+DkmBvg5Jgb4OSWR4X/Asu1zr9odBI4AAAAAElFTkSuQmCC" height = "150"> </td>');
            }
            else
            {
                $("#displayDocument" + data.mData.messageId).prepend('<td> <embed id = "Display-document" src="data:application/pdf;base64,'+ data.mData.encoded +'" width = "500" height = "375" type ="application/pdf"> </td>');
            }
        }
        else if (data.mData.external_url != null)    // external link will display the external url
        {
            $("#displayDocument" + data.mData.messageId).prepend('<td> <a href="'+ data.mData.external_url +'"> ' + data.mData.external_url +' </a> </td>');
        }
        else
        {
            $("#displayDocument" + data.mData.messageId).prepend('<td> <p> no document </p> </td>');
        }
        
    }


    private static showMessage(data: any) {
        if (data.mStatus === "error") {
            console.log("get message mStatus error");
            window.alert("The server replied with an error:\n" + data.mMessage);
        }

        let count = 0;
        console.log(JSON.stringify(data));
        //remove old message
        $(".additionalClass").remove();
        
        //if the author of the message matches the user, render delete and edit button.
        if (data.mData.userId == Login.userId) {
            $("#firstRow" + data.mData.messageId).prepend(
                '<td><button style="height:30px;width:60px" class="additionalClass" id = "ElementList-editbtn' + data.mData.messageId + '" data-value="' + data.mData.messageId + '">Edit</button></td>');
            $("#secondRow" + data.mData.messageId).prepend(
                '<td><button style="height:30px;width:60px" class="additionalClass" id="ElementList-delbtn' + data.mData.messageId + '" data-value="' + data.mData.messageId + '">Delete</button></td> ');
            $('#ElementList-delbtn' + data.mData.messageId).click(ElementList.clickDelete);
            $('#ElementList-editbtn' + data.mData.messageId).click(ElementList.clickEdit);

        }
        //prepend messages
        $("#firstRow" + data.mData.messageId).prepend('<td class="additionalClass" id="vote' + data.mData.messageId + '">' + "Total upvote:" + data.mData.upVoteCount + "</td>");
        
        $("#secondRow" + data.mData.messageId).prepend('<td class="additionalClass" id="vote' + data.mData.messageId + '">' + "Total downvote:" + data.mData.downVoteCount + "</td>");
        $("#secondRow" + data.mData.messageId).prepend('<td class="additionalClass" id="content' + data.mData.messageId + '">' + "  Created by:" + data.mData.username + " on " + data.mData.dateCreated + "</td>");
        $("#firstRow" + data.mData.messageId).prepend('<td class="additionalClass" id="messageContents' + data.mData.messageId + '">' + "  " + data.mData.content
        + '<button id="messageVoice' + data.mData.messageId + '" class="additionalClass" data-value="' + data.mData.messageId + '">Listen</button></td>');
        $("#messageVoice" + data.mData.messageId).click(ElementList.clickTalk);
        $("#readDocument" + data.mData.messageId).prepend('<td class="additionalClass"></td>');
        $("#readDocument" + data.mData.messageId).prepend('<td class="additionalClass"></td>');
        $("#readDocument" + data.mData.messageId).prepend('<td class="additionalClass"><button id=display'+data.mData.messageId+'> Display Document </button></td>');
        
        // after click the button, display document
        let button:any = document.getElementById("display"+data.mData.messageId);
        button.onclick = function() {
            if(count == 0)
            {
                ElementList.readDocument(data); //only click the button first time
                count = count + 1; 
            }
            if(count == 1)
            {
                button.style.display = 'none';  //hide the display button after document displayed
            }
        }

        //show comments
        Comments.refresh(data.mData.messageId);
    }

    

    /**
     * clickDelete is the code we run in response to a click of a delete button
     */
    private static clickDelete() {
        let id = $(this).data("value");
        $.ajax({
            type: "DELETE",
            url: "/messages/" + id,
            dataType: "json",
            headers: {
                'sessionKey': Login.sessionKey,
                'username': Login.username
            },
            /*data: JSON.stringify({
                sessionKey: Login.sessionKey,
                username:Login.username
            }),*/
            // TODO: we should really have a function that looks at the return
            //       value and possibly prints an error message.
            success: ElementList.checkError
        });
    }

    /**
     * clickEdit is the code we run in response to a click of a edit button
     */
    private static clickEdit() {
        // as in clickedit, we need the ID of the row
        //wer are getting the message info, then update it in EditEntryForm.get
        let id = $(this).data("value");
        console.log("Click Edit in Element List.");
        $.ajax({
            type: "Get",
            url: "/messages/" + id,
            dataType: "json",
            headers: {
                'sessionKey': Login.sessionKey,
                'username': Login.username
            },
            success: EditEntryForm.get
        });
    }

    /**
     * clickUpVote is the code we run in response to the click of an upvote (like) button
     */
    private static clickUpVote() {
        let id = $(this).data("value");
        $.ajax({
            type: "PUT",
            url: "/vote/message",
            dataType: "json",
            data: JSON.stringify({ sessionKey: Login.sessionKey, username: Login.username, voteStatus: true, messageId: id }),
            success: ElementList.checkError
        });
    }
    /**
    * clickDownVote is the code we run in response to the click of a downvote (dislike) button
    */
    private static clickDownVote() {
        let id = $(this).data("value");
        $.ajax({
            type: "PUT",
            url: "/vote/message",
            dataType: "json",
            data: JSON.stringify({ sessionKey: Login.sessionKey, username: Login.username, voteStatus: false, messageId: id }),
            success: ElementList.checkError
        });
    }

    private static clickTalk(){
        let id = $(this).data("value");
        $("#messageVoice" + id).remove();
        $.ajax({
            type: "GET",
            url: "/messages/voice/" + id,
            dataType: "json",
            headers: {
                'sessionKey': Login.sessionKey,
                'username': Login.username
            },
            success: ElementList.appendMessageVoice
        });
    }


    private static appendMessageVoice(data:any){
        console.log("voice route worked, voice data:");
        if(data.mStatus === "ok"){
            $("#messageContents" + data.mData.messageId).append('<audio controls><source src="' + data.mData.audioContent + '" type="audio/mpeg"></audio>');
            //$("#messageVoice" + data.mData.messageId).remove();
        }
        else ElementList.checkError(data);
    }

    private static checkError(data: any) {
        if (data.mStatus === "ok") {
            ElementList.refresh();
        } else if (data.mStatus === "error") {
            console.log("vote mStatus error");
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Get ElementList: Unspecified error");
        }
    }
}