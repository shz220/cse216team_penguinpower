
/**
 * The Profile Singleton provides a way of displaying all of the data 
 * stored on the server as an HTML table.
 */
class Profile {
    /**
     * The name of the DOM entry associated with Profile
     */
    private static readonly NAME = "Profile";

    /**
     * Track if the Singleton has been initialized
     */
    private static isInit = false;

    /**
     * Initialize the Profile singleton by creating its element in the DOM.
     * This needs to be called from any public static method, to ensure that the 
     * Singleton is initialized before use.
     */
    private static init() {
        if (!Profile.isInit) {
            Profile.isInit = true;
        }
    }

    /**
     * refresh() is the public method for updating the Profile
     */
    public static refresh() {
        // Make sure the singleton is initialized
        Profile.init();
    }

    public static show() {
        if (!Login.isLogIn) {
            window.alert("You are not signed in.");
            return;
        }
        Profile.update();
    }

    private static hide() {
        $("#" + Profile.NAME).modal("hide");
    }
    /**
     * update() is the private method used by refresh() to update the 
     * Profile
     */
    private static update() {
        $.ajax({
            type: 'POST',
            url: "/users/info",
            dataType: "json",
            data: JSON.stringify({
                'sessionKey': Login.sessionKey,
                'username': Login.username,
                'usernameAskedFor':Login.username
            }),
            success: onSuccess,
            error: function () {
                window.alert("Internet Connection Error");
            }
        });
        function onSuccess(data: any) {
            console.log(JSON.stringify(data));
            if (data.mStatus === "error") {
                window.alert("error from database: " + data.mMessage);
                return;
            }
            // Remove the table of data, if it exists
            $("#" + Profile.NAME).remove();
            // Use a template to re-generate a table from the provided data, and put 
            // the table into our messageList element.
            $("body").append(Handlebars.templates[Profile.NAME + ".hb"]());
            $("#user-Profile").prepend('<img src ="' + data.mData.picture + '" style="width:104px;height:104px;"> </img>');
            $("#user-Profile").prepend('<p> first name: ' + data.mData.givenName + '</p>');
            $("#user-Profile").prepend('<p> last name: ' + data.mData.familyName + '</p>');
            $("#user-Profile").prepend('<p> email: ' + data.mData.email + '</p>');
            $("#Profile-confirm").click(Profile.hide);
            $("#" + Profile.NAME).modal("show");
        }
    }

}