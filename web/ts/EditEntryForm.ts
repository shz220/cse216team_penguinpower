

// a global for the EditEntryForm of the program.  See newEntryForm for 
// explanation
var editEntryForm: EditEntryForm;

/**
 * EditEntryForm encapsulates all of the code for the form for editing an entry
 */
class EditEntryForm {
   
    
    /**
     * The name of the DOM entry associated with EditEntryForm
     */
    private static readonly NAME = "EditEntryForm";

    /**
     * Track if the Singleton has been initialized
     */
    private static isInit = false;
    
    private static init(){
        if(!EditEntryForm.isInit){
            $("body").append(Handlebars.templates[EditEntryForm.NAME + ".hb"]());
            $("#" + EditEntryForm.NAME + "-OK").click(EditEntryForm.submitForm);
            $("#" + EditEntryForm.NAME + "-Cancel").click(EditEntryForm.hide);
            EditEntryForm.isInit = true; 
        }
    }

    public static refresh(){
        EditEntryForm.init();
    }
    /**
     * get() is called from the AJAX GET in clickEdit in ElementList.ts, and should populate the form if and 
     * only if the GET did not have an error
     */
    public static get(data: any) {
        if (data.mStatus === "ok") {
           
            $("#" + EditEntryForm.NAME + "-title").val(data.mData.title);
            $("#" + EditEntryForm.NAME + "-message").val(data.mData.content);
            
            // TODO: add back after backend works
            $("#" + EditEntryForm.NAME + "-link").val(data.mData.external_url);
            $("#" + EditEntryForm.NAME + "-upload").val();

            $("#" + EditEntryForm.NAME + "-OK").data("value", data.mData.messageId);
            // show the edit form
            $("#" + EditEntryForm.NAME).modal("show");
        
        }
        else if (data.mStatus === "error") {
            console.log("get in EditEntryForm");
            window.alert("Error: " + data.mMessage);
        }
        else {
            window.alert("An unspecified error occurred");
        }
    }

    /**
     * Hide the EditEntryForm, clearing its fields first
     */
    private static hide(){
        $("#" + EditEntryForm.NAME + "-title").val("");
        $("#" + EditEntryForm.NAME + "-message").val("");
        $("#" + EditEntryForm.NAME).modal("hide");
    }

    

    /**
     * Check if the input fields are both valid, and if so, do an AJAX call.
     */
    private static submitForm() {
        // get the values of the two fields, force them to be strings, and check 
        // that neither is empty
        let title = "" + $("#" + EditEntryForm.NAME + "-title").val();
        let msg = "" + $("#" + EditEntryForm.NAME + "-message").val();
        let link = "" + $("#" + EditEntryForm.NAME + "-link").val();
        console.log("link is: " + link);

        var base64 = "";
        if (link != ""){
            let username = Login.username;
            // NB: we assume that the user didn't modify the value of #editId
            let id = "" + $("#" + EditEntryForm.NAME + "-OK").data("value");
            if (title === "" || msg === "") {
                window.alert("Error: title or message is not valid");
                return;
            }
            EditEntryForm.hide();
            // set up an AJAX post.  When the server replies, the result will go to
            // onSubmitResponse
            console.log("in submit form in editentryForm");
            $.ajax({
                type: "PUT",
                url: "/messages/"+id,
                dataType: "json",
                data: JSON.stringify({
                    sessionKey: Login.sessionKey,
                    username:Login.username,
                    title:title,
                    content:msg,
                    messageId:id,
                    external_url:link
                }),
                success: EditEntryForm.onSubmitResponse
            });
        }
        else
        {   //link is not empty
            // get base64 String of uploaded file
            var input : any = document.getElementById("EditEntryForm-upload");
            var selectedFile = input.files;
            //Check File is not Empty
            if (selectedFile.length > 0) {
                // Select the very first file from list
                var fileToLoad = selectedFile[0];
                // FileReader function for read the file.
                var fileReader = new FileReader();
                // Onload of file read the file content
                fileReader.onload = function(fileLoadedEvent: any) {
                    base64 = fileLoadedEvent.target.result;
                    // Print data in console
                    console.log("The base 64 String is: "+base64);
                    
                    let username = Login.username;
                    // NB: we assume that the user didn't modify the value of #editId
                    let id = "" + $("#" + EditEntryForm.NAME + "-OK").data("value");
                    if (title === "" || msg === "") {
                        window.alert("Error: title or message is not valid");
                        return;
                    }
                    EditEntryForm.hide();
                    // set up an AJAX post.  When the server replies, the result will go to
                    // onSubmitResponse
                    console.log("in submit form in editentryForm");
                    $.ajax({
                        type: "PUT",
                        url: "/messages/"+id,
                        dataType: "json",
                        data: JSON.stringify({
                            sessionKey: Login.sessionKey,
                            username:Login.username,
                            title:title,
                            content:msg,
                            messageId:id,
                            document:base64
                        }),
                        success: EditEntryForm.onSubmitResponse
                    });
                };
                // Convert data to base64
                fileReader.readAsDataURL(fileToLoad);
            }


            let username = Login.username;
            // NB: we assume that the user didn't modify the value of #editId
            let id = "" + $("#" + EditEntryForm.NAME + "-OK").data("value");
            if (title === "" || msg === "") {
                window.alert("Error: title or message is not valid");
                return;
            }
            EditEntryForm.hide();
            // set up an AJAX post.  When the server replies, the result will go to
            // onSubmitResponse
            console.log("in submit form in editentryForm");
            $.ajax({
                type: "PUT",
                url: "/messages/"+id,
                dataType: "json",
                data: JSON.stringify({
                    sessionKey: Login.sessionKey,
                    username:Login.username,
                    title:title,
                    content:msg,
                    messageId:id
                }),
                success: EditEntryForm.onSubmitResponse
            });
        }
    }

    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a 
     * result.
     * 
     * @param data The object returned by the server
     */
    private static onSubmitResponse(data: any) {
        // If we get an "ok" message, clear the form and refresh the main 
        // listing of messages
        if (data.mStatus === "ok") {
            console.log("mstatus is ok onSumbitReponse in editentryForm");
            ElementList.refresh();
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
    }
} // end class EditEntryForm
