// "any", so that we can use it anywhere, and assume it has any fields or
// methods, without the compiler producing an error.
// a global for the EditComment of the program.  See newEntryForm for 
// explanation
var editComment: EditComment;

/**
 * EditComment encapsulates all of the code for the form for editing an entry
 */
class EditComment {
   
    
    /**
     * The name of the DOM entry associated with EditComment
     */
    private static readonly NAME = "EditComment";
    private static messageId:number;

    /**
     * Track if the Singleton has been initialized
     */
    private static isInit = false;
    
    private static init(){
        if(!EditComment.isInit){
            $("body").append(Handlebars.templates[EditComment.NAME + ".hb"]());
            $("#" + EditComment.NAME + "-OK").click(EditComment.submitForm);
            $("#" + EditComment.NAME + "-Cancel").click(EditComment.hide);
            EditComment.isInit = true; 
        }
    }

    public static refresh(){
        EditComment.init();
    }
    /**
     * get() is called from the AJAX GET in clickEdit in ElementList.ts, and should populate the form if and 
     * only if the GET did not have an error
     */
    public static get(id: number,text:string) {
        $("#" + EditComment.NAME + "-commentText").val(text);
        $("#" + EditComment.NAME + "-OK").data("value", id);
        // show the edit form
        $("#" + EditComment.NAME).modal("show");
        
        EditComment.messageId=id;
    }

    /**
     * Hide the EditComment, clearing its fields first
     */
    private static hide(){
        $("#" + EditComment.NAME + "-message").val("");
        $("#" + EditComment.NAME + "-commenText").val("");
        $("#" + EditComment.NAME).modal("hide");
    }

    

    /**
     * Check if the input fields are both valid, and if so, do an AJAX call.
     */
    private static submitForm() {
        // get the values of the two fields, force them to be strings, and check 
        // that neither is empty
        let msg = "" + $("#" + EditComment.NAME + "-commentText").val();
        let username =Login.username;
        // NB: we assume that the user didn't modify the value of #editId
        let id = "" + $("#" + EditComment.NAME + "-OK").data("value");
        let link = "" + $("#" + EditComment.NAME + "-link").data("");
        console.log("link for comment is: " + link);

        var base64 = "";
        if (link != ""){        //have link
            if ( msg === "") {
                window.alert("Error:comment is not valid");
                return;
            }
            EditComment.hide();
            $.ajax({
                type: "PUT",
                url: "/comments",
                dataType: "json",
                //to add user id also put mU_id: U_id in the Json string 
                data: JSON.stringify({ 
                    username:username, 
                    sessionKey:Login.sessionKey,
                    commentId:id, 
                    content: msg,
                    external_url: link
                }),
                success: EditComment.onSubmitResponse
            });
        }
        else    //no link
        {
            // get base64 String of uploaded file
            var input : any = document.getElementById("EditComment-upload");
            var selectedFile = input.files;
            //Check File is not Empty
            if (selectedFile.length > 0) {
                // Select the very first file from list
                var fileToLoad = selectedFile[0];
                // FileReader function for read the file.
                var fileReader = new FileReader();
                // Onload of file read the file content
                fileReader.onload = function(fileLoadedEvent: any) {
                    base64 = fileLoadedEvent.target.result;
                    // Print data in console
                    console.log("The file upload for comment is: "+base64);

                    if(base64 != "")
                    {
                        if ( msg === "") {
                            window.alert("Error:comment is not valid");
                            return;
                        }
                        EditComment.hide();
                        $.ajax({
                            type: "PUT",
                            url: "/comments",
                            dataType: "json",
                            //to add user id also put mU_id: U_id in the Json string 
                            data: JSON.stringify({ 
                                username:username, 
                                sessionKey:Login.sessionKey,
                                commentId:id, 
                                content: msg 
                            }),
                            success: EditComment.onSubmitResponse
                        });
                    }
                };
                fileReader.readAsDataURL(fileToLoad);
            }

            if (msg === "") {
                window.alert("Error:comment is not valid");
                return;
            }
            EditComment.hide();
            // set up an AJAX post.  When the server replies, the result will go to
            // onSubmitResponse
            $.ajax({
                type: "PUT",
                url: "/comments",
                dataType: "json",
                //to add user id also put mU_id: U_id in the Json string 
                data: JSON.stringify({ 
                    username:username, 
                    sessionKey:Login.sessionKey,
                    commentId:id, 
                    content: msg 
                }),
                success: EditComment.onSubmitResponse
            });
        }
    }

    /**
     * onSubmitResponse runs when the AJAX call in submitForm() returns a 
     * result.
     * 
     * @param data The object returned by the server
     */
    private static onSubmitResponse(data: any) {
        // If we get an "ok" message, clear the form and refresh the main 
        // listing of messages
        if (data.mStatus === "ok") {
            window.alert("Your comment has been successfully changed");
            Comments.refresh(EditComment.messageId);
            console.log("in onsumbitResponse in EditComments "+EditComment.messageId);
        }
        // Handle explicit errors with a detailed popup message
        else if (data.mStatus === "error") {
            window.alert("The server replied with an error:\n" + data.mMessage);
        }
        // Handle other errors with a less-detailed popup message
        else {
            window.alert("Unspecified error");
        }
    }
} // end class EditComment