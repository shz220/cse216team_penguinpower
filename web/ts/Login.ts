/**
 * Login encapsulates all of the code for the form for adding an entry
 */
class Login {

    /**
     * The name of the DOM entry associated with Login
     */
    private static readonly NAME = "Login";

    /**
     * Track if the Singleton has been initialized
     */
    private static isInit = false;

    public static sessionKey = "";
    public static username = "";
    public static userId: any;
    public static GoogleAuth: any;
    public static GoogleUser: any;

    public static isLogIn = false;

    /**
     * Initialize the Login by creating its element in the DOM and 
     * configuring its buttons.  This needs to be called from any public static 
     * method, to ensure that the Singleton is initialized before use
     */
    private static init() {
        if (Login.isInit == true) {
            return;
        }
        Login.isInit = true;
        $("body").append(Handlebars.templates[Login.NAME + ".hb"]());
        gapi.signin2.render('googleSignIn', {
            width: 480,
            height: 100,
            theme: 'dark',
            onsuccess: signedIn,
            onfailure: function () {
                window.alert("Google sign in failed.");
                Login.isLogIn = false;
                ElementList.refresh();
            }
        });
        function signedIn(googleUser: any) {
            Login.GoogleUser = googleUser;
            Login.GoogleAuth = gapi.auth2.getAuthInstance();
            Login.isLogIn = true;
            var id_token = googleUser.getAuthResponse().id_token;
            console.log("id_token = "+ id_token);
            Login.requestLogin(id_token);
        }
    }


    /**
     * Refresh() doesn't really have much meaning, but just like in sNavbar, we
     * have a refresh() method so that we don't have front-end code calling
     * init().
     */
    public static refresh() {
        Login.init();
    }

    /**
     * Hide the Login.  Be sure to clear its fields first
     */
    public static hide() {
        $("#googleSignIn").css("display", "none");
    }

    /**
     * Show the Login.  Be sure to clear its fields, because there are
     * ways of making a Bootstrap modal disapper without clicking Close, and
     * we haven't set up the hooks to clear the fields on the events associated
     * with those ways of making the modal disappear.
     */
    public static show() {
        $("#googleSignIn").css("display", "block");
    }

    public static getInit(): boolean {
        return Login.isInit;
    }



    /**
     * Send data to submit the form only if the fields are both valid.  
     * Immediately hide the form when we send data, so that the user knows that 
     * their click was received.
     */
    private static requestLogin(token: any) {
        //onSuccess runs when the AJAX call in submitForm() returns a result.
        function onSuccess(data: any) {
            // If we get an "ok" message, find the userId.
            if (data.mStatus === "ok") {
                Login.sessionKey = data.mData.sessionKey;
                Login.username = data.mData.username;
                Login.requestUserInfo(Login.sessionKey, Login.username);
            }
            // Handle explicit errors with a detailed popup message
            else if (data.mStatus === "error") {
                window.alert("The server replied with an error:\n" + data.mMessage);
                Login.GoogleAuth.signOut();
                Login.show();
            }
            // Handle other errors with a less-detailed popup message
            else {
                window.alert("Login: Unspecified error");
                Login.GoogleAuth.signOut();
                Login.show();
            }
        }

        // set up an AJAX post.  When the server replies, the result will go to onSuccess
        $.ajax({
            type: "POST",
            url: "/users/login",
            dataType: "json",
            data: JSON.stringify({ accessCode: token }),
            success: onSuccess,
            error: function () {
                alert("Internet Error: Cannot connect to the database.");
                Login.GoogleAuth.signOut();
                Login.show();
            }
        })
    }

    /**
     *  Get the user information. Specifically, get userId and store it in the local variable Login.userId.
     */
    private static requestUserInfo(sessionKey: any, username: any) {
        $.ajax({
            type: "POST",
            url: "/users/info",
            dataType: "json",
            data: JSON.stringify({ username: username, sessionKey: sessionKey, usernameAskedFor: username }),
            success: onSuccess,
            error: function () {
                alert("Internet Error: Cannot connect to the database.");
                Login.GoogleAuth.signOut();
                Login.isLogIn = false;
                ElementList.refresh();
                Login.show();
            }
        });
        function onSuccess(data: any) {
            // If we get an "ok" message, load all forms.
            if (data.mStatus === "ok") {
                Login.userId = data.mData.userId;
                console.log("user id = " + Login.userId);

                //get all the forms refreshed.
                ElementList.refresh();
                Logout.refresh();
                NewEntryForm.refresh();
                EditEntryForm.refresh();
                Profile.refresh();
                Login.hide();
            }
            // Handle explicit errors with a detailed popup message
            else if (data.mStatus === "error") {
                window.alert("The server replied with an error:\n" + data.mMessage);
                Login.GoogleAuth.signOut();
                Login.isLogIn = false;
                ElementList.refresh();
                Login.show();
            }
            // Handle other errors with a less-detailed popup message
            else {
                window.alert("Login: Unspecified error");
                Login.GoogleAuth.signOut();
                Login.isLogIn = false;
                ElementList.refresh();
                Login.show();
            }
        }
    }
}