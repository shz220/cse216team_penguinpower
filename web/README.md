# CSE216Team_PenguinPower Web
## Konka Shi in phase 5
* ElementList.ts & Comments.ts
Refactored to display the document of message and comment with Shirley's help
* Notes 
I am supposed to implement one more function, Googlemap in web, but I did not get enought time to finish it. 
However, our group do have one new function for both android and web. 
* Problem left in Web 
The user is not restricted to only upload either link or document for message and comment. 
We assume that the user only will choose one type of document to update. 

## Briana Brady in phase 5
* ElementList.ts 
Added a new functionality textToSpeech 

## Shengli Zhu
* NewEntryForm.ts:   
add functions for user to add an external link for a pdf or image   
add functions for user to upload a local file   
convert uploaded file to base64 string and send back to backend (console print out that string)   

* ElementList.ts:   
refactored the message list    
add functions for display document for each message (display either pdf/image/external link)    

* Comments.ts:    
add functions for user to add an external link for a pdf or image   
add functions for user to upload a local file   
convert uploaded file to base64 string and send back to backend (console print out that string)   
add functions for display document for each comment (display either pdf/image/external link)    
NOTE: for comments, we can only display the document directly. I tried to also give a "display document" button for each comment, but if we have a button, only the first time you click one button in that comments list for one message work.    
NOTE: now we can only add external_url successfully.    

* EditEntryForm & EditComments:    
add functions for user to edit their existing external link or local file, but need to have successful backend routes to work.    

* Fix last phase problem:    
1 cannot edit a comment problem solved    
2 "Your messages & Comments" are showing the correct messages and comments created by the user who login   3 if comments too short, an alert will pop up, but still added successfully. This issue is solved    

* existing problem:    
backend still cannot connect with google drive successfully, so the image or pdf cannot show up. But I have tested that default image and pdf all works.    
for comments, "comments created by" still shows userId instead of username, because backend don't have time to update that    

* NOTE: 
1 edit comments & edit message still have issue with edit external link or local file due to backend have some issue with the routes.     
2 if backend cannot upload file to google drive successfully, it will store "google drive error" to database as String document. If I get this as String encoded back, I will show the default image for now.

## Xuewei Wang
* Added Google Login Button
* Better Profile page
* Added the User Message & Comments
* Comments Session inside message
* error 1: edit comment button not working
* error 2: the author of messages some times will not be shown.
## To Run the Web Code
First merge the backend code into a branch with the web code (only once, not every time).
In the web folder, type sh deploy.sh. 
In the backend folder, type mvn package; mvn heroku:deploy.
Make sure you are logged into heroku before running mvn heroku:deploy.

