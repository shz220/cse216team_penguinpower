<div class="panel panel-default" id="ElementList">
    <div class="panel-heading">
        <h3 class="panel-title">All Messages & Comments</h3>
    </div>
    <table class="table">
        <col width="1200px" />
        <col width="250px" />
        <col width="250px" />
    {{#each mData}}
        <tbody class="message-show">
 
            <tr class="messageMain">
                
                <td class="ElementList-messageTitle" id="ElementList-messageTitle{{messageId}}" data-value="{{messageId}}" data-toggle="collapse" data-target="#messageMoreInfo-{{messageId}}" aria-expanded="false" aria-controls="messageMoreInfo-{{messageId}}">Title: {{this.title}}</td>

                <td class="ElementList-popularity">
                    <div id="ElementList-popularity{{messageId}}" data-value="{{popularity}}">Popularity: {{popularity}}</div>        
                </td>

                <td id="ElementList-votebtns">
                    <button class="ElementList-upVotebtn" data-value="{{this.messageId}}">
                        <span class="glyphicon glyphicon-arrow-up"></span>
                    </button>
                    <button class="ElementList-downVotebtn" data-value="{{this.messageId}}">
                        <span class="glyphicon glyphicon-arrow-down"></span>
                    </button>
                </td>
            </tr>
        </tbody>
        <tbody id="messageMoreInfo-{{messageId}}" class="collapse">
          
            <tr id="firstRow{{messageId}}"> </tr>
            <tr id="secondRow{{messageId}}"> </tr>
            <tr id="readDocument{{messageId}}">  </tr>
            <tr id="displayDocument{{messageId}}"> </tr>
            <tr id ="commentsRow{{messageId}}"></tr>      
        </tbody>
    {{/each}}
    </table>
</div>
