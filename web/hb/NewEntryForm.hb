<div id="NewEntryForm" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add a New Entry</h4>
            </div>
            <div class="modal-body">
                <label for="NewEntryForm-title">Title</label>
                <input class="form-control" type="text" id="NewEntryForm-title" />
                <label for="NewEntryForm-message">Message</label>
                <textarea class="form-control" id="NewEntryForm-message"></textarea>
                <label for="NewEntryForm-link">Link to a file:</label>
                <input class="form-control" type="text" id="NewEntryForm-link" />
                <label for="NewEntryForm-upload">Or upload file: </label>
                <input ng-model="csv" type="file" id="NewEntryForm-upload" multiple size="50" accept=".pdf,.jpeg">
                

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="NewEntryForm-OK">OK</button>
                <button type="button" class="btn btn-default" id="NewEntryForm-Close">Close</button>
            </div>


        </div>
    </div>
</div>
