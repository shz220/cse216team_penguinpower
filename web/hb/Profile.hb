<div id="Profile" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Profile</h4>
            </div>
            <div class="modal-body">
                <label for="Profile-confirm">Your Profile in Our Database</label>
                <div id="user-Profile"></div>
            </div>   
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="Profile-confirm">Confirm</button>
            </div>
        </div>
    </div>
</div>