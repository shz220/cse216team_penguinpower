<div id="Logout" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Logout page</h4>
            </div>
            <div class="modal-body">
                <label for="Logout-confirm">Are you sure you want to log out?</label>
            </div>   
            <div class="modal-footer">
                <button type="button" class="btn btn-default" id="Logout-yes">Yes</button>
                <button type="button" class="btn btn-default" id="Logout-cancel">Cancel</button>
            </div>
        </div>
    </div>
</div>
