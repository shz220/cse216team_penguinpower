<div class="panel panel-default" id="UserMessagesList">
    <div class="panel-heading">
        <h3 class="panel-title">Your Messages & Comments</h3>
    </div>
    <h4>Messages</h4>
    <div id="UserMessagesTable">
    </div>
    <h4>Comments</h4>
    <div>
    <table id="UserCommentsTable">
    </table>
    </div>
</div>
