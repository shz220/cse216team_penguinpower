<div id="EditEntryForm" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit an Entry</h4>
            </div>
            <div class="modal-body">
                <label for="EditEntryForm-title">Title</label>
                <input class="form-control" type="text" id="EditEntryForm-title" value= "" />
                <label for="EditEntryForm-message">Message</label>
                <textarea class="form-control" id="EditEntryForm-message"></textarea>
                <label for="EditEntryForm-link">Link to a file:</label>
                <input class="form-control" type="text" id="EditEntryForm-link" />
                <label for="EditEntryForm-upload">Or upload file: </label>
                <input ng-model="csv" type="file" id="EditEntryForm-upload" multiple size="50" accept=".pdf,.jpeg">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-value="" id="EditEntryForm-OK">OK</button>
                <button type="button" class="btn btn-default" id="EditEntryForm-Cancel">Cancel</button>
            </div>
        </div>
    </div>
</div>