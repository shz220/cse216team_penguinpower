<div id="EditComment" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Edit an Comment</h4>
            </div>
            <div class="modal-body">
                <label for="EditComment-commentText">Comment</label>
                <textarea class="form-control" id="EditComment-commentText"></textarea>
                <label for="EditComment-link">Link to a file:</label>
                <input class="form-control" type="text" id="EditComment-link" />
                <label for="EditComment-upload">Or upload file: </label>
                <input ng-model="csv" type="file" id="EditComment-upload" multiple size="50" accept=".pdf,.jpeg">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-value="" id="EditComment-OK">OK</button>
                <button type="button" class="btn btn-default" id="EditComment-Cancel">Cancel</button>
            </div>
        </div>
    </div>
</div>