<div class="panel panel-default" id="Comments">
    <div class="panel-heading">
        <h3 class="panel-title">Comments</h3>
    </div>
    <table class="table">
        
        <td>
        New Comment: <input class="form-control" type="text" id="Comments-newCommentText" placeholder="Comment" /> 
        Link to a file: <input class="form-control" type="text" id="Comments-link" placeholder="Link"/> 
        Or upload file: <input ng-model="csv" type="file" id="Comments-upload" multiple size="50" accept=".pdf,.jpeg">
        <br>
        <button id="Comments-newCommentbtn">Add New Comment</button>
        </td>

        {{#each mData}}   
        <tbody>
            <tr class="CommentMain">               
                <td class="Comments-CommentText" id="Comments-CommentText{{commentId}}" data-value="{{commentId}}" data-toggle="collapse" data-target="#commentMoreInfo-{{commentId}}" aria-expanded="false" aria-controls="commentMoreInfo-{{commentId}}">{{this.commentText}}</td>
                <td>Created by:{{username}}  on {{dateCreated}}</td>
            </tr>
        </tbody>
        
        <tbody id="commentButton-{{commentId}}">
          
            <tr id="contentEditDelete{{commentId}}">
                <td><button class="Comments-editbtn" id = "Comments-editbtn{{commentId}}" data-value="{{this.commentId}}" data-text="{{this.commentText}}" data-userId="{{userId}}">Edit</button></td>
                <td><button class="Comments-delbtn" id="Comments-delbtn{{commentId}}" data-value="{{this.commentId}}">Delete</button></td>  
            </tr>
            
        </tbody>
            <tr id="commentdisplayDocument{{commentId}}"> 

            </tr>
        <tbody>

        </tbody>
        {{/each}}
    </table>
</div>