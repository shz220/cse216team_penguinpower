/// <reference path="ts/EditEntryForm.ts"/>
/// <reference path="ts/NewEntryForm.ts"/>
/// <reference path="ts/ElementList.ts"/>
/// <reference path="ts/Navbar.ts"/>
/// <reference path="ts/UiTests.ts"/>
/// <reference path="ts/Login.ts"/>
/// <reference path="ts/Comments.ts"/>
/// <reference path="ts/Logout.ts"/>
/// <reference path="ts/Profile.ts"/>
/// <reference path="ts/EditComment.ts"/>
/// <reference path="ts/UserMessagesList.ts"/>
/// This constant indicates the path to our backend server

//const backendUrl = "https://cse216-penguin-power.herokuapp.com/";
// Prevent compiler errors
let Handlebars: any;
let $: any;


// Run some configuration code when the web page loads
$(document).ready(function () {

    Navbar.refresh();
    Login.refresh();
    //Login.show();
    Logout.refresh();
    NewEntryForm.refresh();
    ElementList.refresh();
    EditEntryForm.refresh();
    
    EditComment.refresh();
    Profile.refresh();
    window.onload = function () { UiTests.runTests() };


});

