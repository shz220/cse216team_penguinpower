# CSE216Team_PenguinPower
This is a team student repository. It is intended for use during phase 4.

## Details
- Semester: Fall 2018
- Student ID: shz220, kas320, yac320, bcb320, xuw220
- Bitbucket Repository:
https://shz220@bitbucket.org/shz220/cse216team_penguinpower.git

## Contributors
1. Shengli Zhu - Web Front-End
2. Kangjia Shi -  Back-End Server
3. Yan Lin Chen - Project Manager
4. Briana Brady - Android App
5. Xuewei Wang - The Admin App

## Main Functions and Issues
At this phase, we added the functionality of adding a link. We didn't get the google drive to work, so we are not 
able to show the custom image/pdf on our front-end. Also, backend didn't have memcashier implemented.
Some of the backend routes is not working. 
When added with a comment with a link first time, the comments without documents will be deleted. After, only a comment with a link/document is allowed to be added.
Put Route with message does not work with updating link.
Put Route with comment might have small issues.
Also, because we don't have google drive, android's cache functionality cannot be tested. 
Admin has a GUI interface implemented.But it doesn't manage the deletion of the files since there's no file. 