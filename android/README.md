# Android App
### phase 4: Briana Brady
##### important notes:
* App now has that ability to take and pick pictures to add to their message or comment (including editing). Requires user permission to access files and documents (app closes initially for this purpose). Save images taken to the gallery, so they can be accessed at anytime by the user.
* Fixed bug where users were unable to edit their own messages or comments. 
* Implements a cache system for storing photos locally. 
* Can now encode and decode images into Base64, images seen when running app are sample photos, not those from backend.
* Note that backend was not able to send and recieve photos by the due date, so post, put, and get methods will have to be adjusted accordingly. Some code should be commented out for that purpose, places to update have also been commented.

##### available pages so far:
no changes from last phase, all edited with added ImageView view for photos when relevant

* Login Page: 
Google Sign-in

* Main page: 
Main page displays each message on a card. Messages is displayed with title, creation date, and popularity. ordered by Message ID from greatest to smallest.

* Manage message page: 
This page contains: message ID, message title, message content, author id, creation date, up vote count, and down vote count. This page also contains buttons for up vote, down vote, edit message, and show comments.

* Add message page: 
Prompt the user for a new message.

* Comments home page: 
After clicking the show comments button, displays each comment on a card. Comments is displayed with content and creation date. ordered by Comments ID from greatest to smallest.

* Manage comment page: 
If the Message Id matches the user Id (who created this comment), display the editable message. The user could choose between edit and delete a message.

* Add comment page: 
Prompt the user for a new comment on one specific message.

##### available functionalities so far:
* take or pick photos to add to a post:
Opens Camera View or Gallery to take/choose a photo, and previews the photo on screen before uploading

* login: 
Google Sign-in

* logout:
Google Sign-out

* Check user's own profile page
The user can see his or her own basic information. 

* check out a message: 
Click at a card on the main page will send us to the message information page which cotains detailed information about that message.

* up vote/ down vote: 
Click up vote / down vote button on information view.

* create message: 
Click the plus button to go to the add message page and further create a message.

* edit / delete message: 
The first way to edit/ delete message is through the option button at the main page. After click Option>Manage Message, the user is sent to the manage message page.
Or User could click the edit button on the message information page. In this case, the Message ID is automatically filled up.
If you are the author of this message, you can go to edit/delete page, otherwise, a message will pop up "You are not the author for this message."

* create comment: 
Click the plus button on comments list page (a comment lists for one message page) to go to the add comment page and further create a comment for that message.

* edit / delete comment: 
Click one comment in the comments list can go to edit/delete a comment page. (if you are not the author for this comment, a message will pop up and you cannot start editing/deleting this comment)

##### available unit test so far:
* As the Backend has not been able to handle images at this point, no changes to the way this app connects to the server were made, new views were also not implemented, and so all previous tests should be sufficient for testing at this moment of time.
From last phase:

* Notes: All the tests don't work except for the LoginActivityTest. The Expresso could not record the process of google logging in, so all the recordded activitiy tests after logging in with google could not be passed through even though they work perfectly when actually running the app. 
 
* LoginActivityTest - google log in 

* CustomerProfileTest - user's infor

* LogoutActivityTest - google log out 

* HomePageUITest - list of messages

* ViewInfoFunctionalTest - test info on list of message

* MessageInfoUITest - test message detailed info displayed

* VoteFunctionalTest - test upvote and downvote

* MessageFunctionalTest_AddEditDelete - test all functions   related to messages: show messages list, add a new message, edit an existing message, delete a message.

* CommentFuntionalTest_AddEditDelete - test all functions related to comments: show comments list, add a new comment, edit an existing comment, delete a comment.

### phase 3: Konka Shi
##### important notes:
* LoginActivity adds google log-in and log-out features. The original way of entering username and password is deleted. 
* ChangePwdFunction is deleted. 

* Added CustomerProfileActivity. It shows the basic information of the user. 

##### available pages so far:
* Login Page: 
Google Sign-in

* Main page: 
Main page displays each message on a card. Messages is displayed with title, creation date, and popularity. ordered by Message ID from greatest to smallest.

* Manage message page: 
This page contains: message ID, message title, message content, author id, creation date, up vote count, and down vote count. This page also contains buttons for up vote, down vote, edit message, and show comments.

* Add message page: 
Prompt the user for a new message.

* Comments home page: 
After clicking the show comments button, displays each comment on a card. Comments is displayed with content and creation date. ordered by Comments ID from greatest to smallest.

* Manage comment page: 
If the Message Id matches the user Id (who created this comment), display the editable message. The user could choose between edit and delete a message.

* Add comment page: 
Prompt the user for a new comment on one specific message.


##### available functionalities so far:
* login: 
Google Sign-in

* logout:
Google Sign-out

* Check user's own profile page
The user can see his or her own basic information. 

* check out a message: 
Click at a card on the main page will send us to the message information page which cotains detailed information about that message.

* up vote/ down vote: 
Click up vote / down vote button on information view.

* create message: 
Click the plus button to go to the add message page and further create a message.

* edit / delete message: 
The first way to edit/ delete message is through the option button at the main page. After click Option>Manage Message, the user is sent to the manage message page.
Or User could click the edit button on the message information page. In this case, the Message ID is automatically filled up.
If you are the author of this message, you can go to edit/delete page, otherwise, a message will pop up "You are not the author for this message."

* create comment: 
Click the plus button on comments list page (a comment lists for one message page) to go to the add comment page and further create a comment for that message.

* edit / delete comment: 
Click one comment in the comments list can go to edit/delete a comment page. (if you are not the author for this comment, a message will pop up and you cannot start editing/deleting this comment)


##### available unit test so far:
Tested all pages and functions.
* Notes: All the tests don't work except for the LoginActivityTest. The Expresso could not record the process of google logging in, so all the recordded activitiy tests after logging in with google could not be passed through even though they work perfectly when actually running the app. 
 
* LoginActivityTest - google log in 

* CustomerProfileTest - user's infor

* LogoutActivityTest - google log out 

* HomePageUITest - list of messages

* ViewInfoFunctionalTest - test info on list of message

* MessageInfoUITest - test message detailed info displayed

* VoteFunctionalTest - test upvote and downvote

* MessageFunctionalTest_AddEditDelete - test all functions   related to messages: show messages list, add a new message, edit an existing message, delete a message.

* CommentFuntionalTest_AddEditDelete - test all functions related to comments: show comments list, add a new comment, edit an existing comment, delete a comment.


### phase 2: Shengli Zhu
##### important notes:
* Unit Tests connects to the real database right now, which means performing tests will result in messages being created/changed. We are planning to set up a mock server before or during next phase.

* ChangePwdFunctionalTest cannot run with other tests, because it will change the login password then other tests cannot login successfully.

##### available pages so far:
* Login Page: 
Login Page requires user to enter username and password before reach out to main page.

* Main page: 
Main page displays each message on a card. Messages is displayed with title, creation date, and popularity. ordered by Message ID from greatest to smallest.

* Manage message page: 
This page contains: message ID, message title, message content, author id, creation date, up vote count, and down vote count. This page also contains buttons for up vote, down vote, edit message, and show comments.

* Add message page: 
Prompt the user for a new message.

* Comments home page: 
After clicking the show comments button, displays each comment on a card. Comments is displayed with content and creation date. ordered by Comments ID from greatest to smallest.

* Manage comment page: 
If the Message Id matches the user Id (who created this comment), display the editable message. The user could choose between edit and delete a message.

* Add comment page: 
Prompt the user for a new comment on one specific message.


##### available functionalities so far:
* login: 
Login with correct username and password can generate sessionKey and reach out to main page.
Or Click Option>Login can login to another user account and automatically logout the current current account.

* change password:
Click Option>Change Password, type in username, old password and new password can update the password (when old password is correct).

* logout:
Click Option>Logout can logout current user and go back to login page where you can login again use same or different user account.

* check out a message: 
Click at a card on the main page will send us to the message information page which cotains detailed information about that message.

* up vote/ down vote: 
Click up vote / down vote button on information view.

* create message: 
Click the plus button to go to the add message page and further create a message.

* edit / delete message: 
The first way to edit/ delete message is through the option button at the main page. After click Option>Manage Message, the user is sent to the manage message page.
Or User could click the edit button on the message information page. In this case, the Message ID is automatically filled up.
If you are the author of this message, you can go to edit/delete page, otherwise, a message will pop up "You are not the author for this message."

* create comment: 
Click the plus button on comments list page (a comment lists for one message page) to go to the add comment page and further create a comment for that message.

* edit / delete comment: 
Click one comment in the comments list can go to edit/delete a comment page. (if you are not the author for this comment, a message will pop up and you cannot start editing/deleting this comment)


##### available unit test so far:
Tested all pages and functions.

* LoginFuntionalTest - test login

* HomePageUITest - list of messages

* ViewInfoFunctionalTest - test info on list of message

* MessageInfoUITest - test message detailed info displayed

* VoteFunctionalTest - test upvote and downvote

* MessageFunctionalTest_AddEditDelete - test all functions   related to messages: show messages list, add a new message, edit an existing message, delete a message.

* CommentFuntionalTest_AddEditDelete - test all functions related to comments: show comments list, add a new comment, edit an existing comment, delete a comment.

* ChangePwdFunctionalTest - test change password function

### phase 1: Xuewei Wang
##### important notes:
Important: Unit Tests connects to the real database right now, which means performing tests will result in messages being created/ changed. That is because I failed to figure out how to set up a fake coonection. In the next phase, a mock server should hopefully be set up.

Important: LoginActivity.java, LoginManager.java, and log_in.xml are not currently used by the rest of the project. However, I intentionally left them here for the possible future usage, when the backend could support such a log in behavoir.

##### available pages so far (here pages are equalent to activities):
*  home page
Home page displays each message on a card. Messages is displayed with title, creation date, and popularity. ordered by Message ID from greatest to smallest.

* message information page
This page contains: message ID, message title, message content, author name, creation date, up vote, and Down Vote. This page also contains buttons for up vote, down vote, and edit message.

* add message page
Prompt the user for a message.

* manage message page
Frist Prompt the user for a Message Id and a User Id. Later desplay the editable message title and content. The user could choose between edit and delete a message.

* customer profile page
The use could click the button "My Profile" to see his or her information, such as "ID", "given name", "family name" and "email". 

##### available functionalies so far:
* check out a message
Click at a card on the home will send us to the message information page which cotains detailed information about that message.

* up vote/ down vote
Click up vote/ down vote button on information view.

* create message
Click the plus button to go to the add message page and further create a message.

* edit/ delete message
The first way to edit/ delete message is through the option button at the home page. After click Option>Manage Message, the user is sent to the manage message page. User could also click the edit button on the message information page. In this case, the Message ID is automatically filled up.

##### available unit test so far:
* UI Tests for every page
* Functional Tests for every functionality



