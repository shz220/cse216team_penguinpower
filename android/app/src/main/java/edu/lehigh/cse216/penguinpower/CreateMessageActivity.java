package edu.lehigh.cse216.penguinpower;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Base64;
import java.util.Base64.Encoder;
import java.util.Base64.Decoder;
/**
 The activity that creates a new message.
 add a new message
 POST /messages
 resquest: { String username, String sessionKey, String title, String content }
 */
public class CreateMessageActivity extends AppCompatActivity {

    File uploadPhoto = null;
    static final int REQUEST_IMAGE = 20001;
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.new_tag);   //compiled new_tag.xml


        Button submitBtn = findViewById(R.id.newSubmitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                //find views
                EditText titleView = findViewById(R.id.newTitle);
                EditText contentView = findViewById(R.id.newContent);

                //check if inputs are valid.
                String title = titleView.getText().toString();
                if (title.length() < 5) {
                    Toast.makeText(CreateMessageActivity.this, "title too short.", Toast.LENGTH_LONG).show();
                    return;
                }
                if (title.length() > 200) {
                    Toast.makeText(CreateMessageActivity.this, "title too long.", Toast.LENGTH_LONG).show();
                    return;
                }
                String content = contentView.getText().toString();
               if (content.length() < 10) {
                    Toast.makeText(CreateMessageActivity.this, "content too short.", Toast.LENGTH_LONG).show();
                    return;
                }

                // to get the sessionkey and username
                SharedPreferences pref = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                String sessionKey = pref.getString("sessionKey", null);
                String username = pref.getString("username", null);

                Base64.Encoder encoder = Base64.getEncoder();
                String encoded = null;
                //determining if attachment exists
                if(uploadPhoto != null){
                    try {
                        encoded = "data:image/jpeg;base64," + encoder.encodeToString(Files.readAllBytes(Paths.get(uploadPhoto.getPath())));
                    } catch (IOException e){
                        Log.d("bcb320", "unable to read upload image");
                    }
                }

               // encoded = "data:image/jpeg;base64,iVBORw0KGgoAAAANSUhEUgAAAGcAAABZCAYAAAAjMTokAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABCuSURBVHhe7d0FkFzFFgbg4O7uwd3d3Z3CHu7u7hQOBVRB4VK4FB6ch0sgQIgCgbxAIIGgAUIgQIDQ736906nNvtnNzuzM7J28OVWnNnNn5t7u8/fRPj3pMmrUqNDgfHIDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBzzBAfO77//HkaPHh3+/PPPyH///Xf4559/2mSfSZ/H7lHs3rXmugfnt99+C3/88UcU8JgxY6Kwf/jhh/Dhhx+GF154Idxwww3htNNOC/vvv3/Yb7/94t/m/z755JPD1VdfHZ588snQv3//8O2330Zw3cs9/dszij272ly34Fjdf/31VwSD8F599dVw/vnnh3XXXTfMM888YaKJJgqTTDJJmHHGGcNcc80VFlxwwbDQQgv9D88777xhlllmCZNPPnno0qVLmHXWWcMqq6wSjj766PDMM89EoJN2WQTFxlItrjtwkpZY0T169AhHHnlkFPDEE08cpptuurDmmmuGww47LNx4441RuH369AlDhw4NI0eOLHq/r7/+OmrZiy++GO6+++6oZZtuumkEFMCA22GHHcLDDz8cfvrppwhUrbSpbsBJmgKY5557LmywwQZxpdOMXXbZJdx2223ho48+iu9XggDarVu3cMQRR0QN86zFFlssXHrppeHHH3+MIFVbk+oCHEIgjH79+o0FhcAuvPDCCEi1iWl74IEHwmabbRa1iVZdddVV4ddff42LoVoBRO7BET2ha665Jkw99dRhzjnnDJdffnn46quv4vVakgXy/PPPh6222iouEL7p7bffjterAVBuwWHTRUzs/K677hqFYeV+/PHHBVF1Lt1///1h0UUXDVNMMUW49dZb4zULqdhcyuVcgpOA+fLLL8Pyyy8foy4mjCPOExnf5ptvHhfOqaeeGq9VEqDcgZOAGTRoUOjatWuMlp566qk48TwSc3b44YdHgORNqFIA5Q4cDpYD5vD5Fza9Huicc86pOEC5AseErESOduaZZw7vvPNOnGi90FlnnRUBOvPMM+PrjgYJuQHHRNAhhxwSE8o8m7K2SFkIQI8++miM4orNtb2cG3D4GfUtE7vkkksKU60/GjFiRFh11VWj5g8bNiwmzsXm2x7OBTipHDL//POHjTbaKJq3eqa33norTDXVVLFygco1b50ODlAQOy1n6N27d3xd76RGxwq8+eabZWtPp4Nj4N99912Ydtppw1FHHVWYWv2Tgup8880XNtlkk/i6HO3pdHA4zXPPPTdWlOU2ExKdd955UXvee++9mCIUm39b3Kng8DWKhwqJBxxwQGFKEw5ZbDPNNFM46KCD4iJkwovJoTXuVHBEaPfcc0+s9Nosaw+ptbHnu+++e6wUl0uet9dee4XjjjsufPPNN4WrlSdBwdxzzx2BKXWLodPASXWyFVZYIW6Qtbdudvzxx0cwp5xyylhBeP/99wvvtJ+++OKLsPTSS8fdT/fad999C+9Unm6//fZo2l5//fWSA4NOA4fWWPkGbqOsvaTQCBimcJpppol9AqWSiNAm3RxzzBEDkdVXX73wTuXJfhN/etFFF8U5F5NFa9wp4Mhj8BJLLBGrzvxOpGzwYeTIkIVvwp1x+fvvhTzh8W7dwvxdu8a9HdvHkr5SiXmxvQ3c2TPtsz2dLWsZZAjDh4/LmRnNBm0zp/Dt0sizVl555bDxxhvH16VEbTUHJ5Vp2Hpa89BDD8XXEZQBA0Lo0SOE7t1DZgfGZdfU2oYODR9l0c9z//53+OWXX5q+WwZZxfoGenuejbtevWSPIUtMxmXXPHfgwCaQyiD+0RY3f1lKUFBzcISU/ARgOORIVm2fPiG89BJPHcJrrxVn7/nM4MFN36sEZWCHV15p4tae7frLL4fQv79yc+GL7aezzz47lnPs/5RSra4pOFaNkHK33XYLs88+e/j000+bRg8cvQCEkIRUjL1HWP/5T5MJ7ChpBrGz2hycYuw94Ag+jLVEuvPOO2PgoS+ulKCgpuBYNd9nvkPdSdQ1DonWhg0L4YMPQujZs8mcNOfMlEWzxwQ1W70SvHfffTfee3zEt2lx0jaVzGvmFJq0p1+/EOwdJVOWmNnr2zeEzz4L2epq+k6J9Oyzz8bdXP0HudUcdj6toqeffrow9BZEIwiBD2rOrrVwymy4BsJJJ500rLPOOrF7k3N/JVvpr2Uahl/KzKBrJ5xwQgw+mFNhtM7OcYhG8GE//zzuc10rQ1ua08uZ1qkb3nffffkFh0kTJc0222zR/naUmAgBhT0UHZ1AInyhNu3E/m1/SOisrUp7bjWTzmJkgcip5Dy5BYcwhb/yivaYoVJIodGW9i233BKbQeQV/l555ZXh8ccfz9xU5qc6iWgOcFiN3IJjYHq+xPxM3P8L6RxlyvmeXGvOtttuG9Zff/0YUteMxmRJ7p8dN6Plku7Q3EdrtEV3yiKLLJL52szZ1ooG/yuL8s4tvKg97bPPPtHP2rZWQywmm2Jc84DAMQ1lkw+EzLWgb6/KwuEuIQw5snChQKOHZBqVhdFVJhGl6sDaa68drUVuKwRU2v66iIpzrAn9nuVGo7I8ZXSWy4QsFP/p0Szx3DqEQTvUBBxdOOarv7tTC5/pNIBBFFshEj/XF1544bD99tsXhl9jGpGB03ueEL5vfyW8XCKHbbbZJloK2xSlBAO4YuAQPM1wfkVBsbVVAjytT3KPN954ozCN6pAc6IILLohHR5xK+EyWj8aMyJKkH7Ik85Us6Xw++/ti0/UKk6ST1hx66KFx3qWYNFxRcACit3nPPfeMgyn2OQ6R7V1ggQXCaqutFg8iVYMsEHs1ElM2f/HFF4/VhNNPPz2MUWj4JzNpg7YLoXvmj765vOlLFaQhQ4ZEC6GGqGRVqtbgioLDrMnU9QO0Bo7VA0Sxv1Ulu68GHXzwwbHfWqSkrqXF1+tll1228ImMRn+RRXEXFF5UjoYPHx6Pq5ifbfhytAZXFBxH9awUe/xtDchnkSN9JnDiiSdGk1gpAr5amwZFdTZlGwmgLfGllloq3HTTTXE1V4PU7LbYYos4r3QsxHyLyWF8PF5wmCGTJezEBElNmwvfNeGxWtbFF18cB+Wz3vPZlmrtNaJlJrLzzjuHzz//PF4rl1SdlWuAMtlkk4Utt9wyXmdG+R7PsXj8VZmuNKmOr7TSSvH+xx57bLxWjjlL3CY4SYDO6av40giCV3a3QoCWAAJOr169oo3XG+A9jt9gaQZquYJ8x+fcU4DAJBJaOQmq+2hBIhjVZ2CP3WXNSNGRwGgr4O64447COx0nWih/0walhmaBoI4Ag9sEx8q/7LLL4oS19zijr/TttQaLTz75JK5KnyVo7UYmrlyRBKWJoi27m8Jvx9bTqvO3VNMjIhMAqHqrOhc7yKs4aj+f7xm70dcBco7o5ptvjn7MuDWwa781H/NqOddSuVVw3BzyohwT4k9w37594+pWgrc6EaED54knnoidJrZkaZDfA+AcDbatsoXvJ5Cvv/76WN4xWUL0Yw2OthNsW0RbbRHouZZDzTDDDPG8THPae++9430lhOWSeTg35DkaVNzPCbzrrrsuzgGX62NacqvgAGbgwIHRhyQHDzAPR4ARojJBCRwaQq3XWGONCFQCpZjGFOOkRe5lg2zDDTeMAk8C2GOPPSJ4NHTw4MHjhOEDBgyIB2iZFhrOtzBvzU2kfRXhrZAacEr5aT5tkUXpvJAwXBkmWY8VV1wxjkcHELPa1gIsh1sFx6D1hAlD7VoSWHqPANlu4FDtBM61114bzZqqM9Pic+WsIpNMQrMPwz/oO3BEhG8iHP6JhonGEgnPNRoyuVqnCC9FhomYO5VxwvU5xci2iK/1XPMiC9GeLXZNgggolTBhxbhNcCRyzJOBJOeWJrvjjjtGW2tgySxx7MwJ4VmhnLDPe68ckHzHcxNQTiPQELuZtAQ4Y7P+jDQL8jvrrbde1DwBilX9s63nFtS9e/fo5wi3LXLyASh8mfsZV9LuaoGSuFVwrF5byepC+q6QiRgYm8vcSfAQIbrOx/A3wlamzeqUY6SznR2ZjAXgOYSCtt566/gjRM1J/UrVgd/baaedorYtueSSMcAQ5pZDtJHWmEPSkvaa6Y5yq+AYAIGnY9x6zM4444xozpw7mX766eMqTuYOyfYJJhX5TjnllDgx3zdJk2v5nHLYuLbbbruw1lprjdUqpXlgcdRMEUCwHxkSMfJZfE6pJNDgR2lNpX3K+LhVcDABI5rDXDEZQCGUZHPTKiKwK664IoaTOjHTCmcaZcwcaiXB4TfSHgmiGRaBnAwZO01D/BKAmFtOvZRzQEr+wNHJkytwCD4BJOdQzFPdJfiWfoS6I8AkEHzfvxNXykYncJjOBI78QmQn7yhG8iCnzAAIKNUDxzMUaW0ft0ZAEVzcdddd8VnFxlMtbhOcxEAg2MTNQSn2uWrbZOAwswICrxFtoNlt5TDGJ/Kzx8LMCalFfTYAWyPRIqvBXHpuy7FUk9sFTt6YkE466aSYy8h3kIhMaD+2/3o8JNQXRvNVbZHnLbPMMrFryHOrvfCac12Cw7zce++9MedRPUh04IEHxjLT+HKXUknya5/KAqil36lLcPhByaT8Q/UikV5k19JPbFWK+DG+SsG3ln6nLsHhO7CcRomGn0OCEfUutTUmqFJkK0MQIW9CtTJtdQsO4at+W9HNtwYUNzl5BddKkrxKAKKe5/nFxlVprjtwEjByF2Ua4Kh3SXLVAmmS182PIzKD8iylGLuT7lMq2a+R49Ei92s5rmpw3YEjx/JzxMJmWwo6W0RpaoCSRVVp1fFEMnuFWCAKIOySKtaWSgIQ1Q6V+gY4RZitR7YSFD5TGO2v/MUGnf2mRK6rrQHNGVRVBJpXDgky1BlLPTrYEa4rcJLj11ZFY8ZHquQ0xlZGW6T3QJnG51S8E7tmL8dzJa58Dq1jVgFU7cCgLsAhBMIgFMISpemkGV+vgaq5/Z3xBQf8EBCbH7rCzJhfe6ep/I3CrnJO+mEK4wFcsTFXgusCHMAgZRYbf6lSLjJrK+F0UMtqT+awNXKulE+xx6NGh/1bx6b+N1vyggyv/WCf6FAAkn7buloaVBfgSPyA4LecRV2PPfZYNDMcvGBAd49KgY23sT84kZFfo6INkkiF13LI71jroVAqsnno/0dwUo7Jw6ha2lM34Pj74IMPRoEQkBWsvmYrWtXYdgATpLNT9dk2haq1Vc+R+1kWRxJtmjGH7gmw9JeALQD92z6n0Klr07kahU+LwaLwXOAYS/ppl/9rcFKEpVhJcwiGoJRrmBd7OMccc0ysDPiRIxGaaM6paedPhdL6BZR2gKWKoCPUdbmSv8stt1zsFeBb+BqACDyA6nfTNKxgJRzM96BqlnPqAhxsdXLASDjLLwAmrWZmjUYRoOvYCvean/LjRzpuFDGBwVQltmNqF1UVQL+dw77+7wT5knv7vvu5v1DdeJCcq5oRW92Ag1PUlkwRYqJEcD179oxmRk80QID2yCOPjGXXCDeZxKQBiROIQLZ7m37LQIGVxqpGIwsEKNWM0hLXFTjNGVAERlCASloFOAASHuB07PAlzVmUpUkxMeGrmbmv+yUgkHvjdM+W46gm1y04LRlYfBMmSEIul4GQ7lXsWbXiCQacCZEb4OSYG+DkmBvg5Jgb4OSYG+DkmBvg5Jgb4OSWR4X/Asu1zr9odBI4AAAAAElFTkSuQmCC";
                Log.d("bcb320", "" +(encoded));
                //issue a request to the server to post a new message.
                try {
                    VolleyConnection con = VolleyConnection.getVolleyConnection(CreateMessageActivity.this);
                    String jsonString="";
                    if(encoded==null){
                        jsonString="{\"username\" : \"" + username + "\",\"sessionKey\" : \"" + sessionKey + "\",\"title\" : \"" + title + "\",\"content\" : \"" + content + "\"}";
                    }else{
                        jsonString="{\"username\" : \"" + username + "\",\"sessionKey\" : \"" + sessionKey + "\",\"title\" : \"" + title + "\",\"content\" : \"" + content + "\", \"document\" : \"" + encoded + "\"}";
                    }
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                            Tools.BASE_URL + "/messages",
                            new JSONObject(jsonString),
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    processResponse(response.toString());
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Tools.toastInternetError(CreateMessageActivity.this);
                                    Log.d("bcb320", "internet/server error encountered");
                                }
                            }) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json");
                            params.put("Accept", "application/json");
                            params.put("Accept-Encoding", "utf-8");
                            return params;
                        }
                    };
                    con.addToRequestQueue(request);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        Button attachPicture = findViewById(R.id.attachBtn);
        attachPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder((CreateMessageActivity.this));
                builder.setTitle("Attach Image:");
                final CharSequence[] options = {"Take Photo", "Pick a Photo", "Cancel"};
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(options[i].equals("Take Photo")){
                            if(MainActivity.permissionCount >= 2) dispatchTakePictureIntent();
                        } else if (options[i].equals("Pick a Photo")) {
                            if(MainActivity.permissionCount >=2) choosePhoto();
                        }
                    }
                });
                builder.show();
            }
        });
    }

    String mCurrentPhotoPath = null;
    public void dispatchTakePictureIntent(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            Log.d("bcb320", "dispatched take picture intent: " + Boolean.toString(takePictureIntent.resolveActivity(getPackageManager()) != null));
            if(takePictureIntent.resolveActivity(getPackageManager()) != null){
                File photo = null;
                try{
                    photo = createImageFile();
                } catch (IOException e){
                    Log.d("bcb320","Cannot create file");
                }

                if(photo != null) {
                    uploadPhoto = photo;
                    //convert file to uri
                    Uri uri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", photo);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    takePictureIntent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE);
                }
            }

    }

    private static int PICK_PHOTO = 10046;
    private void choosePhoto(){
        Intent choosePhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if(choosePhotoIntent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(choosePhotoIntent, PICK_PHOTO);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data ){
        super.onActivityResult(requestCode, resultCode, data);

        ImageView imageView = findViewById(R.id.messageImageView);
        final ImageView largeView = findViewById(R.id.messageImageViewFull);
        if(requestCode == REQUEST_IMAGE && resultCode == RESULT_OK){
            Bitmap largeBitMap = BitmapFactory.decodeFile(mCurrentPhotoPath);
            largeBitMap = getRightOrientation(largeBitMap, mCurrentPhotoPath);
            imageView.setImageBitmap(largeBitMap);
            largeView.setImageBitmap(largeBitMap);
            //insert into gallery
            MediaStore.Images.Media.insertImage(getContentResolver(), largeBitMap, uploadPhoto.getName(), uploadPhoto.getName());

        }
        if(requestCode == PICK_PHOTO && resultCode == RESULT_OK){
            Uri badUri = data.getData();
            String realpath = getRealPathFromURI(badUri);
            Log.d("bcb320", "uri: " + realpath);
            uploadPhoto = new File(realpath);
            Bitmap bitmap = BitmapFactory.decodeFile(realpath);
            bitmap = getRightOrientation(bitmap, realpath);
            imageView.setImageBitmap(bitmap);
            largeView.setImageBitmap(bitmap);

        }
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                largeView.setVisibility(View.VISIBLE);

            }
        });
        largeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                largeView.setVisibility(View.GONE);
            }
        });
    }

    private File createImageFile() throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timestamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        image.deleteOnExit();
        return image;
    }

    private String getRealPathFromURI(Uri contentUri){
        String[] images = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, images, null, null, null);
        int colIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return  cursor.getString(colIndex);
    }

    public static Bitmap getRightOrientation(Bitmap bitmap, String mCurrentPhotoPath){
        ExifInterface exifInterface = null;
        try{
            exifInterface = new ExifInterface(mCurrentPhotoPath);
        }catch(IOException e){
            e.printStackTrace();
        }
        int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);
        Log.d("bcb2320", ""+orientation);

        if(orientation == ExifInterface.ORIENTATION_ROTATE_90){
            Matrix matrix = new Matrix();
            matrix.setRotate(90);
            matrix.postScale(-1,1);
            bitmap = Bitmap.createBitmap(bitmap, 0,0,bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        }
        return bitmap;
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void processResponse(String response) {
        try {
            //parse response cannot be used here since the backend did not comply with the StructuredResponse design
            JSONObject json = new JSONObject(response);
            Log.d("bcb320", "json string 1"+json.toString());
            if (json.getString("mStatus").equals("error")) {
                Toast.makeText(this, json.getString("mMessage"), Toast.LENGTH_LONG).show();

                //Toast.makeText(this, data.getString("M_id"), Toast.LENGTH_LONG).show();
                return;
            }
            Log.d("yac320", "before getting mData");
            JSONObject data = json.getJSONObject("mData");
            Log.d("yac320", "data string 0:" + data.toString() );
            Toast.makeText(this, data.getString("M_id"), Toast.LENGTH_LONG).show();

            Log.d("bcb320", "data string:" + data.toString() );
            //adding attachment was ok, save image to cache
            if(uploadPhoto!=null) {
                File cacheDir = getCacheDir();
                String imageFileName = "msgImage" + data.getString("M_id");
                File messageImage = File.createTempFile(imageFileName, ".jpg", cacheDir);
                //write temp file to file in cache
                Files.write(Paths.get(messageImage.getPath()), Files.readAllBytes(uploadPhoto.toPath()));
            }

            //prepare for leaving current view.
            Intent i = new Intent(this, MainActivity.class);
            i.putExtra("type", "add");
            setResult(Activity.RESULT_OK, i);
            finish();
        } catch (JSONException e) {
            Log.d("bcb320","android json parsing error");
            Tools.toastJSONError(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
