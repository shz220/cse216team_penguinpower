package edu.lehigh.cse216.penguinpower;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.auth.api.signin.GoogleSignIn;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class CustomerProfileActivity extends AppCompatActivity {
    private TextView givenNameView;
    private TextView familyNameView;
    private TextView userIdView;
    private TextView emailView;

    private String familyName = "";
    private String givenName = "";
    private String email = "";
    private int userId = 0;
    private String userIdString = "";
    private boolean justWantUserId = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_profile);

        givenNameView = findViewById(R.id.given_name);
        familyNameView = findViewById(R.id.family_name);
        userIdView = findViewById(R.id.user_id);
        emailView = findViewById(R.id.email);

        loginClick();

    }

    /**
     * display the change password page
     */
    private void setViewDisplayCustomerProfile() {
        //fill in text
        givenNameView.setText("Given Name: "+ givenName);
        familyNameView.setText("Family Name: "+ familyName);
        emailView.setText("Email: "+ email);

        userIdString += userId;
        userIdView.setText("User ID: "+ userIdString);


    }


    private void loginClick() {

        // to get the sessionkey and username
        SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
        final String sessionKey = pref.getString("sessionKey", null);
        final String username = pref.getString("username", null);

        try {   //POST users/info
                    VolleyConnection con = VolleyConnection.getVolleyConnection(CustomerProfileActivity.this);
                    // get user information
                    //request{username, sessionKey, usernameAskedFor}
                    JsonObjectRequest getUserInfo = new JsonObjectRequest(Request.Method.POST,
                            Tools.BASE_URL + "/users/info",
                            new JSONObject("{\"username\" : " + username + ", \"sessionKey\" : \"" + sessionKey + "\", \"usernameAskedFor\" : " + username + "}"),
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    processResponseForUserInfo(response.toString());    //call method processResponseForUserInfo
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Tools.toastInternetError(CustomerProfileActivity.this);
                                }
                            }) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json");
                            params.put("Accept", "application/json");
                            params.put("Accept-Encoding", "utf-8");
                            return params;
                        }
                    };
                    con.addToRequestQueue(getUserInfo);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }



    // get user detailed info
    private void processResponseForUserInfo(String response) {
        try {
            JSONObject json = new JSONObject(response);
            String status = json.getString("mStatus");
            if (status.equals("error")) {
                String errorstr = json.getString("mMessage");
                Toast.makeText(this, "Error from the server when gathering user info: " + errorstr, Toast.LENGTH_LONG).show();
                setResult(Activity.RESULT_CANCELED);
                Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(i);
            }
            if (status.equals("ok")) {
                JSONObject data = json.getJSONObject("mData");

                userId = data.getInt("userId");
                Log.d("kas320", "useid" + userId);

                familyName = data.getString("familyName");
                Log.d("kas320", "familyName" + familyName);

                givenName = data.getString("givenName");
                Log.d("kas320", "givenName" + givenName);

                email = data.getString("email");
                Log.d("kas320", "email"+email);
            }
        } catch (JSONException e) {
            Tools.toastJSONError(this);
        }

        if(!justWantUserId)setViewDisplayCustomerProfile();
    }



}
