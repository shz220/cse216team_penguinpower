
/*
 * Login
 *
 */

package edu.lehigh.cse216.penguinpower;

import java.util.*;
import java.io.IOException;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.Task;

import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{
    //userId to used in EditMessageActivity class

    public static int userId;
    private SignInButton SignIn;
    private GoogleApiClient mGoogleApiClient;
    private static final int REQ_CODE = 9001;

    String idToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.log_in);    //compiled log_in.xml

        //so the pdfActivity will appear (first get the getApplicationContext() to get general context)
       // Intent i = new Intent(getApplicationContext(), PDFViewActivtity.class);
       // startActivity(i);

       // Intent i=new Intent(getApplicationContext(),MapActivity.class);
       // startActivity(i);
      //  Intent i=new Intent(getApplicationContext(),MapsActivity.class);
        //startActivity(i);
        SignIn = (SignInButton)findViewById(R.id.menu_login);
        SignIn.setOnClickListener(this);
        setViewDisplayOn();


        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions signInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        //GoogleClient mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API,signInOptions)
                .build();
    }


    private void setViewDisplayOn() {
        //set visibility
        SignIn.setVisibility(View.VISIBLE);
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, REQ_CODE);

    }
    @Override
    public void onClick(View view) {
        switch(view.getId())
        {
            case R.id.menu_login:
                signIn();
                break;
            case R.id.menu_logout:
                signOut();
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("kas320", "onConnectionFailed: " + connectionResult);
    }


    public void signOut()
    {
        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                updateUI(false);
            }
        });

    }
    private void handleResult(GoogleSignInResult result)
    {
        if(result.isSuccess())
        {
            GoogleSignInAccount account = result.getSignInAccount();
            idToken = account.getIdToken();
            Log.d("kas320", "id token is: " + idToken);

            //send the idToken to server
            loginClick();
            updateUI(true);
        }
        else
        {
            updateUI(false);
        }
    }

    private void updateUI(boolean isLogin)
    {
        if(isLogin)
        {
            //display userinfor, set visibility
            SignIn.setVisibility(View.GONE);
        }

        else
        {

            SignIn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQ_CODE)
        {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
            Log.d("kas320", "onActivityResult: GET_TOKEN: success:" + result. getStatus().isSuccess());
        }
        if(requestCode == 140)
        {
            signOut();
        }

    }

    private void loginClick() {

        try {
            VolleyConnection con = VolleyConnection.getVolleyConnection(LoginActivity.this);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                    Tools.BASE_URL + "/users/login",
                    new JSONObject("{\"accessCode\" : " + idToken + "\"}"),   //request{password, username}
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            processResponse(response.toString());   //call the processResponse method
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Tools.toastInternetError(LoginActivity.this);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");
                    params.put("Accept-Encoding", "utf-8");
                    return params;
                }
            };
            con.addToRequestQueue(request);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void processResponse(String response) {
        try {
            JSONObject json = new JSONObject(response);
            String status = json.getString("mStatus");
            if (status.equals("error")) {
                String errorstr = json.getString("mMessage");
                Toast.makeText(this, "Wrong username or password: " + errorstr, Toast.LENGTH_LONG).show();
                setResult(Activity.RESULT_CANCELED);
                //prepare for leaving current view to our MainActivity page
                Intent i = new Intent(getApplicationContext(),LoginActivity.class);
                startActivity(i);
            }
            if (status.equals("ok")) {
                JSONObject data = json.getJSONObject("mData");
                final String username = data.getString("username");
                final String sessionKey = data.getString("sessionKey");

                // use SharedPreferences to store session_key
                SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
                SharedPreferences.Editor edit = pref.edit();

                // Set/Store data
                edit.putString("sessionKey", sessionKey);
                edit.putString("username", username);
                edit.commit();

                Toast.makeText(this, "session key generated for user:"+username , Toast.LENGTH_LONG).show();
                requestLoginInfo();
                //prepare for leaving current view to our MainActivity page
                Intent i = new Intent(getApplicationContext(),MainActivity.class);

                startActivity(i);
            }
        } catch (JSONException e) {
            Tools.toastJSONError(this);
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();

    }


    private void requestLoginInfo() {
        // to get the sessionkey and username
        SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
        final String sessionKey = pref.getString("sessionKey", null);
        final String username = pref.getString("username", null);

        try {   //POST users/info
            VolleyConnection con = VolleyConnection.getVolleyConnection(LoginActivity.this);
            // get user information
            //request{username, sessionKey, usernameAskedFor}
            JsonObjectRequest getUserInfo = new JsonObjectRequest(Request.Method.POST,
                    Tools.BASE_URL + "/users/info",
                    new JSONObject("{\"username\" : " + username + ", \"sessionKey\" : \"" + sessionKey + "\", \"usernameAskedFor\" : " + username + "}"),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            processResponseForUserInfo(response.toString());    //call method processResponseForUserInfo
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Tools.toastInternetError(LoginActivity.this);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");
                    params.put("Accept-Encoding", "utf-8");
                    return params;
                }
            };
            con.addToRequestQueue(getUserInfo);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void processResponseForUserInfo(String response) {
        try {
            JSONObject json = new JSONObject(response);
            String status = json.getString("mStatus");
            if (status.equals("error")) {
                String errorstr = json.getString("mMessage");
                Toast.makeText(this, "Error from the server when gathering user info: " + errorstr, Toast.LENGTH_LONG).show();
                setResult(Activity.RESULT_CANCELED);
                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(i);
            }
            if (status.equals("ok")) {
                JSONObject data = json.getJSONObject("mData");

                userId = data.getInt("userId");
                Log.d("bcb320", "userid" + userId);

            }
        }catch (JSONException e) {
            Tools.toastJSONError(this);
        }


    }


}


