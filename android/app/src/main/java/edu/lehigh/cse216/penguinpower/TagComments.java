package edu.lehigh.cse216.penguinpower;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;

/*
A tagComments class contains (index, userId, commentsId, Content, Date)
used for comments activity
 */

public class TagComments implements Comparable<TagComments> {
    /**
     * An integer index for this comment
     */
    int mIndex;

    int mUserId;

    int mCommentId;

    String mText;

    String mDate;

    //Bitmap mImage;

    String mDocument;
    String mType;
    String mUsername;

    /**
     * Construct a Tag by setting its index and text
     *
     * @param idx An integer index for this comment
     * @param userId Author of this comment
     * @param commentId The string contents for this message
     * @param text An string contents for this comment
     * @param date An string of date created
     * @param type Type of the document (image/jpeg or appliation/pdf)
     * @param document the actual encoded document
     * @param username username of the author of the comment 
     */
    TagComments(int idx, int userId, int commentId, String text, String date, String type, String document,String username) {
        mIndex = idx;
        mUserId = userId;
        mCommentId = commentId;
        mText = text;
        mDate = date;
        mDocument=document;
        mType=type;
        mUsername=username;
    }

    TagComments(int idx, int userId, int commentId, String text, String date) {
        mIndex = idx;
        mUserId = userId;
        mCommentId = commentId;
        mText = text;
        mDate = date;
    }


    @Override
    public int compareTo(@NonNull  TagComments TagComments) {
        return(this.mIndex - TagComments.mIndex);
    }
}
