package edu.lehigh.cse216.penguinpower;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 The activity that creates a new comment for a specific message.
 add a comment
 POST /comments/:messageId
 request: { public String username, public String sessionKey, public String content }
 */
public class CreateCommentsActivity extends AppCompatActivity {
    // use the mid stored in MessageInfoActivity.java
    int mid = MessageInfoActivity.getMid();
    File uploadPhoto = null;
    static final int REQUEST_IMAGE = 20001;




    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.new_comment);   //compiled new_tag.xml

        Button submitBtn = findViewById(R.id.newSubmitBtn);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                //find views
                EditText contentView = findViewById(R.id.newContent);

                //check if inputs are valid.
                String content = contentView.getText().toString();
                if (content.length() < 10) {
                    Toast.makeText(CreateCommentsActivity.this, "content too short.", Toast.LENGTH_LONG).show();
                    return;
                }

                Base64.Encoder encoder = Base64.getEncoder();
                String encoded = null;
                //determing if attachment exists

                if(uploadPhoto != null){
                    try {
                        encoded = "data:image/jpeg;base64," + encoder.encodeToString(Files.readAllBytes(Paths.get(uploadPhoto.getPath())));
                    } catch (IOException e){
                        Log.d("bcb320", "unable to read upload image");
                    }
                }

                // to get the sessionkey and username
                SharedPreferences pref = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
                String sessionKey = pref.getString("sessionKey", null);
                String username = pref.getString("username", null);

                //issue a request to the server to post a new message.
                try {
                    VolleyConnection con = VolleyConnection.getVolleyConnection(CreateCommentsActivity.this);
                    String jsonString="";

                    if(encoded==null){
                        jsonString="{\"username\" : \"" + username + "\",\"sessionKey\" : \"" + sessionKey + "\",\"content\" : \"" + content + "\"}";
                    }else{
                        jsonString="{\"username\" : \"" + username + "\",\"sessionKey\" : \"" + sessionKey + "\",\"content\" : \"" + content + "\", \"document\" : \"" + encoded + "\"}";
                    }
                    Log.d("yac320","before sending json encoded="+encoded+" username: "+username+" content "+content);
                    JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                            Tools.BASE_URL + "/comments/"+mid,
                            new JSONObject(jsonString),
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    Log.d("yac320","on response");
                                    processResponse(response.toString());
                                    Log.d("yac320","response: "+response.toString());
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    Log.d("yac320","on error listener");
                                    Tools.toastInternetError(CreateCommentsActivity.this);
                                }
                            }) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> params = new HashMap<>();
                            params.put("Content-Type", "application/json");
                            params.put("Accept", "application/json");
                            params.put("Accept-Encoding", "utf-8");
                            return params;
                        }
                    };
                    con.addToRequestQueue(request);

                } catch (JSONException e) {
                    Log.d("yac320","error in json");
                    e.printStackTrace();
                }
                Log.d("yac320","after post");
            }
        });




        Button attachPicture = findViewById(R.id.attachBtnCmt);
        attachPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder((CreateCommentsActivity.this));
                builder.setTitle("Attach Image:");
                final CharSequence[] options = {"Take Photo", "Pick a Photo", "Cancel"};
                builder.setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if(options[i].equals("Take Photo")){
                            if(MainActivity.permissionCount >= 2) dispatchTakePictureIntent();
                        } else if (options[i].equals("Pick a Photo")) {
                            if(MainActivity.permissionCount >=2) choosePhoto();
                        }
                    }
                });
                builder.show();
            }
        });
        Log.d("yac320","end of oncreate");
    }

    String mCurrentPhotoPath = null;
    public void dispatchTakePictureIntent(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Log.d("bcb320", "dispatched take picture intent: " + Boolean.toString(takePictureIntent.resolveActivity(getPackageManager()) != null));
        if(takePictureIntent.resolveActivity(getPackageManager()) != null){
            File photo = null;
            try{
                photo = createImageFile();
            } catch (IOException e){
                Log.d("bcb320","Cannot create file");
            }

            if(photo != null) {
                uploadPhoto = photo;
                //convert file to uri
                Uri uri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", photo);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                takePictureIntent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE);
            }
        }

    }

    public File createImageFile() throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timestamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        image.deleteOnExit();
        return image;
    }

    private static int PICK_PHOTO = 10046;
    private void choosePhoto(){
        Intent choosePhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if(choosePhotoIntent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(choosePhotoIntent, PICK_PHOTO);
        }
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data ){
        super.onActivityResult(requestCode, resultCode, data);

        ImageView imageView = findViewById(R.id.commentImageView);
        if(requestCode == REQUEST_IMAGE && resultCode == RESULT_OK){
            Bitmap largeBitMap = BitmapFactory.decodeFile(mCurrentPhotoPath);
            largeBitMap = CreateMessageActivity.getRightOrientation(largeBitMap, mCurrentPhotoPath);
            imageView.setImageBitmap(largeBitMap);

            MediaStore.Images.Media.insertImage(getContentResolver(), largeBitMap, uploadPhoto.getName(), uploadPhoto.getName());

        }
        if(requestCode == PICK_PHOTO && resultCode == RESULT_OK){
            Uri badUri = data.getData();
            String realpath = getRealPathFromURI(badUri);
            Log.d("bcb320", "uri: " + realpath);
            uploadPhoto = new File(realpath);
            Bitmap bitmap = BitmapFactory.decodeFile(realpath);
            bitmap = CreateMessageActivity.getRightOrientation(bitmap, realpath);
            imageView.setImageBitmap(bitmap);


        }

    }



    public String getRealPathFromURI(Uri contentUri){
        String[] images = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, images, null, null, null);
        int colIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return  cursor.getString(colIndex);
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void processResponse(String response) {
        try {
            JSONObject json = new JSONObject(response);
            if (json.getString("mStatus").equals("error")) {
                Toast.makeText(this, json.getString("mMessage"), Toast.LENGTH_LONG).show();
                return;
            }
            JSONObject data = json.getJSONObject("mData");
            Toast.makeText(this, data.getString("C_id"), Toast.LENGTH_LONG).show();

            //adding attachment was ok, save image to cache
            File cacheDir = getCacheDir();
            String imageFileName = "cmtImage" + data.getString("C_id");
            File messageImage = File.createTempFile(imageFileName,".jpg", cacheDir);
            Files.write(Paths.get(messageImage.getPath()), Files.readAllBytes(uploadPhoto.toPath()));

            //prepare for leaving current view.
            Intent i = new Intent(getApplicationContext(), CommentsMainActivity.class);
            i.putExtra("type", "addComment");
            setResult(Activity.RESULT_OK, i);
            finish();
        } catch (JSONException e) {
            Tools.toastJSONError(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
