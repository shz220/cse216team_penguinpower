package edu.lehigh.cse216.penguinpower;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 An activity that edit/delete a message.
 update a message
 PUT /messages/:messageId
 request: { String username, String sessionKey, int messageId, String title, String content }

 delete a message
 DELETE /messages/:messageId
 request: String sessionKey, String username

 get one message
 GET /messages/:messageId
 request header: { String username, String sessionKey }
 response: { int messageId, int userId, String title, String content, int upVoteCount, int downVoteCount, Date dateCreated }
 */
public class EditMessageActivity extends AppCompatActivity {

    private int mid;

    int userId = LoginActivity.userId; //the user who login

    private TextView midView;
    private TextView titleView;
    private TextView contentView;
    private Button deleteBtn;
    private Button cancelBtn;
    private Button applyBtn;
    private Button showBtn;
    private Button attachBtn;
    private ImageView imageView;

    static final int REQUEST_IMAGE = 20001;
    File uploadPhoto = null;
    File oldPhoto = null;
    private VolleyConnection con;

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        //set an impossible value to ids.
        mid = -1;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_message);

        //find elements by id
        midView = findViewById(R.id.edit_message_id);
        titleView = findViewById(R.id.edit_title);
        contentView = findViewById(R.id.edit_content);
        deleteBtn = findViewById(R.id.edit_delete);
        cancelBtn = findViewById(R.id.edit_cancel);
        applyBtn = findViewById(R.id.edit_apply);
        showBtn = findViewById(R.id.edit_show);
        attachBtn = findViewById(R.id.attachBtnEditMsg);
        imageView = findViewById(R.id.editMessageImageView);
        //set elements for initial display
        setViewDisplayOff();
        //check if the activity is created by an edit button with a specific message.
        //In that case, filling mid automatically.
        Intent i = getIntent();
        //System.err.println(i);
        int midIn = i.getIntExtra("mid", 0);
        if (midIn != 0) {
            midView.setText(String.format("%d", midIn));
        }
        //set connection
        con = VolleyConnection.getVolleyConnection(this);
    }

    /**
     * editViews display on, after click show. Set listeners to appeared buttons.
     */
    private void setViewDisplayOn() {
        //clear text
        midView.setText("");

        //set visibility
        midView.setVisibility(View.INVISIBLE);
        showBtn.setVisibility(View.INVISIBLE);
        titleView.setVisibility(View.VISIBLE);
        contentView.setVisibility(View.VISIBLE);
        deleteBtn.setVisibility(View.VISIBLE);
        cancelBtn.setVisibility(View.VISIBLE);
        applyBtn.setVisibility(View.VISIBLE);
        attachBtn.setVisibility(View.VISIBLE);

        //checking cache for image, set Image view to existing one
        File cacheDir = getCacheDir();
        oldPhoto = new File(cacheDir.getAbsolutePath()+"/msgImage" + mid +".jpg" );

        if(oldPhoto.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(oldPhoto.getPath());
            imageView.setImageBitmap(bitmap);
        }
        //for testing purposes
        else{
            Log.d("bcb320", "image file for mid: " + mid + "does not exist");
        }
        uploadPhoto = oldPhoto;


        //set listeners
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });
        applyBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                apply();
            }
        });
        Button attachPicture = findViewById(R.id.attachBtnEditMsg);
        attachPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPhoto();
            }
        });
    }

    /**
     * editViews display off, before click show. Set listeners to appeared buttons.
     */
    private void setViewDisplayOff() {
        //clear text
        titleView.setText("");
        contentView.setText("");

        //clear values
        mid =  -1;

        //set visibility
        midView.setVisibility(View.VISIBLE);
        showBtn.setVisibility(View.VISIBLE);
        titleView.setVisibility(View.INVISIBLE);
        contentView.setVisibility(View.INVISIBLE);
        deleteBtn.setVisibility(View.INVISIBLE);
        cancelBtn.setVisibility(View.INVISIBLE);
        applyBtn.setVisibility(View.INVISIBLE);
        attachBtn.setVisibility(View.INVISIBLE);

        //set listeners
        showBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show();
            }
        });
    }

    /**
     * Show the edit boxes. Boxes are filled with text found from original message.
     */
    private void show() {
        //read in inputs.
        int midIn;
        try {
            midIn = Integer.parseInt(midView.getText().toString());
        } catch (NumberFormatException e) {
            Tools.toastNumberFormatError(this);
            setViewDisplayOff();
            return;
        }

        // to get the sessionkey and username
        SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
        final String sessionKey = pref.getString("sessionKey", null);
        final String username = pref.getString("username", null);

        //issue a request to the server to get message info.
        // PLAN: change uidIn to the SharedPreference get uid ---- still a problem
        final int finalUidIn = userId;
        final int finalMidIn = midIn;

            //get one message: POST /messages/:messageId  request: { String username, String sessionKey }
            VolleyConnection con = VolleyConnection.getVolleyConnection(EditMessageActivity.this);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                    Tools.BASE_URL + "/messages/" + midIn,
                    null,
                    //new JSONObject("{\"username\" : " + username + ",\"sessionKey\" : " + sessionKey + "}"),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //when success, parse response.
                            try {
                                JSONObject json = (JSONObject) Tools.parseResponse(response.toString());

                                //check if the user id matches.
                                assert json != null;
                                int uid = Integer.parseInt(json.getString("userId"));

                                if (uid != finalUidIn) {
                                    Toast.makeText(EditMessageActivity.this, "You are not the author for this message.", Toast.LENGTH_LONG).show();
                                    return;
                                }
                                mid = finalMidIn;
                                //fill boxes.
                                titleView.setText(json.getString("title"));
                                contentView.setText(json.getString("content"));
                                setViewDisplayOn();
                            } catch (JSONException e) {
                                Tools.toastJSONError(EditMessageActivity.this);
                                e.printStackTrace();
                            } catch (Tools.ServerSideException e) {
                                //when uid and mid don't match.
                                Toast.makeText(EditMessageActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                setViewDisplayOff();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Tools.toastInternetError(EditMessageActivity.this);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("sessionKey",sessionKey);
                    params.put("username",username);

                    Log.w("myApp","sessionKey="+params.get("sessionKey") );
                    Log.w("myApp","username="+params.get("username"));
                    //params.put("Content-Type", "application/json");
                    //params.put("Accept", "application/json");
                    //params.put("Accept-Encoding", "utf-8");
                    return params;
                }
            };
            con.addToRequestQueue(request);
        }


    /**
     * When change is canceled, changes are discarded, and pages are set clean.
     */
    private void cancel() {
        Toast.makeText(EditMessageActivity.this, "changes discarded.", Toast.LENGTH_SHORT).show();
        setViewDisplayOff();
    }


    /**
     * When hit, a request is sent to the server to delete this message.
     */
    private void delete() {
        // to get the sessionkey and username
        SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
        final String sessionKey = pref.getString("sessionKey", null);
        final String username = pref.getString("username", null);

        //send request.
            VolleyConnection con = VolleyConnection.getVolleyConnection(EditMessageActivity.this);
        StringRequest request = new StringRequest(Request.Method.DELETE,
                    Tools.BASE_URL + "/messages/"+mid,
                    //new JSONObject("{\"sessionKey\" : \"" + sessionKey + "\",\"username\" : \"" + username + "\",\"messageId \" : " + mid + "}"),
                    new Response.Listener<String>() {
                        @TargetApi(Build.VERSION_CODES.O)
                        @Override
                        public void onResponse(String response) {
                            try {
                                //a delete post is designed to return null right now...
                                if (Tools.parseResponse(response.toString()) == null) {
                                    //remove from cache
                                    if(uploadPhoto.exists()) uploadPhoto.delete();
                                    //on success, leave current view
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    i.putExtra("mid", mid);
                                    i.putExtra("type", "delete");
                                    setResult(Activity.RESULT_OK, i);
                                    finish();
                                }
                            } catch (JSONException e) {
                                Tools.toastJSONError(EditMessageActivity.this);
                                e.printStackTrace();
                            } catch (Tools.ServerSideException e) {
                                Toast.makeText(EditMessageActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Tools.toastInternetError(EditMessageActivity.this);
                        }
                    }) {

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("sessionKey",sessionKey);
                    params.put("username",username);

                    Log.w("myApp","sessionKey="+params.get("sessionKey") );
                    Log.w("myApp","username="+params.get("username"));
                    //params.put("Content-Type", "application/json");
                    //params.put("Accept", "application/json");
                    //params.put("Accept-Encoding", "utf-8");
                    return params;
                }
            };
            con.addToRequestQueue(request);
        }


    /**
     * apply change and put the change to the server.
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void apply() {
        //get title and check if valid.
        String title = titleView.getText().toString();
        if (title.length() < 5) {
            Toast.makeText(this, "title too short.", Toast.LENGTH_LONG).show();
            return;
        }
        if (title.length() > 200) {
            Toast.makeText(this, "title too long.", Toast.LENGTH_LONG).show();
            return;
        }

        //get content and check if valid.
        String content = contentView.getText().toString();
        if (content.length() < 10) {
            Toast.makeText(this, "content too short.", Toast.LENGTH_LONG).show();
            return;
        }

        // to get the sessionkey and username
        SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
        String sessionKey = pref.getString("sessionKey", null);
        String username = pref.getString("username", null);
        String encoded=null;
        Base64.Encoder encoder = Base64.getEncoder();
        if(uploadPhoto != null){
            try {
                encoded = "data:image/jpeg;base64," + encoder.encodeToString(Files.readAllBytes(Paths.get(uploadPhoto.getPath())));
            } catch (IOException e){
                Log.d("bcb320", "unable to read upload image");
            }
        }
        //issue a request to the server to put the changed message..
        try {
            String jsonString="";
            if(encoded==null){
                jsonString="{\"username\" : \"" + username + "\", \"sessionKey\" : \"" + sessionKey + "\", \"messageId\" : \"" + mid + "\",\"title\" : \"" + title + "\", \"content\" : \"" + content + "\"}";
            }else{
                jsonString="{\"username\" : \"" + username + "\", \"sessionKey\" : \"" + sessionKey + "\", \"messageId\" : \"" + mid + "\",\"title\" : \"" + title + "\", \"content\" : \"" + content + "\",\"document\" : \"" + encoded+ "\"}";
            }
            VolleyConnection con = VolleyConnection.getVolleyConnection(this);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT,
                    Tools.BASE_URL + "/messages/"+mid,
                    new JSONObject(jsonString),
                    //edit: mU_ID changed to userId, etc.
                    new Response.Listener<JSONObject>() {
                        @TargetApi(Build.VERSION_CODES.O)
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Tools.parseResponse(response.toString());
                                //potentially update image of cache
                                //save image to cache, create image if it does not yet exist
                                //undo commenting when route for images is updated
                                /*if(uploadPhoto.exists()){
                                    Files.write(Paths.get(oldPhoto.getPath()), Files.readAllBytes(uploadPhoto.toPath()));
                                }
                                */
                                //on success, set intent and finish
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                i.putExtra("type", "edit");
                                i.putExtra("mid", mid);
                                setResult(Activity.RESULT_OK, i);
                                finish();
                            } catch (JSONException e) {
                                Tools.toastJSONError(EditMessageActivity.this);
                                e.printStackTrace();
                            } catch (Tools.ServerSideException e) {
                                Toast.makeText(EditMessageActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Tools.toastInternetError(EditMessageActivity.this);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");
                    params.put("Accept-Encoding", "utf-8");
                    return params;
                }
            };
            con.addToRequestQueue(request);
        } catch (JSONException e) {
            Tools.toastJSONError(this);
            e.printStackTrace();
        }
    }

    private void getPhoto(){
        AlertDialog.Builder builder = new AlertDialog.Builder((EditMessageActivity.this));

        builder.setTitle("Attach Image:");
        final CharSequence[] options = {"Take Photo", "Pick a Photo", "Cancel"};
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(options[i].equals("Take Photo")){
                    if(MainActivity.permissionCount >= 2) takePicture();
                } else if (options[i].equals("Pick a Photo")) {
                    if(MainActivity.permissionCount >=2) choosePhoto();
                }
            }
        });
        builder.show();
    }

    String mCurrentPhotoPath = null;
    public void takePicture(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Log.d("bcb320", "dispatched take picture intent: " + Boolean.toString(takePictureIntent.resolveActivity(getPackageManager()) != null));
        if(takePictureIntent.resolveActivity(getPackageManager()) != null){
            File photo = null;
            try{
                photo = createImageFile();
            } catch (IOException e){
                Log.d("bcb320","Cannot create file");
            }

            if(photo != null) {
                uploadPhoto = photo;
                //convert file to uri
                Uri uri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", photo);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                takePictureIntent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE);
            }
        }

    }

    private static int PICK_PHOTO = 10046;
    private void choosePhoto(){
        Intent choosePhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if(choosePhotoIntent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(choosePhotoIntent, PICK_PHOTO);
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data ){
        super.onActivityResult(requestCode, resultCode, data);

        ImageView imageView = findViewById(R.id.editMessageImageView);
        if(requestCode == REQUEST_IMAGE && resultCode == RESULT_OK){
            Bitmap largeBitMap = BitmapFactory.decodeFile(mCurrentPhotoPath);
            largeBitMap = CreateMessageActivity.getRightOrientation(largeBitMap, mCurrentPhotoPath);
            imageView.setImageBitmap(largeBitMap);
            MediaStore.Images.Media.insertImage(getContentResolver(), largeBitMap, uploadPhoto.getName(), uploadPhoto.getName());

        }
        if(requestCode == PICK_PHOTO && resultCode == RESULT_OK){
            Uri badUri = data.getData();
            String realpath = getRealPathFromURI(badUri);
            Log.d("bcb320", "uri: " + realpath);
            uploadPhoto = new File(realpath);
            Bitmap bitmap = BitmapFactory.decodeFile(realpath);
            bitmap = CreateMessageActivity.getRightOrientation(bitmap, realpath);
            imageView.setImageBitmap(bitmap);


        }

    }

    private File createImageFile() throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timestamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        image.deleteOnExit();
        return image;
    }

    private String getRealPathFromURI(Uri contentUri){
        String[] images = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, images, null, null, null);
        int colIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return  cursor.getString(colIndex);
    }

}
