package edu.lehigh.cse216.penguinpower;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.SharedPreferences;

/*
mainActivity class, display our main page and link to almost all other classes
 */
public class MainActivity extends AppCompatActivity {

    /**
     * mData holds the data we get from Volley
     */
    ArrayList<TagLite> mData = new ArrayList<>();

    private static Context context;
    static final int REQUEST_PERMISSION_CAMERA = 1001;
    static final int REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE = 1002;
    static int permissionCount = 0;
    @TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //R.id.fab is linking to the auto-generated unique identifier for our FloatingActionButton (that's what android:id="@+id/fab" does).
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), CreateMessageActivity.class);
                startActivityForResult(i, 100);
            }
        });
        populateListFromVolley();
        context = getAppContext();

        //check permissions for camera

        if(checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[] {Manifest.permission.CAMERA}, REQUEST_PERMISSION_CAMERA);
        }
        else permissionCount++;
        if(checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            requestPermissions(new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE);
        }
        else permissionCount++;



    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSION_CAMERA:
            case REQUEST_PERMISSION_WRITE_EXTERNAL_STORAGE:
                if (grantResults[0] ==
                        PackageManager.PERMISSION_GRANTED)
                    permissionCount++;
            default:
                break;
        }
    }

    /**
     * Populate the tags display with data from server.
     */
    private void populateListFromVolley() {

            // Instantiate the connection..
            VolleyConnection con = VolleyConnection.getVolleyConnection(this);

            // to get the sessionkey and username
            SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
            final String sessionKey = pref.getString("sessionKey", null);
            final String username = pref.getString("username", null);

            String url = Tools.BASE_URL + "/messages";
            // Request a string response from the provided URL.
            JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,
                    null, //new JSONObject("{\"username\" : " + username + ", \"sessionKey\" : \"" + sessionKey + "\"}"),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            populateListFromJson(response.toString());
                        }
                    }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("shz220", "That didn't work!");
                    }
                }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("sessionKey",sessionKey);
                    params.put("username",username);

                    Log.w("myApp","sessionKey="+params.get("sessionKey") );
                    Log.w("myApp","username="+params.get("username"));
                    //params.put("Content-Type", "application/json");
                    //params.put("Accept", "application/json");
                    //params.put("Accept-Encoding", "utf-8");
                    return params;
                }
            };

            // Add the request to the RequestQueue.
            con.addToRequestQueue(stringRequest);
        }



    /**
     * Helper method for populateListFromVolley, should not be called.
     * Retrieve data from JSON response and add to the list.
     */
    private void populateListFromJson(String response) {
        //clean up existing tags.
        mData = new ArrayList<>();
        try {
            JSONObject json = new JSONObject(response);
            String status = json.getString("mStatus");
            if (status.equals("error")) {
                String errorStr = json.getString("mMessage");
                Toast.makeText(this, "Error from the server: " + errorStr, Toast.LENGTH_LONG).show();
                return;
            }
            if (status.equals("ok")) {
                //Toast.makeText(this, "Wrong username or password: " + username, Toast.LENGTH_LONG).show();
                JSONArray data = json.getJSONArray("mData");
                for (int i = 0; i < data.length(); i++) {
                    int mid = data.getJSONObject(i).getInt("messageId");
                    String title = data.getJSONObject(i).getString("title");
                    int popularity = data.getJSONObject(i).getInt("popularity");  //change to popularity
                    String date = data.getJSONObject(i).getString("dateCreated");
                    mData.add(new TagLite(mid, title, popularity,  date));
                }
            }
            Collections.sort(mData, Collections.reverseOrder());
        } catch (final JSONException e) {
            Log.d("shz220", "Error parsing JSON file: " + e.getMessage());
            return;
        }
        Log.d("shz220", "Successfully parsed JSON file.");
        RecyclerView rv = findViewById(R.id.list_view);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        ItemListAdapter adapter = new ItemListAdapter(this, mData);
        adapter.setClickListener(new ItemListAdapter.ClickListener() {
            @Override
            public void onClick(TagLite d) {
                Intent i = new Intent(getApplicationContext(), MessageInfoActivity.class);
                i.putExtra("mid", d.mIndex);
                startActivityForResult(i, 110);
            }
        });
        rv.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //refresh tags.
        populateListFromVolley();
        //All intents from different activities will contain the same attribute:
        //type: anActionType
        if (resultCode == RESULT_OK) {
            switch (data.getStringExtra("type")) {
                case "delete":
                    Toast.makeText(this, "Message #" + data.getIntExtra("mid", -1) + " is deleted.", Toast.LENGTH_LONG).show();
                    break;
                case "edit":
                    Toast.makeText(this, "Message #" + data.getIntExtra("mid", -1) + " is edited.", Toast.LENGTH_LONG).show();
                    break;
                case "vote":
                    Toast.makeText(this, "Your Vote to Message #" + data.getIntExtra("mid", -1) + " is registered.", Toast.LENGTH_LONG).show();
                    break;
                case "add":
                    Toast.makeText(this, "Your Message is added.", Toast.LENGTH_LONG).show();
                    break;
                default:
                    Toast.makeText(this, "No Change is applied.", Toast.LENGTH_SHORT).show();
                    break;
            }
        } else {
            Toast.makeText(this, "No Change is applied.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        //Set listener to option manage message
        if (id == R.id.menu_manage) {
            Intent i = new Intent(getApplicationContext(), EditMessageActivity.class);
            startActivityForResult(i, 120);
            return true;
        }

        // open the CustomerProfile.class and run the code in that class
        if (id == R.id.menu_profile) {
            Intent i = new Intent(getApplicationContext(), CustomerProfileActivity.class);
            startActivityForResult(i, 130);
            return true;
        }

        // logout: POST /users/logout
        if (id == R.id.menu_logout) {
            Intent i = new Intent(getApplicationContext(), LoginActivity.class);
            startActivityForResult(i, 140);
            //finish the current activity
            finish();

            try {
                // Instantiate the connection..
                VolleyConnection con = VolleyConnection.getVolleyConnection(this);

                // to get the sessionkey and username
                SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
                String sessionKey = pref.getString("sessionKey", null);
                String username = pref.getString("username", null);

                String url = Tools.BASE_URL + "/users/logout";
                // Request a string response from the provided URL.
                JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, url,
                        new JSONObject("{\"username\" : " + username + ", \"sessionKey\" : \"" + sessionKey + "\"}"),
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                                setResult(Activity.RESULT_OK, i);
                                finish();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Tools.toastInternetError(MainActivity.this);
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        params.put("Accept", "application/json");
                        params.put("Accept-Encoding", "utf-8");
                        return params;
                    }
                };
                con.addToRequestQueue(stringRequest);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return true;

        }

        return super.onOptionsItemSelected(item);
    }


    public static Context getAppContext() {
        return context;
    }
}
