package edu.lehigh.cse216.penguinpower;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

/**
 * The adapter for the recycleView for message list
 */
class ItemListAdapter extends RecyclerView.Adapter<ItemListAdapter.ViewHolder> {

    //User should customize their own click listener.
    interface ClickListener {
        void onClick(TagLite d);
    }

    private ClickListener mClickListener;

    void setClickListener(ClickListener c) {
        mClickListener = c;
    }

    //An inner class that holds the content and the other view for vote.
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        TextView mVoteCount;
        TextView mDate;

        //Constructor, find views.
        ViewHolder(View itemView) {
            super(itemView);
            this.mTitle = itemView.findViewById(R.id.tag_title);
            this.mVoteCount = itemView.findViewById(R.id.tag_vote_count);
            this.mDate = itemView.findViewById(R.id.tag_date);
        }
    }

    private ArrayList<TagLite> mData;
    private LayoutInflater mLayoutInflater;

    /**
     * Initiate an Adapter.
     */
    ItemListAdapter(Context context, ArrayList<TagLite> data) {
        mData = data;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final TagLite d = mData.get(position);
        holder.mTitle.setText(d.mTitle);
        holder.mVoteCount.setText(String.format(Locale.ENGLISH,"popularity: %d", d.mPopularity));
        holder.mDate.setText(d.mDate);
        // Attach a click listener to the view we are configuring
        final View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(d);
            }
        };
        holder.mTitle.setOnClickListener(listener);
        holder.mVoteCount.setOnClickListener(listener);
        holder.mDate.setOnClickListener(listener);
    }
}