package edu.lehigh.cse216.penguinpower;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/*
 A class that provide static methods that could be reused for the app.
 for now, this is only used for vote methods

 cast a vote
 PUT /vote/message
 request: { String sessionKey, String username, boolean voteStatus, int messageId }
 voteStatus - true for up vote.
 response: { int }
 // 0 for the same vote exits in database, abandoned.
 // 1 for add a new vote.
 // 2 for the opposite vote exits in database, the vote status is changed.
 */
public final class Tools {

    /**
     * Base url for the entire project.
     */
    public final static String BASE_URL = "https://cse216-penguin-power.herokuapp.com";

    /**
     * Override the default to prevent initialization from user.
     */
    private Tools() {
    }


    public static void vote(int mid, boolean isUpvote, final Context context, final Callable onResponse) {

        //issue a request to the server to put the changed message..
        try {
            // to get the sessionkey and username
            SharedPreferences pref = context.getSharedPreferences("MyPrefs",0);
            String sessionKey = pref.getString("sessionKey", null);
            String username = pref.getString("username", null);

            VolleyConnection con = VolleyConnection.getVolleyConnection(context);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT,
                    Tools.BASE_URL + "/vote/message",
                    new JSONObject("{\"sessionKey\" : \"" + sessionKey + "\",\"username\" : \"" + username + "\",\"voteStatus\" : \"" + isUpvote + "\", \"messageId\" : \"" + mid + "\"}"),
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                onResponse.call();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    //call if failed
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            String response = "Internet Connection Error: " + error.getMessage();
                            Toast.makeText(context, response, Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");
                    params.put("Accept-Encoding", "utf-8");
                    return params;
                }
            };
            con.addToRequestQueue(request);
        } catch (JSONException e) {
            Tools.toastJSONError(context);
            e.printStackTrace();
        }
    }

    /**
     * Parse a JSON response that is in the format of this project.
     *
     * @return Object that is JSONObject or JSONArray contained inside the input in JSON format if no error.
     * @throws JSONException       when the response is somehow in wrong format.
     * @throws ServerSideException when server response with an exception.
     */
    public static Object parseResponse(String json) throws JSONException, ServerSideException {
        JSONObject response = new JSONObject(json);
        String status = response.getString("mStatus");
        if (status.equals("error")) {
            String errorStr = response.getString("mMessage");
            throw new ServerSideException(errorStr);
        }
        if (status.equals("ok") || status.equals("okay")) {
            try {
                return response.getJSONObject("mData");
            } catch (JSONException e) {
                try {
                    return response.getJSONArray("mData");
                } catch (JSONException ex) {
                    return null;
                }
            }
        } else {
            throw new JSONException("Cannot find right mStatus.");
        }
    }

    /**
     * An exception that is thrown when server returns an error.
     */
    static class ServerSideException extends Throwable {
        ServerSideException(String errorstr) {
            super(errorstr);
        }
    }

    /**
     * Issue a toast contains the standard warning for internet connection error.
     */
    public static void toastInternetError(Context context) {
        Toast.makeText(context, "Internet Connection Error Encountered.", Toast.LENGTH_LONG).show();
    }

    /**
     * Issue a toast contains the standard warning for JSON error.
     */
    public static void toastJSONError(Context context) {
        Toast.makeText(context, "Internal JSON Format Error Encountered.", Toast.LENGTH_LONG).show();
    }


    /**
     * Issue a toast contains the standard warning for number format error.
     */
    public static void toastNumberFormatError(Context context) {
        Toast.makeText(context, "Wrong number format. Enter valid number(s)", Toast.LENGTH_LONG).show();
    }
}