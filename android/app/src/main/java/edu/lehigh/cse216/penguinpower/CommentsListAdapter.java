package edu.lehigh.cse216.penguinpower;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

/**
 * The adapter for the recycleView of comment list
 */
class CommentsListAdapter extends RecyclerView.Adapter<CommentsListAdapter.ViewHolder> {

    //User should customize their own click listener.
    interface ClickListener {
        void onClick(TagComments d);
    }

    private ClickListener mClickListener;

    void setClickListener(ClickListener c) {
        mClickListener = c;
    }

    //An inner class that holds the content and the other view for vote.
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView mComments;
        TextView mDate;
        ImageView mView;

        //Constructor, find views.
        ViewHolder(View itemView) {
            super(itemView);
            this.mComments = itemView.findViewById(R.id.tag_comments);
            this.mDate = itemView.findViewById(R.id.tag_date);
            //this.mView = itemView.findViewById(R.id.tag_image);
        }
    }

    private ArrayList<TagComments> mData;
    private LayoutInflater mLayoutInflater;

    /**
     * Initiate an Adapter.
     */
    CommentsListAdapter(Context context, ArrayList<TagComments> data) {
        mData = data;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.comments_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final TagComments d = mData.get(position);
        holder.mComments.setText(d.mText);
        holder.mDate.setText(d.mDate);
//        holder.mView.setImageBitmap(d.mImage);
        // Attach a click listener to the view we are configuring
        final View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(d);
            }
        };
        holder.mComments.setOnClickListener(listener);
        holder.mDate.setOnClickListener(listener);
    }
}