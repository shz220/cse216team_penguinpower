package edu.lehigh.cse216.penguinpower;

import android.support.annotation.NonNull;

/*
A tagLite class for (index, title, popularity, dateCreated)
used for display message list
 */

public class TagLite implements Comparable<TagLite> {
    /**
     * An integer index for this message
     */
    int mIndex;

    String mTitle;
    /**
     * An integer counter for popularity
     */
    int mPopularity;

    String mDate;

    /**
     * Construct a Tag by setting its index and text
     *
     * @param idx An integer index for this message
     * @param title The string contents for this message
     * @param popularity An integer counter for upvote - downvote
     * @param date An string for date created
     */
    TagLite(int idx, String title, int popularity, String date) {
        mIndex = idx;
        mTitle = title;
        mPopularity = popularity;
        mDate = date;
    }


    @Override
    public int compareTo(@NonNull  TagLite taglite) {
        return(this.mIndex - taglite.mIndex);
    }
}
