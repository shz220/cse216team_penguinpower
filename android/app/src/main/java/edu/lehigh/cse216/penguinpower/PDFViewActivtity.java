package edu.lehigh.cse216.penguinpower;

import android.annotation.TargetApi;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.github.barteksc.pdfviewer.PDFView;

import java.io.File;
import java.io.InputStream;
import java.util.Base64;

public class PDFViewActivtity extends AppCompatActivity{
    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_pdf_activity);

        Bundle extras= getIntent().getExtras();
        String pdfPath = extras.getString("pdfPath");
        Uri uri=Uri.fromFile(new File(pdfPath));

        //Log.d("yac320", "encodePdf" + encodedPdf);
        PDFView pdfView = findViewById(R.id.pdfView);
        InputStream input = getResources().openRawResource(R.raw.default_pdf);
        //Base64.Decoder decoder = Base64.getDecoder();
       // byte[] pdfBytes = decoder.decode(encodedPdf);

        //pdfView.fromStream(input)
        //pdfView.fromBytes(pdfBytes)
        pdfView.fromUri(uri)
                .defaultPage(0)
                .enableSwipe(true)
                .swipeHorizontal(false)
                .enableAnnotationRendering(true)
                .scrollHandle(null)
                .load();;
    }
}
