package edu.lehigh.cse216.penguinpower;

import android.support.annotation.NonNull;

/*
A tag class for (index, content, like, dislike, dateCreated)
used for messageInfoActivity
 */
public class Tag implements Comparable<Tag> {
    /**
     * An integer index for this message
     */
    int mIndex;

    /**
     * The string contents for this message
     */
    String mContent;

    /**
     * An integer counter for upvote
     */
    int mLike;

    /**
     * An integer counter for downvote
     */
    int mDislike;

    String mDate;

    /**
     * Construct a Tag by setting its index and text
     *
     * @param idx An integer index for this message
     * @param content The string contents for this message
     * @param like An integer counter for upvote
     * @param dislike An integer counter for downvote
     */
    Tag(int idx, String content, int like, int dislike, String date) {
        mIndex = idx;
        mContent = content;
        mLike = like;
        mDislike = dislike;
        mDate = date;
    }


    @Override
    public int compareTo(@NonNull  Tag tag) {
        return(this.mIndex - tag.mIndex);
    }
}
