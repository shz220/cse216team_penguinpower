package edu.lehigh.cse216.penguinpower;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * An activity that edit/delete a comment.
 *  edit a comment
 *  PUT /comments
 *  request: { String username, String sessionKey, int commentId, String content }
 *
 *  delete a comment
 *  DELETE /comments/:commentId request: String username, String sessionKey
 */
public class CommentsInfoActivity extends AppCompatActivity {
    // get commentId and comment text from CommentsMainActivity
    int commentId = CommentsMainActivity.getCommentId();
    String comment = CommentsMainActivity.getComment();
    private int uid;
    private File uploadPhoto;
    private File oldPhoto;
    private static int REQUEST_IMAGE = 20001;
    private TextView contentView;
    private Button deleteBtn;
    private Button cancelBtn;
    private Button applyBtn;
    private Button attachBtn;
    private ImageView imageView;

    private VolleyConnection con;

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
        setContentView(R.layout.comments_edit);     //comments_edit.xml

        contentView = findViewById(R.id.edit_content);
        deleteBtn = findViewById(R.id.edit_delete);
        cancelBtn = findViewById(R.id.edit_cancel);
        applyBtn = findViewById(R.id.edit_apply);
        attachBtn = findViewById(R.id.attachBtnEditCmt);
        imageView = findViewById(R.id.editCommentImageView);
        //set connection
        con = VolleyConnection.getVolleyConnection(this);

        // get content of the comment
        contentView.setText(comment);

        //checking cache for image
        File cacheDir = getCacheDir();
        oldPhoto = new File(cacheDir.getAbsolutePath() +"/cmtImage" + commentId +".jpg" );
        if(oldPhoto.exists()){
            Bitmap bitmap = BitmapFactory.decodeFile(oldPhoto.getPath());
            imageView.setImageBitmap(bitmap);
        }
        //for testing purposes
        else{
            Log.d("bcb320", "image file for cid: " + commentId + "does not exist");
        }
        uploadPhoto = oldPhoto;

        setViewDisplayOn();
    }

    /**
     * editViews display on, after click show. Set listeners to appeared buttons.
     */
    private void setViewDisplayOn() {
        contentView.setVisibility(View.VISIBLE);
        deleteBtn.setVisibility(View.VISIBLE);
        cancelBtn.setVisibility(View.VISIBLE);
        applyBtn.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.VISIBLE);
        //set listeners
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel();
            }
        });
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });
        applyBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                apply();
            }
        });
        attachBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addPhoto();
            }
        });
    }

    /**
     * When change is canceled, changes are discarded, and pages are set clean.
     */
    private void cancel() {
        Toast.makeText(CommentsInfoActivity.this, "changes discarded.", Toast.LENGTH_SHORT).show();
        // go back to CommentsMainActivity
        Intent i = new Intent(getApplicationContext(), CommentsMainActivity.class);
        startActivityForResult(i, 110);
    }

    /**
     * When hit, a request is sent to the server to delete this message.
     */
    private void delete() {
        // to get the sessionkey and username
        SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
        final String sessionKey = pref.getString("sessionKey", null);
        final String username = pref.getString("username", null);

            VolleyConnection con = VolleyConnection.getVolleyConnection(CommentsInfoActivity.this);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE,
                    Tools.BASE_URL + "/comments/"+commentId,
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                //a delete post is designed to return null right now...
                                if (Tools.parseResponse(response.toString()) == null) {
                                    //on success, leave current view
                                    //remove image from cache
                                    if(uploadPhoto.exists()) uploadPhoto.delete();
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    i.putExtra("commentId", commentId);
                                    i.putExtra("type", "deleteComment");
                                    setResult(Activity.RESULT_OK, i);
                                    finish();
                                }
                            } catch (JSONException e) {
                                Tools.toastJSONError(CommentsInfoActivity.this);
                                e.printStackTrace();
                            } catch (Tools.ServerSideException e) {
                                Toast.makeText(CommentsInfoActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Tools.toastInternetError(CommentsInfoActivity.this);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("sessionKey",sessionKey);
                    params.put("username",username);

                    Log.w("myApp","sessionKey="+params.get("sessionKey") );
                    Log.w("myApp","username="+params.get("username"));
                    return params;
                }
            };
            con.addToRequestQueue(request);
        }

    /**
     * apply change and put the change to the server.
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void apply() {
        //get content and check if valid.
        String content = contentView.getText().toString();
        if (content.length() < 10) {
            Toast.makeText(this, "content too short.", Toast.LENGTH_LONG).show();
            return;
        }

        // to get the sessionkey and username
        SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
        String sessionKey = pref.getString("sessionKey", null);
        String username = pref.getString("username", null);

        String encoded=null;
        if(uploadPhoto != null){
            try {
                Log.d("yac320","upload photo is not null");
                Base64.Encoder encoder = Base64.getEncoder();
                encoded = "data:image/jpeg;base64," + encoder.encodeToString(Files.readAllBytes(Paths.get(uploadPhoto.getPath())));
            } catch (IOException e){
                Log.d("bcb320", "unable to read upload image");
            }
        }

        String jsonString="";

        if(encoded==null){
            jsonString="{\"sessionKey\" : " + sessionKey + ", \"username\" : " + username + ", \"commentId\" : " + commentId + ",\"content\" : \"" + content + "\"}";
        }else{
            jsonString="{\"sessionKey\" : " + sessionKey + ", \"username\" : " + username + ", \"commentId\" : " + commentId + ",\"content\" : \"" + content + "\",\"document\" : \"" + encoded+ "\"}";
        }
        Log.d("yac320"," encoded :"+encoded);
        try {
            VolleyConnection con = VolleyConnection.getVolleyConnection(this);
            JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT,
                    Tools.BASE_URL + "/comments",
                    new JSONObject(jsonString),
                    new Response.Listener<JSONObject>() {
                        @TargetApi(Build.VERSION_CODES.O)
                        @RequiresApi(api = Build.VERSION_CODES.O)
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Log.d("yac320",":getting response: "+response.toString());
                                Tools.parseResponse(response.toString());
                                //update the cache
                                //save image to cache, create image if it does not yet exist
                                //uncomment once route is updates to accept images
                                /*if(uploadPhoto.exists()){
                                    Files.write(Paths.get(oldPhoto.getPath()), Files.readAllBytes(uploadPhoto.toPath()));
                                }
                                   */
                                //on success, set intent and finish
                                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                i.putExtra("type", "editComment");
                                i.putExtra("commentId", commentId);
                                setResult(Activity.RESULT_OK, i);
                                finish();
                            } catch (JSONException e) {
                                Tools.toastJSONError(CommentsInfoActivity.this);
                                e.printStackTrace();
                            } catch (Tools.ServerSideException e) {
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Tools.toastInternetError(CommentsInfoActivity.this);
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> params = new HashMap<>();
                    params.put("Content-Type", "application/json");
                    params.put("Accept", "application/json");
                    params.put("Accept-Encoding", "utf-8");
                    return params;
                }
            };
            con.addToRequestQueue(request);
        } catch (JSONException e) {
            Tools.toastJSONError(this);
            e.printStackTrace();
        }



    }

    private void addPhoto(){
        AlertDialog.Builder builder = new AlertDialog.Builder((CommentsInfoActivity.this));
        builder.setTitle("Attach Image:");
        final CharSequence[] options = {"Take Photo", "Pick a Photo", "Cancel"};
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if(options[i].equals("Take Photo")){
                    if(MainActivity.permissionCount >= 2) takePhoto();
                } else if (options[i].equals("Pick a Photo")) {
                    if(MainActivity.permissionCount >=2) choosePhoto();
                }
            }
        });
        builder.show();
    }

    private void takePhoto(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Log.d("bcb320", "dispatched take picture intent: " + Boolean.toString(takePictureIntent.resolveActivity(getPackageManager()) != null));
        if(takePictureIntent.resolveActivity(getPackageManager()) != null){
            File photo = null;
            try{
                photo = createImageFile();
            } catch (IOException e){
                Log.d("bcb320","Cannot create file");
            }

            if(photo != null) {
                uploadPhoto = photo;
                //convert file to uri
                Uri uri = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".provider", photo);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                takePictureIntent.setFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE);
            }
        }

    }

    private static int PICK_PHOTO = 10046;
    private void choosePhoto(){
        Log.d("yac320","choose photo");
        Intent choosePhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if(choosePhotoIntent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(choosePhotoIntent, PICK_PHOTO);
        }
    }

    String mCurrentPhotoPath = null;

    private File createImageFile() throws IOException {
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timestamp;
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        mCurrentPhotoPath = image.getAbsolutePath();
        image.deleteOnExit();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data ){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_IMAGE && resultCode == RESULT_OK){
            Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath);
            bitmap = CreateMessageActivity.getRightOrientation(bitmap, mCurrentPhotoPath);
            imageView.setImageBitmap(bitmap);
            MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, uploadPhoto.getName(), uploadPhoto.getName());
        }
        if(requestCode == PICK_PHOTO && resultCode == RESULT_OK){
            Uri badUri = data.getData();
            String realpath = getRealPathFromURI(badUri);
            Log.d("bcb320", "uri: " + realpath);
            uploadPhoto = new File(realpath);
            Bitmap bitmap = BitmapFactory.decodeFile(realpath);
            bitmap = CreateMessageActivity.getRightOrientation(bitmap, realpath);
            imageView.setImageBitmap(bitmap);
        }

    }

    private String getRealPathFromURI(Uri contentUri){
        String[] images = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, images, null, null, null);
        int colIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return  cursor.getString(colIndex);
    }

}
