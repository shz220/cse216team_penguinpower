package edu.lehigh.cse216.penguinpower;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

/*
get comments
GET /comments/:messageId
request header: String username, String sessionKey
response: { int messageId, int userId, int commentId, String commentText, Date dateCreated }
 */
public class CommentsMainActivity extends AppCompatActivity {

    private static Context context;
    private static int commentId;
    private static String comment;
    private static String document;
    private static String type;
    private static int userId;  //author of this comment
    private String username;
    /**
     * mData holds the data we get from Volley
     */
    ArrayList<TagComments> mData = new ArrayList<>();
    // use the mid stored in MessageInfoActivity.java
    int mid = MessageInfoActivity.getMid();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comments_main); //comments_main.xml layout

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {    //create a new comment
                Intent i = new Intent(getApplicationContext(), CreateCommentsActivity.class);
                startActivityForResult(i, 100);
            }
        });
        populateListFromVolley();
        context = getAppContext();

    }

    /**
     * Populate the tags display with data from server.
     */
    private void populateListFromVolley() {
            // Instantiate the connection..
            VolleyConnection con = VolleyConnection.getVolleyConnection(this);

            // to get the sessionKey and username
            SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
            final String sessionKey = pref.getString("sessionKey", null);
            final String username = pref.getString("username", null);

            // POST /comments/getListing/:messageId
            String url = Tools.BASE_URL + "/comments/"+ mid;
            // Request a string response from the provided URL.
            JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.GET, url,
                    null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            populateListFromJson(response.toString());
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("shz220", "That didn't work!");
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("sessionKey",sessionKey);
                    params.put("username",username);

                    Log.w("myApp","sessionKey="+params.get("sessionKey") );
                    Log.w("myApp","username="+params.get("username"));
                    //params.put("Content-Type", "application/json");
                    //params.put("Accept", "application/json");
                    //params.put("Accept-Encoding", "utf-8");
                    return params;
                }
            };

            // Add the request to the RequestQueue.
            con.addToRequestQueue(stringRequest);
        }


    /**
     * Helper method for populateListFromVolley, should not be called.
     * Retrieve data from JSON response and add to the list.
     */
    private void populateListFromJson(String response) {
        //clean up existing tags.
        mData = new ArrayList<>();

        try {
            JSONObject json = new JSONObject(response);
            String status = json.getString("mStatus");
            if (status.equals("error")) {
                String errorStr = json.getString("mMessage");
                Toast.makeText(this, "Error from the server: " + errorStr, Toast.LENGTH_LONG).show();
                return;
            }
            if (status.equals("ok")) {
                //Toast.makeText(this, "Wrong username or password: " + username, Toast.LENGTH_LONG).show();
                JSONArray data = json.getJSONArray("mData");
                for (int i = 0; i < data.length(); i++) {
                    int mid = data.getJSONObject(i).getInt("messageId");
                    int uid = data.getJSONObject(i).getInt("userId");
                    int commentId = data.getJSONObject(i).getInt("commentId");
                    String text = data.getJSONObject(i).getString("commentText");
                    String date = data.getJSONObject(i).getString("dateCreated");
                    Log.d("yac320","before getting c_id");
                    String username=data.getJSONObject(i).getString("username");
                    Log.d("yac320","after getting all data"+data.getJSONObject(i).toString());
                    document=null;
                    type=null;
                    try {
                        document = data.getJSONObject(i).getString("document");
                        type = data.getJSONObject(i).getString("type");
                    }catch(JSONException j){
                        Log.d("yac320","no document for encoded");
                    }
                    Log.d("yac320","commentMainAcitivity after getting all the data");
                    Log.d("yac320","type="+type);
                    Log.d("yac320","document="+document);
                    Intent t=new Intent(getApplicationContext(),ShowSingleCommentActivity.class);
                    t.putExtra("type",type);
                    t.putExtra("document",document);
                    t.putExtra("cid",commentId);
                    mData.add(new TagComments(mid, uid, commentId, text, date, type, document,username));
                    Log.d("yac320",mData.toString());
                    //get encoded string if type is of image once backend works
                    //replace testImage with bitmap of encodedImage per comment, see the getBase64Image method
                }
            }
            Collections.sort(mData, Collections.reverseOrder());
        } catch (final JSONException e) {
            Log.d("shz220", "Error parsing JSON file: " + e.getMessage());
            return;
        }
        Log.d("shz220", "Successfully parsed JSON file.");

        RecyclerView rv = findViewById(R.id.list_view);         //use the same RecyclerView as list of messages
        LinearLayoutManager llm = new LinearLayoutManager(CommentsMainActivity.this);
        rv.setLayoutManager(llm);
        CommentsListAdapter adapter = new CommentsListAdapter(CommentsMainActivity.this, mData);
        adapter.setClickListener(new CommentsListAdapter.ClickListener() {
            @Override
            public void onClick(TagComments d) {    // to comments detailed info page (can edit and delete)
                commentId = d.mCommentId;
                comment = d.mText;
                userId = d.mUserId;
                type=d.mType;
                document=d.mDocument;
                username=d.mUsername;
                /*if (userLoginId != userId) {    // only author of the comment can edit or delete it
                    Toast.makeText(CommentsMainActivity.this, "You are not the author for this comment. User Id: " + userId + "User LoginId: " + userLoginId, Toast.LENGTH_LONG).show();
                    return;
                }
                Intent i = new Intent(getApplicationContext(), CommentsInfoActivity.class);
                i.putExtra("mid", d.mIndex);*/
                Intent i=new Intent(getApplicationContext(),ShowSingleCommentActivity.class);
                i.putExtra("type",type);
                i.putExtra("document",document);
                i.putExtra("username",username);
                i.putExtra("userId",userId);
                startActivityForResult(i, 110);
            }
        });
        rv.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //refresh tags.
        populateListFromVolley();
        //All intents from different activities will contain the same attribute:
        //type: anActionType
        if (resultCode == RESULT_OK) {
            switch (data.getStringExtra("type")) {
                case "deleteComment":
                    Toast.makeText(this, "Comment #" + data.getIntExtra("commentId", -1) + " is deleted.", Toast.LENGTH_LONG).show();
                    break;
                case "editComment":
                    Toast.makeText(this, "Comment #" + data.getIntExtra("commentId", -1) + " is edited.", Toast.LENGTH_LONG).show();
                    break;
                case "addComment":
                    Toast.makeText(this, "Your Comment is added.", Toast.LENGTH_LONG).show();
                break;
                default:
                    Toast.makeText(this, "No Change is applied.", Toast.LENGTH_SHORT).show();
                    break;
            }
        } else {
            Toast.makeText(this, "No Change is applied.", Toast.LENGTH_SHORT).show();
        }
    }

    // to store the commentsId value for use in CommentsInfoActivity.class
    public static int getCommentId()
    {
        return commentId;
    }
    public static String getComment()
    {
        return comment;
    }

    public static Context getAppContext() {
        return context;
    }

}
