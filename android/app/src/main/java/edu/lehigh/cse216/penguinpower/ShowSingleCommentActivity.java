package edu.lehigh.cse216.penguinpower;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.github.barteksc.pdfviewer.PDFView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class ShowSingleCommentActivity extends AppCompatActivity{

    private TextView titleView;
    private TextView contentView;
    private Button editBtn;
    private Button deleteBtn;
    private Button displayDocumentBtn;

    private static int commentId;

    private VolleyConnection con;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.comments_info);

        //find TextViews and Buttons
        titleView= findViewById(R.id.comments_id_view);
        contentView = findViewById(R.id.comments_content_view);
        displayDocumentBtn= findViewById(R.id.Display_Doc);
        editBtn = findViewById(R.id.comments_edit_btn);
        deleteBtn=findViewById(R.id.comments_delete_btn);
        con = VolleyConnection.getVolleyConnection(this);

        commentId = CommentsMainActivity.getCommentId();
        final String comment = CommentsMainActivity.getComment();


        titleView.setText(comment);

        // to get the sessionkey and username
        SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
        final String sessionKey = pref.getString("sessionKey", null);
        final String username = pref.getString("username", null);

        //acquire message info by mid from server.
        //get one message: POST /messages/:messageId  request: { String username, String sessionKey }
        VolleyConnection con = VolleyConnection.getVolleyConnection(ShowSingleCommentActivity.this);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                Tools.BASE_URL + "/commentSingle/" + commentId,
                null, //new JSONObject("{\"username\" : " + username + ",\"sessionKey\" : " + sessionKey + "}"),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Process the response gotten from the server.
                        try {
                            JSONObject json = new JSONObject(response.toString());
                            String status = json.getString("mStatus");
                            if (status.equals("error")) {
                                String errorstr = json.getString("mMessage");
                                //Toast.makeText(this,errorstr , Toast.LENGTH_LONG).show();
                                setResult(Activity.RESULT_CANCELED);
                                finish();
                            }
                            if (status.equals("ok")) {
                                //parse data from json. EDIT: naming all changed
                                JSONObject data = json.getJSONObject("mData");
                                Log.d("yac320", ""+data.toString());
                                final String content = data.getString("commentText");
                                final String date = data.getString("dateCreated");

                                assert json != null;
                                //contentView.setText(String.format(getString(R.string.content_display), content, commentId, date));


                                String type=null;
                                String encoded=null;
                                Bundle extras= getIntent().getExtras();
                                if (extras != null)
                                {
                                    type=extras.getString("type");
                                    Log.d("yac320","in if type:"+type);
                                    encoded=extras.getString("document");
                                    Log.d("yac320","document:"+encoded);
                                }
                                String username=extras.getString("username");
                                int uid=extras.getInt("userId");
                                contentView.setText(String.format("created by: %s\ncreated on: %s", username, date));
                                setBtn(uid);
                                setDisplayDocumentBtn(type,encoded);
                                //update once you can get encoded String from backend
                                // if(!image.exists())showBase64Image();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(ShowSingleCommentActivity.this, "Error: JSON Cannot be Parsed.", Toast.LENGTH_LONG).show();
                            setResult(Activity.RESULT_CANCELED);
                            finish();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Tools.toastInternetError(ShowSingleCommentActivity.this);
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("sessionKey",sessionKey);
                params.put("username",username);

                Log.w("myApp","sessionKey="+params.get("sessionKey") );
                Log.w("myApp","username="+params.get("username"));
                //params.put("Content-Type", "application/json");
                //params.put("Accept", "application/json");
                //params.put("Accept-Encoding", "utf-8");
                return params;
            }
        };
        con.addToRequestQueue(request);


    }


    /**
     * Set texts and listeners for like and dislike buttons.
     */
    private void setBtn(final int uid) {

        //send a intent to EditMessageActivity and opens it.
        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginActivity.userId != uid) {    // only author of the comment can edit or delete it
                    Toast.makeText(ShowSingleCommentActivity.this, "You are not the author for this comment." , Toast.LENGTH_LONG).show();
                    return;
                }
                Intent i = new Intent(ShowSingleCommentActivity.this, CommentsInfoActivity.class);
                i.putExtra("cid", commentId);
                startActivityForResult(i, 230);
            }
        });

        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)   {
                if (LoginActivity.userId != uid) {    // only author of the comment can edit or delete it
                    Toast.makeText(ShowSingleCommentActivity.this, "You are not the author for this comment.",Toast.LENGTH_LONG).show();
                    return;
                }
                // to get the sessionkey and username
                SharedPreferences pref = getSharedPreferences("MyPrefs", 0);
                final String sessionKey = pref.getString("sessionKey", null);
                final String username = pref.getString("username", null);

                VolleyConnection con = VolleyConnection.getVolleyConnection(ShowSingleCommentActivity.this);
                JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE,
                        Tools.BASE_URL + "/comments/"+commentId,
                        null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    //a delete post is designed to return null right now...
                                    if (Tools.parseResponse(response.toString()) == null) {
                                        //on success, leave current view
                                        //remove image from cache
                                       //if(uploadPhoto.exists()) uploadPhoto.delete();
                                        Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                        i.putExtra("commentId", commentId);
                                        i.putExtra("type", "deleteComment");
                                        setResult(Activity.RESULT_OK, i);
                                        finish();
                                    }
                                } catch (JSONException e) {
                                    Tools.toastJSONError(ShowSingleCommentActivity.this);
                                    e.printStackTrace();
                                } catch (Tools.ServerSideException e) {
                                    Toast.makeText(ShowSingleCommentActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Tools.toastInternetError(ShowSingleCommentActivity.this);
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("sessionKey",sessionKey);
                        params.put("username",username);

                        Log.w("myApp","sessionKey="+params.get("sessionKey") );
                        Log.w("myApp","username="+params.get("username"));
                        return params;
                    }
                };
                con.addToRequestQueue(request);

            }

        });

    }

    private void setDisplayDocumentBtn(String type, final String encodedString){
        Log.d("yac320","setDisplayDocumentBtn: "+type+" "+encodedString);
        if(type==null){
            displayDocumentBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(ShowSingleCommentActivity.this,"no document" , Toast.LENGTH_LONG).show();
                }
            });
        }else if(type.equals("image/jpeg")){
            displayDocumentBtn.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ShowSingleCommentActivity.this, ImageViewActivity.class);
                    i.putExtra("cid", commentId);
                    // i.putExtra("encoded",encodedString);

                    // String encodedImage = "iVBORw0KGgoAAAANSUhEUgAAAGcAAABZCAYAAAAjMTokAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABCuSURBVHhe7d0FkFzFFgbg4O7uwd3d3Z3CHu7u7hQOBVRB4VK4FB6ch0sgQIgCgbxAIIGgAUIgQIDQ736906nNvtnNzuzM7J28OVWnNnNn5t7u8/fRPj3pMmrUqNDgfHIDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBxzA5wccwOcHHMDnBzzBAfO77//HkaPHh3+/PPPyH///Xf4559/2mSfSZ/H7lHs3rXmugfnt99+C3/88UcU8JgxY6Kwf/jhh/Dhhx+GF154Idxwww3htNNOC/vvv3/Yb7/94t/m/z755JPD1VdfHZ588snQv3//8O2330Zw3cs9/dszij272ly34Fjdf/31VwSD8F599dVw/vnnh3XXXTfMM888YaKJJgqTTDJJmHHGGcNcc80VFlxwwbDQQgv9D88777xhlllmCZNPPnno0qVLmHXWWcMqq6wSjj766PDMM89EoJN2WQTFxlItrjtwkpZY0T169AhHHnlkFPDEE08cpptuurDmmmuGww47LNx4441RuH369AlDhw4NI0eOLHq/r7/+OmrZiy++GO6+++6oZZtuumkEFMCA22GHHcLDDz8cfvrppwhUrbSpbsBJmgKY5557LmywwQZxpdOMXXbZJdx2223ho48+iu9XggDarVu3cMQRR0QN86zFFlssXHrppeHHH3+MIFVbk+oCHEIgjH79+o0FhcAuvPDCCEi1iWl74IEHwmabbRa1iVZdddVV4ddff42LoVoBRO7BET2ha665Jkw99dRhzjnnDJdffnn46quv4vVakgXy/PPPh6222iouEL7p7bffjterAVBuwWHTRUzs/K677hqFYeV+/PHHBVF1Lt1///1h0UUXDVNMMUW49dZb4zULqdhcyuVcgpOA+fLLL8Pyyy8foy4mjCPOExnf5ptvHhfOqaeeGq9VEqDcgZOAGTRoUOjatWuMlp566qk48TwSc3b44YdHgORNqFIA5Q4cDpYD5vD5Fza9Huicc86pOEC5AseErESOduaZZw7vvPNOnGi90FlnnRUBOvPMM+PrjgYJuQHHRNAhhxwSE8o8m7K2SFkIQI8++miM4orNtb2cG3D4GfUtE7vkkksKU60/GjFiRFh11VWj5g8bNiwmzsXm2x7OBTipHDL//POHjTbaKJq3eqa33norTDXVVLFygco1b50ODlAQOy1n6N27d3xd76RGxwq8+eabZWtPp4Nj4N99912Ydtppw1FHHVWYWv2Tgup8880XNtlkk/i6HO3pdHA4zXPPPTdWlOU2ExKdd955UXvee++9mCIUm39b3Kng8DWKhwqJBxxwQGFKEw5ZbDPNNFM46KCD4iJkwovJoTXuVHBEaPfcc0+s9Nosaw+ptbHnu+++e6wUl0uet9dee4XjjjsufPPNN4WrlSdBwdxzzx2BKXWLodPASXWyFVZYIW6Qtbdudvzxx0cwp5xyylhBeP/99wvvtJ+++OKLsPTSS8fdT/fad999C+9Unm6//fZo2l5//fWSA4NOA4fWWPkGbqOsvaTQCBimcJpppol9AqWSiNAm3RxzzBEDkdVXX73wTuXJfhN/etFFF8U5F5NFa9wp4Mhj8BJLLBGrzvxOpGzwYeTIkIVvwp1x+fvvhTzh8W7dwvxdu8a9HdvHkr5SiXmxvQ3c2TPtsz2dLWsZZAjDh4/LmRnNBm0zp/Dt0sizVl555bDxxhvH16VEbTUHJ5Vp2Hpa89BDD8XXEZQBA0Lo0SOE7t1DZgfGZdfU2oYODR9l0c9z//53+OWXX5q+WwZZxfoGenuejbtevWSPIUtMxmXXPHfgwCaQyiD+0RY3f1lKUFBzcISU/ARgOORIVm2fPiG89BJPHcJrrxVn7/nM4MFN36sEZWCHV15p4tae7frLL4fQv79yc+GL7aezzz47lnPs/5RSra4pOFaNkHK33XYLs88+e/j000+bRg8cvQCEkIRUjL1HWP/5T5MJ7ChpBrGz2hycYuw94Ag+jLVEuvPOO2PgoS+ulKCgpuBYNd9nvkPdSdQ1DonWhg0L4YMPQujZs8mcNOfMlEWzxwQ1W70SvHfffTfee3zEt2lx0jaVzGvmFJq0p1+/EOwdJVOWmNnr2zeEzz4L2epq+k6J9Oyzz8bdXP0HudUcdj6toqeffrow9BZEIwiBD2rOrrVwymy4BsJJJ500rLPOOrF7k3N/JVvpr2Uahl/KzKBrJ5xwQgw+mFNhtM7OcYhG8GE//zzuc10rQ1ua08uZ1qkb3nffffkFh0kTJc0222zR/naUmAgBhT0UHZ1AInyhNu3E/m1/SOisrUp7bjWTzmJkgcip5Dy5BYcwhb/yivaYoVJIodGW9i233BKbQeQV/l555ZXh8ccfz9xU5qc6iWgOcFiN3IJjYHq+xPxM3P8L6RxlyvmeXGvOtttuG9Zff/0YUteMxmRJ7p8dN6Plku7Q3EdrtEV3yiKLLJL52szZ1ooG/yuL8s4tvKg97bPPPtHP2rZWQywmm2Jc84DAMQ1lkw+EzLWgb6/KwuEuIQw5snChQKOHZBqVhdFVJhGl6sDaa68drUVuKwRU2v66iIpzrAn9nuVGo7I8ZXSWy4QsFP/p0Szx3DqEQTvUBBxdOOarv7tTC5/pNIBBFFshEj/XF1544bD99tsXhl9jGpGB03ueEL5vfyW8XCKHbbbZJloK2xSlBAO4YuAQPM1wfkVBsbVVAjytT3KPN954ozCN6pAc6IILLohHR5xK+EyWj8aMyJKkH7Ik85Us6Xw++/ti0/UKk6ST1hx66KFx3qWYNFxRcACit3nPPfeMgyn2OQ6R7V1ggQXCaqutFg8iVYMsEHs1ElM2f/HFF4/VhNNPPz2MUWj4JzNpg7YLoXvmj765vOlLFaQhQ4ZEC6GGqGRVqtbgioLDrMnU9QO0Bo7VA0Sxv1Ulu68GHXzwwbHfWqSkrqXF1+tll1228ImMRn+RRXEXFF5UjoYPHx6Pq5ifbfhytAZXFBxH9awUe/xtDchnkSN9JnDiiSdGk1gpAr5amwZFdTZlGwmgLfGllloq3HTTTXE1V4PU7LbYYos4r3QsxHyLyWF8PF5wmCGTJezEBElNmwvfNeGxWtbFF18cB+Wz3vPZlmrtNaJlJrLzzjuHzz//PF4rl1SdlWuAMtlkk4Utt9wyXmdG+R7PsXj8VZmuNKmOr7TSSvH+xx57bLxWjjlL3CY4SYDO6av40giCV3a3QoCWAAJOr169oo3XG+A9jt9gaQZquYJ8x+fcU4DAJBJaOQmq+2hBIhjVZ2CP3WXNSNGRwGgr4O64447COx0nWih/0walhmaBoI4Ag9sEx8q/7LLL4oS19zijr/TttQaLTz75JK5KnyVo7UYmrlyRBKWJoi27m8Jvx9bTqvO3VNMjIhMAqHqrOhc7yKs4aj+f7xm70dcBco7o5ptvjn7MuDWwa781H/NqOddSuVVw3BzyohwT4k9w37594+pWgrc6EaED54knnoidJrZkaZDfA+AcDbatsoXvJ5Cvv/76WN4xWUL0Yw2OthNsW0RbbRHouZZDzTDDDPG8THPae++9430lhOWSeTg35DkaVNzPCbzrrrsuzgGX62NacqvgAGbgwIHRhyQHDzAPR4ARojJBCRwaQq3XWGONCFQCpZjGFOOkRe5lg2zDDTeMAk8C2GOPPSJ4NHTw4MHjhOEDBgyIB2iZFhrOtzBvzU2kfRXhrZAacEr5aT5tkUXpvJAwXBkmWY8VV1wxjkcHELPa1gIsh1sFx6D1hAlD7VoSWHqPANlu4FDtBM61114bzZqqM9Pic+WsIpNMQrMPwz/oO3BEhG8iHP6JhonGEgnPNRoyuVqnCC9FhomYO5VxwvU5xci2iK/1XPMiC9GeLXZNgggolTBhxbhNcCRyzJOBJOeWJrvjjjtGW2tgySxx7MwJ4VmhnLDPe68ckHzHcxNQTiPQELuZtAQ4Y7P+jDQL8jvrrbde1DwBilX9s63nFtS9e/fo5wi3LXLyASh8mfsZV9LuaoGSuFVwrF5byepC+q6QiRgYm8vcSfAQIbrOx/A3wlamzeqUY6SznR2ZjAXgOYSCtt566/gjRM1J/UrVgd/baaedorYtueSSMcAQ5pZDtJHWmEPSkvaa6Y5yq+AYAIGnY9x6zM4444xozpw7mX766eMqTuYOyfYJJhX5TjnllDgx3zdJk2v5nHLYuLbbbruw1lprjdUqpXlgcdRMEUCwHxkSMfJZfE6pJNDgR2lNpX3K+LhVcDABI5rDXDEZQCGUZHPTKiKwK664IoaTOjHTCmcaZcwcaiXB4TfSHgmiGRaBnAwZO01D/BKAmFtOvZRzQEr+wNHJkytwCD4BJOdQzFPdJfiWfoS6I8AkEHzfvxNXykYncJjOBI78QmQn7yhG8iCnzAAIKNUDxzMUaW0ft0ZAEVzcdddd8VnFxlMtbhOcxEAg2MTNQSn2uWrbZOAwswICrxFtoNlt5TDGJ/Kzx8LMCalFfTYAWyPRIqvBXHpuy7FUk9sFTt6YkE466aSYy8h3kIhMaD+2/3o8JNQXRvNVbZHnLbPMMrFryHOrvfCac12Cw7zce++9MedRPUh04IEHxjLT+HKXUknya5/KAqil36lLcPhByaT8Q/UikV5k19JPbFWK+DG+SsG3ln6nLsHhO7CcRomGn0OCEfUutTUmqFJkK0MQIW9CtTJtdQsO4at+W9HNtwYUNzl5BddKkrxKAKKe5/nFxlVprjtwEjByF2Ua4Kh3SXLVAmmS182PIzKD8iylGLuT7lMq2a+R49Ei92s5rmpw3YEjx/JzxMJmWwo6W0RpaoCSRVVp1fFEMnuFWCAKIOySKtaWSgIQ1Q6V+gY4RZitR7YSFD5TGO2v/MUGnf2mRK6rrQHNGVRVBJpXDgky1BlLPTrYEa4rcJLj11ZFY8ZHquQ0xlZGW6T3QJnG51S8E7tmL8dzJa58Dq1jVgFU7cCgLsAhBMIgFMISpemkGV+vgaq5/Z3xBQf8EBCbH7rCzJhfe6ep/I3CrnJO+mEK4wFcsTFXgusCHMAgZRYbf6lSLjJrK+F0UMtqT+awNXKulE+xx6NGh/1bx6b+N1vyggyv/WCf6FAAkn7buloaVBfgSPyA4LecRV2PPfZYNDMcvGBAd49KgY23sT84kZFfo6INkkiF13LI71jroVAqsnno/0dwUo7Jw6ha2lM34Pj74IMPRoEQkBWsvmYrWtXYdgATpLNT9dk2haq1Vc+R+1kWRxJtmjGH7gmw9JeALQD92z6n0Klr07kahU+LwaLwXOAYS/ppl/9rcFKEpVhJcwiGoJRrmBd7OMccc0ysDPiRIxGaaM6paedPhdL6BZR2gKWKoCPUdbmSv8stt1zsFeBb+BqACDyA6nfTNKxgJRzM96BqlnPqAhxsdXLASDjLLwAmrWZmjUYRoOvYCvean/LjRzpuFDGBwVQltmNqF1UVQL+dw77+7wT5knv7vvu5v1DdeJCcq5oRW92Ag1PUlkwRYqJEcD179oxmRk80QID2yCOPjGXXCDeZxKQBiROIQLZ7m37LQIGVxqpGIwsEKNWM0hLXFTjNGVAERlCASloFOAASHuB07PAlzVmUpUkxMeGrmbmv+yUgkHvjdM+W46gm1y04LRlYfBMmSEIul4GQ7lXsWbXiCQacCZEb4OSYG+DkmBvg5Jgb4OSYG+DkmBvg5Jgb4OSWR4X/Asu1zr9odBI4AAAAAElFTkSuQmCC";
                    Base64.Decoder decoder = Base64.getDecoder();
                    byte[] imageBytes = decoder.decode(encodedString);

                    File cacheDir = getCacheDir();
                    final File image = new File(cacheDir.getAbsolutePath() +"/cmtImage" +commentId +".jpg" );
                    FileOutputStream outputStream;
                    try {
                        outputStream = new FileOutputStream(image);
                        outputStream.write(imageBytes);
                        outputStream.close();
                    }catch (Exception e) {
                        Log.e("yac320","Error writing file", e);
                    }
                    i.putExtra("imagePath",image.getAbsolutePath());
                    startActivityForResult(i, 230);
                }
            });
        }else if(type.equals("application/pdf")){
            displayDocumentBtn.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(ShowSingleCommentActivity.this, PDFViewActivtity.class);
                    i.putExtra("cid", commentId);

                    Base64.Decoder decoder = Base64.getDecoder();
                    byte[] pdfBytes = decoder.decode(encodedString);

                    File cacheDir = getCacheDir();
                    final File pdf = new File(cacheDir.getAbsolutePath() +"/cmtPdf" +commentId +".pdf" );
                    FileOutputStream outputStream;
                    try {
                        outputStream = new FileOutputStream(pdf);
                        outputStream.write(pdfBytes);
                        outputStream.close();
                    }catch (Exception e) {
                        Log.e("yac320","Error writing file", e);
                    }
                    i.putExtra("pdfPath",pdf.getAbsolutePath());
                    // i.putExtra("encoded",tempFilePdf(MessageInfoActivity.this,encodedString));
                    // i.putExtra("encoded",encodedString);
                    startActivityForResult(i, 230);
                }
            });
        }

    }

    /**
     * check if edit success. if so, back to the home view.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            setResult(RESULT_OK, data);
            finish();
        } else {
            Toast.makeText(this, "No Change is applied.", Toast.LENGTH_SHORT).show();
        }
    }

}

