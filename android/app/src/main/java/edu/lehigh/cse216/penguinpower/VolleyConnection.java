package edu.lehigh.cse216.penguinpower;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * A Singleton Class for volley connection to the server
 * No need to change at all
 */

public class VolleyConnection {
    private static VolleyConnection mInstance;
    private RequestQueue mRequestQueue;

    private VolleyConnection(Context context) {
        mRequestQueue = Volley.newRequestQueue(context.getApplicationContext());
    }

    /**
     * Acquire for the connection
     *
     * @param context could be any, Eventually converted the application default.
     */
    public static synchronized VolleyConnection getVolleyConnection(Context context) {
        if (mInstance == null) {
            mInstance = new VolleyConnection(context);
        }
        return mInstance;
    }

    /**
     * Add a request to the queue of requests
     */
    public void  addToRequestQueue(Request request) {
        mRequestQueue.add(request);
    }

    /**
     * Only called when you are sure the connection has been setup.
     *
     * @throws IllegalStateException when the instance is not set up.
     */
    public static synchronized VolleyConnection getInstance() {
        if (null == mInstance) {
            throw new IllegalStateException(VolleyConnection.class.getSimpleName() +
                    " is not initialized, call getInstance(context) first");
        }
        return mInstance;
    }
}

