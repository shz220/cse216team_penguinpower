package edu.lehigh.cse216.penguinpower;


import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeUnit;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ViewInfoFunctionalTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public synchronized void viewInfoFunctionalTest() {
        onView(isRoot()).perform(WaitIdTool.waitId(R.id.card_view, TimeUnit.SECONDS.toMillis(15)));
        ViewInteraction listView = onView(withId(R.id.list_view));
        listView.perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        ViewInteraction textView = onView(withId(R.id.info_content_view));
        textView.check(matches(isDisplayed()));

        ViewInteraction textView2 = onView(withId(R.id.info_title_view));
        textView2.check(matches(isDisplayed()));

        pressBack();

        onView(isRoot()).perform(WaitIdTool.waitId(R.id.card_view, TimeUnit.SECONDS.toMillis(15)));
        ViewInteraction recyclerView = onView(withId(R.id.list_view));
        recyclerView.check(matches(isDisplayed()));

    }

}
