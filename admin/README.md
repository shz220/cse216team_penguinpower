# Administrative Interface
# Phase 5 : Shengli Zhu
## Version Release Note
Connect to google drive successfully and can display files (image and pdf) in google drive on JavaFx GUI now. Drawed the admin class diagram.    

Updates on Database Administration Application:     
"Message --> Get Message Detail --> Documents" can display document for that specific message     
"Comment --> Get Comment Detail --> Documents" can display document for that specific comment     
"Message Document --> Get Document Detail" can display document with that document_id    
"Comment Document --> Get Document Detail" can display document with that document_id    
"Map" can show the map table and display all datas in that table.    

New Table: 
Map: Map_id (integer, primary key), Latitude(float), Longitude(float), Description(String), m_id(integer, forieng key)    
(Used for new API: google map)    

## To Run
Use the command line mvn exec:java, or compile it and directly run the result jar file.

## Existing issue
JavaFx don't have a PDFView to display pdf directly in the same way as image. So I used WebView instead. However, WebView cannot display base64 file and can only display files with http link. So I used the "https://drive.google.com/uc?export=view&id={{fileId}}" instead. This requires user to login to google drive first in order to view that pdf document. I also provided the HTML link on "display document" page if it is a pdf. You can simply copy that link and open it on browser to see it.    

# Phase 4 : Xuewei Wang
## Version Release Note
Since the Command-Line-Based Interface is not sufficient for our needs, Administrision App now is packaged in a JavaFX GUI. The new interface is more interactive, with content displayed visually more pleasing. We also add a new functionality, Build Your Own SQL, which allows the user to do more flexible tests. Our group didn't setup Google Drive successfully on time, so I was not able to build a file management system, show pdfs or photos.
## Things Need to be Done
* Drive Management Tool
* Methods to get file from google drive.
* Methods to show pdfs and pictures.
## To Run
Use the command line mvn exec:java, or compile it and directly run the result jar file.


# Phase 3: Yanlin Chen
## To Run
Run mvn package with the heroku database name, this goes for the same as when you run mvn:exec java. 
## Sending Activation Emails
Ask Briana for the sendgrid api key. You need this in your system environment variables to send any emails. (It's not safe to include it in this file).
## Description
In Database.java, four tables are created. One is User1, Message, Vote, and Comment. There has to be a '1' after User because "User" is not allowed for naming variables in SQL. In DatabaseTest.java, every operation of the table written in Database.java is tested through junit and is run when packaging. No problems should occur when running these tests. 

## Getting Started
    After running App.java, it will show all the characters that users can enter
    The instructions for each character will show up once user enter ? to get the menu. 
    The menu is following: 

    Start menu:
    [m] Message
    [u] User
    [c] Comment
    [v] Vote
    [@] Send Activation Email
    [q] Quit
    [?] Help (this message)
    
    Message menu:
    [c] Create Message Table
    [d] Drop Message Table
    [1] Query for a specific row in Message
    [*] Query for all rows for Message
    [-] Delete a row in Message
    [+] Insert a new row in Message
    [~] Update the Title and Content of a row in Message
    [q] Go back to the main menu
    [?] Help (this message)

    User menu:
    [c] Create User Table
    [d] Drop User Table
    [1] Query for a specific row in User
    [*] Query for all rows for User
    [+] Insert a new row in User
    [~] Update row in User
    [q] Go back to the main menu
    [?] Help (this message)

    Comment menu:
    [c] Create Comment Table
    [d] Drop Comment Table
    [1] Query for a row in Comment
    [*] Query for all rows in Comment
    [+] Insert a new row in Comment
    [-] Delete a row in Comment
    [~] Update a row in Comment
    [q] Go back to the main menu
    [?] Help (this message)

    Vote menu:
    [c] Create Vote Table
    [d] Drop Vote Table
    [1] Query for a row in Vote
    [*] Query for all rows in Vote
    [+] Upvote
    [-] Downvote
    [q] Go back to the main menu
    [?] Help (this message)

