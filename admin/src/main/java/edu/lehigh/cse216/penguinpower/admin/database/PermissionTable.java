package edu.lehigh.cse216.penguinpower.admin.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

// Authorization: each user have different persona, and have different functionality
public class PermissionTable implements StandardTable {
    private Connection mConnection;

    private PreparedStatement mCreatePermissionTable;
    private PreparedStatement mDropPermissionTable;
    private PreparedStatement mSelectAllPermission;

    PermissionTable(Connection connection) {
        mConnection = connection;

        try {
            mCreatePermissionTable = mConnection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS user_permission( " + "permission VARCHAR(20) NOT NULL,"
                            + "U_id INTEGER NOT NULL," + "FOREIGN KEY(u_id) REFERENCES user1(u_id) ON DELETE CASCADE)");
            mDropPermissionTable = mConnection.prepareStatement("DROP TABLE if exists user_permission CASCADE");
            mSelectAllPermission = mConnection.prepareStatement("SELECT * FROM user_permission");
            
        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
        }
    }

    /**
     * create the message_document table
     */
    @Override
    public int create() {
        try {
            return mCreatePermissionTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int drop() {
        try {
            return mDropPermissionTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public ResultSet selectAll() {
        try {
            return mSelectAllPermission.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
