package edu.lehigh.cse216.penguinpower.admin.gui.observables;
// display table

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ObservableResultSet {
    private final StringProperty[] properties;

    public ObservableResultSet(String[] list, ResultSet resultSet) throws SQLException {
        ResultSetMetaData metaData = resultSet.getMetaData();
        properties = new StringProperty[metaData.getColumnCount()];
        for (int i = 1; i <= metaData.getColumnCount(); i++) {
            properties[i - 1] = new SimpleStringProperty(this, metaData.getColumnName(i));
            properties[i - 1].set(list[i - 1]);
        }
    }

    /** IMPORTANT: index starts at 1. */
    public StringProperty getStringProperty(int sqlIndex) {
        return properties[sqlIndex - 1];
    }
}