package edu.lehigh.cse216.penguinpower.admin.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import edu.lehigh.cse216.penguinpower.admin.response.MapResponse;

public class MapTable implements StandardTable{
    private Connection mConnection;

    private PreparedStatement mSelectAllMap;
    private PreparedStatement mSelectMap;
    private PreparedStatement mDeleteMap;
    private PreparedStatement mInsertMap;
    private PreparedStatement mUpdateMap;

    private PreparedStatement mCreateMapTable;
    private PreparedStatement mDropMapTable;

    MapTable(Connection connection){
        mConnection = connection;

        try {

            mCreateMapTable = mConnection
                    .prepareStatement("CREATE TABLE IF NOT EXISTS map ("
                            +"Map_id Serial primary key not null,"
                            + "Latitude DECIMAL(10, 8) not null unique, " 
                            + "Longitude DECIMAL(11, 8) not null unique,"
                            + "Description varchar(100)," 
                            + "m_id INTEGER NOT NULL," 
                            + "FOREIGN KEY(m_id) REFERENCES message(m_id) ON DELETE CASCADE)");

            mDropMapTable = mConnection.prepareStatement("Drop Table if exists map CASCADE");
            mDeleteMap = mConnection.prepareStatement("Delete From map where m_id = ?");
            mInsertMap = mConnection
                    .prepareStatement("Insert into map Values(default, ?, ?, ?, ?, ?) returning m_id");
            mSelectAllMap = mConnection.prepareStatement("Select * From map");
            mSelectMap = mConnection.prepareStatement("Select * From map where m_id = ?");
            mUpdateMap = mConnection.prepareStatement(
                    "UPDATE map SET Latitude = ?, Longitude = ?, Description = ? WHERE m_id = ?");

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
        }
    }

    /**
     * Insert a row into map table
     * 
     * @param Latitude
     * @param Longitude
     * @param description
     * @param m_id
     * @return
     */
    public int insert(Float Latitude, Float Longitude, String description, int m_id) {
        int count = 0;
        try {
            mInsertMap.setFloat(1, Latitude);
            mInsertMap.setFloat(2, Longitude);
            mInsertMap.setString(3, description);
            mInsertMap.setInt(4, m_id);
            ResultSet res = mInsertMap.executeQuery();
            if (res.next())
                count += res.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public ResultSet selectAll() {
        try {
            return mSelectAllMap.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * get a specific user based on their user_id
     * 
     * @param U_id
     */
    public MapResponse selectOne(int messageId) {
        MapResponse res = null;
        try {
            mSelectMap.setInt(1, messageId);
            ResultSet rs = mSelectMap.executeQuery();
            if (rs.next()) {
                res = new MapResponse(rs.getInt("map_id"), rs.getFloat("Latitude"), rs.getFloat("Longitude"),
                        rs.getString("Description"), rs.getInt("m_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * update user
     * 
     * @param U_id
     * @param username
     * @param email
     * @param familyName
     * @param givenName
     * @return
     */
    public int update(Float Latitude, Float Longitude, String description, int m_id) {
        int res = -1;
        try {
            mUpdateMap.setFloat(1, Latitude);
            mUpdateMap.setFloat(2, Longitude);
            mUpdateMap.setString(3, description);
            mUpdateMap.setInt(4, m_id);
            res = mUpdateMap.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * create the user table
     */
    @Override
    public int create() {
        try {
            return mCreateMapTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Remove User from the database. If it does not exist, this will print an
     * error.
     */
    @Override
    public int drop() {
        try {
            return mDropMapTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * @param userId delete a row of user by user_id
     */
    public int delete(int messageId) {
        int res = -1;
        try {
            mDeleteMap.setInt(1, messageId);
            res = mDeleteMap.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

}