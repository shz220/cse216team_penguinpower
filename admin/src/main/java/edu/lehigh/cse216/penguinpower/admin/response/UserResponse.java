package edu.lehigh.cse216.penguinpower.admin.response;

public class UserResponse{
    public int userId;
    public String userName;
    public String email;
    public String familyName;
    public String givenName;
    public String picture;
    /**
     * 
     * @param userId
     * @param userName
     * @param email
     * @param familyName
     * @param givenName
     * @param picture   url for the picture
     */
    public UserResponse(int userId, String userName, String email, String familyName, String givenName,String picture) {
        this.userId = userId;
        this.userName = userName;
        this.email = email;
        this.familyName = familyName;
        this.givenName = givenName;
        this.picture=picture;
    }

}