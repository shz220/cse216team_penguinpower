package edu.lehigh.cse216.penguinpower.admin.response;
import java.util.Date;

public class MessageResponse {
    /**
     * The variables in Message table
     */
    public int messageId;
    public int userId;
    public String title;
    public String content;
    public int upVoteCount;
    public int downVoteCount;
    public Date dateCreated;

    public MessageResponse(int messageId, int userId, String title, String content, int upVoteCount, int downVoteCount, Date dateCreated) {
        this.messageId = messageId;
        this.userId = userId;
        this.title = title;
        this.content = content;
        this.upVoteCount = upVoteCount;
        this.downVoteCount = downVoteCount;
        this.dateCreated = dateCreated;
    }

}