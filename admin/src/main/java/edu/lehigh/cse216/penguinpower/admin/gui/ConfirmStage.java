package edu.lehigh.cse216.penguinpower.admin.gui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

// Risky Action Detected
class ConfirmStage extends Stage {
    private boolean isConfirmed = false;

    ConfirmStage(String errMessage) {
        super();
        setAlwaysOnTop(true);
        setResizable(false);
        setTitle("Warning");
        isConfirmed = false;
        VBox root = new VBox(5);
        root.setPadding(new Insets(5));
        Label errorLb = Elements.getErrLabel("Risky Action Detected: " + errMessage);
        errorLb.setWrapText(true);
        errorLb.setAlignment(Pos.TOP_LEFT);
        errorLb.setPrefSize(300, 130);
        Button btnConfirmed = new Button("Confirm");
        Button btnCancel = new Button("Cancel");
        BorderPane buttonBox = new BorderPane();
        buttonBox.setPrefWidth(Double.MAX_VALUE);
        buttonBox.setLeft(btnConfirmed);
        buttonBox.setRight(btnCancel);
        root.getChildren().addAll(errorLb, buttonBox);
        btnConfirmed.setOnAction(event -> {
            isConfirmed = true;
            this.close();
        });
        btnCancel.setOnAction(event -> {
            isConfirmed = false;
            this.close();
        });
        this.setScene(new Scene(root, 300, 150));
    }

    boolean getResult() {
        return isConfirmed;
    }
}