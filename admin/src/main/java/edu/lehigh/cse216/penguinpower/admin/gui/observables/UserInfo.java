package edu.lehigh.cse216.penguinpower.admin.gui.observables;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
public class UserInfo {

    private final SimpleIntegerProperty userIdProperty;
    private final SimpleStringProperty usernameProperty;
    private final SimpleStringProperty firstNameProperty;
    private final SimpleStringProperty lastnameProperty;
    private final SimpleStringProperty emailProperty;

    public UserInfo(int userId, String username, String firstName, String lastName, String email){
        userIdProperty = new SimpleIntegerProperty(userId);
        usernameProperty = new SimpleStringProperty(username);
        firstNameProperty = new SimpleStringProperty(firstName);
        lastnameProperty = new SimpleStringProperty(lastName);
        emailProperty = new SimpleStringProperty(email);
    }
}