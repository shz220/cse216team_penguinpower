package edu.lehigh.cse216.penguinpower.admin.response;

public class MapResponse{
    public int map_Id;
    public Float Latitude;
    public Float Longitude;
    public String Description;
    public int m_id;
    /**
     * 
     * @param map_Id
     * @param Latitude
     * @param Longitude
     * @param Description
     * @param m_id
     */
    public MapResponse(int map_Id, Float Latitude, Float Longitude, String Description, int m_id){
        this.map_Id = map_Id;
        this.Latitude = Latitude;
        this.Longitude = Longitude;
        this.Description = Description;
        this.m_id = m_id;
    }

}