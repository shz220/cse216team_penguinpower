package edu.lehigh.cse216.penguinpower.admin.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;

import edu.lehigh.cse216.penguinpower.admin.response.SimpleDocumentComment;

public class CommentDocumentTable implements StandardTable {
    private Connection mConnection;

    private PreparedStatement mCreateCommentDocumentTable;
    private PreparedStatement mDropCommentDocumentTable;
    private PreparedStatement mSelectDocumentByComment;
    private PreparedStatement mSelectAll;
    private PreparedStatement mSelectDocumentByDocId;

    CommentDocumentTable(Connection connection) {
        mConnection = connection;

        try {
            mCreateCommentDocumentTable = mConnection.prepareStatement("CREATE TABLE IF NOT EXISTS comment_document( "
                    + "document_id SERIAL PRIMARY KEY NOT NULL," + "google_drive_url VARCHAR(10000),"
                    + "external_url VARCHAR(500)," + "m_id INTEGER NOT NULL," + "date_created DATE NOT NULL,"
                    + "type VARCHAR(20) NOT NULL," + "U_id INTEGER NOT NULL," + "comment_id INTEGER NOT NULL,"
                    + "FOREIGN KEY(m_id) REFERENCES message(m_id) ON DELETE CASCADE,"
                    + "FOREIGN KEY(u_id) REFERENCES user1(u_id) ON DELETE CASCADE,"
                    + "FOREIGN KEY(comment_id) REFERENCES comment(comment_id) ON DELETE CASCADE)");
            mDropCommentDocumentTable = mConnection.prepareStatement("DROP TABLE IF EXISTS comment_document");
            mSelectAll = mConnection.prepareStatement("SELECT * FROM comment_document");
            mSelectDocumentByComment = mConnection.prepareStatement("SELECT * FROM comment_document WHERE comment_id = ?");
            mSelectDocumentByDocId = mConnection.prepareStatement("SELECT * FROM comment_document WHERE document_id = ?");
        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
        }
    }

    public List<SimpleDocumentComment> selectDocumentByDocID(int docId){
        try {
            mSelectDocumentByDocId.setInt(1, docId);
            ResultSet result = mSelectDocumentByDocId.executeQuery();
            ArrayList<SimpleDocumentComment> list = new ArrayList<>();
            while(result.next()){
                boolean isInDrive = false;
                String url = result.getString("external_url");
                if(url == null){
                    isInDrive = true;
                    url = result.getString("google_drive_url");
                }
                String type = result.getString("type");
                list.add(new SimpleDocumentComment(type, isInDrive, url, result.getInt("u_id"), result.getInt("comment_id")));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<SimpleDocumentComment> selectDocumentByComment(int commentId){
        try {
            mSelectDocumentByComment.setInt(1, commentId);
            ResultSet result = mSelectDocumentByComment.executeQuery();
            ArrayList<SimpleDocumentComment> list = new ArrayList<>();
            while(result.next()){
                boolean isInDrive = false;
                String url = result.getString("external_url");
                if(url == null){
                    isInDrive = true;
                    url = result.getString("google_drive_url");
                }
                String type = result.getString("type");
                list.add(new SimpleDocumentComment(type, isInDrive, url, result.getInt("u_id"), result.getInt("comment_id")));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int create() {
        try {
            return mCreateCommentDocumentTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public int drop() {
        try {
            return mDropCommentDocumentTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public ResultSet selectAll() {
        try {
           return mSelectAll.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
