package edu.lehigh.cse216.penguinpower.admin.response;

import java.util.Date;

/**
 * Instead of Returning RowData like in Phase 1, return a different Response class based on information being returned.
 */
public class CommentResponse {
    public int messageId;
    public int userId;
    public int commentId;
    public String commentText;
    public Date dateCreated;


    public CommentResponse(int messageId, int userId, int commentId, String commentText, Date dateCreated) {
        this.messageId = messageId;
        this.userId = userId;
        this.commentId = commentId;
        this.commentText = commentText;
        this.dateCreated = dateCreated;
    }
}