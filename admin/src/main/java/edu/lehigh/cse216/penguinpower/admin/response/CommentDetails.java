package edu.lehigh.cse216.penguinpower.admin.response;

import java.util.List;

public class CommentDetails {
    /**
     * The variables in Comment table
     */
    public CommentResponse commentInfo;
    public List<SimpleDocumentComment> documentList;
    public CommentDetails(CommentResponse commentInfo, List<SimpleDocumentComment> documentList) {
        this.commentInfo = commentInfo;
        this.documentList = documentList;
    }

}