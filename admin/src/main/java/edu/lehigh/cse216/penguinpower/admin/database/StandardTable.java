package edu.lehigh.cse216.penguinpower.admin.database;

import java.sql.ResultSet;

// Interface
/** Defines what is expected from a regular table. */
public interface StandardTable {

    /**
     * Return the result of executeUpdate. Return a value smaller than 0 on
     * Exeception.
     */
    public int create();

    /**
     * Return the result of executeUpdate. Return a value smaller than 0 on
     * Exeception.
     */
    public int drop();

    /** Return the result of executeQuery. Return null on Exception. */
    public ResultSet selectAll();
}