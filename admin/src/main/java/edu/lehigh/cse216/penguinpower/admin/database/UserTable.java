package edu.lehigh.cse216.penguinpower.admin.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import edu.lehigh.cse216.penguinpower.admin.response.UserResponse;

public class UserTable implements StandardTable{
    private Connection mConnection;

    private PreparedStatement mSelectAllUser;
    private PreparedStatement mSelectUser;
    private PreparedStatement mDeleteUser;
    private PreparedStatement mInsertUser;
    private PreparedStatement mUpdateUser;

    private PreparedStatement mCreateUserTable;
    private PreparedStatement mDropUserTable;

    UserTable(Connection connection){
        mConnection = connection;

        try {

            mCreateUserTable = mConnection
                    .prepareStatement("CREATE TABLE IF NOT EXISTS user1 (U_id Serial primary key not null,"
                            + "UserName varchar(50) not null unique, " + "Email varchar(100) not null unique,"
                            + "FamilyName varchar(100) not null," + "GivenName varchar(100) not null,"
                            + "Picture varchar(300))");

            mDropUserTable = mConnection.prepareStatement("Drop Table if exists User1 CASCADE");
            mDeleteUser = mConnection.prepareStatement("Delete From User1 where U_id = ?");
            mInsertUser = mConnection
                    .prepareStatement("Insert into User1 Values(default, ?, ?, ?, ?, ?) returning U_id");
            mSelectAllUser = mConnection.prepareStatement("Select * From User1");
            mSelectUser = mConnection.prepareStatement("Select * From User1 where U_id = ?");
            mUpdateUser = mConnection.prepareStatement(
                    "UPDATE User1 SET UserName = ?, Email = ?, FamilyName = ?, GivenName=?, Picture=? WHERE U_id = ?");

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
        }
    }

    /**
     * Insert a row into user table
     * 
     * @param userName
     * @param email
     * @param familyName
     * @param givenName
     * @return
     */
    public int insert(String userName, String email, String familyName, String givenName, String picture) {
        int count = 0;
        try {
            mInsertUser.setString(1, userName);
            mInsertUser.setString(2, email);
            mInsertUser.setString(3, familyName);
            mInsertUser.setString(4, givenName);
            mInsertUser.setString(5, picture);
            ResultSet res = mInsertUser.executeQuery();
            if (res.next())
                count += res.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public ResultSet selectAll() {
        try {
            return mSelectAllUser.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * get a specific user based on their user_id
     * 
     * @param U_id
     */
    public UserResponse selectOne(int U_id) {
        UserResponse res = null;
        try {
            mSelectUser.setInt(1, U_id);
            ResultSet rs = mSelectUser.executeQuery();
            if (rs.next()) {
                res = new UserResponse(rs.getInt("U_id"), rs.getString("Username"), rs.getString("Email"),
                        rs.getString("FamilyName"), rs.getString("GivenName"), rs.getString("Picture"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * update user
     * 
     * @param U_id
     * @param username
     * @param email
     * @param familyName
     * @param givenName
     * @return
     */
    public int update(int U_id, String username, String email, String familyName, String givenName,
            String picture) {
        int res = -1;
        try {
            mUpdateUser.setString(1, username);
            mUpdateUser.setString(2, email);
            mUpdateUser.setString(3, familyName);
            mUpdateUser.setString(4, givenName);
            mUpdateUser.setString(5, picture);
            mUpdateUser.setInt(6, U_id);
            res = mUpdateUser.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * create the user table
     */
    @Override
    public int create() {
        try {
            return mCreateUserTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Remove User from the database. If it does not exist, this will print an
     * error.
     */
    @Override
    public int drop() {
        try {
            return mDropUserTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * @param userId delete a row of user by user_id
     */
    public int delete(int userId) {
        int res = -1;
        try {
            mDeleteUser.setInt(1, userId);
            res = mDeleteUser.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

}