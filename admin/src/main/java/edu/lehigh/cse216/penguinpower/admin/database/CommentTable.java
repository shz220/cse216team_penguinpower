package edu.lehigh.cse216.penguinpower.admin.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import edu.lehigh.cse216.penguinpower.admin.response.CommentResponse;

public class CommentTable implements StandardTable {
    private Connection mConnection;

    private PreparedStatement mSelectAllComment;
    private PreparedStatement mSelectComment;
    private PreparedStatement mDeleteComment;
    private PreparedStatement mInsertComment;
    private PreparedStatement mUpdateComment;

    private PreparedStatement mSelectCommentByMessageId;

    private PreparedStatement mCreateCommentTable;
    private PreparedStatement mDropCommentTable;

    CommentTable(Connection connection) {
        mConnection = connection;

        try {
            mCreateCommentTable = mConnection
                    .prepareStatement("CREATE TABLE IF NOT EXISTS Comment (Comment_id SERIAL PRIMARY KEY NOT NULL, "
                            + "M_id INTEGER NOT NULL," + "U_id INTEGER NOT NULL,"
                            + "Comment_Text VARCHAR(500) NOT NULL," + "Date_created date,"
                            + "FOREIGN KEY(U_id) REFERENCES User1(U_id),"
                            + "FOREIGN KEY(M_id) REFERENCES Message(M_id) on delete set null)");
            mDropCommentTable = mConnection.prepareStatement("DROP TABLE IF EXISTS Comment");
            mSelectComment = mConnection.prepareStatement("SELECT * FROM Comment WHERE Comment_id = ?");
            mSelectAllComment = mConnection
                    .prepareStatement("SELECT Comment_id, M_id, U_id, Comment_Text, Date_created FROM Comment");
            mInsertComment = mConnection
                    .prepareStatement("INSERT INTO Comment VALUES(default, ?, ?, ?, ?) returning Comment_id");
            mDeleteComment = mConnection.prepareStatement("DELETE FROM Comment where Comment_id = ?");
            mUpdateComment = mConnection.prepareStatement("UPDATE Comment SET Comment_Text = ? WHERE Comment_id = ?");
            mSelectCommentByMessageId = mConnection.prepareStatement("SELECT * FROM comment WHERE m_id = ?");

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
        }
    }

    public List<CommentResponse> selectByMessageId(int messageId) {
        try {
            mSelectCommentByMessageId.setInt(1, messageId);
            ResultSet rs = mSelectCommentByMessageId.executeQuery();
            ArrayList<CommentResponse> list = new ArrayList<>();
            while (rs.next()) {
                list.add(new CommentResponse(rs.getInt("Comment_id"), rs.getInt("M_id"), rs.getInt("U_id"),
                        rs.getString("Comment_Text"), rs.getDate("Date_created")));
            }
            return list;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * 
     * @return All comment rows, as an ArrayList
     */
    @Override
    public ResultSet selectAll() {
        try {
            return mSelectAllComment.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param comment_id get a speific comment based on the comment_id
     */
    public CommentResponse selectOne(int comment_id) {
        CommentResponse res = null;
        try {
            mSelectComment.setInt(1, comment_id);
            ResultSet rs = mSelectComment.executeQuery();
            if (rs.next()) {
                res = new CommentResponse(rs.getInt("Comment_id"), rs.getInt("M_id"), rs.getInt("U_id"),
                        rs.getString("Comment_Text"), rs.getDate("Date_created"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * delete a row of comment by comment_id
     * 
     * @param comment_id
     * @return
     */
    public int delete(int comment_id) {
        int res = -1;
        try {
            mDeleteComment.setInt(1, comment_id);
            res = mDeleteComment.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * update a comment
     * 
     * @param Comment_id
     * @param Content
     * @return
     */
    public int update(int Comment_id, String Content) {
        int res = -1;
        try {
            mUpdateComment.setString(1, Content);
            mUpdateComment.setInt(2, Comment_id);
            res = mUpdateComment.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * create the comment table
     */
    @Override
    public int create() {
        try {
            return mCreateCommentTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * drop the comment table
     */
    @Override
    public int drop() {
        try {
            return mDropCommentTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * insert a comment into comment table
     * 
     * @param M_id
     * @param U_id
     * @param comment_text
     * @return
     */
    public int insert(int M_id, int U_id, String comment_text) {
        int count = 0;
        try {
            java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            mInsertComment.setInt(1, M_id);
            mInsertComment.setInt(2, U_id);
            mInsertComment.setString(3, comment_text);
            mInsertComment.setDate(4, date);
            ResultSet res = mInsertComment.executeQuery();
            if (res.next())
                count += res.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }
}
