package edu.lehigh.cse216.penguinpower.admin.gui;
// first stage shown when running the terminal

import edu.lehigh.cse216.penguinpower.admin.database.Database;
import javafx.application.Application;
import javafx.application.HostServices;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class PrimaryStage extends Application {
    private static final String DEFAULT_DB_URL = "postgres://bgrbiorcgqgciu:943ee61bbd785c65306d808a0e2965d313190920d149e8b048963558685141a2@ec2-50-17-194-186.compute-1.amazonaws.com:5432/d7pqs6ll42inpq";
    private Database db;
    private Stage primaryStage;
    private GridPane prompt;
    private Scene scene;

    @Override
    public void start(Stage primaryStage) throws Exception {
        System.err.println("APP Started");
        this.primaryStage = primaryStage;
        prompt = new GridPane();
        prompt.setAlignment(Pos.TOP_CENTER);
        prompt.setPadding(new Insets(10));
        prompt.setVgap(10);
        prompt.setHgap(5);
        prompt.add(new Label("Database URL:"), 0, 0);
        TextField textField = new TextField();
        textField.setPrefWidth(450);
        textField.setPromptText("Database URL");
        textField.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        prompt.add(textField, 1, 0);
        Button OKBtn = new Button("OK");
        Button DefaultURLBtn = new Button("Default URL");
        OKBtn.setOnAction(e -> {
            System.err.println(textField.getText());
            startToMain(textField.getText());
        });
        DefaultURLBtn.setOnAction(e -> {
            textField.setText(DEFAULT_DB_URL);
        });
        prompt.add(OKBtn, 0, 1);
        prompt.add(DefaultURLBtn, 1, 1);
        GridPane.setHalignment(DefaultURLBtn, HPos.RIGHT);
        scene = new Scene(prompt);
        primaryStage.setTitle("Database Administration Application -- Setup Connection");
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    public void webDisplay(String html){
        HostServices hostServices = getHostServices();
        hostServices.showDocument(html);
    }

    private void startToMain(String dbURL) {
        System.err.println("ok btn clicked");
        db = Database.getDatabase(dbURL);
        if (db == null) {
            Label errorLb = new Label("Cannot connect to specified database.");
            errorLb.setTextFill(Color.RED);
            prompt.add(errorLb, 0, 2, 2, 1);
            primaryStage.sizeToScene();
            return;
        }
        ContentStage contentStage = new ContentStage(db);
        contentStage.show();
        primaryStage.close();
    }

    public static void main(String args[]) {
        Application.launch(args);
    }
}