package edu.lehigh.cse216.penguinpower.admin.response;

public class SimpleDocumentComment{
    public String type;
    public boolean isInDrive;
    public String url;
    public int userId;
    public int commentId;

    public SimpleDocumentComment(String type, boolean isInDrive, String url, int userId, int commentId){
        this.type = type;
        this.isInDrive = isInDrive;
        this.url = url;
        this.userId = userId;
        this.commentId = commentId;
    }

}