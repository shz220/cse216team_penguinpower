package edu.lehigh.cse216.penguinpower.admin.gui.observables;

import javafx.beans.property.SimpleStringProperty;
public class SingleRowResult {

    private final SimpleStringProperty columnNameProperty;
    private final SimpleStringProperty contentProperty;

    public SingleRowResult(String columnName, String content){
        this.columnNameProperty = new SimpleStringProperty(columnName);
        this.contentProperty = new SimpleStringProperty(content);
    }

    public String getColumnNameProperty(){
        return columnNameProperty.get();
    }

    public String getContentProperty(){
        return contentProperty.get();
    }

    public void setColumnNameProperty(String columnName){
        columnNameProperty.set(columnName);
    }

    public void setContentProperty(String content){
        contentProperty.set(content);
    }
    
}