package edu.lehigh.cse216.penguinpower.admin.database;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;

import edu.lehigh.cse216.penguinpower.admin.response.MessageDetails;
import edu.lehigh.cse216.penguinpower.admin.response.MessageResponse;

public class MessageTable implements StandardTable {
    private Connection mConnection;

    private PreparedStatement mSelectAllMessage;
    private PreparedStatement mSelectOneMessage;
    private PreparedStatement mInsertOneMessage;
    private PreparedStatement mDeleteOneMessage;
    private PreparedStatement mUpdateOneMessage;

    private PreparedStatement mCreateMessageTable;
    private PreparedStatement mDropMessageTable;

    MessageTable(Connection connection) {
        mConnection = connection;

        try {
            mCreateMessageTable = mConnection
                    .prepareStatement("create table if not exists Message (M_id SERIAL PRIMARY KEY not null,"
                            + "U_id int not null," + "Title varchar(100) not null," + "Content varchar(500) not null,"
                            + "Date_created date," + "Upvote_count int not null," + "Downvote_count int not null,"
                            + "foreign key(U_id) references User1 on delete set null)");
            mDropMessageTable = mConnection.prepareStatement("DROP TABLE IF EXISTS Message");
            // Standard CRUD operations
            mDeleteOneMessage = mConnection.prepareStatement("DELETE FROM Message WHERE M_id = ?");
            mInsertOneMessage = mConnection
                    .prepareStatement("INSERT INTO Message VALUES (default, ?, ?, ?, ?, 0, 0) returning M_id");
            mSelectAllMessage = mConnection.prepareStatement(
                    "SELECT M_id, U_id, Title, Content, Date_Created, Upvote_count, Downvote_count FROM Message");
            mSelectOneMessage = mConnection.prepareStatement("SELECT * from Message WHERE M_id= ?");
            mUpdateOneMessage = mConnection.prepareStatement("UPDATE Message SET Title= ?, Content= ? WHERE M_id = ?");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Query the database for a list of information
     * 
     * @return All message rows, as an ArrayList
     */

    // int M_id, int U_id, String title, String content, Date date_created
    public ResultSet selectAll() {
        try {
            return mSelectAllMessage.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get all data for a specific row, by ID
     * 
     * @param M_id The id of the row being requested
     * 
     * @return The data for the requested row, or null if the ID was invalid
     */

    // int M_id, int U_id, String title, String content, Date date_created, int
    // upvote
    public MessageResponse selectOne(int M_id) {
        MessageResponse res = null;
        try {
            mSelectOneMessage.setInt(1, M_id);
            ResultSet rs = mSelectOneMessage.executeQuery();
            if (rs.next()) {
                res = new MessageResponse(rs.getInt("M_id"), rs.getInt("U_id"), rs.getString("Title"),
                        rs.getString("Content"), rs.getInt("Upvote_count"), rs.getInt("Downvote_count"),
                        rs.getDate("Date_created"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Insert a row into the Message database
     * 
     * @param U_id
     * @param title
     * @param content
     * 
     * @return The m_id of the row that was inserted
     */

    public int insert(int U_id, String title, String content) {
        int count = 0;
        try {
            java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            mInsertOneMessage.setInt(1, U_id);
            mInsertOneMessage.setString(2, title);
            mInsertOneMessage.setString(3, content);
            mInsertOneMessage.setDate(4, date);
            // return the M_id
            ResultSet res = mInsertOneMessage.executeQuery();
            if (res.next())
                count += res.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    /**
     * Delete a row of message by ID
     * 
     * @param M_id The id of the message row to delete
     * 
     * @return The number of rows that were deleted. -1 indicates an error.
     */
    public int delete(int M_id) {
        int res = -1;
        try {
            mDeleteOneMessage.setInt(1, M_id);
            res = mDeleteOneMessage.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Update the content for a row in the database
     * 
     * @param M_id    The id of the row to update
     * @param Title   The new title of the message
     * @param Content The new content of the message
     * 
     * @return The number of rows that were updated. -1 indicates an error.
     */
    // update the title and content of message
    public int update(int M_id, String Title, String Content) {
        int res = -1;
        try {
            mUpdateOneMessage.setString(1, Title);
            mUpdateOneMessage.setString(2, Content);
            mUpdateOneMessage.setInt(3, M_id);
            res = mUpdateOneMessage.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * Create Message. If it already exists, this will print an error
     */
    public int create() {
        try {
            return mCreateMessageTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * Remove Message from the database. If it does not exist, this will print an
     * error.
     */
    public int drop() {
        try {
            return mDropMessageTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }
}