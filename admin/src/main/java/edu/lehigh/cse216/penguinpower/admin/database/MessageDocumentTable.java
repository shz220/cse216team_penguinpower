package edu.lehigh.cse216.penguinpower.admin.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import edu.lehigh.cse216.penguinpower.admin.response.SimpleDocument;

public class MessageDocumentTable implements StandardTable{
    private Connection mConnection;

    private PreparedStatement mCreateMessageDocumentTable;
    private PreparedStatement mDropMessageDocumentTable;
    private PreparedStatement mInsertMessageDocument;
    private PreparedStatement mSelectDocumentByMessage;
    private PreparedStatement mSelectAll;
    private PreparedStatement mSelectDocumentByDocID;

    MessageDocumentTable(Connection connection) {
        mConnection = connection;

        try {
            mCreateMessageDocumentTable = mConnection.prepareStatement(
                    "CREATE TABLE IF NOT EXISTS message_document( " 
                    + "document_id SERIAL PRIMARY KEY NOT NULL,"
                            + "google_drive_url VARCHAR(10000)," 
                            + "external_url VARCHAR(500),"
                            + "date_created DATE NOT NULL," 
                            + "type VARCHAR(20) NOT NULL," 
                            + "u_id INTEGER NOT NULL,"
                            + "m_id INTEGER NOT NULL," 
                            + "FOREIGN KEY(u_id) REFERENCES user1(u_id) ON DELETE CASCADE,"
                            + "FOREIGN KEY(m_id) REFERENCES message(m_id) ON DELETE CASCADE)");
            mDropMessageDocumentTable = mConnection.prepareStatement("DROP TABLE IF EXISTS message_document");
            mInsertMessageDocument = mConnection
                    .prepareStatement("Insert into message_document Values(default, ?, ?, ?, ?, ?, ?)");
            mSelectAll = mConnection.prepareStatement("SELECT * FROM message_document");
            mSelectDocumentByMessage = mConnection.prepareStatement("SELECT * FROM message_document WHERE m_id = ?");
            mSelectDocumentByDocID = mConnection.prepareStatement("SELECT * FROM message_document WHERE document_id = ?");

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
        }
    }

    public List<SimpleDocument> selectDocumentByDocID(int document_id){
        try {
            mSelectDocumentByDocID.setInt(1, document_id);
            ResultSet result = mSelectDocumentByDocID.executeQuery();
            ArrayList<SimpleDocument> list = new ArrayList<>();
            while(result.next()){
                boolean isInDrive = false;
                String url = result.getString("external_url");
                if(url == null){
                    isInDrive = true;
                    url = result.getString("google_drive_url");
                }
                String type = result.getString("type");
                list.add(new SimpleDocument(type, isInDrive, url, result.getInt("u_id"), result.getInt("m_id")));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<SimpleDocument> selectDocumentByMessage(int messageId){
        try {
            mSelectDocumentByMessage.setInt(1, messageId);
            ResultSet result = mSelectDocumentByMessage.executeQuery();
            ArrayList<SimpleDocument> list = new ArrayList<>();
            while(result.next()){
                boolean isInDrive = false;
                String url = result.getString("external_url");
                if(url == null){
                    isInDrive = true;
                    url = result.getString("google_drive_url");
                }
                String type = result.getString("type");
                list.add(new SimpleDocument(type, isInDrive, url, result.getInt("u_id"), result.getInt("m_id")));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * create the message_document table
     */
    @Override
    public int create() {
        try {
            return mCreateMessageDocumentTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public int drop() {
        try {
            return mDropMessageDocumentTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    int insertMessageDocument(String url, String link, String type, int U_id, int M_id) {
        int count = 0;
        try {
            java.sql.Date date_created = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            mInsertMessageDocument.setString(1, url);
            mInsertMessageDocument.setString(2, link);
            mInsertMessageDocument.setDate(3, date_created);
            mInsertMessageDocument.setString(4, type);
            mInsertMessageDocument.setInt(5, U_id);
            mInsertMessageDocument.setInt(6, M_id);
            count += mInsertMessageDocument.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public ResultSet selectAll() {
        try{
            return mSelectAll.executeQuery();
        }catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }


}
