package edu.lehigh.cse216.penguinpower.admin.response;

import java.util.List;

public class MessageDetails {
    /**
     * The variables in Message table
     */
    public MessageResponse messageInfo;
    public List<UserResponse> upVoteUsers;
    public List<UserResponse> downVoteUsers;
    public List<CommentResponse> commentList;
    public List<SimpleDocument> documentList;
    public MessageDetails(MessageResponse messageInfo, List<UserResponse> upVoteUserNames, List<UserResponse> downVoteUserNames, List<CommentResponse> commentList, List<SimpleDocument> documentList) {
        this.messageInfo = messageInfo;
        this.upVoteUsers = upVoteUserNames;
        this.downVoteUsers = downVoteUserNames;
        this.documentList = documentList;
        this.commentList = commentList;
    }

}