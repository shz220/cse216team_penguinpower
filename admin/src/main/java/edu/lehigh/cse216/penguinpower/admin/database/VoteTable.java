package edu.lehigh.cse216.penguinpower.admin.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import edu.lehigh.cse216.penguinpower.admin.response.VoteResponse;

public class VoteTable implements StandardTable {
    private Connection mConnection;

    private PreparedStatement mSelectAllVote;
    private PreparedStatement mSelectVote;
    private PreparedStatement mDeleteVote;
    private PreparedStatement mInsertVote;
    private PreparedStatement mUpdateVote;

    private PreparedStatement mGetVoteByMessage;

    private PreparedStatement mIncreaseUpvote;
    private PreparedStatement mIncreaseDownvote;
    private PreparedStatement mDecreaseUpvote;
    private PreparedStatement mDecreaseDownvote;

    private PreparedStatement mCreateVoteTable;
    private PreparedStatement mDropVoteTable;

    private PreparedStatement mVoteExists;

    VoteTable(Connection connection) {
        mConnection = connection;

        try {

            mCreateVoteTable = mConnection.prepareStatement("CREATE TABLE IF NOT EXISTS Vote ("
                    + "M_id INTEGER NOT NULL," + "U_id INTEGER NOT NULL," + "Vote_status BOOLEAN NOT NULL,"
                    + "FOREIGN KEY(M_id) REFERENCES Message(M_id) on delete cascade,"
                    + "FOREIGN KEY(U_id) REFERENCES User1(U_id)," + "PRIMARY KEY(M_id, U_id))");
            mDropVoteTable = mConnection.prepareStatement("DROP TABLE IF EXISTS Vote");
            mSelectVote = mConnection.prepareStatement("SELECT * FROM Vote WHERE M_id = ? AND U_id = ?");
            mSelectAllVote = mConnection
                    .prepareStatement("SELECT M_id, U_id, Vote_status FROM Vote ORDER BY U_id DESC");
            mInsertVote = mConnection.prepareStatement("INSERT INTO Vote VALUES(?,?,?)");
            mUpdateVote = mConnection.prepareStatement("UPDATE Vote SET Vote_status = ? WHERE M_id = ? AND U_id = ?");
            mVoteExists = mConnection
                    .prepareStatement("SELECT exists (SELECT * FROM Vote WHERE M_id = ? AND U_id = ?)");
            mDecreaseUpvote = mConnection.prepareStatement("UPDATE Message SET Upvote_count = upvote_count - 1");
            mDecreaseDownvote = mConnection.prepareStatement("UPDATE Message SET Downvote_count = downvote_count - 1");
            mDeleteVote = mConnection.prepareStatement("DELETE FROM Vote WHERE M_id = ? AND U_id = ?");

            mGetVoteByMessage = mConnection.prepareStatement("SELECT * FROM vote WHERE m_id = ? AND vote_status = ?");

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
        }
    }

    /** Get a listing of vote with specified message id and vote status. */
    public ResultSet getVoteByMessage(int messageId, boolean voteStatus) {
        try {
            mGetVoteByMessage.setInt(1, messageId);
            mGetVoteByMessage.setBoolean(2, voteStatus);
            return mGetVoteByMessage.executeQuery();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
    }

    /**
     * insert a vote into vote table based on the vote status
     * 
     * @param M_id
     * @param U_id
     * @param voteStatus true: upvote false: downvote
     */
    public int insertVote(int M_id, int U_id, boolean voteStatus) {
        int count = 0;
        try {
            mInsertVote.setInt(1, M_id);
            mInsertVote.setInt(2, U_id);
            mInsertVote.setBoolean(3, voteStatus);
            count += mInsertVote.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    @Override
    public ResultSet selectAll() {
        try {
            return mSelectAllVote.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * @param M_id messageId
     * @param U_id UserId get a speific vote based on the M_id and U_id
     */
    public VoteResponse selectOne(int M_id, int U_id) {
        VoteResponse res = null;
        try {
            mSelectVote.setInt(1, M_id);
            mSelectVote.setInt(2, U_id);
            ResultSet rs = mSelectVote.executeQuery();
            if (rs.next()) {
                res = new VoteResponse(rs.getInt("M_id"), rs.getInt("U_id"), rs.getBoolean("Vote_status"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * delete a vote
     * 
     * @param messageId
     * @param userId
     * @return
     */
    public int delete(int messageId, int userId) {
        int res = -1;
        try {
            mDeleteVote.setInt(1, messageId);
            mDeleteVote.setInt(2, userId);
            res = mDeleteVote.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     * increase the upvote by 1 on a message
     * 
     * @param M_id
     * @param U_id
     * @return
     */
    public int upVote(int M_id, int U_id) {
        int res = -1;
        try {
            mVoteExists.setInt(1, M_id);
            mVoteExists.setInt(2, U_id);
            ResultSet res1 = mVoteExists.executeQuery();
            if (res1.next() && res1.getBoolean(1) == true) {

                try {
                    VoteResponse res2 = selectOne(M_id, U_id);

                    if (res2.voteStatus) {
                        return 0;
                    }
                    // if the vote is negative, replace with a possitive vote.
                    mUpdateVote.setBoolean(1, true);
                    mUpdateVote.setInt(2, M_id);
                    mUpdateVote.setInt(3, U_id);
                    res = mUpdateVote.executeUpdate();
                    mDecreaseDownvote.executeUpdate();
                    mIncreaseUpvote.executeUpdate();
                    return 2;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return res;
                }

            } else { // else insert it
                mInsertVote.setInt(1, M_id);
                mInsertVote.setInt(2, U_id);
                mInsertVote.setBoolean(3, true);
                res = mInsertVote.executeUpdate();
                try {
                    mIncreaseUpvote.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return res;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return res;
        }

        return res;
    }

    /**
     * increase the downvote by 1 on a message
     * 
     * @param M_id
     * @param U_id
     * @return
     */
    public int downVote(int M_id, int U_id) {
        int res = -1;
        try {
            mVoteExists.setInt(1, M_id);
            mVoteExists.setInt(2, U_id);
            ResultSet res1 = mVoteExists.executeQuery();
            // if vote exists already
            if (res1.next() && res1.getBoolean(1) == true) {

                try {
                    VoteResponse res2 = selectOne(M_id, U_id);

                    if (!res2.voteStatus) {
                        return 0;
                    }

                    mUpdateVote.setBoolean(1, false);
                    mUpdateVote.setInt(2, M_id);
                    mUpdateVote.setInt(3, U_id);
                    res = mUpdateVote.executeUpdate();
                    mDecreaseUpvote.executeUpdate();
                    mIncreaseDownvote.executeUpdate();
                    return 2;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return -1;
                }
            } // else insert it
            else {
                mInsertVote.setInt(1, M_id);
                mInsertVote.setInt(2, U_id);
                mInsertVote.setBoolean(3, false);
                res = mInsertVote.executeUpdate();
                try {
                    mIncreaseDownvote.executeUpdate();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return res;
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            return res;
        }

        return res;
    }

    /**
     * drop vote table
     */
    public int drop() {
        try {
            return mDropVoteTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    /**
     * create vote table
     */
    public int create() {
        try {
            return mCreateVoteTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }
}
