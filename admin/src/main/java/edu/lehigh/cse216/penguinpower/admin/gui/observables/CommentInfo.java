package edu.lehigh.cse216.penguinpower.admin.gui.observables;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
public class CommentInfo {

    private final SimpleIntegerProperty commentIdProperty;
    private final SimpleStringProperty commentTextProperty;
    private final SimpleIntegerProperty userIdProperty;

    public CommentInfo(int commentId, String commentText, int userId){
        commentIdProperty = new SimpleIntegerProperty(commentId);
        commentTextProperty = new SimpleStringProperty(commentText);
        userIdProperty = new SimpleIntegerProperty(userId);
    }

    public int getCommentIdProperty(){
        return commentIdProperty.get();
    }

    public String getCommentTextProperty(){
        return  commentTextProperty.get();
    }

    public int getUserIdProperty(){
        return userIdProperty.get();
    }

    public void setCommentIdProperty(int commentId){
        commentIdProperty.set(commentId);
    }
    public void setCommentTextProperty(String commentText){
        commentTextProperty.set(commentText);
    }
    public void setUserIdProperty(int userId){
        userIdProperty.set(userId);
    }
}