package edu.lehigh.cse216.penguinpower.admin;

import com.sendgrid.*;

import java.io.IOException;
import java.util.ArrayList;
import edu.lehigh.cse216.penguinpower.admin.response.*;

public class EmailSender{
    private static Mail buildActivationEmail(UserResponse user) throws IOException{
        Email from = new Email("bcb320@lehigh.edu");
        String subject = "Hello World From Sendgrid";
        Email to = new Email("bcb320@lehigh.edu");
        Content content = new Content("text/plain", "Welcome to Penguin Power " + user.familyName +" "+ user.givenName+ ", your company's new place for sharing thoughts and ideas!" + "\n Your username is: " + user.userName + 
        "\nYour new password is: PenguinPower");
        Mail mail = new Mail(from, subject, to, content);
        if(!user.email.equals("bcb320@lehigh.edu")) {
            Email email = new Email(user.email);
            mail.personalization.get(0).addTo(email);
        }
       
       
        return mail;
    }
    public static int sendActivationEmail(ArrayList<UserResponse> users) throws IOException {
        if(users == null || users.get(0) == null) return -1;
        SendGrid sg = new SendGrid(System.getenv("SENDGRID_API_KEY"));
        for(UserResponse user : users){
            Request request = new Request();
            Mail activationEmail;
            try{activationEmail = buildActivationEmail(user);}
            catch(IOException e){e.printStackTrace(); return -1;}
            
            try{
                request.setMethod(Method.POST);
                request.setEndpoint("mail/send");
                request.setBody(activationEmail.build());
                Response response = sg.api(request);
                System.out.println(response.getStatusCode());
                System.out.println(response.getBody());
                System.out.println(response.getHeaders());  
            }catch (IOException ex) {
                throw ex;
            }
        }   
        return 0;
    }
}