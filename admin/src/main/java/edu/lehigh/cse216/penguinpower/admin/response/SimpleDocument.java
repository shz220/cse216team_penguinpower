package edu.lehigh.cse216.penguinpower.admin.response;

public class SimpleDocument{
    public String type;
    public boolean isInDrive;
    public String url;
    public int userId;
    public int messageId;

    public SimpleDocument(String type, boolean isInDrive, String url, int userId, int messageId){
        this.type = type;
        this.isInDrive = isInDrive;
        this.url = url;
        this.userId = userId;
        this.messageId = messageId;
    }

}