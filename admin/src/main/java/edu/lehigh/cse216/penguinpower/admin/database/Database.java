package edu.lehigh.cse216.penguinpower.admin.database;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.struts.util.MessageResources;

import edu.lehigh.cse216.penguinpower.admin.response.CommentResponse;
import edu.lehigh.cse216.penguinpower.admin.response.MessageDetails;
import edu.lehigh.cse216.penguinpower.admin.response.MessageResponse;
import edu.lehigh.cse216.penguinpower.admin.response.SimpleDocument;
import edu.lehigh.cse216.penguinpower.admin.response.SimpleDocumentComment;
import edu.lehigh.cse216.penguinpower.admin.response.UserResponse;
import edu.lehigh.cse216.penguinpower.admin.response.CommentDetails;
import edu.lehigh.cse216.penguinpower.admin.response.MapResponse;

public class Database {

    private static Database INSTANCE;
    /**
     * The connection to the database. When there is no connection, it should be
     * null. Otherwise, there is a valid open connection
     */
    private Connection mConnection;

    public MessageTable message;
    public UserTable user;
    public VoteTable vote;
    public CommentTable comment;
    public CommentDocumentTable commentDocument;
    public MessageDocumentTable messageDocument;
    public PermissionTable permission;
    public DriveDocumentTable driveDocument;
    public MapTable map;

    /**
     * The Database constructor is private: we only create Database objects through
     * the getDatabase() method.
     */
    private Database() {
    }

    /**
     * Get a fully-configured connection to the database
     * 
     * @return A Database object, or null if we cannot connect properly
     */
    public static Database getDatabase(String dbURL) {
        if (INSTANCE != null) {
            return INSTANCE;
        }

        // Give the Database object a connection, fail if we cannot get one
        try {
            Class.forName("org.postgresql.Driver");
            URI dbUri = new URI(dbURL);
            String username;
            String password;
            try {
                username = dbUri.getUserInfo().split(":")[0];
                password = dbUri.getUserInfo().split(":")[1];
            } catch (NullPointerException e) {
                e.printStackTrace();
                return null;
            }
            String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath()
                    + "?sslmode=require";
            Connection conn = DriverManager.getConnection(dbUrl, username, password);
            if (conn == null) {
                System.err.println("Error: DriverManager.getConnection() returned a null object");
                return null;
            }
            INSTANCE = new Database();
            INSTANCE.mConnection = conn;
            INSTANCE.message = new MessageTable(conn);
            INSTANCE.user = new UserTable(conn);
            INSTANCE.vote = new VoteTable(conn);
            INSTANCE.comment = new CommentTable(conn);
            INSTANCE.permission = new PermissionTable(conn);
            INSTANCE.commentDocument = new CommentDocumentTable(conn);
            INSTANCE.messageDocument = new MessageDocumentTable(conn);
            INSTANCE.driveDocument = new DriveDocumentTable(conn);
            INSTANCE.map = new MapTable(conn);

            return INSTANCE;
        } catch (SQLException e) {
            System.err.println("Error: DriverManager.getConnection() threw a SQLException");
            e.printStackTrace();
            return null;
        } catch (ClassNotFoundException cnfe) {
            System.out.println("Unable to find postgresql driver");
            return null;
        } catch (URISyntaxException s) {
            System.out.println("URI Syntax Error");
            return null;
        }
    }

    /**
     * Close the current connection to the database, if one exists.
     * 
     * NB: The connection will always be null after this call, even if an error
     * occurred during the closing operation.
     * 
     * @return True if the connection was cleanly closed, false otherwise
     */
    public boolean disconnect() {
        if (mConnection == null) {
            System.err.println("Unable to close connection: Connection was null");
            return false;
        }
        try {
            mConnection.close();
        } catch (SQLException e) {
            System.err.println("Error: Connection.close() threw a SQLException");
            e.printStackTrace();
            mConnection = null;
            return false;
        }
        mConnection = null;
        return true;
    }

    public MessageDetails getMessageDetails(int messageId) {
        try {
            MessageResponse messageInfo = message.selectOne(messageId);
            if (messageInfo == null) {
                return null;
            }
            ResultSet upvotes = vote.getVoteByMessage(messageInfo.messageId, true);
            ArrayList<UserResponse> upvoteUsers = new ArrayList<>();
            while (upvotes.next()) {
                UserResponse ur = user.selectOne(upvotes.getInt("u_id"));
                if (ur != null) {
                    upvoteUsers.add(ur);
                }
            }
            ResultSet downvotes = vote.getVoteByMessage(messageInfo.messageId, false);
            ArrayList<UserResponse> downvoteUsers = new ArrayList<>();
            while (downvotes.next()) {
                UserResponse ur = user.selectOne(downvotes.getInt("u_id"));
                if (ur != null) {
                    downvoteUsers.add(ur);
                }
            }
            List<SimpleDocument> docList = messageDocument.selectDocumentByMessage(messageId);
            List<CommentResponse> commentList = comment.selectByMessageId(messageId);
            
            if(upvoteUsers.isEmpty()){
                System.err.println("db: no upvote");
            }
            if(downvoteUsers.isEmpty()){
                System.err.println("db: no upvote");
            }
            if(commentList.isEmpty()){
                System.err.println("db: no comment");
            }
            if(docList.isEmpty()){
                System.err.println("db: no doc");
            }
            return new MessageDetails(messageInfo, upvoteUsers, downvoteUsers, commentList, docList);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public CommentDetails getCommentDetails(int commentId) {
        
            CommentResponse commentInfo = comment.selectOne(commentId);
            if (commentInfo == null) {
                return null;
            }
            
            List<SimpleDocumentComment> docList = commentDocument.selectDocumentByComment(commentId);
            
            if(docList.isEmpty()){
                System.err.println("db: no doc");
            }
            return new CommentDetails(commentInfo, docList);
        
    }

    public ResultSet diySQLstatement(String sql) {
        PreparedStatement statement;
        try {
            statement = mConnection.prepareStatement(sql);
            statement.execute();
            ResultSet result = statement.getResultSet();
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

    }
}
