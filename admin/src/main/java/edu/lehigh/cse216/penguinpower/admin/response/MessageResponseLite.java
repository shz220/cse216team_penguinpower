package edu.lehigh.cse216.penguinpower.admin.response;
import java.util.Date;

public class MessageResponseLite {
    /**
     * The variables in Message table
     */
    public int messageId;
    public int userId;
    public String title;
    public int popularity;
    public Date dateCreated;

    public MessageResponseLite(int messageId, int userId, String title, int popularity, Date dateCreated) {
        this.messageId = messageId;
        this.userId = userId;
        this.title = title;
        this.popularity = popularity;
        this.dateCreated = dateCreated;
    }

   
}