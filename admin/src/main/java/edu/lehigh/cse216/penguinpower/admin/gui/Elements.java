package edu.lehigh.cse216.penguinpower.admin.gui;

import javafx.scene.control.Label;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

class Elements {
    public static final Font HEADLINE = new Font("Arial", 24);
    public static final Font BIG_FONT = new Font("Arial", 18);
    public static final Label NUM_FORMAT_ERR_LABEL = getErrLabel("Wrong Number Format. Please Enter A Valid Integer.");
    public static final Label DB_NULL_RESULT_LABEL = getErrLabel(
            "Error Encountered. Did not Get Response From Database.");

    public static Label getErrLabel(String text) {
        Label retval = new Label(text);
        retval.setTextFill(Color.RED);
        retval.setWrapText(true);
        return retval;
    }
}