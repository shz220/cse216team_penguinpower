package edu.lehigh.cse216.penguinpower.admin.response;

public class VoteResponse{
    public int messageId;
    public int userId;
    public boolean voteStatus;

    public VoteResponse(int messageId, int userId, boolean voteStatus) {
        this.messageId = messageId;
        this.userId = userId;
        this.voteStatus = voteStatus;
    }
}