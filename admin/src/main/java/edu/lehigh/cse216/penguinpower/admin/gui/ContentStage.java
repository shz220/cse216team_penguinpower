package edu.lehigh.cse216.penguinpower.admin.gui;
// main stage

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import com.google.api.services.drive.model.FileList;

import edu.lehigh.cse216.penguinpower.admin.GoogleDrive;
import edu.lehigh.cse216.penguinpower.admin.database.Database;
import edu.lehigh.cse216.penguinpower.admin.database.StandardTable;
import edu.lehigh.cse216.penguinpower.admin.gui.observables.CommentInfo;
import edu.lehigh.cse216.penguinpower.admin.gui.observables.ObservableResultSet;
import edu.lehigh.cse216.penguinpower.admin.gui.observables.SingleRowResult;
import edu.lehigh.cse216.penguinpower.admin.gui.observables.UserInfo;
import edu.lehigh.cse216.penguinpower.admin.response.CommentResponse;
import edu.lehigh.cse216.penguinpower.admin.response.MessageDetails;
import edu.lehigh.cse216.penguinpower.admin.response.CommentDetails;
import edu.lehigh.cse216.penguinpower.admin.response.MessageResponse;
import edu.lehigh.cse216.penguinpower.admin.response.SimpleDocument;
import edu.lehigh.cse216.penguinpower.admin.response.SimpleDocumentComment;
import edu.lehigh.cse216.penguinpower.admin.response.UserResponse;
import edu.lehigh.cse216.penguinpower.admin.response.MapResponse;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.Group;
import javafx.stage.Stage;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;
import javafx.application.HostServices;

class ContentStage extends Stage {
    private VBox secondaryNavBar;
    Database db;
    private HBox buttonArea;
    private BorderPane root;
    private Pane centerPane;
    private Control resultNode;

    // subclass: e.g. in message, get message detail
    class SearchArea extends VBox {
        private Button searchBtn;
        private TextField textField;
        private Label label;

        private SearchArea(String labelStr, String hintStr, String btnStr) {
            super(5);
            setPadding(new Insets(5));
            setBackground(new Background(new BackgroundFill(Color.LIGHTBLUE, new CornerRadii(5), Insets.EMPTY)));
            label = new Label(labelStr);        //get Message Detail
            textField = new TextField();
            textField.setPrefWidth(Double.MAX_VALUE);
            textField.setPromptText(hintStr);   //messageId
            searchBtn = new Button(btnStr);     // GET
            getChildren().addAll(label, textField, searchBtn);
        }
    }

    ContentStage(Database db) {
        super();
        this.db = db;

        root = new BorderPane();
        this.setTitle("Database Administration Application");
        buttonArea = new HBox();
        buttonArea.setPrefHeight(Double.MAX_VALUE);
        setPrimaryNavBar();
        root.setLeft(buttonArea);
        this.setScene(new Scene(root, 1200, 600));
    }

    private void setPrimaryNavBar() {
        VBox navBar = new VBox(10);
        navBar.setBackground(new Background(new BackgroundFill(Color.LIGHTGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        // create buttons
        ToggleButton messageBtn = new ToggleButton("Message");
        ToggleButton userBtn = new ToggleButton("User");
        ToggleButton commentBtn = new ToggleButton("Comment");
        ToggleButton voteBtn = new ToggleButton("Vote");
        ToggleButton permissionBtn = new ToggleButton("Permission");
        ToggleButton messageDocumentBtn = new ToggleButton("Message Document");
        ToggleButton commentDocumentBtn = new ToggleButton("Comment Document");
        ToggleButton mapBtn = new ToggleButton("Map"); 
        ToggleButton diyBtn = new ToggleButton("Build Your Own SQL");
        ToggleGroup toggleGroup = new ToggleGroup();
        messageBtn.setToggleGroup(toggleGroup);
        userBtn.setToggleGroup(toggleGroup);
        commentBtn.setToggleGroup(toggleGroup);
        voteBtn.setToggleGroup(toggleGroup);
        permissionBtn.setToggleGroup(toggleGroup);
        messageDocumentBtn.setToggleGroup(toggleGroup);
        commentDocumentBtn.setToggleGroup(toggleGroup);
        mapBtn.setToggleGroup(toggleGroup);
        diyBtn.setToggleGroup(toggleGroup);
        messageBtn.setUserData("message");
        userBtn.setUserData("user");
        commentBtn.setUserData("comment");
        voteBtn.setUserData("vote");
        permissionBtn.setUserData("permission");
        messageDocumentBtn.setUserData("message_document");
        commentDocumentBtn.setUserData("comment_document");
        mapBtn.setUserData("map");
        // ajust size
        navBar.setPrefWidth(150);
        navBar.setPadding(new Insets(10));
        messageBtn.setMinWidth(150);
        userBtn.setMinWidth(150);
        commentBtn.setMinWidth(150);
        voteBtn.setMinWidth(150);
        permissionBtn.setMinWidth(150);
        messageDocumentBtn.setMinWidth(150);
        commentDocumentBtn.setMinWidth(150);
        mapBtn.setMinWidth(150);
        diyBtn.setMinWidth(150);
        // set behavoir
        toggleGroup.selectedToggleProperty().addListener((ov, toggle, newToggle) -> {
            removeSecondaryNavbar();
            removeCenterPane();
            // append the new one
            if (newToggle != null) {
                // special case for diyBtn
                if (newToggle == diyBtn) {
                    setDIYSQLpane();
                    return;
                }
                setSecondaryNavBar((String) newToggle.getUserData());
            }
        });
        navBar.getChildren().addAll(messageBtn, userBtn, commentBtn, voteBtn, permissionBtn, messageDocumentBtn,
                commentDocumentBtn, mapBtn, diyBtn);
        buttonArea.getChildren().add(navBar);
    }

    private void setDIYSQLpane() {
        removeCenterPane();
        removeSecondaryNavbar();
        centerPane = new VBox(10);
        centerPane.setPadding(new Insets(5));
        centerPane.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
        centerPane.getChildren().add(new Label("This is an advanced feature. Be certain about what you are doing."));
        centerPane.getChildren().add(new Label("Your SQL Statement:"));
        TextField textField = new TextField();
        textField.setPrefWidth(Double.MAX_VALUE);
        textField.setPromptText("SQL Statement Here");
        centerPane.getChildren().add(textField);
        Button okBtn = new Button("ok");
        okBtn.setOnAction(e -> {
            ResultSet result = db.diySQLstatement(textField.getText());
            setNodeFromResultSet(result);
            resultNode.setPrefSize(Double.MAX_VALUE, 500);
            centerPane.getChildren().add(resultNode);
            VBox.setVgrow(resultNode, Priority.ALWAYS);
        });
        centerPane.getChildren().add(okBtn);
        root.setCenter(centerPane);
    }

    private void setSecondaryNavBar(String userData) {
        removeSecondaryNavbar();
        removeCenterPane();
        secondaryNavBar = new VBox(10);
        secondaryNavBar.setPrefWidth(200);
        secondaryNavBar.setPrefHeight(Double.MAX_VALUE);
        secondaryNavBar.setPadding(new Insets(10));
        secondaryNavBar
                .setBackground(new Background(new BackgroundFill(Color.DARKGRAY, CornerRadii.EMPTY, Insets.EMPTY)));
        Label caption = new Label(userData.toUpperCase());
        caption.setFont(Elements.BIG_FONT);
        caption.setTextFill(Color.WHITE);
        secondaryNavBar.getChildren().add(caption);
        Button recreateBtn = new Button("Recreate Table");
        Button selectAllBtn = new Button("Select All Rows");
        recreateBtn.setMinWidth(secondaryNavBar.getPrefWidth() / 2);
        selectAllBtn.setMinWidth(secondaryNavBar.getPrefWidth() / 2);
        secondaryNavBar.getChildren().addAll(recreateBtn, selectAllBtn);
        StandardTable table = null;
        switch (userData) {
        case "message":
            table = db.message;
            secondaryNavBar.getChildren().add(generateSelectOneMessageSearchArea());
            break;
        case "user":
            table = db.user;
            break;
        case "comment":
            table = db.comment;
            secondaryNavBar.getChildren().add(generateSelectOneCommentSearchArea());
            break;
        case "vote":
            table = db.vote;
            break;
        case "permission":
            table = db.permission;
            break;
        case "message_document":
            table = db.messageDocument;
            secondaryNavBar.getChildren().add(generateSelectOneMessageDocSearchArea());
            break;
        case "comment_document":
            table = db.commentDocument;
            secondaryNavBar.getChildren().add(generateSelectOneCommentDocSearchArea());
            break;
        case "map":
            table = db.map;
            break;
        }
        if (table == null)
            return;
        final StandardTable finalTable = table;

        selectAllBtn.setOnAction(e -> {
            ResultSet response = finalTable.selectAll();
            centerPane = new StackPane();
            centerPane.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
            root.setCenter(centerPane);
            setNodeFromResultSet(response);
            centerPane.getChildren().add(resultNode);
        });
        recreateBtn.setOnAction(e -> {
            ConfirmStage confirmStage = new ConfirmStage(
                    "Trying to recreate Table " + userData + ". data in this table will be deleted.");
            confirmStage.show();
            confirmStage.setOnHidden(event -> {
                System.err.println("action confirm window closed.");
                if (confirmStage.getResult()) {
                    removeCenterPane();
                    centerPane = new Pane();
                    int result = finalTable.drop();
                    if (result < 0) {
                        Label errorLb = Elements.getErrLabel("Database error encountered. DROP Execution declined.");
                        centerPane.getChildren().add(errorLb);
                    } else {
                        centerPane.getChildren()
                                .add(new Label("DROP Execution successed. " + result + " rows affected."));
                    }
                    result = finalTable.create();
                    if (result < 0) {
                        Label errorLb = Elements.getErrLabel("Database error encountered. CREATE Execution declined.");
                        centerPane.getChildren().add(errorLb);
                    } else {
                        centerPane.getChildren()
                                .add(new Label("CREATE Execution successed. " + result + " rows affected."));
                    }
                    root.setCenter(centerPane);

                }
            });
        });
        buttonArea.getChildren().add(secondaryNavBar);
    }

    private void removeSecondaryNavbar() {
        if (secondaryNavBar != null) {
            buttonArea.getChildren().remove(secondaryNavBar);
            secondaryNavBar = null;
        }
    }

    private void removeCenterPane() {
        if (centerPane != null) {
            root.getChildren().remove(centerPane);
            centerPane = null;
        }
    }

    private void removeResultTable() {
        if (resultNode != null && centerPane != null) {
            centerPane.getChildren().remove(resultNode);
            resultNode = null;
        }
    }

    /**
     * set the resultNode field to a TableView from provided resultSet. On error set
     * resultNode to an error label.
     */
    private void setNodeFromResultSet(ResultSet result) {
        removeResultTable();
        System.err.println("resultArea removed.");
        TableView<ObservableResultSet> table = new TableView<ObservableResultSet>();
        try {
            ObservableList<ObservableResultSet> data = FXCollections.observableArrayList();
            // populate data
            while (result.next()) {
                String[] list = new String[result.getMetaData().getColumnCount()];
                for (int i = 1; i <= result.getMetaData().getColumnCount(); i++) {
                    Object cell = result.getObject(i);
                    if (cell == null) {
                        list[i - 1] = "null";
                    } else {
                        list[i - 1] = cell.toString();
                    }
                }
                data.add(new ObservableResultSet(list, result));
            }

            // set up columns
            for (int i = 1; i <= result.getMetaData().getColumnCount(); i++) {
                TableColumn<ObservableResultSet, String> column = new TableColumn<>(
                        result.getMetaData().getColumnName(i));
                // set max width to 300 and min with to 50
                column.setPrefWidth(Math.max(50, Math.min(result.getMetaData().getColumnDisplaySize(i) * 10, 300)));
                final int finalIdx = i;
                column.setCellValueFactory(param -> {
                    return param.getValue().getStringProperty(finalIdx);
                });
                table.getColumns().add(column);
            }
            table.setItems(data);
            table.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
            System.err.println("table constructed.");
            resultNode = table;
        } catch (SQLException | NullPointerException e) {
            e.printStackTrace();
            // replace the center area with error message
            resultNode = Elements.DB_NULL_RESULT_LABEL;
        }
    }

    private void setSingleMessageResult(int messageId) {
        removeCenterPane();

        MessageDetails message = db.getMessageDetails(messageId);
        if (message == null) {
            centerPane = new Pane();
            setSingleLableResult(Elements.DB_NULL_RESULT_LABEL);
            return;
        }

        GridPane pane = new GridPane();
        centerPane = pane;
        pane.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
        pane.setHgap(5);
        pane.setVgap(5);
        root.setCenter(pane);
        Label title = new Label("MESSAGE DETAILED INFOMATION");
        title.setFont(Elements.HEADLINE);
        title.setTextFill(Color.DARKGREY);
        pane.add(title, 0, 0, 2, 1);

        MessageResponse messageInfo = message.messageInfo;
        // create message info table
        final ObservableList<SingleRowResult> messageInfoData = FXCollections.observableArrayList(
                new SingleRowResult("Message ID", "" + messageInfo.messageId),
                new SingleRowResult("Author User ID", "" + messageInfo.userId),
                new SingleRowResult("Title", messageInfo.title), new SingleRowResult("Content", messageInfo.content),
                new SingleRowResult("Date Created", messageInfo.dateCreated.toString()),
                new SingleRowResult("Upvote Count", "" + messageInfo.upVoteCount),
                new SingleRowResult("Downvote Count", "" + messageInfo.downVoteCount));
        TableView<SingleRowResult> messageInfoTable = new TableView<SingleRowResult>();
        TableColumn<SingleRowResult, String> nameColumn = new TableColumn<>();
        nameColumn.setPrefWidth(150);
        nameColumn.setCellValueFactory(new PropertyValueFactory<SingleRowResult, String>("columnNameProperty"));
        TableColumn<SingleRowResult, String> contentColumn = new TableColumn<>();
        contentColumn.setPrefWidth(350);
        contentColumn.setCellValueFactory(new PropertyValueFactory<SingleRowResult, String>("contentProperty"));
        messageInfoTable.setItems(messageInfoData);
        messageInfoTable.getColumns().add(nameColumn);
        messageInfoTable.getColumns().add(contentColumn);
        messageInfoTable.setPrefSize(250, 500);
        Label messageTitle = new Label("SIMPLE INFORMATION");
        messageTitle.setFont(Elements.BIG_FONT);
        VBox simpleMessagePane = new VBox(5);
        simpleMessagePane.getChildren().addAll(messageTitle, messageInfoTable);
        pane.add(simpleMessagePane, 0, 1);

        // append show documents
        VBox documentListButtons = new VBox(5);
        List<SimpleDocument> documentList = db.messageDocument.selectDocumentByMessage(messageId);
        if (documentList == null) {
            Label lb = Elements.getErrLabel("Database error encountered.");
            documentListButtons.getChildren().add(lb);
        } else {
            if (documentList.isEmpty()) {
                Label lb = new Label("No Document Attached.");
                documentListButtons.getChildren().add(lb);
            }
            for (SimpleDocument document : documentList) {
                documentListButtons.getChildren().add(new Label(document.url + ":"));
                Button docButton = new Button(document.type.toUpperCase());
                docButton.setOnAction(event -> {
                    displayDocument(document);
                });
                documentListButtons.getChildren().add(docButton);
            }
        }

        ScrollPane scrollingWrapper = new ScrollPane();
        scrollingWrapper.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        scrollingWrapper.setContent(documentListButtons);

        VBox documentListPane = new VBox(5);
        Label documentPaneTitle = new Label("DOCUMENTS");
        documentPaneTitle.setFont(Elements.BIG_FONT);
        documentListPane.getChildren().addAll(documentPaneTitle, scrollingWrapper);
        pane.add(documentListPane, 1, 1);

        // append show comments
        TableColumn<CommentInfo, Integer> commentIdColumn = new TableColumn<>("Comment ID");
        commentIdColumn.setCellValueFactory(new PropertyValueFactory<CommentInfo, Integer>("commentIdProperty"));
        TableColumn<CommentInfo, String> commentTextColumn = new TableColumn<>("Content");
        commentTextColumn.setCellValueFactory(new PropertyValueFactory<CommentInfo, String>("commentTextProperty"));
        TableColumn<CommentInfo, Integer> commentUserIdColumn = new TableColumn<>("User ID");
        commentUserIdColumn.setCellValueFactory(new PropertyValueFactory<CommentInfo, Integer>("userIdProperty"));

        ArrayList<CommentInfo> commentInfoListing = new ArrayList<>();
        Node commentNode;
        if (message.commentList == null) {
            commentNode = Elements.getErrLabel("Database Error Encountered. No comment fetched.");
        } else {
            if (message.commentList.isEmpty()) {
                System.err.println("no comment attached");
            }
            for (CommentResponse comment : message.commentList) {
                commentInfoListing.add(new CommentInfo(comment.commentId, comment.commentText, comment.userId));
            }
            final ObservableList<CommentInfo> commentData = FXCollections.observableArrayList(commentInfoListing);
            TableView<CommentInfo> commentTable = new TableView<>();
            commentTable.setItems(commentData);
            // upvoteUsersTb.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
            commentTable.getColumns().add(commentIdColumn);
            commentTable.getColumns().add(commentTextColumn);
            commentTable.getColumns().add(commentUserIdColumn);
            commentNode = commentTable;
        }
        VBox commentPane = new VBox(5);
        Label commentTitle = new Label("COMMENTS");
        commentTitle.setFont(Elements.BIG_FONT);
        commentPane.getChildren().addAll(commentTitle, commentNode);
        commentPane.setPrefWidth(500);
        pane.add(commentPane, 1, 2, 1, 5);

        // create vote listing
        Label upvoteLb = new Label("UPVOTE USERS");
        upvoteLb.setFont(Elements.BIG_FONT);
        Label downvoteLb = new Label("DOWNVOTE USERS");
        downvoteLb.setFont(Elements.BIG_FONT);
        pane.add(upvoteLb, 0, 3);
        pane.add(downvoteLb, 0, 5);

        // set columns listening to properties of UserInfo class.
        TableColumn<UserInfo, Integer> userIdColumn = new TableColumn<>("User ID");
        userIdColumn.setCellValueFactory(new PropertyValueFactory<UserInfo, Integer>("userIdProperty"));
        TableColumn<UserInfo, String> userNameColumn = new TableColumn<>("Username");
        userNameColumn.setCellValueFactory(new PropertyValueFactory<UserInfo, String>("usernameProperty"));
        TableColumn<UserInfo, String> firstNameColumn = new TableColumn<>("First Name");
        firstNameColumn.setCellValueFactory(new PropertyValueFactory<UserInfo, String>("firstNameProperty"));
        TableColumn<UserInfo, String> lastNameColumn = new TableColumn<>("Last Name");
        lastNameColumn.setCellValueFactory(new PropertyValueFactory<UserInfo, String>("lastNameProperty"));
        TableColumn<UserInfo, String> emailColumn = new TableColumn<>("Email");
        emailColumn.setCellValueFactory(new PropertyValueFactory<UserInfo, String>("emailProperty"));

        // upvote table
        TableView<UserInfo> upvoteUsersTb = new TableView<>();
        ArrayList<UserInfo> userInfoListing = new ArrayList<>();
        for (UserResponse userRes : message.upVoteUsers) {
            System.err.println("upvote user: " + userRes.userName);
            userInfoListing.add(new UserInfo(userRes.userId, userRes.userName, userRes.givenName, userRes.familyName,
                    userRes.email));
        }
        final ObservableList<UserInfo> upvoteUserData = FXCollections.observableArrayList(userInfoListing);
        upvoteUsersTb.setItems(upvoteUserData);
        // upvoteUsersTb.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
        upvoteUsersTb.getColumns().add(userIdColumn);
        upvoteUsersTb.getColumns().add(userNameColumn);
        upvoteUsersTb.getColumns().add(firstNameColumn);
        upvoteUsersTb.getColumns().add(lastNameColumn);
        upvoteUsersTb.getColumns().add(emailColumn);
        pane.add(upvoteUsersTb, 0, 4);

        // downvote table
        TableView<UserInfo> downvoteUsersTb = new TableView<>();
        ArrayList<UserInfo> downvoteUserInfoListing = new ArrayList<>();
        for (UserResponse userRes : message.downVoteUsers) {
            downvoteUserInfoListing.add(new UserInfo(userRes.userId, userRes.userName, userRes.givenName,
                    userRes.familyName, userRes.email));
        }
        final ObservableList<UserInfo> downvoteUserData = FXCollections.observableArrayList(downvoteUserInfoListing);
        downvoteUsersTb.setItems(downvoteUserData);
        // downvoteUsersTb.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
        downvoteUsersTb.getColumns().addAll(userIdColumn, userNameColumn, firstNameColumn, lastNameColumn, emailColumn);
        pane.add(downvoteUsersTb, 0, 6);

        root.setCenter(centerPane);
    }

    private void setSingleCommentResult(int commentId) {
        removeCenterPane();

        CommentDetails comment = db.getCommentDetails(commentId);
        if (comment == null) {
            centerPane = new Pane();
            setSingleLableResult(Elements.DB_NULL_RESULT_LABEL);
            return;
        }

        GridPane pane = new GridPane();
        centerPane = pane;
        pane.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
        pane.setHgap(5);
        pane.setVgap(5);
        root.setCenter(pane);
        Label title = new Label("COMMENT DETAILED INFOMATION");
        title.setFont(Elements.HEADLINE);
        title.setTextFill(Color.DARKGREY);
        pane.add(title, 0, 0, 2, 1);

        CommentResponse commentInfo = comment.commentInfo;
        // create comment info table
        final ObservableList<SingleRowResult> commentInfoData = FXCollections.observableArrayList(
                new SingleRowResult("Message ID", "" + commentInfo.messageId),
                new SingleRowResult("Author User ID", "" + commentInfo.userId),
                new SingleRowResult("Comment ID", "" + commentInfo.commentId),
                new SingleRowResult("Content", commentInfo.commentText.toString()), 
                new SingleRowResult("Date Created", commentInfo.dateCreated.toString()));
        TableView<SingleRowResult> commentInfoTable = new TableView<SingleRowResult>();
        TableColumn<SingleRowResult, String> nameColumn = new TableColumn<>();
        nameColumn.setPrefWidth(150);
        nameColumn.setCellValueFactory(new PropertyValueFactory<SingleRowResult, String>("columnNameProperty"));
        TableColumn<SingleRowResult, String> contentColumn = new TableColumn<>();
        contentColumn.setPrefWidth(350);
        contentColumn.setCellValueFactory(new PropertyValueFactory<SingleRowResult, String>("contentProperty"));
        commentInfoTable.setItems(commentInfoData);
        commentInfoTable.getColumns().add(nameColumn);
        commentInfoTable.getColumns().add(contentColumn);
        commentInfoTable.setPrefSize(250, 500);
        Label commentTitle = new Label("SIMPLE INFORMATION");
        commentTitle.setFont(Elements.BIG_FONT);
        VBox simpleMessagePane = new VBox(5);
        simpleMessagePane.getChildren().addAll(commentTitle, commentInfoTable);
        pane.add(simpleMessagePane, 0, 1);

        // append show documents
        VBox documentListButtons = new VBox(5);
        List<SimpleDocumentComment> documentList = db.commentDocument.selectDocumentByComment(commentId);
        if (documentList == null) {
            Label lb = Elements.getErrLabel("Database error encountered.");
            documentListButtons.getChildren().add(lb);
        } else {
            if (documentList.isEmpty()) {
                Label lb = new Label("No Document Attached.");
                documentListButtons.getChildren().add(lb);
            }
            for (SimpleDocumentComment document : documentList) {
                documentListButtons.getChildren().add(new Label(document.url + ":"));
                Button docButton = new Button(document.type.toUpperCase());
                docButton.setOnAction(event -> {
                    displayDocumentComment(document);
                });
                documentListButtons.getChildren().add(docButton);
            }
        }

        ScrollPane scrollingWrapper = new ScrollPane();
        scrollingWrapper.setVbarPolicy(ScrollBarPolicy.ALWAYS);
        scrollingWrapper.setContent(documentListButtons);

        VBox documentListPane = new VBox(5);
        Label documentPaneTitle = new Label("DOCUMENTS");
        documentPaneTitle.setFont(Elements.BIG_FONT);
        documentListPane.getChildren().addAll(documentPaneTitle, scrollingWrapper);
        pane.add(documentListPane, 1, 1);

        root.setCenter(centerPane);
    }

    private void setSingleMessageDocResult(int docId) {
        removeCenterPane();

        GridPane pane = new GridPane();
        centerPane = pane;
        pane.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
        pane.setHgap(5);
        pane.setVgap(5);
        root.setCenter(pane);
        Label title = new Label("DISPLAY DOCUMENT");
        title.setFont(Elements.HEADLINE);
        title.setTextFill(Color.DARKGREY);
        pane.add(title, 0, 0, 2, 1);

        VBox documentPane = new VBox(5);
        List<SimpleDocument> documentList = db.messageDocument.selectDocumentByDocID(docId);
        if (documentList == null) {
            Label lb = Elements.getErrLabel("Database error encountered.");
            documentPane.getChildren().add(lb);
        }
        else {
            if (documentList.isEmpty()) {
                Label lb = new Label("No Document Attached.");
                documentPane.getChildren().add(lb);
            }
            for (SimpleDocument document : documentList) {
                // for now we only have one document attached for each message
                String fileId = document.url;
                String type = document.type;
                System.err.println("type is:"+type);

                try{
                    String base64 = GoogleDrive.downloadFile(fileId);
                    if(type.equals("image/jpeg")){
                        // encode base64 back to a file
                        ByteArrayInputStream input = new ByteArrayInputStream(Base64.getDecoder().decode(base64));
                        Image image = new Image(input);
                        ImageView imageView = new ImageView(image);
                        imageView.setX(50); 
                        imageView.setY(25);
                        imageView.setFitHeight(300); 
                        imageView.setFitWidth(400); 
                        imageView.setPreserveRatio(true);  
    
                        documentPane.getChildren().add(imageView);
                        pane.add(documentPane, 1, 3);
                    }
                    if(type.equals("application/pdf")){
                        ByteArrayInputStream input = new ByteArrayInputStream(Base64.getDecoder().decode(base64));
                        //String html = "data:application/pdf;base64,"+base64;
                        
                        String html = "https://drive.google.com/uc?export=view&id="+fileId;
                        System.err.println(html);

                        Label lb = new Label("If the pdf cannot display successfully in this application, click the link below to view on web:");
                        documentPane.getChildren().add(lb);

                        final Hyperlink link = new Hyperlink();
                        link.setText(html);

                        PrimaryStage primaryStage = new PrimaryStage();
                        link.setOnAction(t->{
                            primaryStage.webDisplay(html);
                        });
                        documentPane.getChildren().add(link);
                        pane.add(documentPane, 1, 2);

                        WebView browser = new WebView();
                        WebEngine webEngine = browser.getEngine();
                        webEngine.load(html);
                        documentPane.getChildren().add(browser);
                    }
                    else {
                        System.err.println("The type of this document is not image/pdf");
                    }

                }
                catch (Exception ex){
                    System.out.println("exception caught.");
                }
            }
        }
        root.setCenter(centerPane);
    }

    
    private void setSingleCommentDocResult(int docId) {
        removeCenterPane();

        GridPane pane = new GridPane();
        centerPane = pane;
        pane.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
        pane.setHgap(5);
        pane.setVgap(5);
        root.setCenter(pane);
        Label title = new Label("DISPLAY DOCUMENT");
        title.setFont(Elements.HEADLINE);
        title.setTextFill(Color.DARKGREY);
        pane.add(title, 0, 0, 2, 1);

        VBox documentPane = new VBox(5);
        List<SimpleDocumentComment> documentList = db.commentDocument.selectDocumentByDocID(docId);
        if (documentList == null) {
            Label lb = Elements.getErrLabel("Database error encountered.");
            documentPane.getChildren().add(lb);
        }
        else {
            if (documentList.isEmpty()) {
                Label lb = new Label("No Document Attached.");
                documentPane.getChildren().add(lb);
            }
            for (SimpleDocumentComment document : documentList) {
                // for now we only have one document attached for each message
                String fileId = document.url;
                String type = document.type;
                System.err.println("type is:"+type);

                try{
                    String base64 = GoogleDrive.downloadFile(fileId);
                    if(type.equals("image/jpeg")){
                        // encode base64 back to a file
                        ByteArrayInputStream input = new ByteArrayInputStream(Base64.getDecoder().decode(base64));
                        Image image = new Image(input);
                        ImageView imageView = new ImageView(image);
                        imageView.setX(50); 
                        imageView.setY(25);
                        imageView.setFitHeight(300); 
                        imageView.setFitWidth(400); 
                        imageView.setPreserveRatio(true);  
    
                        documentPane.getChildren().add(imageView);
                        pane.add(documentPane, 1, 3);
                    }
                    if(type.equals("application/pdf")){
                        String html = "https://drive.google.com/uc?export=view&id="+fileId;
                        System.err.println(html);

                        Label lb = new Label("If the pdf cannot display successfully in this application, click the link below to view on web:");
                        documentPane.getChildren().add(lb);

                        Hyperlink link = new Hyperlink();
                        link.setText(html);

                        PrimaryStage primaryStage = new PrimaryStage();
                        link.setOnAction(t->{
                            primaryStage.webDisplay(html);
                        });
                        documentPane.getChildren().add(link);
                        pane.add(documentPane, 1, 2);

                        WebView browser = new WebView();
                        WebEngine webEngine = browser.getEngine();
                        webEngine.load(html);
                        documentPane.getChildren().add(browser);
                    }
                    else {
                        System.err.println("The type of this document is not image/pdf");
                    }
                }
                catch (Exception ex){
                    System.out.println("exception caught.");
                }
            }
        }
        root.setCenter(centerPane);
    }

    // display document
    private void displayDocument(SimpleDocument document) {
        removeCenterPane();

        GridPane pane = new GridPane();
        centerPane = pane;
        pane.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
        pane.setHgap(5);
        pane.setVgap(5);
        root.setCenter(pane);
        Label title = new Label("DISPLAY DOCUMENT");
        title.setFont(Elements.HEADLINE);
        title.setTextFill(Color.DARKGREY);
        pane.add(title, 0, 0, 2, 1);

        VBox documentPane = new VBox(5);
        List<SimpleDocument> documentList = db.messageDocument.selectDocumentByMessage(document.messageId);
        if (documentList == null) {
            Label lb = Elements.getErrLabel("Database error encountered.");
            documentPane.getChildren().add(lb);
        }
        else {
            if (documentList.isEmpty()) {
                Label lb = new Label("No Document Attached.");
                documentPane.getChildren().add(lb);
            }
                String fileId = document.url;
                String type = document.type;

                try{
                    String base64 = GoogleDrive.downloadFile(fileId);
                    if(type.equals("image/jpeg")){
                        // encode base64 back to a file
                        ByteArrayInputStream input = new ByteArrayInputStream(Base64.getDecoder().decode(base64));
                        Image image = new Image(input);
                        ImageView imageView = new ImageView(image);
                        imageView.setX(50); 
                        imageView.setY(25);
                        imageView.setFitHeight(300); 
                        imageView.setFitWidth(400); 
                        imageView.setPreserveRatio(true);  
    
                        documentPane.getChildren().add(imageView);
                        pane.add(documentPane, 1, 3);
                    }
                    if(type.equals("application/pdf")){
                        ByteArrayInputStream input = new ByteArrayInputStream(Base64.getDecoder().decode(base64));
                        //String html = "data:application/pdf;base64,"+base64;
                        
                        String html = "https://drive.google.com/uc?export=view&id="+fileId;
                        System.err.println(html);

                        Label lb = new Label("If the pdf cannot display successfully in this application, click the link below to view on web:");
                        documentPane.getChildren().add(lb);

                        Hyperlink link = new Hyperlink();
                        link.setText(html);

                        PrimaryStage primaryStage = new PrimaryStage();
                        link.setOnAction(t->{
                            primaryStage.webDisplay(html);
                        });
                        documentPane.getChildren().add(link);
                        pane.add(documentPane, 1, 2);

                        WebView browser = new WebView();
                        WebEngine webEngine = browser.getEngine();
                        webEngine.load(html);
                        documentPane.getChildren().add(browser);
                    }
                    else {
                        System.err.println("The type of this document is not image/pdf");
                    }

                }
                catch (Exception ex){
                    System.out.println("exception caught.");
                }
            
        }
        root.setCenter(centerPane);
    }

    private void displayDocumentComment(SimpleDocumentComment document) {
        removeCenterPane();

        GridPane pane = new GridPane();
        centerPane = pane;
        pane.setPrefSize(Double.MAX_VALUE, Double.MAX_VALUE);
        pane.setHgap(5);
        pane.setVgap(5);
        root.setCenter(pane);
        Label title = new Label("DISPLAY DOCUMENT");
        title.setFont(Elements.HEADLINE);
        title.setTextFill(Color.DARKGREY);
        pane.add(title, 0, 0, 2, 1);

        VBox documentPane = new VBox(5);
        List<SimpleDocumentComment> documentList = db.commentDocument.selectDocumentByComment(document.commentId);
        if (documentList == null) {
            Label lb = Elements.getErrLabel("Database error encountered.");
            documentPane.getChildren().add(lb);
        }
        else {
            if (documentList.isEmpty()) {
                Label lb = new Label("No Document Attached.");
                documentPane.getChildren().add(lb);
            }
                String fileId = document.url;
                String type = document.type;

                try{
                    String base64 = GoogleDrive.downloadFile(fileId);
                    if(type.equals("image/jpeg")){
                        // encode base64 back to a file
                        ByteArrayInputStream input = new ByteArrayInputStream(Base64.getDecoder().decode(base64));
                        Image image = new Image(input);
                        ImageView imageView = new ImageView(image);
                        imageView.setX(50); 
                        imageView.setY(25);
                        imageView.setFitHeight(300); 
                        imageView.setFitWidth(400); 
                        imageView.setPreserveRatio(true);  
    
                        documentPane.getChildren().add(imageView);
                        pane.add(documentPane, 1, 3);
                    }
                    if(type.equals("application/pdf")){
                        String html = "https://drive.google.com/uc?export=view&id="+fileId;
                        System.err.println(html);

                        Label lb = new Label("If the pdf cannot display successfully in this application, click the link below to view on web:");
                        documentPane.getChildren().add(lb);

                        Hyperlink link = new Hyperlink();
                        link.setText(html);
                        PrimaryStage primaryStage = new PrimaryStage();
                        link.setOnAction(t->{
                            primaryStage.webDisplay(html);
                        });
                        documentPane.getChildren().add(link);
                        pane.add(documentPane, 1, 2);

                        WebView browser = new WebView();
                        WebEngine webEngine = browser.getEngine();
                        webEngine.load(html);
                        documentPane.getChildren().add(browser);
                    }
                    else {
                        System.err.println("The type of this document is not image/pdf");
                    }
                }
                catch (Exception ex){
                    System.out.println("exception caught.");
                }
            
        }
        root.setCenter(centerPane);
    }

    private SearchArea generateSelectOneMessageSearchArea() {
        SearchArea retval = new SearchArea("Get Message Detail", "Message ID", "GET");
        retval.searchBtn.setOnAction(e -> {

            String input = retval.textField.getText();
            int messageId;
            try {
                messageId = Integer.parseInt(input);
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
                setSingleLableResult(Elements.NUM_FORMAT_ERR_LABEL);
                return;
            }
            setSingleMessageResult(messageId);
        });

        return retval;
    }

    private void setSingleLableResult(Label label) {
        removeCenterPane();
        centerPane = new Pane();
        centerPane.getChildren().add(label);
        root.setCenter(centerPane);
    }

    private SearchArea generateSelectOneCommentSearchArea() {

        SearchArea retval = new SearchArea("Get Comment Detail", "Comment ID", "GET");

        retval.searchBtn.setOnAction(e -> {

            String input = retval.textField.getText();
            int commentId;
            try {
                commentId = Integer.parseInt(input);
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
                setSingleLableResult(Elements.NUM_FORMAT_ERR_LABEL);
                return;
            }
            setSingleCommentResult(commentId);
        });

        return retval;
    }

    private SearchArea generateSelectOneMessageDocSearchArea() {
        SearchArea retval = new SearchArea("Get Document Detail", "Document ID", "GET");
        retval.searchBtn.setOnAction(e -> {
            String input = retval.textField.getText();
            int docId;
            try {
                docId = Integer.parseInt(input);
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
                setSingleLableResult(Elements.NUM_FORMAT_ERR_LABEL);
                return;
            }
            setSingleMessageDocResult(docId);
        });

        return retval;
    }

    private SearchArea generateSelectOneCommentDocSearchArea() {
        SearchArea retval = new SearchArea("Get Document Detail", "Document ID", "GET");
        retval.searchBtn.setOnAction(e -> {
            String input = retval.textField.getText();
            int docId;
            try {
                docId = Integer.parseInt(input);
            } catch (NumberFormatException ex) {
                ex.printStackTrace();
                setSingleLableResult(Elements.NUM_FORMAT_ERR_LABEL);
                return;
            }
            setSingleCommentDocResult(docId);
        });

        return retval;
    }


}
