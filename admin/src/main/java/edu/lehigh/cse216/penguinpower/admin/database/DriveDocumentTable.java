package edu.lehigh.cse216.penguinpower.admin.database;

// unused now

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.ArrayList;
import edu.lehigh.cse216.penguinpower.admin.GoogleDrive;

public class DriveDocumentTable implements StandardTable {
    private Connection mConnection;

    private PreparedStatement mCreateDriveDocumentTable;
    private PreparedStatement mInsertToDriveDocumentTable;
    private PreparedStatement mDropDriveDocumentTable;
    private PreparedStatement mSelectFileIdByDocID;    
    private PreparedStatement mSelectFileNameByDocID;
    private PreparedStatement mSelectAll;
    private PreparedStatement mSelectFileByFileName;

    ArrayList<String> filesId = GoogleDrive.getFileId();
    ArrayList<String> filesName = GoogleDrive.getFileName();   

    DriveDocumentTable(Connection connection) {
        mConnection = connection;

        try {
            mCreateDriveDocumentTable = mConnection.prepareStatement("CREATE TABLE IF NOT EXISTS drive_document( "
                    + "document_id SERIAL PRIMARY KEY NOT NULL,"
                    + "drive_file_name VARCHAR(100) NOT NULL,"
                    + "drive_file_id VARCHAR(100) NOT NULL)");
            mDropDriveDocumentTable = mConnection.prepareStatement("DROP TABLE IF EXISTS drive_document");
            mInsertToDriveDocumentTable = mConnection.prepareStatement("INSERT INTO drive_document (document_id, drive_file_name, drive_file_id) values (DEFAULT, ? , ?)");
            mSelectAll = mConnection.prepareStatement("SELECT * FROM drive_document");
            mSelectFileIdByDocID = mConnection.prepareStatement("SELECT drive_file_id FROM drive_document WHERE document_id = ?");
            mSelectFileNameByDocID = mConnection.prepareStatement("SELECT drive_file_name FROM drive_document WHERE document_id = ?");
            mSelectFileByFileName = mConnection.prepareStatement("SELECT drive_file_id FROM drive_document WHERE drive_file_name = ?");

        } catch (SQLException e) {
            System.err.println("Error creating prepared statement");
            e.printStackTrace();
        }
    }

    // get the fileId by entering the docId
    public String selectDocumentByFileName(String fileName){
        try {
            mSelectFileByFileName.setString(1, fileName);
            ResultSet result = mSelectFileByFileName.executeQuery();
            String fileId = "";
            while(result.next()){
                fileId = result.getString("drive_file_id");
            }
            return fileId;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    // get the fileId by entering the docId
    public String selectFileIDByDocID(int docId){
        try {
            mSelectFileIdByDocID.setInt(1, docId);
            ResultSet result = mSelectFileIdByDocID.executeQuery();
            String fileId = "";
            while(result.next()){
                fileId = result.getString("drive_file_id");
            }
            return fileId;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    // get the fileName by entering the docId
    public String selectFileNameByDocID(int docId){
        try {
            mSelectFileNameByDocID.setInt(1, docId);
            ResultSet result = mSelectFileNameByDocID.executeQuery();
            String fileId = "";
            while(result.next()){
                fileId = result.getString("drive_file_name");
            }
            return fileId;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean insert(String fileName, String fileId){
        try{
            mInsertToDriveDocumentTable.setString(1, fileName);
            mInsertToDriveDocumentTable.setString(2, fileId);
            return mInsertToDriveDocumentTable.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }
    @Override
    public int create() {
        try {
            return mCreateDriveDocumentTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public int drop() {
        try {
            return mDropDriveDocumentTable.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    @Override
    public ResultSet selectAll() {
        try {
           return mSelectAll.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
