// package edu.lehigh.cse216.penguinpower.admin;

// import edu.lehigh.cse216.penguinpower.admin.CommentResponse;
// import edu.lehigh.cse216.penguinpower.admin.MessageResponse;
// import edu.lehigh.cse216.penguinpower.admin.MessageResponseLite;
// import edu.lehigh.cse216.penguinpower.admin.UserResponse;
// import edu.lehigh.cse216.penguinpower.admin.VoteResponse;
// //import jdk.jfr.Timestamp;
// //import junit.framework.Test;
// //import junit.framework.TestCase;
// //import junit.framework.TestSuite;
// import org.junit.Test;
// import org.junit.*;
// import static org.junit.Assert.*;

// import java.util.Arrays;
// import java.util.ArrayList;
// import java.util.Map;

// /**
//  * Unit Tests for Database class
//  */

//  public class DatabaseTest
//  {
      
//       private Database db;

//       @Before
//       public void setUp(){
//        // get the Postgres configuration from the environment

//        // Get a fully-configured connection to the database, or exit 
//        // immediately
//        db = Database.getDatabase();
//        assertNotNull(db);
//     }

//     /**
//      * Multiple functions must be tested in a single junit test because junit test methods are not run in order
//      * Thus, if we do not do so, we might try to delete a message in one test before we insert it into another
//      */
//     @Test
//     public void testMessageFunctions(){
//         //test insertMessage
//         int messageId = db.insertMessage(1, "Test Message", "Test Content");
//         assertFalse(messageId == 0);
//         //test SelectMessage
//         MessageResponse insertedMessage = db.selectMessage(messageId);
//         assertEquals("Titles should be the same", "Test Message", insertedMessage.title);
//         assertEquals("Content should be the same", "Test Content", insertedMessage.content);
//         assertEquals("Downvote count should be 0", 0, insertedMessage.downVoteCount);
//         assertEquals("Upvote count should be 0", 0, insertedMessage.upVoteCount);

//         //test SelectAllMessage
//         ArrayList<MessageResponseLite> messageList =  db.selectAllMessage();
//         assertNotNull(messageList);
//         assertFalse(messageList.isEmpty());
//         assertTrue(messageList.size() > 0);
//         boolean insertedPresent = false;
//         for(MessageResponseLite message : messageList){
//             if(message.messageId == insertedMessage.messageId){
//                 insertedPresent = true;
//             }
//         }
//         assertTrue("message that was inserted found in select all", insertedPresent);

//         //test updateMessage
//         int result = db.updateMessage(messageId, "New Title", "New Content");
//         assertTrue(result > 0);
//         MessageResponse updatedMessage = db.selectMessage(messageId);
//         assertEquals("Title should be updated", "New Title", updatedMessage.title);
//         assertEquals("Content should be updated", "New Content", updatedMessage.content);

//         //test deleteMessage
//         result = db.deleteMessage(messageId);
//         assertTrue(result > -1);

//     }

//     @Test
//     public void testUserFunctions(){
//         //testInsertUser
//         int userId = db.insertUser("TestUserName", "blahblah@lehigh.edu", "Mok", "Kevin","https://happy");
//         assertTrue(userId > 0);
//         //test Select User
//         UserResponse insertedUser = db.selectUser(userId);
//         assertNotNull(insertedUser);
//         assertEquals("Username should be the same", "TestUserName", insertedUser.userName);
//         assertEquals("Last name should be the same", "Mok", insertedUser.familyName);
//         assertEquals("First name should be the same", "Kevin", insertedUser.givenName);
//         assertEquals("Email should be the same", "blahblah@lehigh.edu", insertedUser.email);
//         assertEquals("Picture should be the same","https://happy",insertedUser.picture);

//         //test SelectAllUser
//         ArrayList<UserResponse> users = db.selectAllUser();
//         assertNotNull(users);
//         assertFalse(users.isEmpty());
//         boolean insertedPresent = false;
//         for(UserResponse user : users){
//             if(user.userId == insertedUser.userId){
//                 insertedPresent = true;
//             }
//         }
//         assertTrue(insertedPresent);

//         //test updateUser
//         int result = db.updateUser(userId, "BetterUser","qwertyuiop@gmail.com","D.", "Best-Tester","https://www.me.com");
//         assertTrue(result > 0);
//         UserResponse updatedUser = db.selectUser(userId);
//         assertNotNull(updatedUser);
//         assertEquals("Username is updated", "BetterUser", updatedUser.userName);
//         assertEquals("Lastname is updated", "D.", updatedUser.familyName);
//         assertEquals("Firstname is updated", "Best-Tester", updatedUser.givenName);
//         assertEquals("Email is updated", "qwertyuiop@gmail.com", updatedUser.email);
//         assertEquals("Picture is updated","https://www.me.com",updatedUser.picture);

//         //test delete User
//         result = db.deleteUser(userId);
//         assertTrue(result > -1);
        
//     }

//     @Test
//     public void testCommentFunctions(){

//         int messageId = db.insertMessage(1,"testing", "testing");
//         //userid of one is known to exist beforehand
//         //test insertComment
//         int commentId = db.insertComment(messageId, 1, "Amazing!");
//         assertTrue(commentId > 0);

//         //test selectComment
//         CommentResponse insertedComment = db.selectComment(commentId);
//         assertNotNull(insertedComment);
//         assertEquals("Comment should be the same", "Amazing!", insertedComment.commentText);

//         //test selectAllComment
//         ArrayList<CommentResponse> comments = db.selectAllComment();
//         assertNotNull(comments);
//         assertFalse(comments.isEmpty());
//         boolean insertedPresent = false;
//         for(CommentResponse comment : comments){
//             if(comment.commentId == insertedComment.commentId){
//                 insertedPresent = true;
//             }
//         }
//         assertTrue(insertedPresent);
        
//         //test updateComment
//         int result = db.updateComment(commentId, "Horrible!");
//         assertTrue(result > -1);
//         CommentResponse updatedComment = db.selectComment(commentId);
//         assertEquals("Comment should be updated", "Horrible!", updatedComment.commentText);

//         //test deleteComment
//         result = db.deleteComment(commentId);
//         assertTrue(result > -1);

//         db.deleteMessage(messageId);

//     }

//     @Test
//     public void testVoteFunctions(){
//         //test insertVote
//         int messageId1 = db.insertMessage(1, "hello", "there");
//         assertTrue(messageId1 > 0);
//         int result = db.insertVote(messageId1, 1, true);
//         assertTrue(result > -1);
        
//         //test selectVote
//         VoteResponse firstVote = db.selectVote(messageId1, 1);
//         assertNotNull(firstVote);
//         assertTrue(firstVote.voteStatus);
        

//         //test if upvote inserts new vote with value of true
//         int messageId2 = db.insertMessage(1, "hey", "there");
//         result = db.upVote(messageId2, 1);
//         assertTrue(result > -1);
//         VoteResponse secondVote = db.selectVote(messageId2, 1);
//         assertNotNull(secondVote);
//         assertTrue(secondVote.voteStatus);

//         //test downVote
//         result = db.downVote(messageId1, 1);
//         assertTrue(result > -1);
//         firstVote = db.selectVote(messageId1, 1);
//         assertFalse(firstVote.voteStatus);

//         //test selectAllVote
//         ArrayList<VoteResponse> votes = db.selectAllVote();
//         assertNotNull(votes);
//         assertFalse(votes.isEmpty());
//         boolean firstVotePresent = false;
//         boolean secondVotePresent = false;
//         for(VoteResponse vote: votes){
//             if(vote.messageId == firstVote.messageId && vote.userId == firstVote.userId){
//                 firstVotePresent = true;
//             }
//             if(vote.messageId == secondVote.messageId && vote.userId == secondVote.userId){
//                 secondVotePresent = true;
//             }
//         }
//         assertTrue(firstVotePresent);
//         assertTrue(secondVotePresent);

//         //test deleteVote
//         result = db.deleteVote(messageId1, 1);
//         assertTrue(result > -1);
//         result = db.deleteVote(messageId2, 1);
//         assertTrue(result > -1);

//         db.deleteMessage(messageId1);
//         db.deleteMessage(messageId2);        

//     }



//     @After
//     public void closeDatabase(){
//         db.disconnect();
//     }

// }